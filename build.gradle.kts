plugins {
    id("java-library")
    alias(libs.plugins.spotless)
    alias(libs.plugins.springBoot) apply false
}

allprojects {
    apply {
        plugin("java-library")
        plugin("maven-publish")
        plugin(rootProject.libs.plugins.spotless.get().pluginId)
    }

    repositories {
        maven {
            url = uri("https://maven.restlet.talend.com/")
        }
        mavenCentral()
    }

    dependencies {
        testImplementation(rootProject.libs.junit)
    }

    java {
        withJavadocJar()
        withSourcesJar()
    }

//    tasks.compileJava {
//        options.isFork = true
//        options.forkOptions.jvmArgs!!.add("-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=5005")
//    }

    plugins.withId("maven-publish") {
        // Injected by the CI job
        val artifactoryReleaseUsername: String? by project
        val artifactoryReleasePassword: String? by project
        val artifactorySnapshotUsername: String? by project
        val artifactorySnapshotPassword: String? by project

        // Try to read app version from environment for jenkins jobs
        val envVersion: String? = System.getenv("AUTO_VERSION")
        val publishVersion = if (!envVersion.isNullOrEmpty()) envVersion else project.version
        val isSnapshot = publishVersion.toString().endsWith("SNAPSHOT")

        configure<PublishingExtension> {
            publications {
                register<MavenPublication>("mavenJava") {
                    from(components["java"])
                }
            }
            repositories {
                maven {

                    val releasesRepoUrl: String = project.properties["gd_maven_releases_url"] as String? ?: ""
                    val snapshotsRepoUrl: String = project.properties["gd_maven_snapshots_url"] as String? ?: ""
                    url = uri(if (isSnapshot) snapshotsRepoUrl else releasesRepoUrl)
                    credentials {
                        username = if (isSnapshot) artifactorySnapshotUsername else artifactoryReleaseUsername
                        password = if (isSnapshot) artifactorySnapshotPassword else artifactoryReleasePassword
                    }
                }
            }
        }
    }

    spotless {
        java {
            // Modified from gradle-convention-plugins-v2 glassdoor.standard-conventions.gradle.kts
            toggleOffOn()
            importOrder()
            removeUnusedImports()
            googleJavaFormat("1.19.2").aosp() // AOSP uses 4-space indentation
            targetExclude("**/generated/**")
            ratchetFrom("origin/master") // Only format java files differing from origin/master
            licenseHeaderFile("${project.rootDir}/glassdoor.license.java")
        }
    }
}

configurations.all {
    exclude(group = "commons-logging", module = "commons-logging")
}


