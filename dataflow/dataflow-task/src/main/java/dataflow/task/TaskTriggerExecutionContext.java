/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TaskTriggerExecutionContext.java
 */
package dataflow.task;

import java.util.Map;

public class TaskTriggerExecutionContext {

    private String dataFlowId;
    private TaskTriggerEvent triggerEvent;
    private Map<String, Object> parsedTaskConfig;
    private Map<String, Object> triggerConfig;

    public TaskTriggerEvent getTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(TaskTriggerEvent triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public String getTriggerId() {
        return triggerEvent.getTriggerId();
    }

    public Map<String, Object> getTaskConfig() {
        return parsedTaskConfig;
    }

    public void setTaskConfig(Map<String, Object> parsedTaskConfig) {
        this.parsedTaskConfig = parsedTaskConfig;
    }

    public Map<String, Object> getTriggerConfig() {
        return triggerConfig;
    }

    public void setTriggerConfig(Map<String, Object> triggerConfig) {
        this.triggerConfig = triggerConfig;
    }

    public String getDataFlowId() {
        return dataFlowId;
    }

    public void setDataFlowId(String dataFlowId) {
        this.dataFlowId = dataFlowId;
    }

    public String getExecutionId() {
        return triggerEvent.getExecutionId();
    }
}
