/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TaskTriggerEvent.java
 */
package dataflow.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskTriggerEvent {

    private final String projectName;
    private final String variantName;
    private final String taskName;

    private final String triggerId;
    private final String executionId;
    private final String taskConfigYaml;
    private final Map<String, Object> triggerConfigOverrides;

    @JsonCreator
    public TaskTriggerEvent(
            @JsonProperty(value = "projectName", required = false) String projectName,
            @JsonProperty(value = "variantName", required = false) String variantName,
            @JsonProperty(value = "taskName", required = false) String taskName,
            @JsonProperty(value = "triggerId", required = true) String triggerId,
            @JsonProperty(value = "executionId", required = true) String executionId,
            @JsonProperty(value = "taskConfigYaml", required = true) String taskConfigYaml,
            @JsonProperty(value = "triggerConfigOverrides")
                    Map<String, Object> triggerConfigOverrides) {
        if (triggerId == null) {
            throw new DataFlowConfigurationException("Missing triggerId for task trigger");
        }
        if (executionId == null || executionId.isBlank()) {
            throw new DataFlowConfigurationException("Missing executionId for task trigger");
        }
        if (taskConfigYaml == null) {
            throw new DataFlowConfigurationException("Missing task config for task trigger");
        }
        this.projectName = projectName;
        this.variantName = variantName;
        this.taskName = taskName;
        this.taskConfigYaml = taskConfigYaml;
        this.triggerId = triggerId;
        this.executionId = executionId;
        this.triggerConfigOverrides =
                new HashMap<>(
                        triggerConfigOverrides != null
                                ? triggerConfigOverrides
                                : Collections.emptyMap());
    }

    public String getProjectName() {
        return projectName;
    }

    public String getVariantName() {
        return variantName;
    }

    public String getTaskName() {
        return taskName;
    }

    public String getExecutionId() {
        return executionId;
    }

    public String getTriggerId() {
        return triggerId;
    }

    public Map<String, Object> getTriggerConfigOverrides() {
        return Collections.unmodifiableMap(triggerConfigOverrides);
    }

    public String getTaskConfigYaml() {
        return taskConfigYaml;
    }
}
