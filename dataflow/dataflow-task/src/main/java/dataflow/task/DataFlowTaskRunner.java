/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowTaskRunner.java
 */
package dataflow.task;

import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.util.MapUtil;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class DataFlowTaskRunner {

    private static final Logger logger = LoggerFactory.getLogger(DataFlowTaskRunner.class);

    private final DataFlowEnvironment env;
    private final DataFlowTaskMetricRegistry metricRegistry;

    public DataFlowTaskRunner(DataFlowEnvironment env) {
        this.env = env;
        this.metricRegistry = new DataFlowTaskMetricRegistry(env);
    }

    public Object run(TaskTriggerEvent triggerEvent) throws Exception {
        return run(triggerEvent, Collections.emptyMap());
    }

    public Object run(TaskTriggerEvent triggerEvent, Map<String, Object> dataFlowParamOverrides)
            throws Exception {
        TaskTriggerExecutionContext triggerContext = buildExecutionContext(triggerEvent);

        String dataFlowId = triggerContext.getDataFlowId();
        if (triggerEvent.getProjectName() != null) {
            MDC.put("projectName", triggerEvent.getProjectName());
        }
        if (triggerEvent.getVariantName() != null) {
            MDC.put("variantName", triggerEvent.getVariantName());
        }
        if (triggerEvent.getTaskName() != null) {
            MDC.put("taskName", triggerEvent.getTaskName());
        }
        MDC.put("dataFlowId", dataFlowId);
        MDC.put("executionId", triggerContext.getExecutionId());
        MDC.put("triggerId", triggerContext.getTriggerId());

        Map<String, Object> dataFlowParams =
                getDataFlowParams(triggerContext, dataFlowParamOverrides);
        logger.warn(
                "Running dataflow {}: taskConfig: {}, params: {}",
                dataFlowId,
                triggerContext.getTriggerEvent().getTaskConfigYaml(),
                dataFlowParams);

        Instant dataflowStart = Instant.now();
        DataFlowInstance instance = null;
        Object output;
        try {
            instance =
                    env.getRegistry()
                            .getDataFlowRegistration(dataFlowId)
                            .getInstanceFactory()
                            .newInstance(
                                    dfInstance -> {
                                        // Supply parameters to the data flow instance.
                                        dataFlowParams.forEach(dfInstance::setValue);
                                    });

            // Execute dataflow instance.
            instance.execute();

            output = instance.getOutput();

            logger.warn("Finished dataflow {}", dataFlowId);

            metricRegistry.addDataflowSuccess(dataFlowId);
        } catch (Exception e) {
            logger.error("Exception thrown when executing dataflow instance.", e);
            metricRegistry.addDataflowException(e, dataFlowId);
            throw e;
        } finally {
            Duration dataflowDuration = Duration.between(dataflowStart, Instant.now());
            metricRegistry.addDataflowDuration(dataflowDuration, dataFlowId);

            if (instance != null) {
                instance.close();
            }
        }

        logger.info("Finished running dataflow " + dataFlowId);

        return output;
    }

    private Map<String, Object> getDataFlowParams(
            TaskTriggerExecutionContext context, Map<String, Object> dataFlowParamOverrides) {
        Map<String, Object> params = new HashMap<>();
        params.put("executionId", context.getExecutionId());
        if (context.getTriggerEvent().getProjectName() != null) {
            params.put("trigger_projectName", context.getTriggerEvent().getProjectName());
        }
        if (context.getTriggerEvent().getVariantName() != null) {
            params.put("trigger_variantName", context.getTriggerEvent().getVariantName());
        }
        if (context.getTriggerEvent().getTaskName() != null) {
            params.put("trigger_taskName", context.getTriggerEvent().getTaskName());
        }
        params.put("trigger_id", context.getTriggerId());
        params.put("trigger_config", context.getTriggerConfig());
        params.put("trigger_taskConfig", context.getTaskConfig());

        params.putAll(
                (Map<String, Object>)
                        context.getTaskConfig().getOrDefault("params", Collections.emptyMap()));
        params.putAll(
                (Map<String, Object>)
                        context.getTriggerConfig().getOrDefault("params", Collections.emptyMap()));

        params.putAll(dataFlowParamOverrides);
        return params;
    }

    private TaskTriggerExecutionContext buildExecutionContext(TaskTriggerEvent triggerEvent) {
        if (triggerEvent.getExecutionId() == null || triggerEvent.getExecutionId().isBlank()) {
            throw new DataFlowConfigurationException("Missing executionId for task trigger");
        }
        if (triggerEvent.getTaskConfigYaml() == null) {
            throw new DataFlowConfigurationException("Missing task config for task trigger");
        }
        TaskTriggerExecutionContext context = new TaskTriggerExecutionContext();

        context.setTriggerEvent(triggerEvent);
        context.setTaskConfig(
                TaskConfigParser.parse(
                        new ByteArrayInputStream(
                                triggerEvent
                                        .getTaskConfigYaml()
                                        .getBytes(StandardCharsets.UTF_8))));

        Map<String, Object> triggersConfig = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        triggersConfig.putAll(
                (Map<String, Object>)
                        context.getTaskConfig().getOrDefault("triggers", Collections.emptyMap()));

        Map<String, Object> triggerConfig = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        if (context.getTriggerId() != null) {
            triggerConfig.putAll(
                    (Map<String, Object>)
                            triggersConfig.getOrDefault(
                                    context.getTriggerId(), Collections.emptyMap()));
        }

        if (triggerEvent.getTriggerConfigOverrides() != null) {
            MapUtil.deepMerge(triggerConfig, triggerEvent.getTriggerConfigOverrides());
        }
        context.setTriggerConfig(triggerConfig);

        String dataFlowId =
                (String)
                        (triggerConfig.containsKey("dataFlowId")
                                ? triggerConfig.get("dataFlowId")
                                : context.getTaskConfig().get("dataFlowId"));
        if (dataFlowId == null) {
            throw new DataFlowConfigurationException("Missing dataflow id for task trigger");
        }
        context.setDataFlowId(dataFlowId);
        return context;
    }
}
