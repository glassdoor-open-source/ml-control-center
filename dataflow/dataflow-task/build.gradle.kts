

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowCodegen)
    annotationProcessor(projects.dataflowComponentCodegen)

    api(projects.dataflowCore)
    api(projects.dataflowRetry)
    api(libs.jacksonDatabind)
    api(libs.micrometerCore)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

group = "dataflow"
description = "dataflow-task"
