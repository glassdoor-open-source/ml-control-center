package dataflow.test;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.AbstractDataFlowInstance;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.type.BooleanValue;
import dataflow.core.type.DoubleValue;
import dataflow.core.type.FloatValue;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;
import dataflow.core.type.ValueType;
import dataflow.core.util.MapUtil;
import java.lang.AutoCloseable;
import java.util.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;

public class TestFlow2 extends AbstractDataFlowInstance {
    public static final int ENVIRONMENT_VERSION = 2;
    // Base 64 encoded versions of the configs used to generate this code
    public static final String YAML_CONFIG = "RGF0YUZsb3c6CiAgaWQ6IHRlc3RfMgogIFBpcGVsaW5lOgogICAgLSAhU3RyaW5nSm9pblZhbHVlUHJvdmlkZXIKICAgICAgYWJzdHJhY3Q6IHRydWUKICAgICAgaWQ6IHByb3ZpZGVyMQogICAgICBzZXBhcmF0b3I6ICc6JwogICAgLSAhVmFsdWVQcm92aWRlcgogICAgICBpZDogcHJvdmlkZXIyCiAgICAgIGV4dGVuZHM6IHByb3ZpZGVyMQogICAgICBpbnB1dDogeyB2YWx1ZXM6IFsgJ2EnLCAnYicgXSB9CiAgICAtICFGYWxsYmFja1ZhbHVlUHJvdmlkZXIKICAgICAgaWQ6IHByb3ZpZGVyMwogICAgICBpbnB1dDoKICAgICAgICB2YWx1ZTogJChwcm92aWRlcjIpCiAgICAgICAgZmFsbGJhY2tWYWx1ZXM6CiAgICAgICAgICAtICFDb25zdFZhbHVlUHJvdmlkZXIKICAgICAgICAgICAgdmFsdWU6ICdhL2InCgogIG91dHB1dDogJChwcm92aWRlcjMpCg==";
    public static final Map<String, String> IMPORT_CONFIGS = new HashMap<>();
    static {
    }
    public static final String PARENT_ID = null;
    private final Map<String, Runnable> componentInitRunnables = new HashMap<>();
    private volatile java.lang.Object __provider2_const0_value = "a";
    private boolean __provider2_const0_available = true;
    private volatile boolean __provider2_const0_finished = true;
    private volatile java.lang.Object __provider2_const1_value = "b";
    private boolean __provider2_const1_available = true;
    private volatile boolean __provider2_const1_finished = true;
    private dataflow.core.component.collection.ListValueProvider __provider2_list0_component;
    private volatile java.util.List<java.lang.Object> __provider2_list0_value;
    private Map<String, Object> __provider2_list0_inputMap = new LinkedHashMap<>();
    private Map.Entry __provider2_list0_inputMapEntry_0;
    private Map.Entry __provider2_list0_inputMapEntry_1;
    private boolean __provider2_list0_available = true;
    private volatile boolean __provider2_list0_finished = false;
    private dataflow.test.provider.StringJoinValueProvider _provider2_component;
    private volatile java.lang.String _provider2_value;
    private boolean _provider2_available = true;
    private volatile boolean _provider2_finished = false;
    private volatile java.lang.Object __provider3_ConstValueProvider0_value = "a/b";
    private boolean __provider3_ConstValueProvider0_available = true;
    private volatile boolean __provider3_ConstValueProvider0_finished = true;
    private dataflow.core.component.collection.ListValueProvider __provider3_list0_component;
    private volatile java.util.List<java.lang.Object> __provider3_list0_value;
    private Map<String, Object> __provider3_list0_inputMap = new LinkedHashMap<>();
    private Map.Entry __provider3_list0_inputMapEntry_0;
    private boolean __provider3_list0_available = true;
    private volatile boolean __provider3_list0_finished = false;
    private dataflow.core.component.FallbackValueProvider _provider3_component;
    private volatile java.lang.Object _provider3_value;
    private boolean _provider3_available = true;
    private volatile boolean _provider3_finished = false;

    TestFlow2(DataFlowEnvironment env, Executor executor, DataFlowConfig dataFlowConfig, Consumer<DataFlowInstance> initFn) {
        super(dataFlowConfig, env, executor);
        initFn.accept(this);
        init();
    }

    public void init() {
        createExecutionContext();
        try {
            __provider2_list0_init();
            _provider2_init();
            __provider3_list0_init();
            _provider3_init();
            updateAvailability();
        } finally { DataFlowExecutionContext.popExecutionContext(); }
    }

    private void __provider2_list0_init() {
        this.__provider2_list0_component = (dataflow.core.component.collection.ListValueProvider) buildObject("ListValueProvider", dataFlowConfig.getComponents().get("_provider2_list0").getProperties(), this.getValues());
        this.__provider2_list0_inputMap.put("0", null);
        this.__provider2_list0_inputMap.put("1", null);
        this.__provider2_list0_inputMapEntry_0 = MapUtil.getEntry("0", __provider2_list0_inputMap);
        this.__provider2_list0_inputMapEntry_1 = MapUtil.getEntry("1", __provider2_list0_inputMap);
    }

    private void _provider2_init() {
        this._provider2_component = (dataflow.test.provider.StringJoinValueProvider) buildObject("StringJoinValueProvider", dataFlowConfig.getComponents().get("provider2").getProperties(), this.getValues());
    }

    private void __provider3_list0_init() {
        this.__provider3_list0_component = (dataflow.core.component.collection.ListValueProvider) buildObject("ListValueProvider", dataFlowConfig.getComponents().get("_provider3_list0").getProperties(), this.getValues());
        this.__provider3_list0_inputMap.put("0", null);
        this.__provider3_list0_inputMapEntry_0 = MapUtil.getEntry("0", __provider3_list0_inputMap);
    }

    private void _provider3_init() {
        this._provider3_component = (dataflow.core.component.FallbackValueProvider) buildObject("FallbackValueProvider", dataFlowConfig.getComponents().get("provider3").getProperties(), this.getValues());
    }

    public Map<String, Object> getComponents() {
        Map<String, Object> components = new LinkedHashMap<>();
        components.put("_provider2_list0", __provider2_list0_component);
        components.put("provider2", _provider2_component);
        components.put("_provider3_list0", __provider3_list0_component);
        components.put("provider3", _provider3_component);
        return components;
    }

    public void setValue(String id, Object value) {
        switch(id) {
            case "provider2":
                _provider2_value = (java.lang.String)value;
                _provider2_finished = true;
                _provider3_finished = false;
                break;
            case "provider3":
                _provider3_value = value;
                _provider3_finished = true;
                break;
        }
        updateAvailability();
    }

    public Map<String, Object> getValues() {
        Map<String, Object> values = new HashMap<>();
        values.put("_provider2_const0", __provider2_const0_value);
        values.put("_provider2_const1", __provider2_const1_value);
        values.put("_provider2_list0", __provider2_list0_value);
        values.put("provider2", _provider2_value);
        values.put("_provider3_ConstValueProvider0", __provider3_ConstValueProvider0_value);
        values.put("_provider3_list0", __provider3_list0_value);
        values.put("provider3", _provider3_value);
        return values;
    }

    public Object getValue(String id) {
        switch(id) {
            case "_provider2_list0":
                return __provider2_list0_value;
            case "provider2":
                return _provider2_value;
            case "_provider3_list0":
                return __provider3_list0_value;
            case "provider3":
                return _provider3_value;
        }
        return null;
    }

    public String getOutputType() {
        return "java.lang.Object";
    }

    public String getValueType(String id) {
        switch(id) {
            case "_provider2_list0":
                return "java.util.List<java.lang.Object>";
            case "provider2":
                return "java.lang.String";
            case "_provider3_list0":
                return "java.util.List<java.lang.Object>";
            case "provider3":
                return "java.lang.Object";
        }
        return null;
    }

    public void execute() throws DataFlowExecutionException {
        DataFlowExecutionContext executionContext = createExecutionContext();
        this.startTimeMillis = System.currentTimeMillis();
        fireDataFlowExecutionStartedEvent();
        try {
            componentInitRunnables.values().forEach(runnable -> runnable.run());
            if(!componentInitRunnables.isEmpty()) updateAvailability();
            componentInitRunnables.clear();
            clearExecutedFutures();
            // Component provider2._provider2_list0: {}
            if(!__provider2_list0_available) {
            }
            if(__provider2_list0_available && !__provider2_list0_finished) {
                fireComponentExecutionStartedEvent("_provider2_list0");
                try {
                    __provider2_list0_inputMapEntry_0.setValue("a");
                    __provider2_list0_inputMapEntry_1.setValue("b");
                    __provider2_list0_value = ((java.util.List<java.lang.Object>)__provider2_list0_component.getValue(__provider2_list0_inputMap));
                    fireComponentExecutionFinishedEvent("_provider2_list0");
                } catch (Throwable t) {
                    __provider2_list0_value = null;
                    fireComponentExecutionErrorEvent("_provider2_list0", t);
                    throw t;
                }
                __provider2_list0_finished = true;
            }
            // Component provider2: {id=provider2, extends=provider1, input={values=[a, b]}, abstract=true, separator=:}
            if(!_provider2_available) {
                if(!(__provider2_list0_available))
                    throw new DataFlowExecutionException("Component provider2 {id=provider2, extends=provider1, input={values=[a, b]}, abstract=true, separator=:} is missing a required dependency: provider2._provider2_list0 is not available");
            }
            if(_provider2_available && !_provider2_finished) {
                fireComponentExecutionStartedEvent("provider2");
                try {
                    _provider2_value = ((java.lang.String)_provider2_component.getValue(((java.util.List<java.lang.String>) dataflow.core.type.TypeUtil.convert(__provider2_list0_value, dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List<java.lang.Object>"), dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List<java.lang.String>"), environment.getRegistry().getTypeRegistry()))));
                    fireComponentExecutionFinishedEvent("provider2");
                } catch (Throwable t) {
                    _provider2_value = null;
                    fireComponentExecutionErrorEvent("provider2", t);
                    throw t;
                }
                _provider2_finished = true;
            }
            // Component provider3._provider3_list0: {}
            if(!__provider3_list0_available) {
            }
            if(__provider3_list0_available && !__provider3_list0_finished) {
                fireComponentExecutionStartedEvent("_provider3_list0");
                try {
                    __provider3_list0_inputMapEntry_0.setValue("a/b");
                    __provider3_list0_value = ((java.util.List<java.lang.Object>)__provider3_list0_component.getValue(__provider3_list0_inputMap));
                    fireComponentExecutionFinishedEvent("_provider3_list0");
                } catch (Throwable t) {
                    __provider3_list0_value = null;
                    fireComponentExecutionErrorEvent("_provider3_list0", t);
                    throw t;
                }
                __provider3_list0_finished = true;
            }
            // Component provider3: {id=provider3, input={value=$(provider2), fallbackValues=[{value=a/b}]}}
            if(!_provider3_available) {
                if(!(_provider2_available))
                    throw new DataFlowExecutionException("Component provider3 {id=provider3, input={value=$(provider2), fallbackValues=[{value=a/b}]}} is missing a required dependency: provider2 is not available");
                if(!(__provider3_list0_available))
                    throw new DataFlowExecutionException("Component provider3 {id=provider3, input={value=$(provider2), fallbackValues=[{value=a/b}]}} is missing a required dependency: provider3._provider3_list0 is not available");
            }
            if(_provider3_available && !_provider3_finished) {
                fireComponentExecutionStartedEvent("provider3");
                try {
                    _provider3_value = ((java.lang.Object)_provider3_component.getValue(_provider2_value, ((java.util.List) dataflow.core.type.TypeUtil.convert(__provider3_list0_value, dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List<java.lang.Object>"), dataflow.core.type.TypeUtil.valueTypeFromString("java.util.List"), environment.getRegistry().getTypeRegistry()))));
                    fireComponentExecutionFinishedEvent("provider3");
                } catch (Throwable t) {
                    _provider3_value = null;
                    fireComponentExecutionErrorEvent("provider3", t);
                    throw t;
                }
                _provider3_finished = true;
            }
            waitForExecutedFutures();
            this.endTimeMillis = System.currentTimeMillis();
            fireDataFlowExecutionFinishedEvent();
        } catch(Throwable t) {
            this.endTimeMillis = System.currentTimeMillis();
            fireDataFlowExecutionErrorEvent(t);
            cancelExecutedFutures();
            throw new DataFlowExecutionException(t);
        } finally {
            DataFlowExecutionContext.popExecutionContext();
        }
    }

    public java.lang.Object getOutput() {
        return _provider3_value;
    }

    public void updateAvailability() {
        __provider2_list0_available = true;
        _provider2_available = __provider2_list0_available;
        __provider3_list0_available = true;
        _provider3_available = _provider2_available && __provider3_list0_available;
    }

    public void close() {
        super.close();
        try {
            if(__provider2_list0_component != null && (__provider2_list0_component instanceof AutoCloseable)) ((AutoCloseable) __provider2_list0_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(_provider2_component != null && (_provider2_component instanceof AutoCloseable)) ((AutoCloseable) _provider2_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(__provider3_list0_component != null && (__provider3_list0_component instanceof AutoCloseable)) ((AutoCloseable) __provider3_list0_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
        try {
            if(_provider3_component != null && (_provider3_component instanceof AutoCloseable)) ((AutoCloseable) _provider3_component).close();
        } catch(Exception ex) {
            fireDataFlowExecutionErrorEvent(ex);
        }
    }
}
