/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MetricsUtilTest.java
 */
package dataflow.core.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import dataflow.core.engine.DataFlowInstance;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MetricsUtilTest {

    @Mock private DataFlowInstance mockInstance;

    @Test
    public void testGetMetricName() {
        when(mockInstance.getMetricNamePrefix()).thenReturn("app");
        assertEquals(
                "app.rest.requestCount",
                MetricsUtil.getMetricName("rest", "requestCount", mockInstance));
    }

    @Test
    public void testTagsToStringArray() {
        Map<String, String> tags = new LinkedHashMap<>();
        tags.put("test1", "abc");
        tags.put("test2", "efg");
        tags.put("test3", "123");
        assertArrayEquals(
                new String[] {"test1", "abc", "test2", "efg", "test3", "123"},
                MetricsUtil.tagsToStringArray(tags));
    }
}
