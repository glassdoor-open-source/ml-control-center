/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DotFileGeneratorITest.java
 */
package dataflow.core.doc;

import static org.junit.Assert.assertEquals;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.parser.DataFlowParser;
import java.io.IOException;
import org.junit.Test;

public class DotFileGeneratorITest {

    @Test
    public void generateDOTFile() throws DataFlowParseException, IOException {
        DataFlowEnvironment env =
                new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);

        DataFlowParser parser = new DataFlowParser(env.getRegistry());
        DataFlowConfig config =
                parser.parse(
                        ClassLoader.getSystemResourceAsStream(
                                "dataflow/core/parser/test-flow-2.yaml"));
        assertEquals(
                "digraph test_2 {\n"
                        + "    compound=true;\n"
                        + "    subgraph cluster_test_2 {\n"
                        + "        test_2_provider2 -> test_2_provider3[ label=\"value\" ];\n"
                        + "        test_2__provider3_list0 -> test_2_provider3[ label=\"fallbackValues\" ];\n"
                        + "        test_2__provider2_list0 -> test_2_provider2[ label=\"values\" ];\n"
                        + "        test_2__provider2_const0 -> test_2__provider2_list0[ label=\"0\" ];\n"
                        + "        test_2__provider2_const1 -> test_2__provider2_list0[ label=\"1\" ];\n"
                        + "        test_2__provider3_ConstValueProvider0 -> test_2__provider3_list0[ label=\"0\" ];\n"
                        + "        test_2__provider3_list0 [label=< <i>ListValueProvider</i> >];\n"
                        + "        test_2__provider2_const1 [label=< <i>ConstValueProvider</i> >];\n"
                        + "        test_2__provider2_list0 [label=< <i>ListValueProvider</i> >];\n"
                        + "        test_2__provider2_const0 [label=< <i>ConstValueProvider</i> >];\n"
                        + "        test_2__provider3_ConstValueProvider0 [label=< <i>ConstValueProvider</i> >];\n"
                        + "        test_2_provider2 [label=< <b>provider2</b><br/><i>StringJoinValueProvider</i> >];\n"
                        + "        test_2_provider3 [shape=diamond, label=< <b>provider3</b><br/><i>FallbackValueProvider</i> >];\n"
                        + "    }\n"
                        + "}\n",
                DotFileGenerator.generateDOTFile(config));
    }
}
