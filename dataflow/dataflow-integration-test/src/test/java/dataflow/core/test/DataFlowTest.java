/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowTest.java
 */
package dataflow.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.TestDataFlowEventHandler;
import dataflow.core.codegen.DataFlowInstanceCodeGenConfig;
import dataflow.core.codegen.DataFlowInstanceCodeGenerator;
import dataflow.core.compiler.CompileException;
import dataflow.core.component.LoggerComponent;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.DependencyInjector;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.test.TestConfigurableObj;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class DataFlowTest {

    private static final String COMPILE_TEST_YAML =
            "DataFlow:\n"
                    + "  id: ${id}\n"
                    + "  Pipeline:\n"
                    + "    - !ConstValueProvider\n"
                    + "        id: in2\n"
                    + "        value: ${in2Value}\n"
                    + "    - !StringJoinValueProvider\n"
                    + "        id: provider1\n"
                    + "        separator: \":\"\n"
                    + "        input:\n"
                    + "          values:\n"
                    + "            - $(in1)\n"
                    + "            - $(in2)\n"
                    + "\n"
                    + "  output: $(provider1)";
    private DataFlowEnvironment env;
    private DependencyInjector dependencyInjector;

    @Before
    public void setUp() throws IOException {
        this.dependencyInjector = new SimpleDependencyInjector("myTestVal");
        this.env = new DataFlowEnvironment("test", dependencyInjector, null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void test_singleValueProvider() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_singleValueProvider");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        assertEquals(1L, ((LongValue) instance.getOutput()).toLong());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-counter, ComponentExecutionFinished-counter, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_multipleInputValues() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_multipleInputValues");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", 35);
        instance.execute();
        assertEquals(57, ((IntegerValue) instance.getOutput()).toInt());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_placeholderString() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_placeholderStringProperty");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("sep1", "%");
        instance.setValue("sep2", "@");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A_%_@B_%_@C", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-_placeholderString0, ComponentExecutionFinished-_placeholderString0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_placeholderString_multipleValueRefs() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_placeholderString_multipleValueRefs");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("sep1", "%");
        instance.setValue("sep2", "@");
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A%_@B%_@C", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-_placeholderString0, ComponentExecutionFinished-_placeholderString0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_placeholderString_duplicateValueRef() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_placeholderString_duplicateValueRef");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.execute();
        assertEquals("A A:B", instance.getOutput());
    }

    @Test
    public void test_placeholderStringInInput() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_placeholderStringInInput");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", ",");
        instance.setValue("in2", "A");
        instance.setValue("in3", "B");
        instance.execute();
        assertEquals("A_test2,test3_B", instance.getOutput());
    }

    @Test
    public void test_buildMap() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_buildMap");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "X");
        instance.execute();
        Map outputMap = (Map) instance.getOutput();
        assertEquals(3, outputMap.size());
        assertEquals("X", outputMap.get("val1"));
        assertEquals("someValue2", outputMap.get("val2"));
        assertEquals("X,Y", outputMap.get("val3"));
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, ComponentExecutionStarted-provider2, ComponentExecutionFinished-provider2, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_mapInput() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_mapInput");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        Map outputMap = (Map) instance.getOutput();
        assertNotNull(outputMap);
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_map1, ComponentExecutionFinished-_provider1_map1, ComponentExecutionStarted-_provider1_map0, ComponentExecutionFinished-_provider1_map0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_mapEntry() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_mapEntry");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "A");
        instance.setValue("in2", ImmutableMap.of("value", "B"));
        instance.execute();
        assertEquals("B", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_mapEntry0, ComponentExecutionFinished-_mapEntry0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, ComponentExecutionStarted-_mapEntry1, ComponentExecutionFinished-_mapEntry1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_mapInvalidEntry() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_mapInvalidEntry");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        try {
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (DataFlowExecutionException ex) {
            assertEquals(
                    "java.lang.IllegalArgumentException: Map must have an entry with key in2: {v2=B, v1=A}",
                    ex.getMessage());
            assertEquals(
                    "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, ComponentExecutionStarted-_mapEntry0, ComponentExecutionError-_mapEntry0-Map must have an entry with key in2: {v2=B, v1=A}, DataFlowExecutionError-Map must have an entry with key in2: {v2=B, v1=A}]",
                    eventHandler.getEvents().toString());
        }
    }

    @Test
    public void test_incremental() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_incremental");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);

        instance.execute();
        assertNull(instance.getValue("provider1"));
        assertNull(instance.getOutput());

        instance.setValue("in1", "A");
        instance.execute();
        assertNull(instance.getValue("provider1"));
        assertNull(instance.getOutput());

        instance.setValue("in2", "B");
        instance.execute();
        assertEquals("A,B", instance.getValue("provider1"));
        assertNull(instance.getOutput());

        instance.setValue("in3", "C");
        instance.execute();
        assertEquals("A,B", instance.getValue("provider1"));
        assertEquals("A,B,C", instance.getOutput());

        instance.setValue("in2", "X");
        instance.execute();
        assertEquals("A,X", instance.getValue("provider1"));
        assertEquals("A,X,C", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, DataFlowExecutionFinished, DataFlowExecutionStarted, DataFlowExecutionFinished, DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished, DataFlowExecutionStarted, ComponentExecutionStarted-_provider2_list0, ComponentExecutionFinished-_provider2_list0, ComponentExecutionStarted-provider2, ComponentExecutionFinished-provider2, DataFlowExecutionFinished, DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, ComponentExecutionStarted-_provider2_list0, ComponentExecutionFinished-_provider2_list0, ComponentExecutionStarted-provider2, ComponentExecutionFinished-provider2, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_asyncValueProvider1() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncValueProvider1");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input={input=test}A}B", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionStarted-provider2, ComponentExecutionFinished-provider1, ComponentExecutionFinished-provider2, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_asyncValueProvider2() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncValueProvider2");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{in1={input=test}A, in2={input=test}B}C", instance.getOutput());
    }

    @Test
    public void test_asyncThreadValueProvider1() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncThreadValueProvider1");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input={input=test}A}B", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionStarted-provider2, ComponentExecutionFinished-provider1, ComponentExecutionFinished-provider2, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_asyncThreadValueProvider2() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncThreadValueProvider2");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        Stopwatch stopwatch = Stopwatch.createStarted();
        assertEquals(
                "{in1={input=test}A, in2={input=test}B, in3={input=test}C, in4={input=test}D}E",
                instance.getOutput());

        // If the four inputs execute concurrently then this test should complete in about 10
        // seconds.
        assertTrue(stopwatch.elapsed(TimeUnit.SECONDS) < 15);
    }

    @Test
    public void test_syncValueProviderAfterAsync() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncValueProviderAfterAsync");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("{input=test}A:{input=test}B", instance.getOutput());
    }

    @Test
    public void test_errorNoFallback() {
        try {
            DataFlowInstance instance = getInstance("testFlow_errorNoFallback");
            TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
            env.registerEventHandler(eventHandler);
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("java.io.IOException", ex.getMessage());
        }
    }

    @Test
    public void test_syncErrorFallback() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncErrorFallback");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_asyncErrorFallback_const() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorFallback_const");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_asyncErrorFallback_sync() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorFallback_sync");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "test");
        instance.execute();
        assertEquals("test", instance.getOutput());
    }

    @Test
    public void test_asyncErrorNoFallback() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorNoFallback");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        try {
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals(null, instance.getOutput());
        }
    }

    @Test
    public void test_asyncErrorNoFallbackNoFail() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorNoFallbackNoFail");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals(null, instance.getOutput());
    }

    @Test
    public void test_syncMissingValueFallback() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncMissingValueFallback");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in", "test");
        instance.execute();
        assertEquals("ABC", instance.getOutput());
    }

    @Test
    public void test_joinList() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_joinList");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "X");
        instance.setValue("in3", "Z");
        instance.execute();
        assertEquals("X:Y:Z", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_dataFlowValueProvider() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_dataFlowValueProvider");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        assertEquals("X:Y:Z", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-dataFlowOutput, DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished, ComponentExecutionFinished-dataFlowOutput, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_imports() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_imports");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "X");
        instance.setValue("in3", "Z");
        instance.execute();
        assertEquals("X#Y#Z", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider2, ComponentExecutionFinished-provider2, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_transformDataRecords() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_transformDataRecords");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("count", 5);
        records.add(record);
        instance.setValue("in", records);
        instance.execute();
        DataRecords output = (DataRecords) instance.getOutput();
        assertEquals(1, output.size());
        assertEquals(2, output.get(0).size());
        assertEquals(6, output.get(0).get("count"));
        assertEquals("abc", output.get(0).get("test"));
        assertEquals(1, records.get(0).size());
        assertEquals(ImmutableList.of("count", "test"), output.getColumnNames());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_dynamicProperty() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_dynamicProperty");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in3", "X");
        instance.setValue("in4", "Y");
        instance.execute();
        assertEquals("BA:XY", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider2_list0, ComponentExecutionFinished-_provider2_list0, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, ComponentExecutionStarted-provider2, ComponentExecutionFinished-provider2, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_customProperties() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_customProperties");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        assertEquals("EDCBA", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_MaxLengthStringTransformer0, ComponentExecutionFinished-_MaxLengthStringTransformer0, ComponentExecutionStarted-_ToUppercaseStringTransformer0, ComponentExecutionFinished-_ToUppercaseStringTransformer0, ComponentExecutionStarted-_ReverseStringTransformer0, ComponentExecutionFinished-_ReverseStringTransformer0, ComponentExecutionStarted-_list0, ComponentExecutionFinished-_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_dynamicProvidedProperty() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_dynamicProvidedProperty");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("separator", "@");
        instance.setValue("in1", "X");
        instance.setValue("in2", "Y");
        instance.execute();
        assertEquals("X@Y", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_missingInput() {
        try {
            DataFlowParser parser = new DataFlowParser(env.getRegistry());
            DataFlowConfig dataFlowConfig =
                    parser.parse(
                            ClassLoader.getSystemResourceAsStream(
                                    "dataflow/core/test/TestFlow-missingInput.yaml"));
            DataFlowInstanceCodeGenerator codeGenerator =
                    new DataFlowInstanceCodeGenerator(env.getRegistry());
            DataFlowInstanceCodeGenConfig codeGenConfig =
                    new DataFlowInstanceCodeGenConfig(
                            "dataflow.environment",
                            "TestFlow_missingInputDataFlow",
                            true,
                            true,
                            true);
            codeGenerator.generateSource(dataFlowConfig, "", codeGenConfig);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Input values not defined for component provider1", ex.getMessage());
        }
    }

    @Test
    public void test_intConstToStringConversion() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_intConstToStringConversion");
        instance.execute();
        assertEquals("54321", instance.getOutput());
    }

    @Test
    public void test_intToStringConversion() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_intToStringConversion");
//        DataFlowInstance instance = getCompiledInstance(
//            "dataflow/core/test/TestFlow-intToStringConversion.yaml",
//            newInstance -> {});
        instance.setValue("in1", 12345);
        instance.setValue("in2", 56789);
        instance.execute();
        assertEquals("98765", instance.getOutput());
    }

    @Test
    public void test_dependencyInjection() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_dependencyInjection");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        assertEquals("myTestVal", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_constOutput() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_constOutput");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        assertEquals("result", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_constReference() throws Exception {
        DataFlowInstance instance =
                getInstance(
                        "testFlow_constReference",
                        dataFlowInstance -> dataFlowInstance.setValue("in1", "abc"));
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        Map<String, String> result = (Map<String, String>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("abc:test", result.get("val1"));
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_testMap_testMap_StringJoinValueProvider0_list0, ComponentExecutionFinished-_testMap_testMap_StringJoinValueProvider0_list0, ComponentExecutionStarted-_testMap_StringJoinValueProvider0, ComponentExecutionFinished-_testMap_StringJoinValueProvider0, ComponentExecutionStarted-testMap, ComponentExecutionFinished-testMap, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_mapObjectReferenceAsync() throws Exception {
        DataFlowInstance instance =
                getInstance(
                        "testFlow_mapObjectReferenceAsync",
                        dataFlowInstance -> dataFlowInstance.setValue("in", "abc"));
//        DataFlowInstance instance = getCompiledInstance("dataflow/core/test/TestFlow-mapObjectReferenceAsync.yaml",
//            dataFlowInstance -> dataFlowInstance.setValue("in", "abc"));
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        String result = (String) instance.getOutput();
        assertEquals("{item1={in1=abc}A, item2=XYZ}", result);
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-asyncValue, ComponentExecutionFinished-asyncValue, ComponentExecutionStarted-_testMap_TestConfigurableObj0, ComponentExecutionFinished-_testMap_TestConfigurableObj0, ComponentExecutionStarted-_testMap_TestConfigurableObj1, ComponentExecutionFinished-_testMap_TestConfigurableObj1, ComponentExecutionStarted-_testMap_map0, ComponentExecutionFinished-_testMap_map0, ComponentExecutionStarted-testMap, ComponentExecutionFinished-testMap, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_objectReference() throws Exception {
        DataFlowInstance instance =
                getInstance(
                        "testFlow_objectReference",
                        dataFlowInstance -> dataFlowInstance.setValue("in", "abc"));
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        TestConfigurableObj testObj = (TestConfigurableObj) result.get("val1");
        assertEquals("{in1=abc}A", testObj.getValue());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-asyncValue, ComponentExecutionFinished-asyncValue, ComponentExecutionStarted-myObj, ComponentExecutionFinished-myObj, ComponentExecutionStarted-testMap, ComponentExecutionFinished-testMap, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_objectReferenceAsync() throws Exception {
//        DataFlowInstance instance =
//                getInstance(
//                        "testFlow_objectReferenceAsync",
//                        dataFlowInstance -> dataFlowInstance.setValue("in", "abc"));

        DataFlowInstance instance = getCompiledInstance(
            "dataflow/core/test/TestFlow-objectReferenceAsync.yaml",
            newInstance -> newInstance.setValue("in", "abc"));

        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(2, result.size());
        TestConfigurableObj testObj = (TestConfigurableObj) result.get("val1");
        assertEquals("{in1=abc}A", testObj.getValue());
        assertEquals("abc", result.get("val2"));
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-asyncValue, ComponentExecutionFinished-asyncValue, ComponentExecutionStarted-_testMap_TestConfigurableObj0, ComponentExecutionFinished-_testMap_TestConfigurableObj0, ComponentExecutionStarted-testMap, ComponentExecutionFinished-testMap, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_asyncError_valueCleared() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_asyncErrorValueCleared");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("shouldFail", false);
        instance.execute();
        assertEquals("OK", instance.getOutput());
        instance.setValue("shouldFail", true);
        instance.execute();
        assertNull(instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished, DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_syncError_valueCleared() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_syncErrorValueCleared");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("shouldFail", false);
        instance.execute();
        assertEquals("OK", instance.getOutput());
        instance.setValue("shouldFail", true);
        instance.execute();
        assertNull(instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished, DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionError-provider1-null, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_optionalInput_missing() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_optionalInputMissing");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        assertEquals("EMPTY", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_optionalInput_present() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_optionalInputPresent");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.execute();
        assertEquals("test", instance.getOutput());
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_sink() throws Exception {
        List<String> list = new ArrayList<>();
        DataFlowInstance instance = getInstance("testFlow_sink");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("list", list);
        assertEquals(0, list.size());
        instance.execute();
        assertEquals(ImmutableMap.of("v1", "A", "v2", "B"), instance.getOutput());
        assertEquals(1, list.size());
        assertEquals("test", list.get(0));
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, ComponentExecutionStarted-_TestSinkValueProvider0, ComponentExecutionFinished-_TestSinkValueProvider0, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_sinkWithOutputAsDependency() throws Exception {
        List<String> list = new ArrayList<>();
        DataFlowInstance instance = getInstance("testFlow_sinkWithOutputAsDependency");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "A");
        instance.setValue("in2", "B");
        instance.setValue("list", list);
        assertEquals(0, list.size());
        instance.execute();
        assertEquals("A:B", instance.getOutput());
        assertEquals(1, list.size());
        assertEquals("A:B", list.get(0));
        assertEquals(
                "[DataFlowExecutionStarted, ComponentExecutionStarted-_provider1_list0, ComponentExecutionFinished-_provider1_list0, ComponentExecutionStarted-provider1, ComponentExecutionFinished-provider1, ComponentExecutionStarted-_TestSinkValueProvider0, ComponentExecutionFinished-_TestSinkValueProvider0, DataFlowExecutionFinished]",
                eventHandler.getEvents().toString());
    }

    @Test
    public void test_sink_sinkPropertyNotSet() {
        try {
            DataFlowParser parser = new DataFlowParser(env.getRegistry());
            DataFlowConfig dataFlowConfig =
                    parser.parse(
                            ClassLoader.getSystemResourceAsStream(
                                    "dataflow/core/test/TestFlow-sink-propNotSet.yaml"));
            DataFlowInstanceCodeGenerator codeGenerator =
                    new DataFlowInstanceCodeGenerator(env.getRegistry());
            DataFlowInstanceCodeGenConfig codeGenConfig =
                    new DataFlowInstanceCodeGenConfig(
                            "dataflow.environment", "TestFlow_sink_propNotSet", true, true, true);
            codeGenerator.generateSource(dataFlowConfig, "", codeGenConfig);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals(
                    "[_TestSinkValueProvider0{input={list=$(list)}}] is not used as a direct or indirect dependency of the dataflow output. If this component is not used to compute the final output but you would still like it to run add sink: true to the component config.  If this component is only used as a base for inheritance add abstract: true to the component config.",
                    ex.getMessage());
        }
    }

    @Test
    public void test_logger() throws Exception {
        DataRecords records = new DataRecords();
        records.setColumnNames("col1", "col2");
        records.add(
                new DataRecord(
                        new ImmutableMap.Builder<String, Object>()
                                .put("col1", "A")
                                .put("col2", "B")
                                .build()));
        records.add(
                new DataRecord(
                        new ImmutableMap.Builder<String, Object>()
                                .put("col1", ImmutableMap.of("k1", "v1", "k2", "10"))
                                .put("col2", "3.14")
                                .build()));

        try (DataFlowInstance instance = getInstance("testFlow_logger")) {

            Field loggerComponentField =
                    instance.getClass().getDeclaredField("__Logger0_component");
            loggerComponentField.setAccessible(true);
            LoggerComponent loggerComponent = (LoggerComponent) loggerComponentField.get(instance);
            Field loggerField = LoggerComponent.class.getDeclaredField("logger");
            loggerField.setAccessible(true);
            Logger logger = (Logger) loggerField.get(loggerComponent);
            ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
            listAppender.start();
            logger.addAppender(listAppender);
            instance.setValue("records", records);
            instance.execute();

            records.clear();
            records.add(
                    new DataRecord(
                            new ImmutableMap.Builder<String, Object>()
                                    .put("col1", "C")
                                    .put("col2", "D")
                                    .build()));
            instance.setValue("records", records);

            instance.execute();

            List<ILoggingEvent> logsList = listAppender.list;
            assertEquals(2, logsList.size());
            assertEquals(
                    "{records=DataRecords{keyColumnNames=null, columnNames=[col1, col2], records=[{col1=A, col2=B}], [{col1={k1=v1, k2=10}, col2=3.14}]}}",
                    logsList.get(0).getMessage());
            assertEquals(Level.WARN, logsList.get(0).getLevel());
            assertEquals(
                    "{records=DataRecords{keyColumnNames=null, columnNames=[col1, col2], records=[{col1=C, col2=D}]}}",
                    logsList.get(1).getMessage());
            assertEquals(Level.WARN, logsList.get(1).getLevel());
        }
    }

    @Test
    public void test_mock_childDataFlow() throws Exception {
        DataFlowInstance instance =
                getInstance(
                        "testFlow_mock_childDataFlow",
                        newInstance -> newInstance.setValue("prefix", "mock-"));
//        DataFlowInstance instance = getCompiledInstance("dataflow/core/test/TestFlow-mock-childDataFlow.yaml",
//            newInstance -> newInstance.setValue("prefix", "mock-"));
        List<String> inputList = ImmutableList.of("A", "B", "C");
        instance.setValue("stream", inputList.stream());
        List<String> out = Collections.synchronizedList(new ArrayList<>());
        instance.setValue("list", out);
        instance.execute();
        assertEquals(3, out.size());
        assertTrue(out.contains("mock-A"));
        assertTrue(out.contains("mock-B"));
        assertTrue(out.contains("mock-C"));
    }

    @Test
    public void test_compile_dataflow() throws Exception {
        String id = "testFlow_compiled";
        String in2Value = UUID.randomUUID().toString();
        String yaml = COMPILE_TEST_YAML.replace("${id}", id).replace("${in2Value}", in2Value);
        env.registerDataFlow(new ByteArrayInputStream(yaml.getBytes()), id);
        try (DataFlowInstance instance =
                env.getRegistry().getDataFlowRegistration(id).getInstanceFactory().newInstance()) {
            instance.setValue("in1", "123");
            instance.execute();
            String output = new String(instance.getOutput().toString());
            assertEquals("123:" + in2Value, output);
        } finally {
            assertNotNull(env.getRegistry().unregisterDataFlow(id));
        }
    }

    @Test
    public void test_doubleChildDataFlow() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_doubleChildDataFlow");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        Stream<Stream<String>> doubleStream =
                ImmutableList.of(ImmutableList.of("A", "B", "C").stream()).stream();
        instance.setValue("stream", doubleStream);
        List<String> out = Collections.synchronizedList(new ArrayList<>());
        instance.setValue("list", out);
        instance.execute();
        assertEquals(3, out.size());
        assertTrue(out.contains("A-success"));
        assertTrue(out.contains("B-success"));
        assertTrue(out.contains("C-success"));
        assertEquals(5, eventHandler.getDataFlowStartCount());
        assertEquals(5, eventHandler.getDataFlowFinishCount());
        assertEquals(7, eventHandler.getComponentStartCount());
        assertEquals(7, eventHandler.getComponentFinishCount());
        assertEquals(0, eventHandler.getDataFlowErrorCount());
        assertEquals(0, eventHandler.getComponentErrorCount());
    }

    @Test
    public void test_doubleChildDataFlow2() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_doubleChildDataFlow2");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        Stream<Stream<String>> doubleStream1 =
                ImmutableList.of(ImmutableList.of("A", "B").stream()).stream();
        Stream<Stream<String>> doubleStream2 =
                ImmutableList.of(ImmutableList.of("C", "D").stream()).stream();

        instance.setValue("stream1", doubleStream1);
        instance.setValue("stream2", doubleStream2);
        List<String> out = Collections.synchronizedList(new ArrayList<>());
        instance.setValue("list", out);
        instance.execute();
        assertEquals(4, out.size());
        assertTrue(out.contains("A-success1"));
        assertTrue(out.contains("B-success1"));
        assertTrue(out.contains("C-success2"));
        assertTrue(out.contains("D-success2"));
        assertEquals(7, eventHandler.getDataFlowStartCount());
        assertEquals(7, eventHandler.getDataFlowFinishCount());
        assertEquals(12, eventHandler.getComponentStartCount());
        assertEquals(12, eventHandler.getComponentFinishCount());
        assertEquals(0, eventHandler.getDataFlowErrorCount());
        assertEquals(0, eventHandler.getComponentErrorCount());
    }

    @Test @Ignore
    public void test_conditionalValue_firstValue() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_conditionalValue");
        TestDataFlowEventHandler eventHandler = new TestDataFlowEventHandler(instance);
        env.registerEventHandler(eventHandler);
        instance.setValue("in1", "A");
        instance.execute();
        assertEquals("A", instance.getOutput());
        assertEquals("", eventHandler.getEvents().toString());
    }

    @Test @Ignore
    public void test_compile_dataflow_stressTest() throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        List<Future<String>> futures = new ArrayList<>();
        for (int cnt = 1; cnt < 1000; cnt++) {
            String id = "testFlow_compiled" + cnt;
            futures.add(
                    executor.submit(
                            () -> {
                                String in2Value = UUID.randomUUID().toString();
                                String yaml =
                                        COMPILE_TEST_YAML
                                                .replace("${id}", id)
                                                .replace("${in2Value}", in2Value);
                                env.registerDataFlow(new ByteArrayInputStream(yaml.getBytes()), id);
                                try (DataFlowInstance instance =
                                        env.getRegistry()
                                                .getDataFlowRegistration(id)
                                                .getInstanceFactory()
                                                .newInstance()) {
                                    instance.setValue("in1", "123");
                                    instance.execute();
                                    String output = new String(instance.getOutput().toString());
                                    assertEquals("123:" + in2Value, output);
                                    System.out.println(id + ": " + output);
                                    return output;
                                } finally {
                                    assertNotNull(env.getRegistry().unregisterDataFlow(id));
                                }
                            }));
        }
        for (Future<String> future : futures) {
            future.get();
        }
    }

    private DataFlowInstance getInstance(String id) throws Exception {
        return env.getRegistry().getDataFlowRegistration(id).getInstanceFactory().newInstance();
    }

    private DataFlowInstance getInstance(String id, Consumer<DataFlowInstance> initFn)
            throws Exception {
        return env.getRegistry()
                .getDataFlowRegistration(id)
                .getInstanceFactory()
                .newInstance(initFn);
    }

    private DataFlowInstance getCompiledInstance(String resourceFileName, Consumer<DataFlowInstance> initFn) {
        try {
            String id = "testflow";
            env.registerDataFlow(ClassLoader.getSystemResourceAsStream(resourceFileName), id);
            return env.getRegistry().getDataFlowRegistration(id).getInstanceFactory().newInstance(initFn);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
