/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ComponentMetricsTest.java
 */
package dataflow.core.test;

import static org.junit.Assert.assertEquals;

import dataflow.core.component.annotation.MetricType;
import dataflow.test.provider.TestMetricsComponentMetadata.Metrics;
import org.junit.Test;

public class ComponentMetricsTest {

    @Test
    public void testMetrics() {
        assertEquals(2, Metrics.VALUES.size());

        assertEquals("testMetric1", Metrics.testMetric1.getName());
        assertEquals(MetricType.COUNTER, Metrics.testMetric1.getType());
        assertEquals("The first test metric", Metrics.testMetric1.getDescription());

        assertEquals("testMetric2", Metrics.testMetric2.getName());
        assertEquals(MetricType.TIMER, Metrics.testMetric2.getType());
        assertEquals("", Metrics.testMetric2.getDescription());
    }
}
