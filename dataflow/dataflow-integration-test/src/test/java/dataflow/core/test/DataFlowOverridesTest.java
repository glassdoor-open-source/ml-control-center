/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowOverridesTest.java
 */
package dataflow.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.ImmutableList;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.DependencyInjector;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

public class DataFlowOverridesTest {

    private DataFlowEnvironment env;
    private DependencyInjector dependencyInjector;

    @Before
    public void setUp() throws IOException {
        this.dependencyInjector = new SimpleDependencyInjector("myTestVal");
        this.env = new DataFlowEnvironment("test", dependencyInjector, null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void test_overrides() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_overrides");
        instance.setValue("in1", "X");
        instance.setValue("in3", "Z");
        instance.execute();
        Map<String, String> output = (Map<String, String>) instance.getOutput();
        assertEquals(3, output.size());
        assertEquals("X", output.get("val1"));
        assertEquals("someValue2", output.get("val2"));
        assertEquals("myOverride", output.get("val3"));
    }

    @Test
    public void test_overrides_childDataFlow() throws Exception {
        DataFlowInstance instance = getInstance("testFlow_overrides_childDataFlow");
        List<String> inputList = ImmutableList.of("A", "B", "C");
        instance.setValue("stream", inputList.stream());
        List<String> out = Collections.synchronizedList(new ArrayList<>());
        instance.setValue("list", out);
        instance.execute();
        assertEquals(3, out.size());
        assertTrue(out.contains("override-A"));
        assertTrue(out.contains("override-B"));
        assertTrue(out.contains("override-C"));
    }

    private DataFlowInstance getInstance(String id) throws Exception {
        return env.getRegistry().getDataFlowRegistration(id).getInstanceFactory().newInstance();
    }
}
