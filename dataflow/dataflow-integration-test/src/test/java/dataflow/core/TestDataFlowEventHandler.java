/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestDataFlowEventHandler.java
 */
package dataflow.core;

import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEventHandler;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TestDataFlowEventHandler implements DataFlowEventHandler {

    private final String instanceId;
    private final List<String> events = Collections.synchronizedList(new ArrayList<>());
    private final AtomicInteger dataFlowStartCount = new AtomicInteger();
    private final AtomicInteger dataFlowFinishCount = new AtomicInteger();
    private final AtomicInteger dataFlowErrorCount = new AtomicInteger();
    private final List<Throwable> dataFlowErrors = Collections.synchronizedList(new ArrayList<>());

    private final AtomicInteger componentStartCount = new AtomicInteger();
    private final AtomicInteger componentFinishCount = new AtomicInteger();
    private final AtomicInteger componentErrorCount = new AtomicInteger();
    private final List<Throwable> componentErrors = Collections.synchronizedList(new ArrayList<>());

    public TestDataFlowEventHandler(DataFlowInstance instance) {
        this.instanceId = instance.getInstanceId();
    }

    @Override
    public void onDataFlowExecutionStarted(DataFlowInstance instance) {
        dataFlowStartCount.incrementAndGet();
        events.add("DataFlowExecutionStarted");
    }

    @Override
    public void onDataFlowExecutionFinished(DataFlowInstance instance) {
        dataFlowFinishCount.incrementAndGet();
        events.add("DataFlowExecutionFinished");
    }

    @Override
    public void onDataFlowExecutionError(DataFlowInstance instance, Throwable error) {
        dataFlowErrorCount.incrementAndGet();
        dataFlowErrors.add(error);
        events.add("DataFlowExecutionError-" + error.getMessage());
    }

    @Override
    public void onComponentExecutionStarted(DataFlowInstance instance, String componentId) {
        componentStartCount.incrementAndGet();
        events.add("ComponentExecutionStarted-" + componentId);
    }

    @Override
    public void onComponentExecutionFinished(DataFlowInstance instance, String componentId) {
        componentFinishCount.incrementAndGet();
        events.add("ComponentExecutionFinished-" + componentId);
    }

    @Override
    public void onComponentExecutionError(
            DataFlowInstance instance, String componentId, Throwable error) {
        componentErrorCount.incrementAndGet();
        componentErrors.add(error);
        events.add("ComponentExecutionError-" + componentId + "-" + error.getMessage());
    }

    public String getInstanceId() {
        return instanceId;
    }

    public List<String> getEvents() {
        return events;
    }

    public int getDataFlowStartCount() {
        return dataFlowStartCount.get();
    }

    public int getDataFlowFinishCount() {
        return dataFlowFinishCount.get();
    }

    public int getDataFlowErrorCount() {
        return dataFlowErrorCount.get();
    }

    public List<Throwable> getDataFlowErrors() {
        return dataFlowErrors;
    }

    public int getComponentStartCount() {
        return componentStartCount.get();
    }

    public int getComponentFinishCount() {
        return componentFinishCount.get();
    }

    public int getComponentErrorCount() {
        return componentErrorCount.get();
    }

    public List<Throwable> getComponentErrors() {
        return componentErrors;
    }
}
