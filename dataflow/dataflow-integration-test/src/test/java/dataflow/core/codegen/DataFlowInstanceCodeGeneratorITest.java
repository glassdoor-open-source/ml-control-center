/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstanceCodeGeneratorITest.java
 */
package dataflow.core.codegen;

import static org.junit.Assert.*;

import com.google.common.io.Resources;
import dataflow.core.compiler.CompileException;
import dataflow.core.compiler.JavaCompilerUtil;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.parser.DataFlowParser;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.Before;
import org.junit.Test;

public class DataFlowInstanceCodeGeneratorITest {

    private DataFlowParser parser;
    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);

        parser = new DataFlowParser(env.getRegistry());
    }

    @Test
    public void testGenerateSource_parser_testFlow2()
            throws DataFlowParseException, IOException, URISyntaxException {
        String yamlConfig =
                Files.readString(
                        Path.of(
                                ClassLoader.getSystemResource(
                                                "dataflow/core/parser/test-flow-2.yaml")
                                        .toURI()));
        DataFlowConfig config =
                parser.parse(new ByteArrayInputStream(yamlConfig.getBytes(StandardCharsets.UTF_8)));

        DataFlowInstanceCodeGenerator generator =
                new DataFlowInstanceCodeGenerator(env.getRegistry());

        DataFlowInstanceCodeGenConfig codeGenConfig =
                DataFlowInstanceCodeGenConfig.builder()
                        .setPackage("dataflow.test")
                        .setClassName("TestFlow2")
                        .build();
        String source = generator.generateSource(config, yamlConfig, codeGenConfig);

        try {
            assertNotNull(
                    JavaCompilerUtil.compile(
                            codeGenConfig.getFullName(), source, this.getClass().getClassLoader()));
        } catch (CompileException ex) {
            System.err.println(ex.getDiagnostics().toString());
            System.err.println("\nsource:\n\n" + source);
            fail(ex.getMessage());
        }
        String expected =
                Resources.toString(
                        ClassLoader.getSystemResource("dataflow/core/parser/TestFlow2.java"),
                        Charset.defaultCharset());
        assertEquals(expected, source);
    }
}
