/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestMetricsComponent.java
 */
package dataflow.test.provider;

import static dataflow.core.component.annotation.MetricType.COUNTER;
import static dataflow.core.component.annotation.MetricType.TIMER;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowMetric;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent(
        metrics = {
            @DataFlowMetric(
                    name = "testMetric1",
                    description = "The first test metric",
                    type = COUNTER),
            @DataFlowMetric(name = "testMetric2", type = TIMER)
        })
public class TestMetricsComponent {

    @DataFlowConfigurable
    public TestMetricsComponent() {}

    @OutputValue
    public Void getValue() {
        return null;
    }
}
