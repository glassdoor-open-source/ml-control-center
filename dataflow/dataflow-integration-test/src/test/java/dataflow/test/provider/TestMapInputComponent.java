/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestMapInputComponent.java
 */
package dataflow.test.provider;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.test.TestConfigurableObj;
import java.util.Map;
import java.util.TreeMap;

@DataFlowComponent
public class TestMapInputComponent {

    @DataFlowConfigurable
    public TestMapInputComponent() {}

    @OutputValue
    public String getValue(@InputValue Map<String, TestConfigurableObj> map) {
        return new TreeMap<>(map).toString();
    }
}
