/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestPreloadingValueProvider.java
 */
package dataflow.test.provider;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.environment.DataFlowEnvironment;
import java.util.HashSet;
import java.util.Set;

@DataFlowComponent
public class TestPreloadingValueProvider {

    public static final Set<String> preloadedValues = new HashSet<>();

    @DataFlowConfigurable
    public TestPreloadingValueProvider() {}

    @SuppressWarnings("unchecked")
    public static void onLoad(
            ComponentConfig componentConfig,
            DataFlowConfig dataFlowConfig,
            DataFlowEnvironment env) {
        if (Boolean.TRUE.equals(componentConfig.getProperties().get("simulateLoadError"))) {
            throw new RuntimeException("Simulated load error");
        }
        preloadedValues.add((String) componentConfig.getProperties().get("valueToPreload"));
    }

    public static void onUnload(
            ComponentConfig componentConfig,
            DataFlowConfig dataFlowConfig,
            DataFlowEnvironment env) {
        if (Boolean.TRUE.equals(componentConfig.getProperties().get("simulateUnloadError"))) {
            throw new RuntimeException("Simulated unload error");
        }
        preloadedValues.remove(componentConfig.getProperties().get("valueToPreload"));
    }

    @OutputValue
    public Set<String> getValue() {
        return preloadedValues;
    }
}
