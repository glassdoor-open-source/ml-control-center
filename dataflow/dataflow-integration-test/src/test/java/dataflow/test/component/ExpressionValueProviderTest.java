/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ExpressionValueProviderTest.java
 */
package dataflow.test.component;

import static org.junit.Assert.*;

import com.google.common.collect.ImmutableMap;
import dataflow.core.codegen.DataFlowInstanceCodeGenConfig;
import dataflow.core.codegen.DataFlowInstanceCodeGenerator;
import dataflow.core.compiler.CompileException;
import dataflow.core.compiler.JavaCompilerUtil;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.type.IntegerValue;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Predicate;
import org.junit.Before;
import org.junit.Test;

public class ExpressionValueProviderTest {

    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void testExpression_noValueRef() throws Exception {
        testExprFlow("exprFlow_noValueRef", Collections.emptyMap(), 7);
    }

    @Test
    public void testExpression_singleValueRef() throws Exception {
        testExprFlow(
                "exprFlow_singleValueRef",
                ImmutableMap.<String, Object>builder().put("testValue", 4).build(),
                4);
    }

    @Test
    public void testExpression_singleValueRef_withPropertyValueRef() throws Exception {
        testExprFlow(
                "exprFlow_singleValueRef_withPropValueRef",
                Collections.emptyMap(),
                new IntegerValue(1));
    }

    @Test
    public void testExpression_multipleValueRefs() throws Exception {
        testExprFlow(
                "exprFlow_multipleValueRefs",
                ImmutableMap.<String, Object>builder()
                        .put("testVal1", 3)
                        .put("testVal2", 7)
                        .build(),
                21);
    }

    @Test
    public void testExpression_singleValueRefInExpr() throws Exception {
        testExprFlow(
                "exprFlow_singleValueRef_inExpr",
                ImmutableMap.<String, Object>builder().put("testValue", 4).build(),
                4);
    }

    @Test
    public void testExpression_singleValue_multipleInputs() throws Exception {
        testExprFlow(
                "exprFlow_singleValueRef_multipleInputs",
                ImmutableMap.<String, Object>builder().put("input1", 4).put("input2", 6).build(),
                ImmutableMap.builder().put("in1", 4).put("in2", 6).build());
    }

    @Test
    public void testExpression_singleValue_withPropertyAndInput() throws Exception {
        testExprFlow(
                "exprFlow_singleValueRef_withPropertyAndInput",
                Collections.emptyMap(),
                new IntegerValue(2));
    }

    @Test
    public void testExpression_nestedValueRef() throws Exception {
        testExprFlow(
                "exprFlow_nestedValueRef",
                Collections.emptyMap(),
                ImmutableMap.builder().put("in", new IntegerValue(1)).build());
    }

    @Test
    public void testExpression_inlineEval() throws Exception {
        testExprFlow(
                "exprFlow_inlineEval",
                ImmutableMap.<String, Object>builder().put("testValue", "test").build(),
                "start_test:test_end");
    }

    @Test
    public void testExpression_invalidExpr() {
        try {
            DataFlowParser parser = new DataFlowParser(env.getRegistry());
            DataFlowConfig dataFlowConfig =
                    parser.parse(
                            ClassLoader.getSystemResourceAsStream(
                                    "dataflow/core/component/expr-flow-invalidExpr.yaml"));
            DataFlowInstanceCodeGenerator codeGenerator =
                    new DataFlowInstanceCodeGenerator(env.getRegistry());
            DataFlowInstanceCodeGenConfig codeGenConfig =
                    new DataFlowInstanceCodeGenConfig(
                            "dataflow.environment",
                            "ExprFlow_invalidExprDataFlow",
                            true,
                            true,
                            true);
            String source = codeGenerator.generateSource(dataFlowConfig, "", codeGenConfig);
            Class<DataFlowInstance> instanceClass =
                    JavaCompilerUtil.compile(
                            codeGenConfig.getPackageName() + "." + codeGenConfig.getClassName(),
                            source,
                            this.getClass().getClassLoader());
            Constructor<DataFlowInstance> constructor =
                    instanceClass.getDeclaredConstructor(
                            DataFlowEnvironment.class, Executor.class, Consumer.class);
            constructor.setAccessible(true);
            DataFlowInstance instance =
                    constructor.newInstance(env, Executors.newSingleThreadExecutor(), null);
            instance.execute();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals(CompileException.class, ex.getClass());
        }
    }

    @Test
    public void testExpression_invalidValueRef() {
        try {
            DataFlowParser parser = new DataFlowParser(env.getRegistry());
            DataFlowConfig dataFlowConfig =
                    parser.parse(
                            ClassLoader.getSystemResourceAsStream(
                                    "dataflow/core/component/expr-flow-invalidValueRef.yaml"));
            DataFlowInstanceCodeGenerator codeGenerator =
                    new DataFlowInstanceCodeGenerator(env.getRegistry());
            DataFlowInstanceCodeGenConfig codeGenConfig =
                    new DataFlowInstanceCodeGenConfig(
                            "dataflow.environment",
                            "ExprFlow_invalidValueRefDataFlow",
                            true,
                            true,
                            true);
            codeGenerator.generateSource(dataFlowConfig, "", codeGenConfig);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Expression not defined for expr", ex.getMessage());
        }
    }

    @Test
    public void testExpression_invalidValueRef_noId() throws Exception {
        try {
            DataFlowParser parser = new DataFlowParser(env.getRegistry());
            DataFlowConfig dataFlowConfig =
                    parser.parse(
                            ClassLoader.getSystemResourceAsStream(
                                    "dataflow/core/component/expr-flow-invalidValueRef-noId.yaml"));
            DataFlowInstanceCodeGenerator codeGenerator =
                    new DataFlowInstanceCodeGenerator(env.getRegistry());
            DataFlowInstanceCodeGenConfig codeGenConfig =
                    new DataFlowInstanceCodeGenConfig(
                            "dataflow.environment",
                            "ExprFlow_invalidValueRef_noId_DataFlow",
                            true,
                            true,
                            true);
            codeGenerator.generateSource(dataFlowConfig, "", codeGenConfig);
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("Error parsing expression id=expr \"@(\" -> \")\"", ex.getMessage());
        }
    }

    private void testExprFlowWithError(
            String filename, Map<String, Object> input, Predicate<Exception> exceptionPredicate) {
        try {
            testExprFlow(filename, input, null);
            fail("Expected exception was not thrown");
        } catch (Exception e) {
            // Expected.
            assertTrue(exceptionPredicate.test(e));
        }
    }

    private void testExprFlow(String id, Map<String, Object> input, Object expectedOutput)
            throws Exception {
        try (DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration(id)
                        .getInstanceFactory()
                        .newInstance(i -> {})) {
            input.forEach(instance::setValue);
            instance.execute();
            assertEquals(expectedOutput, instance.getOutput());
        }
    }
}
