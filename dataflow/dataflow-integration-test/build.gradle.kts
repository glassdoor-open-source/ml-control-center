

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.dataflowIntegration)

    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(projects.dataflowStream)
    api(projects.dataflowComponentCore)
    api(projects.dataflowIntegration)
    api(libs.guava)
    api(libs.logback)
    testImplementation(libs.mockitoCore)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-integration-test"
