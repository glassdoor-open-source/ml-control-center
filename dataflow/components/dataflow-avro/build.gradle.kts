

plugins {
    id("java-library")
}

dependencies {
    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(libs.avro)
    api(libs.micrometerCore)

    testImplementation(libs.jsonassert)
    testImplementation(libs.springTest)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.micrometerCore)
    testImplementation(libs.springBootTestAutoconfigure)
}

group = "dataflow"
description = "dataflow-avro"
