/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AvroDataRecordSerializationScheme.java
 */
package dataflow.avro;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.data.DataRecordReader;
import dataflow.data.DataRecordSerializationScheme;
import dataflow.data.DataRecordWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.avro.Schema;

public class AvroDataRecordSerializationScheme implements DataRecordSerializationScheme {

    private final Schema schema;

    @DataFlowConfigurable
    public AvroDataRecordSerializationScheme(@DataFlowConfigProperty String schema) {
        this.schema = new Schema.Parser().parse(schema);
    }

    @Override
    public DataRecordReader createReader(InputStream in) throws IOException {
        return new AvroDataRecordReader(schema, in);
    }

    @Override
    public DataRecordWriter createWriter(OutputStream out) throws IOException {
        return new AvroDataRecordWriter(schema, out);
    }
}
