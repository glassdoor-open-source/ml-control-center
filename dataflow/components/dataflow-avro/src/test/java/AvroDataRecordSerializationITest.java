/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AvroDataRecordSerializationITest.java
 */
import static org.mockito.Mockito.when;

import avro.shaded.com.google.common.collect.ImmutableList;
import dataflow.avro.AvroDataRecordReader;
import dataflow.avro.AvroDataRecordWriter;
import dataflow.core.datasource.DataSource;
import dataflow.data.DataRecord;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class AvroDataRecordSerializationITest extends TestCase {

    private static final String TEST_SCHEMA_1 =
            "{\"type\":\"record\",\"namespace\":\"test\",\"name\":\"User\",\"fields\":[{\"name\":\"Name\",\"type\":\"string\"},{\"name\":\"Age\",\"type\":\"int\"}]}";

    @Mock private DataSource mockDataSource;

    @Test
    public void testWriteAndRead() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        when(mockDataSource.getOutputStream()).thenReturn(out);
        AvroDataRecordWriter writer = new AvroDataRecordWriter(TEST_SCHEMA_1, mockDataSource);
        DataRecord r1 = new DataRecord();
        r1.put("Name", "Jack");
        r1.put("Age", 30);
        writer.write(r1);
        writer.close();

        when(mockDataSource.getInputStream())
                .thenAnswer(
                        (Answer<InputStream>) inv -> new ByteArrayInputStream(out.toByteArray()));
        AvroDataRecordReader reader = new AvroDataRecordReader(TEST_SCHEMA_1, mockDataSource);
        assertEquals(ImmutableList.of("Name", "Age"), reader.getColumnNames());
        List<DataRecord> readRecords = new ArrayList<>();
        while (reader.hasNext()) {
            readRecords.add(reader.next());
        }
        assertEquals(1, readRecords.size());
        assertEquals("Jack", readRecords.get(0).get("Name"));
        assertEquals(30, readRecords.get(0).get("Age"));
    }
}
