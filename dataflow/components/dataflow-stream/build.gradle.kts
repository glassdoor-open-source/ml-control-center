

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowCore)
    api(projects.dataflowRetry)
    api(projects.dataflowTask)
    api(libs.guava)
    api(libs.reactiveStreams)
    api(libs.reactorCore)
    api(libs.jacksonAnnotations)
    api(libs.micrometerCore)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

group = "dataflow"
description = "dataflow-stream"
