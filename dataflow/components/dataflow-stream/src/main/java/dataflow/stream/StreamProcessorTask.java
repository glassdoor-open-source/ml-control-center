/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StreamProcessorTask.java
 */
package dataflow.stream;

import static dataflow.core.config.DataFlowConfig.DEFAULT_ITEM_VALUE_NAME;
import static java.util.Objects.requireNonNullElse;

import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.ThreadLocalDataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.util.MetricsUtil;
import dataflow.retry.Retry;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.util.StringUtils;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import reactor.core.Exceptions;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/** A stream processing task. */
@SuppressWarnings("unused")
class StreamProcessorTask {

    // Defaults to use if they have not been set by the caller.
    private static final int DEFAULT_TASK_PARALLELISM = 3;
    private static final int DEFAULT_TASK_PREFETCH_COUNT = 1;
    private static final int DEFAULT_TASK_BATCH_SIZE = 200;
    private static final String DEFAULT_TIMEOUT = "P365D";
    private static final String BEFORE_START_ID_PREFIX = "beforeStart_";
    private static final String BEFORE_START_OUTPUT_ID = "beforeStart_output";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Stream<?> stream;
    private DataFlowExecutionContext executionContext;
    private final ThreadLocalDataFlowInstance pipeline;
    private final DataFlowInstance beforeStartFlow;
    private Map<String, Object> beforeStartValues = new HashMap<>();
    private Object beforeStartOutputValue;
    private Object afterCompleteOutputValue;
    private final DataFlowInstance afterCompleteFlow;

    protected final DataFlowEnvironment env;

    private final String itemValueName;
    protected final String metricNamePrefix;
    private Timer pipelineExecutionTimer;
    private Timer batchTimer;
    private Counter cancelCounter;
    private AtomicLong taskRuntimeSecondsGauge;
    private AtomicLong recordCountGauge;
    protected final MeterRegistry meterRegistry;
    private final TaskContext context = new TaskContext();

    private CompletableFuture<Object> futureResult;

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final AtomicBoolean canceled = new AtomicBoolean(false);
    private final TaskConfig taskConfig;
    private ScheduledFuture<?> progressUpdater;
    private Date lastMetricsUpdateTime;
    private final AtomicLong lastMetricsUpdateCompletedRecordCount = new AtomicLong();

    public StreamProcessorTask(
            Stream<?> stream,
            ThreadLocalDataFlowInstance pipeline,
            ThreadLocalDataFlowInstance beforeStartFlow,
            ThreadLocalDataFlowInstance afterCompleteFlow,
            boolean dryRun,
            int dryRunRecordCount,
            TaskConfig defaultTaskConfig,
            String metricNamePrefix,
            String itemValueName,
            MeterRegistry meterRegistry,
            DataFlowEnvironment env) {
        this.stream = stream;
        this.pipeline = pipeline;
        this.beforeStartFlow = beforeStartFlow;
        this.afterCompleteFlow = afterCompleteFlow;
        this.env = env;
        context.setTaskId(UUID.randomUUID().toString().replace("-", ""));
        context.setDryRun(dryRun);
        context.setDryRunMaxItemCount(dryRunRecordCount);
        this.itemValueName = itemValueName != null ? itemValueName : DEFAULT_ITEM_VALUE_NAME;
        this.metricNamePrefix =
                StringUtils.isEmpty(metricNamePrefix)
                        ? getClass().getSimpleName()
                        : String.format("%s.%s", metricNamePrefix, getClass().getSimpleName());
        this.meterRegistry = meterRegistry;
        this.taskConfig = buildTaskConfig(defaultTaskConfig, dryRun);
        context.setRetry(Retry.clone(this.taskConfig.getRetry()));

        Map<String, Object> configProperties = new HashMap<>(taskConfig.getProperties());
        if (dryRun) {
            configProperties.putAll(taskConfig.getDryRunProperties());
        }
        context.setConfigProperties(configProperties);
    }

    private TaskConfig buildTaskConfig(TaskConfig defaultTaskConfig, boolean dryRun) {
        TaskConfig taskConfig = new TaskConfig();
        taskConfig.mergePropertiesIfNotSet(defaultTaskConfig);
        taskConfig.mergePropertiesIfNotSet(buildDefaultTaskConfig());
        return taskConfig;
    }

    public String getId() {
        return context.getTaskId();
    }

    public synchronized CompletableFuture<Object> start() {
        if (futureResult != null) {
            return futureResult;
        }
        running.set(true);
        canceled.set(false);

        futureResult = new CompletableFuture<>();
        context.setStartTime(new Date());

        executionContext = DataFlowExecutionContext.getCurrentExecutionContext();
        env.executeAsync(this::execute, StreamProcessorTask.class.getSimpleName());
        return futureResult;
    }

    private void execute() {
        DataFlowExecutionContext priorExecutionContext =
                DataFlowExecutionContext.getCurrentExecutionContext();

        Scheduler inputScheduler = null;
        Scheduler pipelineScheduler = null;

        try {
            inputScheduler =
                    Schedulers.newBoundedElastic(
                            taskConfig.getPrefetchCount(), Integer.MAX_VALUE, "mlccInput");
            pipelineScheduler =
                    Schedulers.newBoundedElastic(
                            taskConfig.getParallelism(), Integer.MAX_VALUE, "mlccPipeline");

            DataFlowExecutionContext.setCurrentExecutionContext(executionContext);
            setMDC(context);
            logger.info("Starting stream processing");

            logger.info(taskConfig.toString());

            initMetrics(context.getRetry());
            addRetryLogging(context.getRetry());

            onUpdateStart(context);

            if (progressUpdater != null && !progressUpdater.isDone()) {
                progressUpdater.cancel(true);
            }
            this.progressUpdater =
                    env.scheduleAtFixedRate(
                            () -> {
                                if (running.get()) {
                                    updateMetrics(context);
                                    logProgress(context);
                                }
                            },
                            5,
                            30,
                            TimeUnit.SECONDS,
                            StreamProcessorTask.class.getSimpleName() + "-ProgressUpdater");

            // Run before update pipeline
            runBeforeStartPipeline();

            // Define and run the pipeline.

            Flux<TaskBatchContext> inputFlux = createStreamFlux(stream, context);
            inputFlux
                    .subscribeOn(inputScheduler)
                    .parallel(taskConfig.getParallelism(), taskConfig.getPrefetchCount())
                    .runOn(pipelineScheduler, taskConfig.getPrefetchCount())
                    .flatMap(createPipelineFn())
                    .flatMap(createFinishBatchFn())
                    .sequential()
                    .doOnError(ex -> doOnError(context, ex))
                    .blockLast(
                            Duration.ofSeconds(
                                    requireNonNullElse(
                                            taskConfig.getTimeoutSeconds(),
                                            Duration.parse(DEFAULT_TIMEOUT).getSeconds())));

            // Run after update pipeline.
            runAfterCompletePipeline();

            // Update is finished.
            doOnComplete(context);
        } catch (Throwable ex) {
            doOnError(context, ex);
        } finally {
            try {
                if (inputScheduler != null) {
                    inputScheduler.dispose();
                }
            } catch (Throwable e) {
                logger.warn("Failed to release input thread pool");
            }
            try {
                if (pipelineScheduler != null) {
                    pipelineScheduler.dispose();
                }
            } catch (Throwable e) {
                logger.warn("Failed to release pipeline thread pool");
            }

            if (!futureResult.isDone()) {
                logger.warn("Task did not complete the result future " + getId());
            }
            DataFlowExecutionContext.setCurrentExecutionContext(priorExecutionContext);
        }
    }

    public boolean isRunning() {
        return running.get();
    }

    public boolean isCanceled() {
        return canceled.get();
    }

    public void setCanceled(boolean canceled) {
        this.canceled.set(canceled);
    }

    public CompletableFuture<?> getFutureResult() {
        return futureResult;
    }

    public Date getStartTime() {
        return context.getStartTime();
    }

    public int getRetryCount() {
        return context.getRetryCount().intValue();
    }

    public double getRunTimeHours() {
        if (context.getStartTime() == null) {
            return 0.0;
        }
        return (System.currentTimeMillis() - context.getStartTime().getTime())
                / 1000.0
                / 60.0
                / 60.0;
    }

    protected void onUpdateStart(TaskContext context) {}

    protected void onBatchCompletion(TaskContext context) {}

    protected void onUpdateFailure(TaskContext context, Throwable ex) {}

    protected void onUpdateCanceled(TaskContext context) {}

    protected void finishUpdate(TaskContext context) {}

    protected Counter getErrorCounter(String errorType) {
        if (meterRegistry == null) {
            return null;
        }
        List<Tag> tags =
                getTags().entrySet().stream()
                        .map(entry -> Tag.of(entry.getKey(), entry.getValue()))
                        .collect(Collectors.toList());
        if (errorType != null) {
            tags.add(Tag.of("errorType", errorType));
        }
        return meterRegistry.counter(getMetricName("errorCount"), tags);
    }

    protected TaskConfig getTaskConfig() {
        return new TaskConfig();
    }

    protected Map<String, String> getTags() {
        Map<String, String> tags = new HashMap<>();
        tags.put("dryRun", String.valueOf(context.isDryRun()));
        return tags;
    }

    protected TaskConfig buildDefaultTaskConfig() {
        TaskConfig taskConfig = new TaskConfig();
        taskConfig.setBatchSize(DEFAULT_TASK_BATCH_SIZE);
        taskConfig.setParallelism(DEFAULT_TASK_PARALLELISM);
        taskConfig.setPrefetchCount(DEFAULT_TASK_PREFETCH_COUNT);
        return taskConfig;
    }

    protected void updateMetrics(TaskContext context) {
        setMDC(context);
        try {
            long recordCount = context.getCompletedItemCount().get();
            if (lastMetricsUpdateTime != null) {
                int completedRecordsSinceMetricsUpdate =
                        (int) (recordCount - lastMetricsUpdateCompletedRecordCount.get());
                long millisSinceMetricsUpdate =
                        System.currentTimeMillis() - lastMetricsUpdateTime.getTime();
                if (completedRecordsSinceMetricsUpdate > 0) {
                    context.getTimeEstimator()
                            .recordExecutionTime(
                                    completedRecordsSinceMetricsUpdate, millisSinceMetricsUpdate);
                }
            }
            if (recordCountGauge != null) {
                recordCountGauge.set(recordCount);
            }
            if (taskRuntimeSecondsGauge != null) {
                taskRuntimeSecondsGauge.set(
                        (System.currentTimeMillis() - context.getStartTime().getTime()) / 1000);
            }
            lastMetricsUpdateTime = new Date();
            lastMetricsUpdateCompletedRecordCount.set(recordCount);
        } catch (Throwable ex) {
            logger.warn(getClass().getSimpleName() + " metrics update error", ex);
        }
    }

    protected void logProgress(TaskContext context) {
        setMDC(context);
        try {
            long recordCount = context.getCompletedItemCount().get();
            double avgItemsPerSecond =
                    1000.0 / context.getTimeEstimator().getAverageTimePerItemInMillis();
            String timeEstimateStr = "";
            String totalCountEstimateStr = "";
            if (context.getEstimatedItemCount().get() > 0) {
                long timeEstimateSeconds =
                        context.getTimeEstimator()
                                .getRemainingTimeEstimateSeconds(
                                        context.getEstimatedItemCount().get());
                timeEstimateStr =
                        String.format(
                                " - %.2f records/sec. - estimated hours remaining: %.1f",
                                avgItemsPerSecond, timeEstimateSeconds / 60.0 / 60.0);
                totalCountEstimateStr = "/" + context.getEstimatedItemCount().get();
            } else if (avgItemsPerSecond > 0) {
                timeEstimateStr = String.format(" - %.2f records/sec.", avgItemsPerSecond);
            }

            double runTimeHours = getRunTimeHours();
            logger.info(
                    String.format(
                            "Stream processing is in progress - %d total items completed%s in %.1f hours%s",
                            recordCount, totalCountEstimateStr, runTimeHours, timeEstimateStr));
        } catch (Throwable ex) {
            logger.warn(getClass().getSimpleName() + " progress logger error", ex);
        }
    }

    private void doOnComplete(TaskContext context) {
        try {
            if (!canceled.get()) {
                updateMetrics(context);
                finishUpdate(context);

                logger.info(
                        String.format(
                                "Finished processing stream, %d items processed",
                                context.getCompletedItemCount().get()));
            } else {
                logger.info("Stream processing was canceled");
            }
        } finally {
            cleanUp();

            if (!canceled.get()) {
                futureResult.complete(afterCompleteOutputValue);
            } else {
                futureResult.completeExceptionally(
                        new DataFlowExecutionException("The update was canceled"));
            }
        }
    }

    protected String getErrorType(Throwable ex) {
        if (ex.getCause() != null) {
            return getErrorType(ex.getCause());
        }
        return ex.getClass().getSimpleName();
    }

    private void doOnError(TaskContext context, Throwable ex) {
        try {
            if (futureResult.isCompletedExceptionally()) {
                // The task has already completed with an error.
                return;
            }
            onUpdateFailure(context, ex);

            Counter errorCounter = getErrorCounter(getErrorType(ex));
            if (errorCounter != null) {
                errorCounter.increment();
            }
            logger.warn("Stream processing failed", ex);
        } finally {
            cleanUp();
            futureResult.completeExceptionally(ex);
        }
    }

    private void cleanUp() {
        running.set(false);
        try {
            logger.info("Performing final clean up");

            pipeline.close();

            if (progressUpdater != null) {
                progressUpdater.cancel(false);
                progressUpdater = null;
            }
        } catch (Throwable ex) {
            logger.warn("Error performing cleanup after feature update", ex);
        }
        MDC.clear();
    }

    private void runBeforeStartPipeline() throws Exception {
        if (beforeStartFlow != null) {
            logger.info("Executing beforeStart actions");
            for (String providedValueId : beforeStartFlow.getConfig().getProvidedValueIds()) {
                beforeStartFlow.setValue(
                        providedValueId, executionContext.getInstance().getValue(providedValueId));
            }
            beforeStartFlow.execute();
            beforeStartValues = beforeStartFlow.getValues();
            beforeStartOutputValue = beforeStartFlow.getOutput();
            logger.info("Finished executing beforeStart actions");
        }
    }

    private void runAfterCompletePipeline() throws Exception {
        if (afterCompleteFlow != null) {
            logger.info("Executing afterComplete actions");
            setProvidedValues(afterCompleteFlow, executionContext);
            afterCompleteFlow.execute();
            afterCompleteOutputValue = afterCompleteFlow.getOutput();
            logger.info("Finished executing afterComplete actions");
        }
    }

    private Flux<TaskBatchContext> createStreamFlux(Stream<?> stream, TaskContext context) {
        logger.info("Starting processing stream");

        return Flux.using(
                () -> stream,
                (Function<Stream<?>, Publisher<TaskBatchContext>>)
                        inputItemStream -> {
                            if (Boolean.TRUE.equals(taskConfig.getPrefetchAllItems())) {
                                // Preload all items into memory.
                                inputItemStream = preloadItems(inputItemStream);
                            }
                            Iterator<?> itemsIt = inputItemStream.iterator();
                            return Flux.generate(
                                    sink -> {
                                        setMDC(context);
                                        if (context.isDryRun()
                                                && context.getInputItemCount().get()
                                                        > context.getDryRunMaxItemCount()) {
                                            logger.info("Read all items needed for dry run");
                                            sink.complete();
                                            return;
                                        }
                                        if (canceled.get()) {
                                            logger.info("Processing of stream was canceled");

                                            if (cancelCounter != null) {
                                                cancelCounter.increment();
                                            }
                                            onUpdateCanceled(context);
                                            sink.complete();
                                            return;
                                        }

                                        TaskBatchContext batchContext = new TaskBatchContext();
                                        batchContext.setTaskContext(context);
                                        batchContext.setStartTime(new Date());

                                        try {
                                            if (taskConfig.getBatchSize() > 1) {
                                                List<Object> items = new ArrayList<>();
                                                while (itemsIt.hasNext()
                                                        && items.size()
                                                                < taskConfig.getBatchSize()) {
                                                    items.add(itemsIt.next());
                                                    context.getInputItemCount().addAndGet(1);
                                                }
                                                if (items.size() > 0) {
                                                    batchContext.setItem(items);
                                                    sink.next(batchContext);
                                                }
                                            } else if (itemsIt.hasNext()) {
                                                context.getInputItemCount().addAndGet(1);
                                                batchContext.setItem(itemsIt.next());
                                                sink.next(batchContext);
                                            }
                                            if (!itemsIt.hasNext()) {
                                                logger.info("Finished reading input records");
                                                sink.complete();
                                            }
                                        } catch (Throwable e) {
                                            logger.warn("Error reading item stream", e);
                                            sink.error(
                                                    new DataFlowExecutionException(
                                                            "Error reading item stream.", e));
                                        }
                                    });
                        },
                inputRecordsStream -> {
                    try {
                        inputRecordsStream.close();
                    } catch (Throwable t) {
                        logger.warn("Error closing input stream", t);
                        throw Exceptions.propagate(t);
                    }
                });
    }

    private <T> Stream<T> preloadItems(Stream<T> stream) {
        logger.info("Preloading all items");
        List<T> allItems = new ArrayList<>();
        AtomicInteger itemCount = new AtomicInteger();

        java.util.Timer timer = new java.util.Timer();
        try {
            timer.scheduleAtFixedRate(
                    new TimerTask() {
                        @Override
                        public void run() {
                            logger.info("Preloaded " + itemCount.get() + " items");
                        }
                    },
                    0,
                    TimeUnit.SECONDS.toMillis(60));
            stream.forEach(
                    item -> {
                        allItems.add(item);
                        itemCount.addAndGet(1);
                    });
        } finally {
            timer.cancel();
            logger.info("Finished preloading " + itemCount.get() + " items");
        }
        return allItems.stream();
    }

    private Function<TaskBatchContext, Publisher<TaskBatchContext>> createPipelineFn() {
        return batchContext -> {
            TaskContext context = batchContext.getTaskContext();
            try {
                Object item = batchContext.getItem();
                setMDC(context);

                setProvidedValues(pipeline, executionContext);
                pipeline.setValue(itemValueName, item);

                recordExecutionTime(
                        pipelineExecutionTimer,
                        () -> {
                            logger.debug("Executing pipeline");
                            pipeline.execute();
                            logger.debug("Finished executing pipeline");
                            return pipeline.getOutput();
                        });
                return Mono.just(batchContext).subscribeOn(Schedulers.boundedElastic());
            } catch (Throwable e) {
                throw new RuntimeException("Error executing pipeline.", e);
            }
        };
    }

    private void setProvidedValues(
            DataFlowInstance instance, DataFlowExecutionContext executionContext) {
        for (String providedValueId : instance.getConfig().getProvidedValueIds()) {
            Object providedValue = executionContext.getInstance().getValue(providedValueId);
            if (providedValue == null && providedValueId.startsWith(BEFORE_START_ID_PREFIX)) {
                providedValue =
                        beforeStartValues.get(
                                providedValueId.substring(BEFORE_START_ID_PREFIX.length()));
            }
            if (providedValue == null && providedValueId.equals(BEFORE_START_OUTPUT_ID)) {
                providedValue = beforeStartOutputValue;
            }
            if (providedValue != null) {
                instance.setValue(providedValueId, providedValue);
            }
        }
    }

    private Function<TaskBatchContext, Publisher<TaskBatchContext>> createFinishBatchFn() {
        return batchContext -> {
            TaskContext context = batchContext.getTaskContext();
            setMDC(context);

            try {
                Object item = batchContext.getItem();
                int itemCount = 1;
                if (item instanceof Collection) {
                    itemCount = ((Collection<?>) item).size();
                }
                context.getCompletedItemCount().addAndGet(itemCount);
                long batchExecutionTimeMillis =
                        (System.currentTimeMillis() - batchContext.getStartTime().getTime());
                onBatchCompletion(context);

                if (batchTimer != null) {
                    batchTimer.record(batchExecutionTimeMillis, TimeUnit.MILLISECONDS);
                }

                logger.debug(
                        String.format(
                                "Finished batch in %d sec. %d item(s) completed",
                                batchExecutionTimeMillis / 1000, itemCount));
                return Mono.just(batchContext).subscribeOn(Schedulers.boundedElastic());
            } catch (Throwable e) {
                throw new RuntimeException("Error finishing batch", e);
            }
        };
    }

    protected void setMDC(TaskContext context) {
        MDC.clear();
        MDC.put("taskId", context.getTaskId());
        if (context.isDryRun()) {
            MDC.put("dryRun", "true");
        }
    }

    private void initMetrics(Retry retry) {
        if (meterRegistry != null) {
            Iterable<Tag> tags =
                    getTags().entrySet().stream()
                            .map(entry -> Tag.of(entry.getKey(), entry.getValue()))
                            .collect(Collectors.toList());
            this.pipelineExecutionTimer =
                    meterRegistry.timer(getMetricName("pipelineExecutionTimer"), tags);
            this.recordCountGauge =
                    meterRegistry.gauge(getMetricName("recordCount"), tags, new AtomicLong());
            this.taskRuntimeSecondsGauge =
                    meterRegistry.gauge(
                            getMetricName("updateRunTimeSeconds"), tags, new AtomicLong());
            this.batchTimer = meterRegistry.timer(getMetricName("batchTimer"), tags);
            this.cancelCounter = meterRegistry.counter(getMetricName("cancelCount"), tags);
            if (retry != null) {
                Counter retryCounter = meterRegistry.counter(getMetricName("retryCount"), tags);
                retry.getEventPublisher().onRetry(event -> retryCounter.increment());
            }
        }
    }

    private void addRetryLogging(Retry retry) {
        if (retry != null) {
            retry.getEventPublisher()
                    .onRetry(
                            event -> {
                                setMDC(context);
                                logger.warn(event.toString());
                                context.getRetryCount().set(event.getNumberOfRetryAttempts());
                            });
            retry.getEventPublisher()
                    .onError(
                            event -> {
                                setMDC(context);
                                logger.error(event.toString());
                            });
        }
    }

    private String getMetricName(String name) {
        StringBuilder finalName = new StringBuilder();
        MetricsUtil.appendToMetricName(metricNamePrefix, finalName);
        MetricsUtil.appendToMetricName(name, finalName);
        return finalName.toString();
    }

    private static <T> void recordExecutionTime(Timer timer, Callable<T> callable)
            throws Exception {
        if (timer != null) {
            timer.recordCallable(callable);
            return;
        }
        callable.call();
    }
}
