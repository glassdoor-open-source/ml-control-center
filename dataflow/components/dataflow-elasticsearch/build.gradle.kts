

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(libs.googleAutoService)
    annotationProcessor(projects.dataflowCodegen)
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowComponentCore)

    api(projects.dataflowCore)
    api(projects.dataflowCodegen)
    api(projects.dataflowComponentCodegen)

    api(libs.slf4jApi)
    api(libs.micrometerCore)
    api(libs.reactiveStreams)
    api(libs.jacksonDatabind)
    api(libs.jacksonAnnotations)
    api(libs.elasticsearchRestHighLevelClient)

    testImplementation(libs.guava)
    testImplementation(libs.reactorCore)
    testImplementation(libs.mockitoCore)
}

configurations.all {
    resolutionStrategy {
        force(libs.elasticsearchRestHighLevelClient)
        force(libs.elasticsearch)
        force(libs.elasticsearchXContent)
    }
}

group = "dataflow"
description = "dataflow-elasticsearch"
