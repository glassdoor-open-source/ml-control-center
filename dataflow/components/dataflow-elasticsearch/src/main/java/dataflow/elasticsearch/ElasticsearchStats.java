/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchStats.java
 */
package dataflow.elasticsearch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElasticsearchStats {

    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchStats.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static final Map<String, Double> METRIC_VALUES = new HashMap<>();

    public static void updateIndexStats(
            String metricNamePrefix,
            RestHighLevelClient restHighLevelClient,
            MeterRegistry meterRegistry) {
        try {
            Response response =
                    restHighLevelClient
                            .getLowLevelClient()
                            .performRequest(
                                    new Request("GET", "/_cat/indices?v&bytes=gb&format=json"));

            List<IndexDetails> list;
            if (response != null) {
                String rawBody = EntityUtils.toString(response.getEntity());
                TypeReference<List<IndexDetails>> typeRef =
                        new TypeReference<List<IndexDetails>>() {};
                list = objectMapper.readValue(rawBody, typeRef);

                for (IndexDetails indexDetail : list) {
                    String indexName = indexDetail.getIndex();
                    List<Tag> tags = new ArrayList<>();
                    tags.add(Tag.of("indexName", indexName));

                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.index.size",
                            tags,
                            strValueToDouble(indexDetail.getSize()));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.index.primaryShardSize",
                            tags,
                            strValueToDouble(indexDetail.getPrimaryShardSize()));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.index.docCount",
                            tags,
                            strValueToDouble(indexDetail.getDocsCount()));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.index.deletedDocCount",
                            tags,
                            strValueToDouble(indexDetail.getDocsDeleted()));
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public static void updateDiskStats(
            String metricNamePrefix,
            RestHighLevelClient restHighLevelClient,
            MeterRegistry meterRegistry) {
        try {
            Response response =
                    restHighLevelClient
                            .getLowLevelClient()
                            .performRequest(
                                    new Request("GET", "/_cat/allocation?v&format=json&bytes=gb"));

            List<Map<String, String>> list;
            if (response != null) {
                String rawBody = EntityUtils.toString(response.getEntity());

                TypeReference<List<Map<String, String>>> typeRef = new TypeReference<>() {};
                list = objectMapper.readValue(rawBody, typeRef);

                for (Map<String, String> indiciesMap : list) {
                    String node = indiciesMap.get("node");
                    List<Tag> tags = new ArrayList<>();
                    tags.add(Tag.of("node", node));

                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.node.shards",
                            tags,
                            strValueToDouble(indiciesMap.get("shards")));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.node.diskIndices",
                            tags,
                            strValueToDouble(indiciesMap.get("disk.indices")));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.node.diskUsed",
                            tags,
                            strValueToDouble(indiciesMap.get("disk.used")));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.node.diskAvail",
                            tags,
                            strValueToDouble(indiciesMap.get("disk.avail")));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.node.diskTotal",
                            tags,
                            strValueToDouble(indiciesMap.get("disk.total")));
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.node.diskPercent",
                            tags,
                            strValueToDouble(indiciesMap.get("disk.percent")));
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static Double strValueToDouble(String strValue) {
        if (strValue == null) {
            return 0.0d;
        }
        try {
            return Double.valueOf(strValue);
        } catch (NumberFormatException ex) {
            logger.warn("Unexpected double value " + strValue);
            return -1.0;
        }
    }

    public static void updateIndexMetadataStats(
            String metricNamePrefix,
            IndexManager indexManager,
            RestHighLevelClient restHighLevelClient,
            MeterRegistry meterRegistry)
            throws IOException, IndexerException {
        List<IndexMetadata> indexMetadataList = indexManager.getIndexMetadata();
        Set<String> types =
                indexMetadataList.stream().map(IndexMetadata::getType).collect(Collectors.toSet());
        for (String type : types) {
            List<Tag> tags = new ArrayList<>();
            tags.add(Tag.of("clusterId", indexManager.getClusterId()));
            tags.add(Tag.of("type", type));
            IndexMetadata activeIndexMetadata =
                    indexManager.getActiveIndexMetadata(type).orElse(null);

            if (activeIndexMetadata != null) {
                if (activeIndexMetadata.getIndexUpdatedThroughDateTime() != null) {
                    double timeSinceUpdateMillis =
                            System.currentTimeMillis()
                                    - activeIndexMetadata
                                            .getIndexUpdatedThroughDateTime()
                                            .getTime();
                    gauge(
                            meterRegistry,
                            metricNamePrefix + ".elasticsearch.index.timeSinceUpdateMillis",
                            tags,
                            timeSinceUpdateMillis);
                }
                double timeSinceCreatedMillis =
                        System.currentTimeMillis()
                                - activeIndexMetadata.getIndexCreateDateTime().getTime();
                gauge(
                        meterRegistry,
                        metricNamePrefix + ".elasticsearch.index.timeSinceCreationMillis",
                        tags,
                        timeSinceCreatedMillis);

                updateDocCountMetric(
                        metricNamePrefix,
                        type,
                        activeIndexMetadata.getName(),
                        indexManager,
                        restHighLevelClient,
                        meterRegistry);
            }

            IndexMetadata inProgressIndexMetadata =
                    indexMetadataList.stream()
                            .filter(metadata -> metadata.getType().equals(type))
                            .filter(
                                    metadata ->
                                            metadata.getIndexingStatus()
                                                    == IndexingStatusEnum.IN_PROGRESS)
                            .findFirst()
                            .orElse(null);
            if (inProgressIndexMetadata != null) {
                double fullUpdateTimeMillis =
                        System.currentTimeMillis()
                                - inProgressIndexMetadata.getIndexCreateDateTime().getTime();
                gauge(
                        meterRegistry,
                        metricNamePrefix + ".elasticsearch.index.fullUpdateTimeMillis",
                        tags,
                        fullUpdateTimeMillis);
            }

            double numBackupIndexCount =
                    (double)
                            indexMetadataList.stream()
                                    .filter(metadata -> metadata.getType().equals(type))
                                    .filter(
                                            metadata ->
                                                    metadata.getIndexingStatus()
                                                            == IndexingStatusEnum.COMPLETE)
                                    .count();
            numBackupIndexCount = activeIndexMetadata != null ? numBackupIndexCount - 1 : 0;
            gauge(
                    meterRegistry,
                    metricNamePrefix + ".elasticsearch.index.numBackups",
                    tags,
                    numBackupIndexCount);

            double failedIndexCount =
                    (double)
                            indexMetadataList.stream()
                                    .filter(metadata -> metadata.getType().equals(type))
                                    .filter(
                                            metadata ->
                                                    metadata.getIndexingStatus()
                                                            == IndexingStatusEnum.FAILED)
                                    .count();
            gauge(
                    meterRegistry,
                    metricNamePrefix + ".elasticsearch.index.failedIndexCount",
                    tags,
                    failedIndexCount);
        }
    }

    private static void updateDocCountMetric(
            String metricNamePrefix,
            String type,
            String indexName,
            IndexManager indexManager,
            RestHighLevelClient restHighLevelClient,
            MeterRegistry meterRegistry)
            throws IOException {
        Response response =
                restHighLevelClient
                        .getLowLevelClient()
                        .performRequest(new Request("GET", "/" + indexName + "/_count"));
        String rawBody = EntityUtils.toString(response.getEntity());
        CountResponse statsResponse = objectMapper.readValue(rawBody, CountResponse.class);

        List<Tag> tags = new ArrayList<>();
        tags.add(Tag.of("clusterId", indexManager.getClusterId()));
        tags.add(Tag.of("type", type));
        gauge(
                meterRegistry,
                metricNamePrefix + ".elasticsearch.index.activeIndexDocCount",
                tags,
                (double) statsResponse.getCount());
    }

    public static void updateNodeStats(
            String metricNamePrefix,
            RestHighLevelClient restHighLevelClient,
            IndexManager indexManager,
            MeterRegistry meterRegistry) {
        try {
            Response response =
                    restHighLevelClient
                            .getLowLevelClient()
                            .performRequest(new Request("GET", "/_nodes/stats/process"));
            String rawBody = EntityUtils.toString(response.getEntity());
            StatsResponse statsResponse = objectMapper.readValue(rawBody, StatsResponse.class);

            for (Map.Entry<String, Node> entry : statsResponse.nodeStats.entrySet()) {
                String node = entry.getKey();
                List<Tag> tags = new ArrayList<>();
                tags.add(Tag.of("clusterId", indexManager.getClusterId()));
                tags.add(Tag.of("node", node));
                gauge(
                        meterRegistry,
                        metricNamePrefix + ".elasticsearch.node.cpu",
                        tags,
                        (double) entry.getValue().getProcess().getCpu().percent);
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void gauge(
            MeterRegistry meterRegistry, String metricKey, List<Tag> tags, Double value) {
        String key =
                metricKey
                        + tags.stream()
                                .map(t -> "{" + t.getKey() + "=" + t.getValue() + "}")
                                .collect(Collectors.joining());
        METRIC_VALUES.put(key, value);
        meterRegistry.gauge(metricKey, tags, METRIC_VALUES, v -> v.get(key));
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CountResponse {

        private long count;

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class IndexDetails {

        private String health;
        private String status;
        private String index;
        private String uuid;
        private String pri;
        private String rep;

        @JsonProperty("docs.count")
        private String docsCount;

        @JsonProperty("docs.deleted")
        private String docsDeleted;

        @JsonProperty("store.size")
        private String size;

        @JsonProperty("pri.store.size")
        private String primaryShardSize;

        public IndexDetails() {}

        public String getHealth() {
            return health;
        }

        public void setHealth(String health) {
            this.health = health;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public String getPri() {
            return pri;
        }

        public void setPri(String pri) {
            this.pri = pri;
        }

        public String getRep() {
            return rep;
        }

        public void setRep(String rep) {
            this.rep = rep;
        }

        public String getDocsCount() {
            return docsCount;
        }

        public void setDocsCount(String docsCount) {
            this.docsCount = docsCount;
        }

        public String getDocsDeleted() {
            return docsDeleted;
        }

        public void setDocsDeleted(String docsDeleted) {
            this.docsDeleted = docsDeleted;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getPrimaryShardSize() {
            return primaryShardSize;
        }

        public void setPrimaryShardSize(String primaryShardSize) {
            this.primaryShardSize = primaryShardSize;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CPU {

        @JsonProperty("percent")
        int percent;

        public int getPercent() {
            return percent;
        }

        public void setPercent(int percent) {
            this.percent = percent;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Memory {

        @JsonProperty("total_virtual_in_bytes")
        private long totalVirtualInBytes;

        public long getTotalVirtualInBytes() {
            return totalVirtualInBytes;
        }

        public void setTotalVirtualInBytes(long totalVirtualInBytes) {
            this.totalVirtualInBytes = totalVirtualInBytes;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Process {

        @JsonProperty("open_file_descriptors")
        private int openFileDescriptors;

        @JsonProperty("max_file_descriptors")
        private int maxFileDescriptors;

        @JsonProperty("cpu")
        private CPU cpu;

        @JsonProperty("mem")
        private Memory memory;

        public int getOpenFileDescriptors() {
            return openFileDescriptors;
        }

        public void setOpenFileDescriptors(int openFileDescriptors) {
            this.openFileDescriptors = openFileDescriptors;
        }

        public int getMaxFileDescriptors() {
            return maxFileDescriptors;
        }

        public void setMaxFileDescriptors(int maxFileDescriptors) {
            this.maxFileDescriptors = maxFileDescriptors;
        }

        public CPU getCpu() {
            return cpu;
        }

        public void setCpu(CPU cpu) {
            this.cpu = cpu;
        }

        public Memory getMemory() {
            return memory;
        }

        public void setMemory(Memory memory) {
            this.memory = memory;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Node {

        @JsonProperty("process")
        private Process process;

        public Process getProcess() {
            return process;
        }

        public void setProcess(Process process) {
            this.process = process;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class StatsResponse {

        @JsonProperty("cluster_name")
        private String clusterName;

        @JsonProperty("nodes")
        private Map<String, Node> nodeStats;

        public String getClusterName() {
            return clusterName;
        }

        public void setClusterName(String clusterName) {
            this.clusterName = clusterName;
        }

        public Map<String, Node> getNodeStats() {
            return nodeStats;
        }

        public void setNodeStats(Map<String, Node> nodeStats) {
            this.nodeStats = nodeStats;
        }
    }
}
