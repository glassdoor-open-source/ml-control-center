/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchDeleteDocs.java
 */
package dataflow.elasticsearch;

import static dataflow.core.util.MetricsUtil.createCounter;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowMetric;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.MetricType;
import dataflow.core.component.annotation.OutputValue;
import dataflow.elasticsearch.ElasticsearchDeleteDocsMetadata.Metrics;
import io.micrometer.core.instrument.Counter;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@DataFlowComponent(
        metrics = {
            @DataFlowMetric(
                    name = "deletedDocCount",
                    description = "Number of documents deleted",
                    type = MetricType.COUNTER)
        })
public class ElasticsearchDeleteDocs extends AbstractElasticsearchAction {

    private static final int DEFAULT_TIMEOUT_MILLIS = 1000;

    private final boolean waitForIndexingToFinish;
    private final int timeoutMillis;
    private final int retryCount;
    private final int retryIntervalMillis;
    private Optional<Counter> deletedDocCounter;

    @DataFlowConfigurable
    public ElasticsearchDeleteDocs(
            @DataFlowConfigProperty String clusterId,
            @DataFlowConfigProperty String type,
            @DataFlowConfigProperty String name,
            @DataFlowConfigProperty(required = false) String version,
            @DataFlowConfigProperty(required = false) Boolean waitForIndexingToFinish,
            @DataFlowConfigProperty(required = false) Integer timeoutMillis,
            @DataFlowConfigProperty(required = false) Integer retryCount,
            @DataFlowConfigProperty(required = false) Integer retryIntervalMillis,
            @DataFlowConfigProperty(required = false) String metricNamePrefix,
            @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        super(clusterId, type, name, version, metricNamePrefix, metricTags);

        this.waitForIndexingToFinish =
                waitForIndexingToFinish != null ? waitForIndexingToFinish : false;
        this.timeoutMillis = timeoutMillis != null ? timeoutMillis : DEFAULT_TIMEOUT_MILLIS;
        this.retryCount = retryCount != null ? retryCount : 1;
        this.retryIntervalMillis =
                retryIntervalMillis != null ? retryIntervalMillis : this.timeoutMillis;
    }

    @OutputValue
    public CompletableFuture<Set<String>> getValue(@InputValue Set<String> docIds)
            throws IOException, IndexerException {
        IndexMetadata metadata = getIndexMetadata();
        long startTime = System.currentTimeMillis();
        CompletableFuture<Set<String>> deleteFuture =
                indexManager.deleteDocuments(
                        docIds,
                        metadata,
                        waitForIndexingToFinish,
                        timeoutMillis,
                        retryCount,
                        retryIntervalMillis,
                        (retries) -> incrementRetryCounter());
        incrementRequestCounter();
        return deleteFuture.whenComplete(
                (deletedIds, ex) -> {
                    long runTimeMillis = System.currentTimeMillis() - startTime;
                    recordExecutionTime(runTimeMillis);
                    if (ex != null) {
                        recordError(ex);
                    } else {
                        incrementDeletedDocCounter(deletedIds.size());
                    }
                });
    }

    private void incrementDeletedDocCounter(int count) {
        if (deletedDocCounter == null) {
            initMetrics();
        }
        deletedDocCounter.ifPresent(counter -> counter.increment(count));
    }

    @Override
    protected void initMetrics() {
        super.initMetrics();
        deletedDocCounter =
                createCounter(Metrics.deletedDocCount, this, metricNamePrefix, getMetricTags());
    }
}
