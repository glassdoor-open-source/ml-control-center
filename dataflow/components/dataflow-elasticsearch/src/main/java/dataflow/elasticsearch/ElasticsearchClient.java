/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ElasticsearchClient.java
 */
package dataflow.elasticsearch;

import java.io.IOException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.cluster.storedscripts.GetStoredScriptRequest;
import org.elasticsearch.action.admin.cluster.storedscripts.GetStoredScriptResponse;
import org.elasticsearch.action.admin.cluster.storedscripts.PutStoredScriptRequest;
import org.elasticsearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeRequest;
import org.elasticsearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.elasticsearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.GetAliasesResponse;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;

public interface ElasticsearchClient {

    /*
     * Get the endpoint of this rest client
     */
    String getEndpoint();

    /*
     * Document requests
     */
    DeleteResponse delete(DeleteRequest req) throws IOException;

    SearchResponse search(SearchRequest req) throws IOException;

    SearchResponse search(SearchRequest req, RequestOptions requestOptions) throws IOException;

    MultiSearchResponse msearch(MultiSearchRequest req, RequestOptions requestOptions)
            throws IOException;

    GetResponse get(GetRequest req, RequestOptions requestOptions) throws IOException;

    void indexAsync(IndexRequest req, ActionListener listener);

    IndexResponse indexSync(IndexRequest req) throws IOException;

    void bulkAsync(BulkRequest req, ActionListener<BulkResponse> listener);

    BulkResponse bulk(BulkRequest req, RequestOptions options) throws IOException;

    /*
     * indices() requests - buried a level down under indices() in the real elastic high level client
     */
    AcknowledgedResponse indices_delete(DeleteIndexRequest req) throws IOException;

    boolean indices_exists(GetIndexRequest req) throws IOException;

    AcknowledgedResponse indices_updateAliases(IndicesAliasesRequest req) throws IOException;

    GetAliasesResponse indices_getAlias(GetAliasesRequest req) throws IOException;

    CreateIndexResponse indices_create(CreateIndexRequest req) throws IOException;

    void indices_forcemergeAsync(
            ForceMergeRequest req, ActionListener<ForceMergeResponse> listener);

    ClusterHealthResponse cluster_health() throws IOException;

    void indices_updateSettings(UpdateSettingsRequest request, RequestOptions requestOptions)
            throws IOException;

    /*
     * Access to low level client
     */
    Response lowLevelClientCall(Request req) throws IOException;

    /*
     * Script functions
     */
    AcknowledgedResponse putScript(PutStoredScriptRequest req) throws IOException;

    GetStoredScriptResponse getScript(GetStoredScriptRequest req) throws IOException;
}
