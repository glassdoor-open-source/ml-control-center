

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowCore)
    api(libs.opensearchRestHighLevelClient)
    api(libs.reactiveStreams)
    api(libs.jacksonAnnotations)
    api(libs.jacksonDatabind)
    api(libs.micrometerCore)
    api(libs.slf4jApi)
    testImplementation(libs.guava)
    testImplementation(libs.reactorCore)
    testImplementation(libs.mockitoCore)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

group = "dataflow"
description = "dataflow-opensearch"
