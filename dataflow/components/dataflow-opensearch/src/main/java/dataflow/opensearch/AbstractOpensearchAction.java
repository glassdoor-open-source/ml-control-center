/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractOpensearchAction.java
 */
package dataflow.opensearch;

import static dataflow.core.util.MetricsUtil.getMetricName;
import static dataflow.core.util.MetricsUtil.tagsToStringArray;

import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.exception.DataFlowConfigurationException;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class AbstractOpensearchAction {

    protected final String indexName;
    protected final String indexType;
    protected final String indexVersion;
    protected final IndexManager indexManager;
    protected final IndexClusterManager clusterManager;
    protected final MeterRegistry meterRegistry;
    protected final String metricNamePrefix;
    protected final Map<String, String> additionalTags;
    private Counter requestCounter;
    private Timer requestTimer;
    private Counter errorCounter;
    private Counter retryCounter;
    private IndexMetadata indexMetadata;

    public AbstractOpensearchAction(
            String clusterId,
            String indexType,
            String indexName,
            String indexVersion,
            String metricNamePrefix,
            Map<String, String> additionalTags) {
        DataFlowExecutionContext executionContext =
                DataFlowExecutionContext.getCurrentExecutionContext();
        this.clusterManager =
                executionContext.getDependencyInjector().getInstance(IndexClusterManager.class);
        if (clusterManager == null) {
            throw new DataFlowConfigurationException(
                    "Opensearch cluster manager is not configured");
        }
        this.indexManager = clusterManager.getIndexManager(clusterId);
        if (this.indexManager == null) {
            throw new DataFlowConfigurationException(
                    "Opensearch cluster " + clusterId + " does not exist");
        }
        this.indexType = indexType;
        this.indexName = indexName;
        this.indexVersion = indexVersion;

        this.meterRegistry =
                executionContext.getDependencyInjector().getInstance(MeterRegistry.class);
        this.metricNamePrefix =
                metricNamePrefix != null ? metricNamePrefix : getClass().getSimpleName();
        this.additionalTags = additionalTags != null ? additionalTags : Collections.emptyMap();
    }

    protected void incrementRequestCounter() {
        if (requestCounter == null) {
            initMetrics();
        }
        if (requestCounter != null) {
            requestCounter.increment();
        }
    }

    protected void recordError(Throwable t) {
        if (errorCounter == null) {
            initMetrics();
        }
        if (errorCounter != null) {
            errorCounter.increment();
        }
    }

    protected void incrementRetryCounter() {
        if (retryCounter == null) {
            initMetrics();
        }
        if (retryCounter != null) {
            retryCounter.increment();
        }
    }

    protected void recordExecutionTime(long timeMillis) {
        if (requestTimer == null) {
            initMetrics();
        }
        if (requestTimer != null) {
            requestTimer.record(timeMillis, TimeUnit.MILLISECONDS);
        }
    }

    protected void initMetrics() {
        if (meterRegistry == null) {
            return;
        }
        DataFlowInstance instance =
                DataFlowExecutionContext.getCurrentExecutionContext().getInstance();
        Map<String, String> tags = getMetricTags();
        requestCounter =
                meterRegistry.counter(
                        getMetricName(metricNamePrefix, "requestCounter", instance),
                        tagsToStringArray(tags));
        requestTimer =
                meterRegistry.timer(
                        getMetricName(metricNamePrefix, "requestTimer", instance),
                        tagsToStringArray(tags));
        errorCounter =
                meterRegistry.counter(
                        getMetricName(metricNamePrefix, "errorCounter", instance),
                        tagsToStringArray(tags));
        retryCounter =
                meterRegistry.counter(
                        getMetricName(metricNamePrefix, "retryCounter", instance),
                        tagsToStringArray(tags));
    }

    protected Map<String, String> getMetricTags() {
        Map<String, String> tags =
                new HashMap<>(
                        DataFlowExecutionContext.getCurrentExecutionContext()
                                .getInstance()
                                .getMetricTagsForComponent(this));

        tags.put("clusterId", indexManager.getClusterId());
        try {
            IndexMetadata metadata = getIndexMetadata();
            tags.put("indexType", metadata.getType());
            tags.put("indexName", metadata.getName());
            tags.put("indexVersion", metadata.getVersion());
        } catch (IOException | IndexerException e) {
            tags.put("indexType", indexType);
            tags.put("indexName", indexName);
            if (indexVersion != null) {
                tags.put("indexVersion", indexVersion);
            }
        }
        tags.putAll(this.additionalTags);
        return tags;
    }

    protected IndexMetadata getIndexMetadata() throws IOException, IndexerException {
        if (indexMetadata != null) {
            return indexMetadata;
        }
        if (indexName == null || indexVersion == null) {
            indexMetadata = indexManager.getActiveIndexMetadata(indexType).orElse(null);
        } else {
            indexMetadata =
                    indexManager.getIndexMetadata(indexType, indexName, indexVersion).orElse(null);
        }
        if (indexMetadata == null) {
            throw new IllegalStateException(
                    String.format(
                            "No metadata for %s index %s:%s", indexType, indexName, indexVersion));
        }
        return indexMetadata;
    }
}
