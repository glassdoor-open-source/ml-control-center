/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RESTHighLevelOpensearchClient.java
 */
package dataflow.opensearch;

import java.io.IOException;
import java.util.List;
import org.opensearch.action.ActionListener;
import org.opensearch.action.admin.cluster.health.ClusterHealthRequest;
import org.opensearch.action.admin.cluster.health.ClusterHealthResponse;
import org.opensearch.action.admin.cluster.storedscripts.GetStoredScriptRequest;
import org.opensearch.action.admin.cluster.storedscripts.GetStoredScriptResponse;
import org.opensearch.action.admin.cluster.storedscripts.PutStoredScriptRequest;
import org.opensearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.opensearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.opensearch.action.admin.indices.delete.DeleteIndexRequest;
import org.opensearch.action.admin.indices.forcemerge.ForceMergeRequest;
import org.opensearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.opensearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.opensearch.action.bulk.BulkRequest;
import org.opensearch.action.bulk.BulkResponse;
import org.opensearch.action.delete.DeleteRequest;
import org.opensearch.action.delete.DeleteResponse;
import org.opensearch.action.get.GetRequest;
import org.opensearch.action.get.GetResponse;
import org.opensearch.action.index.IndexRequest;
import org.opensearch.action.index.IndexResponse;
import org.opensearch.action.search.MultiSearchRequest;
import org.opensearch.action.search.MultiSearchResponse;
import org.opensearch.action.search.SearchRequest;
import org.opensearch.action.search.SearchResponse;
import org.opensearch.action.support.master.AcknowledgedResponse;
import org.opensearch.client.GetAliasesResponse;
import org.opensearch.client.Node;
import org.opensearch.client.Request;
import org.opensearch.client.RequestOptions;
import org.opensearch.client.Response;
import org.opensearch.client.RestHighLevelClient;
import org.opensearch.client.indices.CreateIndexRequest;
import org.opensearch.client.indices.CreateIndexResponse;
import org.opensearch.client.indices.GetIndexRequest;

public class RESTHighLevelOpensearchClient implements OpensearchClient {

    protected final RestHighLevelClient restHighLevelClient;

    public RESTHighLevelOpensearchClient(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }

    public RestHighLevelClient getRestHighLevelClient() {
        return restHighLevelClient;
    }

    @Override
    public String getEndpoint() {
        List<Node> n = restHighLevelClient.getLowLevelClient().getNodes();
        if (n == null || n.size() == 0) {
            return "UNDEFINED";
        }
        return n.get(0).getHost().getHostName();
    }

    @Override
    public void indices_forcemergeAsync(
            ForceMergeRequest req, ActionListener<ForceMergeResponse> listener) {
        restHighLevelClient.indices().forcemergeAsync(req, RequestOptions.DEFAULT, listener);
    }

    @Override
    public ClusterHealthResponse cluster_health() throws IOException {
        return restHighLevelClient
                .cluster()
                .health(new ClusterHealthRequest(), RequestOptions.DEFAULT);
    }

    public void indices_updateSettings(UpdateSettingsRequest request, RequestOptions requestOptions)
            throws IOException {
        restHighLevelClient.indices().putSettings(request, requestOptions);
    }

    @Override
    public void indexAsync(IndexRequest req, ActionListener listener) {
        restHighLevelClient.indexAsync(req, RequestOptions.DEFAULT, listener);
    }

    @Override
    public IndexResponse indexSync(IndexRequest req) throws IOException {
        return restHighLevelClient.index(req, RequestOptions.DEFAULT);
    }

    @Override
    public void bulkAsync(BulkRequest req, ActionListener<BulkResponse> listener) {
        restHighLevelClient.bulkAsync(req, RequestOptions.DEFAULT, listener);
    }

    @Override
    public BulkResponse bulk(BulkRequest req, RequestOptions options) throws IOException {
        return restHighLevelClient.bulk(req, options);
    }

    @Override
    public AcknowledgedResponse indices_updateAliases(IndicesAliasesRequest req)
            throws IOException {
        return restHighLevelClient.indices().updateAliases(req, RequestOptions.DEFAULT);
    }

    @Override
    public GetAliasesResponse indices_getAlias(GetAliasesRequest req) throws IOException {
        return restHighLevelClient.indices().getAlias(req, RequestOptions.DEFAULT);
    }

    @Override
    public Response lowLevelClientCall(Request req) throws IOException {
        return restHighLevelClient.getLowLevelClient().performRequest(req);
    }

    @Override
    public SearchResponse search(SearchRequest req) throws IOException {
        return restHighLevelClient.search(req, RequestOptions.DEFAULT);
    }

    @Override
    public SearchResponse search(SearchRequest req, RequestOptions requestOptions)
            throws IOException {
        return restHighLevelClient.search(req, requestOptions);
    }

    @Override
    public MultiSearchResponse msearch(MultiSearchRequest req, RequestOptions requestOptions)
            throws IOException {
        return restHighLevelClient.msearch(req, requestOptions);
    }

    @Override
    public GetResponse get(GetRequest req, RequestOptions requestOptions) throws IOException {
        return restHighLevelClient.get(req, requestOptions);
    }

    @Override
    public DeleteResponse delete(DeleteRequest req) throws IOException {
        return restHighLevelClient.delete(req, RequestOptions.DEFAULT);
    }

    @Override
    public AcknowledgedResponse indices_delete(DeleteIndexRequest req) throws IOException {
        return restHighLevelClient.indices().delete(req, RequestOptions.DEFAULT);
    }

    @Override
    public boolean indices_exists(GetIndexRequest req) throws IOException {
        return restHighLevelClient.indices().exists(req, RequestOptions.DEFAULT);
    }

    @Override
    public CreateIndexResponse indices_create(CreateIndexRequest req) throws IOException {
        return restHighLevelClient.indices().create(req, RequestOptions.DEFAULT);
    }

    @Override
    public AcknowledgedResponse putScript(PutStoredScriptRequest req) throws IOException {
        return restHighLevelClient.putScript(req, RequestOptions.DEFAULT);
    }

    @Override
    public GetStoredScriptResponse getScript(GetStoredScriptRequest req) throws IOException {
        return restHighLevelClient.getScript(req, RequestOptions.DEFAULT);
    }
}
