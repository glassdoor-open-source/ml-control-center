/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IndexManager.java
 */
package dataflow.opensearch;

import static java.lang.Thread.sleep;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dataflow.core.engine.DataFlowExecutionContext;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.opensearch.OpenSearchStatusException;
import org.opensearch.action.ActionListener;
import org.opensearch.action.DocWriteRequest;
import org.opensearch.action.DocWriteResponse;
import org.opensearch.action.admin.indices.alias.IndicesAliasesRequest;
import org.opensearch.action.admin.indices.alias.IndicesAliasesRequest.AliasActions;
import org.opensearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.opensearch.action.admin.indices.delete.DeleteIndexRequest;
import org.opensearch.action.admin.indices.forcemerge.ForceMergeRequest;
import org.opensearch.action.admin.indices.forcemerge.ForceMergeResponse;
import org.opensearch.action.admin.indices.settings.put.UpdateSettingsRequest;
import org.opensearch.action.bulk.BulkItemResponse;
import org.opensearch.action.bulk.BulkRequest;
import org.opensearch.action.bulk.BulkResponse;
import org.opensearch.action.delete.DeleteRequest;
import org.opensearch.action.delete.DeleteResponse;
import org.opensearch.action.index.IndexRequest;
import org.opensearch.action.index.IndexResponse;
import org.opensearch.action.search.SearchRequest;
import org.opensearch.action.search.SearchResponse;
import org.opensearch.action.support.WriteRequest;
import org.opensearch.action.support.master.AcknowledgedResponse;
import org.opensearch.client.GetAliasesResponse;
import org.opensearch.client.RequestOptions;
import org.opensearch.client.indices.CreateIndexRequest;
import org.opensearch.client.indices.CreateIndexResponse;
import org.opensearch.client.indices.GetIndexRequest;
import org.opensearch.cluster.health.ClusterHealthStatus;
import org.opensearch.common.settings.Settings;
import org.opensearch.common.unit.TimeValue;
import org.opensearch.common.xcontent.XContentType;
import org.opensearch.index.query.QueryBuilders;
import org.opensearch.rest.RestStatus;
import org.opensearch.search.SearchHit;
import org.opensearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexManager implements AutoCloseable {

    static final String DOC_TYPE = "_doc";
    private static final Logger logger = LoggerFactory.getLogger(IndexManager.class);

    private final String clusterId;
    private final OpensearchClient opensearchClient;
    private final String metadataIndexName = "indexmetadata";
    private final ObjectMapper jsonMapper = new ObjectMapper();
    private final Map<String, String> typeActiveAliasNameMap;
    private final ObjectMapper objectMapper;
    private final int defaultNumPriorSchemaVersionsToKeep;
    private final int defaultNumActiveBackupsForCurrentVersion;
    private final int defaultMinIndexAgeForCleanupHours;

    public IndexManager(
            String clusterId,
            Map<String, String> typeActiveAliasNameMap,
            ObjectMapper objectMapper,
            OpensearchClient opensearchClient,
            int defaultNumPriorSchemaVersionsToKeep,
            int defaultNumActiveBackupsForCurrentVersion,
            int defaultMinIndexAgeForCleanupHours) {
        this.clusterId = clusterId;
        this.typeActiveAliasNameMap =
                typeActiveAliasNameMap != null ? typeActiveAliasNameMap : new HashMap<>();
        this.objectMapper = objectMapper;
        this.opensearchClient = opensearchClient;
        this.defaultNumPriorSchemaVersionsToKeep = defaultNumPriorSchemaVersionsToKeep;
        this.defaultNumActiveBackupsForCurrentVersion = defaultNumActiveBackupsForCurrentVersion;
        this.defaultMinIndexAgeForCleanupHours = defaultMinIndexAgeForCleanupHours;

        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public String getClusterId() {
        return clusterId;
    }

    public IndexMetadata createIndex(
            String type,
            String name,
            String version,
            String schemaDefinitionJson,
            int schemaVersion)
            throws IOException, IndexerException {
        // Opensearch requires lowercase index names.
        name = name.toLowerCase();
        IndexMetadata metadata = new IndexMetadata();
        metadata.setIndexCreateDateTime(new Date());
        metadata.setType(type);
        metadata.setName(name);
        metadata.setVersion(version);
        metadata.setSchemaVersion(schemaVersion);
        metadata.setIndexingStatus(IndexingStatusEnum.IN_PROGRESS);
        metadata.setSwitchPercent(0);

        // does index exist?
        GetIndexRequest existsReq = new GetIndexRequest(name);

        if (!opensearchClient.indices_exists(existsReq)) {
            logger.info(
                    "{} index {}:{} does not exist, will create it with appropriate mappings",
                    type,
                    name,
                    version);

            removeIndexMetadata(metadata);

            CreateIndexRequest createRequest = new CreateIndexRequest(name);
            createRequest.source(schemaDefinitionJson, XContentType.JSON);
            CreateIndexResponse response = opensearchClient.indices_create(createRequest);

            if (response == null
                    || !response.isAcknowledged()
                    || !response.isShardsAcknowledged()) {
                throw new IndexerException(
                        String.format(
                                "%s index %s:%s create index failed, "
                                        + "acknowledged: %b, shardsAcknowledged: %b",
                                type,
                                name,
                                version,
                                response != null && response.isAcknowledged(),
                                response != null && response.isShardsAcknowledged()));
            }

            // create a header status doc
            updateMetadata(metadata);

            logger.info("{} index {}:{} successfully created", type, name, version);
        } else {
            throw new IndexerException(
                    String.format(
                            "%s index %s:%s exists, cannot continue, "
                                    + "this should not happen, each run should have its own unique name",
                            type, name, version));
        }
        return metadata;
    }

    /*
     * Returns a list of metadata for all indexes.
     */
    public List<IndexMetadata> getIndexMetadata() throws IOException {
        SearchSourceBuilder getPhysicalIndexesQuery =
                new SearchSourceBuilder()
                        .size(10000)
                        .fetchSource(new String[] {"*"}, null)
                        .query(QueryBuilders.matchAllQuery());

        SearchRequest opensearchRequest = new SearchRequest();
        opensearchRequest.indices(metadataIndexName);
        opensearchRequest.source(getPhysicalIndexesQuery);

        SearchResponse searchResponse = opensearchClient.search(opensearchRequest);

        List<IndexMetadata> metadataList = new ArrayList<>();
        if (searchResponse == null || searchResponse.getHits() == null) {
            return metadataList;
        }
        for (SearchHit hit : searchResponse.getHits().getHits()) {
            try {
                metadataList.add(
                        jsonMapper.readValue(hit.getSourceAsString(), IndexMetadata.class));
            } catch (Throwable t) {
                logger.warn("Error deserializing index metadata: " + hit.getSourceAsString());
            }
        }

        metadataList.sort(Comparator.comparing(IndexMetadata::getIndexCreateDateTime).reversed());
        return metadataList;
    }

    public Optional<IndexMetadata> getIndexMetadata(String type, String name, String version)
            throws IOException {
        return getIndexMetadata().stream()
                .filter(metadata -> metadata.getType().equals(type))
                // Opensearch requires lowercase index names.
                .filter(metadata -> metadata.getName().equals(name.toLowerCase()))
                .filter(metadata -> metadata.getVersion().equals(version))
                .findAny();
    }

    /** Returns the metadata for the active index of the given type. */
    public Optional<IndexMetadata> getActiveIndexMetadata(String type)
            throws IndexerException, IOException {
        List<Integer> schemaVersions =
                getIndexMetadata().stream()
                        .filter(metadata -> metadata.getType().equals(type))
                        .map(IndexMetadata::getSchemaVersion)
                        .distinct()
                        .sorted(Comparator.reverseOrder())
                        .collect(Collectors.toList());

        for (int schemaVersion : schemaVersions) {
            Optional<IndexMetadata> metadata =
                    getIndexMetadataForAlias(getAliasName(type, schemaVersion));
            if (metadata.isPresent()) {
                return metadata;
            }
        }
        return Optional.empty();
    }

    /*
     * If metadata for the index exists, delete it from the index manager index.
     */
    private void removeIndexMetadata(IndexMetadata metadata) throws IndexerException {
        DeleteRequest req = new DeleteRequest(metadataIndexName, metadata.getName());
        try {
            opensearchClient.delete(req);
        } catch (OpenSearchStatusException es) {
            if (es.status() != RestStatus.NOT_FOUND) {
                throw new IndexerException(
                        String.format(
                                "Error removing metadata for %s index %s:%s",
                                metadata.getType(), metadata.getName(), metadata.getVersion()),
                        es);
            }
        } catch (Throwable t) {
            throw new IndexerException(
                    String.format(
                            "Error removing metadata for %s index %s:%s",
                            metadata.getType(), metadata.getName(), metadata.getVersion()),
                    t);
        }
    }

    protected void updateMetadata(IndexMetadata metadata) throws IOException {
        String json = jsonMapper.writeValueAsString(metadata);
        IndexRequest req =
                new IndexRequest(metadataIndexName)
                        .id(metadata.getName() /* <-- the id of the doc is the name of the index */)
                        .source(json, XContentType.JSON)
                        .timeout("5m");

        // Wait for refresh before returning request
        // https://www.elastic.co/guide/en/elasticsearch/reference/7.17/docs-refresh.html
        req.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);

        ActionListener<IndexResponse> listener =
                new ActionListener<IndexResponse>() {
                    @Override
                    public void onResponse(IndexResponse indexResponse) {}

                    @Override
                    public void onFailure(Exception e) {
                        logger.warn(
                                "Error asynchronously updating metadata for {} index {}:{}",
                                metadata.getType(),
                                metadata.getName(),
                                metadata.getVersion(),
                                e);
                    }
                };

        IndexResponse resp = opensearchClient.indexSync(req);
        if (resp.getResult() != DocWriteResponse.Result.CREATED
                && resp.getResult() != DocWriteResponse.Result.UPDATED) {
            logger.warn(
                    "Error updating metadata for {} index {}:{}",
                    metadata.getType(),
                    metadata.getName(),
                    metadata.getVersion());
        }
    }

    public Optional<IndexMetadata> getIndexMetadataForAlias(String alias) throws IndexerException {
        GetAliasesRequest req = new GetAliasesRequest(alias);

        try {
            GetAliasesResponse response = opensearchClient.indices_getAlias(req);

            // alias doesn't exist at all yet
            if (response.status() == RestStatus.NOT_FOUND) {
                return Optional.empty();
            }

            for (String indexName : response.getAliases().keySet()) {
                if (response.getAliases().get(indexName).stream()
                        .anyMatch(aliasMetadata -> aliasMetadata.getAlias().equals(alias))) {
                    Optional<IndexMetadata> metadata =
                            getIndexMetadata().stream()
                                    .filter(md -> md.getName().equals(indexName))
                                    .findAny();
                    if (metadata.isPresent()) {
                        return metadata;
                    }
                }
            }
            return Optional.empty();
        } catch (Throwable t) {
            throw new IndexerException("Error trying to retrieve index aliases", t);
        }
    }

    /*
     * If there is an index with the given alias then removes the alias from that index.
     */
    public boolean removeAlias(String alias) throws IndexerException, IOException {
        Optional<IndexMetadata> metadata = getIndexMetadataForAlias(alias);
        IndicesAliasesRequest request = new IndicesAliasesRequest();

        if (metadata.isPresent()) {
            request.addAliasAction(
                    new IndicesAliasesRequest.AliasActions(AliasActions.Type.REMOVE)
                            .index(metadata.get().getName())
                            .alias(alias));
            AcknowledgedResponse indicesAliasesResponse =
                    opensearchClient.indices_updateAliases(request);
            return indicesAliasesResponse.isAcknowledged();
        }
        return true;
    }

    /*
     * Makes the physical index passed the active index, atomically, for the logical index type
     */
    public boolean makeActiveIndex(IndexMetadata metadata) throws IOException, IndexerException {
        IndicesAliasesRequest request = new IndicesAliasesRequest();

        String aliasName = getAliasName(metadata.getType(), metadata.getSchemaVersion());

        Optional<IndexMetadata> existingActiveIndexMetadata =
                getActiveIndexMetadata(metadata.getType());

        if (existingActiveIndexMetadata.isPresent()) {
            request.addAliasAction(
                    new IndicesAliasesRequest.AliasActions(AliasActions.Type.REMOVE)
                            .index(existingActiveIndexMetadata.get().getName())
                            .alias(aliasName));
        }
        request.addAliasAction(
                new IndicesAliasesRequest.AliasActions(AliasActions.Type.ADD)
                        .index(metadata.getName())
                        .alias(aliasName));

        AcknowledgedResponse indicesAliasesResponse =
                opensearchClient.indices_updateAliases(request);

        return indicesAliasesResponse.isAcknowledged();
    }

    /*
     * Force drops an index and removes it from the index manager list.
     */
    public void removeIndex(IndexMetadata metadata) throws IOException, IndexerException {
        Optional<IndexMetadata> activeIndexMetadata = getActiveIndexMetadata(metadata.getType());
        if (activeIndexMetadata.isPresent()
                && metadata.getName().equals(activeIndexMetadata.get().getName())
                && metadata.getVersion().equals(activeIndexMetadata.get().getVersion())) {
            String msg =
                    String.format("Attempted to remove the active %s index", metadata.getType());
            logger.warn(msg);
            throw new IllegalArgumentException(msg);
        }

        // it's ok to drop
        try {
            deleteIndexManagerDocument(metadata.getName());
        } catch (Throwable t) {
            // swallow
        }

        dropIndex(metadata);
    }

    /** Sets the number of replicas for the specified index. */
    public void setNumberOfReplicas(IndexMetadata metadata, int count) throws IOException {
        UpdateSettingsRequest request =
                new UpdateSettingsRequest(metadata.getName())
                        .settings(
                                Settings.builder().put("index.number_of_replicas", count).build());
        opensearchClient.indices_updateSettings(request, RequestOptions.DEFAULT);
    }

    public void setTransLogAsync(IndexMetadata metadata, boolean async) throws IOException {
        UpdateSettingsRequest request =
                new UpdateSettingsRequest(metadata.getName())
                        .settings(
                                Settings.builder()
                                        .put(
                                                "index.translog.durability",
                                                async ? "async" : "request")
                                        .build());
        opensearchClient.indices_updateSettings(request, RequestOptions.DEFAULT);
    }

    public void switchIndex(
            IndexMetadata metadata,
            int switchDuration,
            TimeUnit switchDurationTimeUnit,
            Supplier<Boolean> canceledFn)
            throws IOException, IndexerException {
        long switchStartTime = System.currentTimeMillis();
        if (switchDuration > 0) {
            logger.info(
                    "Starting gradual switch to {} index {}:{} over {} minutes",
                    metadata.getType(),
                    metadata.getName(),
                    metadata.getVersion(),
                    switchDurationTimeUnit.toMinutes(switchDuration));
            metadata.setIndexingStatus(IndexingStatusEnum.SWITCH_IN_PROGRESS);
            long switchDurationMillis = switchDurationTimeUnit.toMillis(switchDuration);
            long elapsed = 0;
            while (elapsed < switchDurationMillis) {
                elapsed = System.currentTimeMillis() - switchStartTime;
                if (canceledFn.get()) {
                    metadata.setSwitchPercent(0);
                    updateMetadata(metadata);
                    throw new IndexerException("Indexing was canceled");
                }
                metadata.setSwitchPercent(
                        Math.max(1, (int) (100 * (float) elapsed / switchDurationMillis)));
                updateMetadata(metadata);
                logger.info(
                        "{} index {} switch at {} percent",
                        metadata.getType(),
                        metadata.getName(),
                        metadata.getSwitchPercent());
                try {
                    sleep(Math.min(switchDurationMillis / 10, 60 * 1000));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            metadata.setSwitchPercent(100);
            updateMetadata(metadata);
        }
        if (canceledFn.get()) {
            metadata.setSwitchPercent(0);
            updateMetadata(metadata);
            throw new IndexerException("Indexing was canceled");
        }
    }

    public void forceMergeSegments(IndexMetadata metadata, int maxNumSegments) {
        // force merge segments for performance
        if (metadata.getIndexingStatus() == IndexingStatusEnum.COMPLETE) {
            logger.info(
                    "Forcing merge on segments on {} index {}:{}, "
                            + "please be patient, will take several minutes",
                    metadata.getType(),
                    metadata.getName(),
                    metadata.getVersion());
            ForceMergeRequest fmr = new ForceMergeRequest(metadata.getName());
            fmr.maxNumSegments(maxNumSegments);

            Object forceMergeWaiter = new Object();

            ActionListener<ForceMergeResponse> fmListener =
                    new ActionListener<ForceMergeResponse>() {
                        @Override
                        public void onResponse(ForceMergeResponse forceMergeResponse) {
                            logger.info(
                                    "Force Merge successful on {} index {}:{}",
                                    metadata.getType(),
                                    metadata.getName(),
                                    metadata.getVersion());
                            synchronized (forceMergeWaiter) {
                                forceMergeWaiter.notifyAll();
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            if (e instanceof org.apache.http.ConnectionClosedException) {
                                logger.warn(
                                        "Connection was closed while merging {} index {}:{}, "
                                                + "merge will likely be successful and finish up in background",
                                        metadata.getType(),
                                        metadata.getName(),
                                        metadata.getVersion(),
                                        e);
                            } else {
                                logger.warn(
                                        "Merge of {} index {}:{} failed with exception",
                                        metadata.getType(),
                                        metadata.getName(),
                                        metadata.getVersion());
                            }
                            synchronized (forceMergeWaiter) {
                                forceMergeWaiter.notifyAll();
                            }
                        }
                    };

            opensearchClient.indices_forcemergeAsync(fmr, fmListener);

            logger.info("Waiting on force merge, please wait");
            synchronized (forceMergeWaiter) {
                try {
                    forceMergeWaiter.wait();
                } catch (InterruptedException e) {
                }
            }
            logger.info("Force merge finished");
        }
    }

    public CompletableFuture<Map<String, Object>> writeDocuments(
            Map<String, Object> docIdDocMap,
            IndexMetadata metadata,
            boolean waitForIndexingToFinish,
            int timeOutMillis,
            int retryCount,
            int retryIntervalMillis,
            Consumer<Integer> retryCountConsumer) {
        CompletableFuture<Map<String, Object>> futureResult = new CompletableFuture<>();
        if (docIdDocMap.isEmpty()) {
            logger.warn(
                    "docIdDocMap is empty, cannot write empty documents to index for IndexMetadata={}",
                    metadata);
            futureResult.complete(new HashMap<>());
        } else {
            DataFlowExecutionContext.getCurrentExecutionContext()
                    .getEnvironment()
                    .executeAsync(
                            () -> {
                                try {
                                    Map<String, String> docIdJsonMap =
                                            docIdDocMap.entrySet().stream()
                                                    .collect(
                                                            Collectors.toMap(
                                                                    Entry::getKey,
                                                                    entry -> {
                                                                        try {
                                                                            return objectMapper
                                                                                    .writeValueAsString(
                                                                                            entry
                                                                                                    .getValue());
                                                                        } catch (Throwable e) {
                                                                            logger.warn(
                                                                                    "Unable to serialize opensearch document",
                                                                                    e);
                                                                            throw new RuntimeException(
                                                                                    "Unable to serialize document",
                                                                                    e);
                                                                        }
                                                                    }));
                                    List<DocWriteRequest> opensearchRequests = new ArrayList<>();

                                    docIdJsonMap.forEach(
                                            (docId, json) ->
                                                    opensearchRequests.add(
                                                            new IndexRequest(metadata.getName())
                                                                    .id(docId)
                                                                    .source(
                                                                            json,
                                                                            XContentType.JSON)));

                                    BulkRequest bulkRequest = new BulkRequest();
                                    if (waitForIndexingToFinish) {
                                        bulkRequest.setRefreshPolicy(
                                                WriteRequest.RefreshPolicy.WAIT_UNTIL);
                                    }

                                    opensearchRequests.forEach(bulkRequest::add);
                                    bulkRequest.timeout(TimeValue.timeValueMillis(timeOutMillis));

                                    executeAsyncBulkRequest(
                                            bulkRequest,
                                            futureResult,
                                            docIdDocMap,
                                            "write",
                                            retryCount,
                                            retryIntervalMillis,
                                            retryCountConsumer);
                                } catch (Throwable t) {
                                    logger.warn("Error writing opensearch documents", t);
                                    futureResult.completeExceptionally(t);
                                }
                            },
                            IndexManager.class.getSimpleName());
        }
        return futureResult;
    }

    public CompletableFuture<Set<String>> deleteDocuments(
            Set<String> docIds,
            IndexMetadata metadata,
            boolean waitForIndexingToFinish,
            int timeOutMillis,
            int retryCount,
            int retryIntervalMillis,
            Consumer<Integer> retryCountConsumer) {
        CompletableFuture<Set<String>> futureResult = new CompletableFuture<>();
        DataFlowExecutionContext.getCurrentExecutionContext()
                .getEnvironment()
                .executeAsync(
                        () -> {
                            List<DocWriteRequest> opensearchRequests = new ArrayList<>();

                            docIds.forEach(
                                    docId ->
                                            opensearchRequests.add(
                                                    new DeleteRequest(metadata.getName(), docId)));

                            BulkRequest bulkRequest = new BulkRequest();
                            if (waitForIndexingToFinish) {
                                bulkRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL);
                            }

                            opensearchRequests.forEach(bulkRequest::add);
                            bulkRequest.timeout(TimeValue.timeValueMillis(timeOutMillis));

                            executeAsyncBulkRequest(
                                    bulkRequest,
                                    futureResult,
                                    docIds,
                                    "delete",
                                    retryCount,
                                    retryIntervalMillis,
                                    retryCountConsumer);
                        },
                        "IndexManager-DeleteDocs");
        return futureResult;
    }

    public boolean checkClusterHealth() {
        try {
            if (opensearchClient.cluster_health().getStatus() != ClusterHealthStatus.GREEN) {
                logger.warn("Cluster " + opensearchClient.getEndpoint() + " is not healthy");
                return false;
            }
        } catch (IOException e) {
            throw new RuntimeException(
                    "Error performing health check of " + opensearchClient.getEndpoint(), e);
        }
        return true;
    }

    private <T> void executeAsyncBulkRequest(
            BulkRequest bulkRequest,
            CompletableFuture<T> futureResult,
            T returnValueOnComplete,
            String operation,
            int maxRetryCount,
            int retryIntervalMillis,
            Consumer<Integer> retryCountConsumer) {

        ActionListener<BulkResponse> listener =
                new ActionListener<>() {
                    int retries = 0;

                    @Override
                    public void onResponse(BulkResponse bulkResponse) {
                        if (!bulkResponse.hasFailures()) {
                            futureResult.complete(returnValueOnComplete);
                        } else {
                            StringBuilder docErrors = new StringBuilder();
                            Arrays.stream(bulkResponse.getItems())
                                    .filter(BulkItemResponse::isFailed)
                                    .forEach(
                                            resp ->
                                                    docErrors
                                                            .append("Document failure: ")
                                                            .append(resp.getFailureMessage())
                                                            .append("\n"));
                            futureResult.completeExceptionally(
                                    new IndexerException(
                                            "Error performing " + operation + ": " + docErrors));
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        if (e instanceof java.net.ConnectException
                                || e instanceof java.net.SocketTimeoutException) {
                            if (retries < maxRetryCount) {
                                retries++;
                                try {
                                    logger.warn(
                                            "Document "
                                                    + operation
                                                    + " timed out. will retry"
                                                    + " ("
                                                    + retries
                                                    + "/"
                                                    + maxRetryCount
                                                    + ")");
                                    retryCountConsumer.accept(retries);
                                    sleep(retryIntervalMillis);
                                    opensearchClient.bulkAsync(bulkRequest, this);
                                    return;
                                } catch (Throwable ex) {
                                    logger.warn("Error while retrying document write", ex);
                                }
                            }
                        }
                        futureResult.completeExceptionally(e);
                    }
                };
        opensearchClient.bulkAsync(bulkRequest, listener);
    }

    public List<IndexMetadata> cleanupOldIndexes(
            String indexType,
            int numPriorSchemaVersionsToKeep,
            int numActiveBackupsForCurrentVersion,
            int minIndexAgeForCleanupHours)
            throws IndexerException, IOException {
        return cleanupOldIndexes(
                indexType,
                numPriorSchemaVersionsToKeep,
                numActiveBackupsForCurrentVersion,
                minIndexAgeForCleanupHours,
                false);
    }

    /**
     * Cleans up old indexes and returns a list containing the metadata of any indexes that were
     * dropped.
     */
    public List<IndexMetadata> cleanupOldIndexes(
            String indexType,
            int numPriorSchemaVersionsToKeep,
            int numActiveBackupsForCurrentVersion,
            int minIndexAgeForCleanupHours,
            boolean cleanupFailedIndexes)
            throws IndexerException, IOException {
        logger.warn("Beginning cleanup for {} indexes", indexType);

        List<IndexMetadata> indexesRemoved = new ArrayList<>();
        IndexMetadata activeIndexMetadata = getActiveIndexMetadata(indexType).orElse(null);
        if (activeIndexMetadata == null) {
            logger.warn("{} index does not have an active version. skipping cleanup", indexType);
            return indexesRemoved;
        }
        for (int v = activeIndexMetadata.getSchemaVersion(); v > 0; v--) {
            indexesRemoved.addAll(
                    cleanupOneIndexVersion(
                            indexType,
                            v,
                            activeIndexMetadata,
                            numPriorSchemaVersionsToKeep,
                            numActiveBackupsForCurrentVersion,
                            minIndexAgeForCleanupHours,
                            cleanupFailedIndexes));
        }

        logger.warn(
                "{} indexes cleanup finished, {} indexes dropped in cleanup",
                indexType,
                indexesRemoved.size());

        indexesRemoved.forEach(
                removed ->
                        logger.warn(
                                "{} index {}:{} dropped",
                                removed.getType(),
                                removed.getName(),
                                removed.getVersion()));

        return indexesRemoved;
    }

    public boolean physicalIndexExists(IndexMetadata metadata) throws IOException {
        GetIndexRequest request = new GetIndexRequest(metadata.getName());
        return opensearchClient.indices_exists(request);
    }

    public String getAliasName(String type, int schemaVersion) {
        return typeActiveAliasNameMap.getOrDefault(type, type) + "_v" + schemaVersion;
    }

    private List<IndexMetadata> cleanupOneIndexVersion(
            String indexType,
            int schemaVersion,
            IndexMetadata activeIndexMetadata,
            int numPriorSchemaVersionsToKeep,
            int numActiveBackupsForCurrentVersion,
            int minIndexAgeForCleanupHours,
            boolean cleanupFailedIndexes)
            throws IOException, IndexerException {
        logger.warn(
                "Index Manager Cleanup: Starting {} index cleanup for schema version {}",
                indexType,
                schemaVersion);
        int currentSchemaVersion = activeIndexMetadata.getSchemaVersion();
        String alias = getAliasName(indexType, schemaVersion);
        Optional<IndexMetadata> aliasMetadata = getIndexMetadataForAlias(alias);

        if (currentSchemaVersion - schemaVersion > numPriorSchemaVersionsToKeep) {
            logger.warn(
                    "Index Manager Cleanup: {} is more than {} versions old, removing alias {}",
                    schemaVersion,
                    numPriorSchemaVersionsToKeep,
                    alias);
            removeAlias(alias);
        } else {
            if (!aliasMetadata.isPresent() && schemaVersion == currentSchemaVersion) {
                logger.warn(
                        "Index Manager Cleanup: there is no alias {}!  Refusing to clean indexes for schema version {}, manually fix or reindex",
                        alias,
                        schemaVersion);
                return Collections.emptyList();
            }

            if (aliasMetadata.isPresent()
                    && !physicalIndexExists(aliasMetadata.get())
                    && schemaVersion == currentSchemaVersion) {
                logger.warn(
                        "Index Manager Cleanup: existing alias for current version points to an index which does not physically exist!  Refusing to clean indexes, manually fix or reindex");
                return Collections.emptyList();
            }
            logger.warn(
                    "Index Manager Cleanup: existing alias {} points to {}",
                    alias,
                    aliasMetadata.isPresent() ? aliasMetadata.get().getName() : "NOTHING");
        }

        List<IndexMetadata> indices =
                getIndexMetadata().stream()
                        .filter(metadata -> metadata.getType().equals(indexType))
                        .filter(metadata -> metadata.getSchemaVersion() == schemaVersion)
                        .sorted(
                                Comparator.comparing(IndexMetadata::getIndexCreateDateTime)
                                        .reversed())
                        .collect(Collectors.toList());

        List<IndexMetadata> indexesToRemove = new ArrayList<>();

        boolean foundExistingPhysical = false;
        List<IndexMetadata> backups = new ArrayList<>();

        logger.warn(
                "Index Manager Cleanup: Indexes to be inspected, in order of freshest to least fresh:");
        indices.forEach(
                metadata ->
                        logger.info(
                                "  {}:{} {}",
                                metadata.getName(),
                                metadata.getVersion(),
                                metadata.equals(activeIndexMetadata) ? "** ACTIVE" : ""));

        for (IndexMetadata metadata : indices) {
            logger.warn(
                    "Index Manager Cleanup: inspecting {}:{}",
                    metadata.getName(),
                    metadata.getVersion());

            if (currentSchemaVersion - schemaVersion > numPriorSchemaVersionsToKeep) {
                logger.warn(
                        "Index Manager Cleanup: index is more than {} schema versions ago, removing",
                        numPriorSchemaVersionsToKeep);
                indexesToRemove.add(metadata);
            } else if (aliasMetadata.isPresent() && metadata.equals(aliasMetadata.get())) {
                logger.warn(
                        "Index Manager Cleanup: found entry for current active index {}, looking for backups",
                        metadata.getName());
                foundExistingPhysical = true;
            } else if (!foundExistingPhysical
                    && metadata.getIndexingStatus() == IndexingStatusEnum.COMPLETE) {
                logger.warn(
                        "Index Manager Cleanup: {} is complete and newer than the active index",
                        metadata.getName());
            } else if (metadata.getIndexingStatus() != IndexingStatusEnum.COMPLETE) {
                logger.warn(
                        "Index Manager Cleanup: {} cannot be backup because it is not complete",
                        metadata.getName());
                if (cleanupFailedIndexes) {
                    logger.warn("Cleaning up failed index {}", metadata.getName());
                    indexesToRemove.add(metadata);
                }
            } else if (!physicalIndexExists(metadata)) {
                logger.warn(
                        "Index Manager Cleanup: {} cannot be backup because the physical index does not actually exist",
                        metadata.getName());
                if (cleanupFailedIndexes) {
                    logger.warn("Cleaning up failed index {}", metadata.getName());
                    indexesToRemove.add(metadata);
                }
            } else if (backups.size() < numActiveBackupsForCurrentVersion) {
                backups.add(metadata);
                logger.warn(
                        "Index Manager Cleanup: found good backup for current active index - backup {} is {}",
                        backups.size(),
                        metadata.getName());
            } else {
                // this index is neither the main, nor the backup
                logger.warn(
                        "Index Manager Cleanup: {} is neither the current active or a valid backup, it will be removed",
                        metadata.getName());
                indexesToRemove.add(metadata);
            }
        }

        List<IndexMetadata> removedIndexes = new ArrayList<>();
        for (IndexMetadata indexToRemove : indexesToRemove) {
            LocalDateTime indexCreateDateTime =
                    indexToRemove.getIndexCreateDateTime() != null
                            ? indexToRemove
                                    .getIndexCreateDateTime()
                                    .toInstant()
                                    .atZone(ZoneId.systemDefault())
                                    .toLocalDateTime()
                            : null;
            if ((indexCreateDateTime == null
                    || indexCreateDateTime.isAfter(
                            LocalDateTime.now().minusHours(minIndexAgeForCleanupHours)))) {
                logger.warn(
                        "Index Manager Cleanup: Skipping cleanup of {} because it is less than {} hours old",
                        indexToRemove.getName(),
                        minIndexAgeForCleanupHours);
                continue;
            }
            // remove it from index manager, and remove the physical index too
            if (physicalIndexExists(indexToRemove) && !dropIndex(indexToRemove)) {
                logger.warn(
                        "Index Manager Cleanup: could not drop {} even though it exists!!  Will try again later...",
                        indexToRemove.getName());
            } else if (!deleteIndexManagerDocument(indexToRemove.getName())) {
                logger.warn(
                        "Index Manager Cleanup: could not delete the index manager document for {}.... will try again later...",
                        indexToRemove.getName());
            } else {
                removedIndexes.add(indexToRemove);
            }
        }
        return removedIndexes;
    }

    private boolean dropIndex(IndexMetadata metadata) throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest(metadata.getName());
        AcknowledgedResponse deleteIndexResponse = opensearchClient.indices_delete(request);
        return deleteIndexResponse != null && deleteIndexResponse.isAcknowledged();
    }

    private boolean deleteIndexManagerDocument(String name) throws IOException {
        DeleteRequest request = new DeleteRequest(metadataIndexName, name);
        DeleteResponse deleteResponse = opensearchClient.delete(request);
        return deleteResponse != null
                && deleteResponse.getResult() == DocWriteResponse.Result.DELETED;
    }

    public int getDefaultNumPriorSchemaVersionsToKeep() {
        return defaultNumPriorSchemaVersionsToKeep;
    }

    public int getDefaultNumActiveBackupsForCurrentVersion() {
        return defaultNumActiveBackupsForCurrentVersion;
    }

    public int getDefaultMinIndexAgeForCleanupHours() {
        return defaultMinIndexAgeForCleanupHours;
    }

    @Override
    public void close() throws Exception {}
}
