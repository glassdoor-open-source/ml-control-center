/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IndexMetadata.java
 */
package dataflow.opensearch;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import java.util.Objects;

public class IndexMetadata {

    private static final String dateFormatFromOpensearch = "MMM dd yyyy HH:mm:ss z";

    private String type;
    private String name;
    private String version;
    private int schemaVersion;
    private IndexingStatusEnum indexingStatus;
    private int switchPercent;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = dateFormatFromOpensearch)
    private Date indexCreateDateTime;

    // the datetime line where we are guaranteed to have changes through.  We *might* have some
    // after this but it's not guaranteed.
    // we will have all changes BEFORE this.  The next incremental update should start at this time.
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = dateFormatFromOpensearch)
    private Date indexUpdatedThroughDateTime;

    public static String getDateFormatFromOpensearch() {
        return dateFormatFromOpensearch;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        // Index names must be lower case.
        return name.toLowerCase();
    }

    public void setName(String name) {
        // Index names must be lower case.
        this.name = name.toLowerCase();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(int schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public IndexingStatusEnum getIndexingStatus() {
        return indexingStatus;
    }

    public void setIndexingStatus(IndexingStatusEnum indexingStatus) {
        this.indexingStatus = indexingStatus;
    }

    public int getSwitchPercent() {
        return switchPercent;
    }

    public void setSwitchPercent(int switchPercent) {
        this.switchPercent = switchPercent;
    }

    public Date getIndexCreateDateTime() {
        return indexCreateDateTime;
    }

    public void setIndexCreateDateTime(Date indexCreateDateTime) {
        this.indexCreateDateTime = indexCreateDateTime;
    }

    public Date getIndexUpdatedThroughDateTime() {
        return indexUpdatedThroughDateTime;
    }

    public void setIndexUpdatedThroughDateTime(Date indexUpdatedThroughDateTime) {
        this.indexUpdatedThroughDateTime = indexUpdatedThroughDateTime;
    }

    @Override
    public String toString() {
        return "IndexMetadata{"
                + "type='"
                + type
                + '\''
                + ", name='"
                + name
                + '\''
                + ", version='"
                + version
                + '\''
                + ", schemaVersion="
                + schemaVersion
                + ", indexingStatus="
                + indexingStatus
                + ", switchPercent="
                + switchPercent
                + ", indexCreateDateTime="
                + indexCreateDateTime
                + ", indexUpdatedThroughDateTime="
                + indexUpdatedThroughDateTime
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IndexMetadata metadata = (IndexMetadata) o;
        return getSchemaVersion() == metadata.getSchemaVersion()
                && Objects.equals(getType(), metadata.getType())
                && Objects.equals(getName(), metadata.getName())
                && Objects.equals(getVersion(), metadata.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getName(), getVersion(), getSchemaVersion());
    }
}
