

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowCore)
    api(libs.resilience4jRetry)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

group = "dataflow"
description = "dataflow-retry"
