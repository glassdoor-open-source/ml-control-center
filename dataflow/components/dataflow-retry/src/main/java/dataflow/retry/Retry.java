/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Retry.java
 */
package dataflow.retry;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.exception.DataFlowConfigurationException;
import io.github.resilience4j.core.EventConsumer;
import io.github.resilience4j.core.IntervalFunction;
import io.github.resilience4j.retry.Retry.EventPublisher;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryConfig.Builder;
import io.github.resilience4j.retry.event.*;
import io.vavr.CheckedFunction0;
import io.vavr.control.Either;
import io.vavr.control.Try;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;

/** A retry behavior with either a fixed or randomized interval. */
public class Retry {

    private static class RetryEventPublisher implements EventPublisher {

        private final List<EventConsumer<RetryOnRetryEvent>> onRetryEventConsumers =
                new ArrayList<>();
        private final List<EventConsumer<RetryOnSuccessEvent>> onSuccessEventConsumers =
                new ArrayList<>();
        private final List<EventConsumer<RetryOnErrorEvent>> onErrorEventConsumers =
                new ArrayList<>();
        private final List<EventConsumer<RetryOnIgnoredErrorEvent>> onIgnoredEventConsumers =
                new ArrayList<>();
        private final List<EventConsumer<RetryEvent>> onEventConsumers = new ArrayList<>();

        @Override
        public EventPublisher onRetry(EventConsumer<RetryOnRetryEvent> eventConsumer) {
            onRetryEventConsumers.add(eventConsumer);
            return this;
        }

        @Override
        public EventPublisher onSuccess(EventConsumer<RetryOnSuccessEvent> eventConsumer) {
            onSuccessEventConsumers.add(eventConsumer);
            return this;
        }

        @Override
        public EventPublisher onError(EventConsumer<RetryOnErrorEvent> eventConsumer) {
            onErrorEventConsumers.add(eventConsumer);
            return this;
        }

        @Override
        public EventPublisher onIgnoredError(
                EventConsumer<RetryOnIgnoredErrorEvent> eventConsumer) {
            onIgnoredEventConsumers.add(eventConsumer);
            return this;
        }

        @Override
        public void onEvent(EventConsumer<RetryEvent> onEventConsumer) {
            this.onEventConsumers.add(onEventConsumer);
        }
    }

    private final io.github.resilience4j.retry.Retry retry;
    private final RetryEventPublisher eventPublisher = new RetryEventPublisher();

    public static <T> T executeWithRetry(Retry retry, Callable<T> function) throws Exception {
        return retry != null ? retry.executeCallable(function) : function.call();
    }

    public static Retry clone(Retry retry) {
        if (retry == null) {
            return null;
        }
        return new Retry(retry);
    }

    @DataFlowConfigurable
    public Retry(
            @DataFlowConfigProperty Integer maxAttempts,
            @DataFlowConfigProperty(required = false) Boolean failAfterMaxAttempts,
            @DataFlowConfigProperty(required = false) Integer intervalMillis,
            @DataFlowConfigProperty(required = false) Boolean randomizedInterval,
            @DataFlowConfigProperty(required = false) List<String> retryExceptions,
            @DataFlowConfigProperty(required = false) List<String> ignoreExceptions) {
        RetryConfig.Builder<?> retryBuilder =
                retryBuilder(maxAttempts, failAfterMaxAttempts, retryExceptions, ignoreExceptions);
        if (intervalMillis != null) {
            if (randomizedInterval == null || !randomizedInterval) {
                retryBuilder.intervalFunction(IntervalFunction.of(intervalMillis));
            } else {
                retryBuilder.intervalFunction(IntervalFunction.ofRandomized(intervalMillis));
            }
        }
        this.retry = io.github.resilience4j.retry.Retry.of("DataFlowRetry", retryBuilder.build());
        addListeners();
    }

    public Retry(Retry retry) {
        this(
                io.github.resilience4j.retry.Retry.of(
                        retry.retry.getName(), retry.retry.getRetryConfig()));
        eventPublisher.onRetryEventConsumers.addAll(retry.eventPublisher.onRetryEventConsumers);
        eventPublisher.onSuccessEventConsumers.addAll(retry.eventPublisher.onSuccessEventConsumers);
        eventPublisher.onErrorEventConsumers.addAll(retry.eventPublisher.onErrorEventConsumers);
        eventPublisher.onIgnoredEventConsumers.addAll(retry.eventPublisher.onIgnoredEventConsumers);
        eventPublisher.onEventConsumers.addAll(retry.eventPublisher.onEventConsumers);
    }

    public Retry(io.github.resilience4j.retry.Retry retry) {
        this.retry = retry;
        addListeners();
    }

    private void addListeners() {
        retry.getEventPublisher()
                .onRetry(
                        event ->
                                eventPublisher.onRetryEventConsumers.forEach(
                                        c -> c.consumeEvent(event)));
        retry.getEventPublisher()
                .onSuccess(
                        event ->
                                eventPublisher.onSuccessEventConsumers.forEach(
                                        c -> c.consumeEvent(event)));
        retry.getEventPublisher()
                .onError(
                        event ->
                                eventPublisher.onErrorEventConsumers.forEach(
                                        c -> c.consumeEvent(event)));
        retry.getEventPublisher()
                .onIgnoredError(
                        event ->
                                eventPublisher.onIgnoredEventConsumers.forEach(
                                        c -> c.consumeEvent(event)));
        retry.getEventPublisher()
                .onEvent(
                        event ->
                                eventPublisher.onEventConsumers.forEach(
                                        c -> c.consumeEvent(event)));
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    public <T> T executeCheckedSupplier(CheckedFunction0<T> checkedSupplier) throws Throwable {
        return retry.executeCheckedSupplier(checkedSupplier);
    }

    public <T> T executeSupplier(Supplier<T> supplier) {
        return retry.executeSupplier(supplier);
    }

    public <E extends Exception, T> Either<E, T> executeEitherSupplier(
            Supplier<Either<E, T>> supplier) {
        return retry.executeEitherSupplier(supplier);
    }

    public <T> Try<T> executeTrySupplier(Supplier<Try<T>> supplier) {
        return retry.executeTrySupplier(supplier);
    }

    public <T> T executeCallable(Callable<T> callable) throws Exception {
        return retry.executeCallable(callable);
    }

    public void executeRunnable(Runnable runnable) {
        retry.executeRunnable(runnable);
    }

    public <T> CompletionStage<T> executeCompletionStage(
            ScheduledExecutorService scheduler, Supplier<CompletionStage<T>> supplier) {
        return retry.executeCompletionStage(scheduler, supplier);
    }

    protected static RetryConfig.Builder<?> retryBuilder(
            Integer maxAttempts,
            Boolean failAfterMaxAttempts,
            List<String> retryExceptions,
            List<String> ignoreExceptions) {
        Builder<Object> retryBuilder = RetryConfig.custom();
        if (maxAttempts != null) {
            retryBuilder.maxAttempts(maxAttempts);
            retryBuilder.failAfterMaxAttempts(
                    failAfterMaxAttempts != null ? failAfterMaxAttempts : true);
        }
        if (retryExceptions != null && !retryExceptions.isEmpty()) {
            retryBuilder.retryExceptions(buildExceptionArray(retryExceptions));
        }
        if (ignoreExceptions != null && !ignoreExceptions.isEmpty()) {
            retryBuilder.ignoreExceptions(buildExceptionArray(ignoreExceptions));
        }
        return retryBuilder;
    }

    protected static Class<? extends Throwable>[] buildExceptionArray(
            List<String> exceptionClassNames) {
        Class<? extends Throwable>[] exceptions = new Class[exceptionClassNames.size()];
        for (int idx = 0; idx < exceptions.length; idx++) {
            String exceptionClassName = exceptionClassNames.get(idx);
            try {
                Class<?> exceptionClass = Class.forName(exceptionClassName);
                if (!exceptionClass.isAssignableFrom(Throwable.class)) {
                    throw new DataFlowConfigurationException(
                            "Invalid retry configuration. "
                                    + exceptionClassName
                                    + " is not an exception class name");
                }
                exceptions[idx] = (Class<? extends Throwable>) exceptionClass;
            } catch (ClassNotFoundException e) {
                throw new DataFlowConfigurationException(
                        "Invalid retry configuration. "
                                + exceptionClassName
                                + " is not an exception class name");
            }
        }
        return exceptions;
    }

    @Override
    public String toString() {
        return "Retry{" + "maxAttempts=" + retry.getRetryConfig().getMaxAttempts() + '}';
    }
}
