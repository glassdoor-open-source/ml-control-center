/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowMetricMetadata.java
 */
package dataflow.core.component.metadata.metrics;

import dataflow.core.component.annotation.MetricType;

/** Holds information about a single dataflow component metric. */
public class DataFlowMetricMetadata {

    private final String name;

    private final String description;
    private final MetricType type;

    public DataFlowMetricMetadata(String name, String description, MetricType type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public MetricType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "DataFlowMetricMetadata{"
                + "name='"
                + name
                + '\''
                + ", description='"
                + description
                + '\''
                + ", type="
                + type
                + '}';
    }
}
