

plugins {
    id("java-library")
}

dependencies {
    api(libs.micrometerCore)
    testImplementation(libs.guava)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.jsonassert)
}

group = "dataflow"
description = "dataflow-component-core"
