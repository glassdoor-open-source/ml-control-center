

plugins {
    id("java-library")
}

dependencies {
    api(projects.dataflowCore)
    api(libs.awsJavaSdkS3)
    api(libs.jaxbApi)
    api(libs.jaxbRuntime)
    testImplementation(libs.s3mock213)
}

group = "dataflow"
description = "dataflow-aws"
