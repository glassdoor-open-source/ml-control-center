/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  HttpMethodConverter.java
 */
package dataflow.rest;

import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.ValueType;
import org.springframework.http.HttpMethod;

public class HttpMethodConverter {

    public static final class String_to_HttpMethod_converter
            extends AbstractValueTypeConverter<String, HttpMethod> {

        public String_to_HttpMethod_converter() {
            super(String.class, HttpMethod.class);
        }

        @Override
        public HttpMethod convert(final String value, ValueType toValueType) {
            return HttpMethod.valueOf(value);
        }
    }
}
