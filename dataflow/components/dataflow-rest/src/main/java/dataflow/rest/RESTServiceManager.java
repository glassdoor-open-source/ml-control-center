/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RESTServiceManager.java
 */
package dataflow.rest;

import dataflow.core.datasource.DataSource;
import dataflow.core.datasource.DataSourceEventListener;
import dataflow.core.datasource.DataSourceManager;
import dataflow.core.datasource.DataSourceSupplier;
import dataflow.core.exception.DataFlowConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.core5.util.TimeValue;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.yaml.snakeyaml.Yaml;

/**
 * Manager for REST service definitions. Services can be registered by calling the {@link
 * #register(String, String, RestTemplate)} method or can be specified using YAML configuration
 * using {@link #registerConfigProvider}.
 *
 * <p>The following configuration properties are supported in the YAML config: basePath,
 * maxConnections, maxConnectionsPerRoute, connectionEstablishTimeoutMillis,
 * eachRequestSocketTimeoutMillis, connectionLeaseRequestTimeoutMillis, retryCount, retrySentRequest
 *
 * <p>Example:
 *
 * <pre>
 * testService1:
 *   basePath: http://localhost:8080
 *   maxConnections: 100
 *   maxConnectionsPerRoute: 25
 *   connectionEstablishTimeoutMillis: 5000
 *   eachRequestSocketTimeoutMillis: 2000
 *   connectionLeaseRequestTimeoutMillis: 1000
 *   retryCount: 2
 *
 * testService2:
 *   basePath: http://localhost:8081
 *   maxConnections: 101
 *   maxConnectionsPerRoute: 30
 *   connectionEstablishTimeoutMillis: 6000
 *   eachRequestSocketTimeoutMillis: 3000
 *   connectionLeaseRequestTimeoutMillis: 500
 * </pre>
 */
public class RESTServiceManager {

    private static final String MAX_CONNECTIONS = "maxConnections";
    private static final String MAX_CONNECTIONS_PER_ROUTE = "maxConnectionsPerRoute";
    private static final String CONNECTION_ESTABLISH_TIMEOUT_MILLIS =
            "connectionEstablishTimeoutMillis";
    private static final String EACH_REQUEST_SOCKET_TIMEOUT_MILLIS =
            "eachRequestSocketTimeoutMillis";
    private static final String CONNECTION_LEASE_REQUEST_TIMEOUT_MILLIS =
            "connectionLeaseRequestTimeoutMillis";
    private static final String RETRY_COUNT = "retryCount";
    private static final String RETRY_DELAY_INTERVAL = "retryDelayInterval";

    public static class RESTServiceRegistration {

        private String basePath;
        private RestTemplate restTemplate;

        public RESTServiceRegistration(String basePath, RestTemplate restTemplate) {
            this.basePath = basePath;
            this.restTemplate = restTemplate;
        }

        public String getBasePath() {
            return basePath;
        }

        public RestTemplate getRestTemplate() {
            return restTemplate;
        }
    }

    private class EventListener implements DataSourceEventListener<DataSource> {

        @Override
        public void onAdd(DataSource dataSource) {
            try {
                loadConfig(dataSource);
            } catch (IOException e) {
                onError(e);
            }
        }

        @Override
        public void onChange(DataSource dataSource) {
            try {
                loadConfig(dataSource);
            } catch (IOException e) {
                onError(e);
            }
        }

        @Override
        public void onRemove(DataSource dataSource) {
            serviceIdRegistrationMap.clear();
        }

        @Override
        public void onError(Exception error) {
            // TODO(thorntonv): Implement method.
            error.printStackTrace();
        }
    }

    private final DataSourceManager<DataSource> dataSourceManager;
    private volatile Map<String, RESTServiceRegistration> serviceIdRegistrationMap =
            new HashMap<>();
    private long refreshIntervalSeconds = 300;

    public RESTServiceManager() {
        this.dataSourceManager = new DataSourceManager<>();
        dataSourceManager.addEventListener(new EventListener());
    }

    public void registerConfigProvider(DataSourceSupplier<DataSource> configProvider) {
        dataSourceManager.register(configProvider, refreshIntervalSeconds);
    }

    public RESTServiceRegistration get(String serviceId) {
        return serviceIdRegistrationMap.get(serviceId);
    }

    public Map<String, RESTServiceRegistration> getAllServiceRegistrations() {
        return new HashMap<>(serviceIdRegistrationMap);
    }

    public void register(String serviceId, String basePath, RestTemplate restTemplate) {
        serviceIdRegistrationMap.put(
                serviceId, new RESTServiceRegistration(basePath, restTemplate));
    }

    private void loadConfig(DataSource dataSource) throws IOException {
        Yaml yaml = new Yaml();
        Map<String, Object> config = yaml.load(dataSource.getInputStream());
        Map<String, RESTServiceRegistration> newRegistrationMap = new HashMap<>();
        config.forEach(
                (serviceId, serviceConfig) -> {
                    if (serviceConfig instanceof Map) {
                        Map<String, Object> serviceConfigMap = (Map<String, Object>) serviceConfig;
                        String basePath = (String) serviceConfigMap.get("basePath");
                        if (basePath == null) {
                            throw new DataFlowConfigurationException(
                                    "No base path specified for REST service "
                                            + serviceId
                                            + ": "
                                            + config);
                        }
                        RestTemplate restTemplate = buildRESTTemplate(serviceConfigMap);
                        newRegistrationMap.put(
                                serviceId, new RESTServiceRegistration(basePath, restTemplate));
                    } else {
                        throw new DataFlowConfigurationException(
                                "Invalid configuration for service "
                                        + serviceId
                                        + ": "
                                        + serviceConfig);
                    }
                });

        serviceIdRegistrationMap = newRegistrationMap;
    }

    private RestTemplate buildRESTTemplate(Map<String, Object> config) {
        PoolingHttpClientConnectionManager httpClientConnectionManager =
                new PoolingHttpClientConnectionManager();
        if (config.containsKey(MAX_CONNECTIONS)) {
            httpClientConnectionManager.setMaxTotal((Integer) config.get(MAX_CONNECTIONS));
        }
        if (config.containsKey(MAX_CONNECTIONS_PER_ROUTE)) {
            httpClientConnectionManager.setDefaultMaxPerRoute(
                    (Integer) config.get(MAX_CONNECTIONS_PER_ROUTE));
        }

        RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();
        if (config.containsKey(CONNECTION_ESTABLISH_TIMEOUT_MILLIS)) {
            requestConfigBuilder.setConnectTimeout(
                    (Integer) config.get(CONNECTION_ESTABLISH_TIMEOUT_MILLIS),
                    TimeUnit.MILLISECONDS);
        }
        if (config.containsKey(EACH_REQUEST_SOCKET_TIMEOUT_MILLIS)) {
            requestConfigBuilder.setResponseTimeout(
                    (Integer) config.get(EACH_REQUEST_SOCKET_TIMEOUT_MILLIS),
                    TimeUnit.MILLISECONDS);
        }
        if (config.containsKey(CONNECTION_LEASE_REQUEST_TIMEOUT_MILLIS)) {
            requestConfigBuilder.setConnectionRequestTimeout(
                    (Integer) config.get(CONNECTION_LEASE_REQUEST_TIMEOUT_MILLIS),
                    TimeUnit.MILLISECONDS);
        }

        HttpClientBuilder httpClientBuilder =
                HttpClientBuilder.create()
                        .setConnectionManager(httpClientConnectionManager)
                        .setDefaultRequestConfig(requestConfigBuilder.build());

        if (config.containsKey(RETRY_COUNT)) {
            long retryDelayInterval = 500L;
            if (config.containsKey(RETRY_DELAY_INTERVAL)) {
                retryDelayInterval = ((Integer) config.get(RETRY_DELAY_INTERVAL)).longValue();
            }
            httpClientBuilder.setRetryStrategy(
                    new DefaultHttpRequestRetryStrategy(
                            (Integer) config.get(RETRY_COUNT),
                            TimeValue.ofMilliseconds(retryDelayInterval)));
        }

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(
                new HttpComponentsClientHttpRequestFactory(httpClientBuilder.build()));
        return restTemplate;
    }
}
