

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowCore)
    api(projects.dataflowComponentCore)
    api(projects.dataflowRetry)
    api(libs.snakeyaml)
    api(libs.httpclient)
    api(libs.httpclient5)
    api(libs.micrometerCore)
    api(libs.springContext)
    api(libs.springBeans)
    api(libs.springWeb)
    api(libs.jacksonCore)
    api(libs.jacksonAnnotations)
    api(libs.jacksonDatabind)
    api(libs.jacksonJaxrsJsonProvider)
    api(libs.jacksonDatatypeThreetenbp)
    api(libs.javaxAnnotationApi)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

configurations.all {
    exclude(group = "org.slf4j", module = "slf4j-log4j12")
    exclude(group = "log4j", module = "log4j")
}

group = "dataflow"
description = "dataflow-rest"
