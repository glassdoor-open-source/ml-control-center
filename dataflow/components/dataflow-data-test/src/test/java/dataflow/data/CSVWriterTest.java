/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CSVWriterTest.java
 */
package dataflow.data;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.common.collect.ImmutableMap;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.datasource.DataSource;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import org.junit.Before;
import org.junit.Test;

public class CSVWriterTest {

    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void testFlow() throws Exception {
        DataSource mockDataSource = mock(DataSource.class);
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        when(mockDataSource.getOutputStream()).thenReturn(byteOut);
        DataRecords records = new DataRecords();
        records.setColumnNames("col1", "col2");
        records.add(
                new DataRecord(
                        new ImmutableMap.Builder<String, Object>()
                                .put("col1", "A")
                                .put("col2", "B")
                                .build()));
        records.add(
                new DataRecord(
                        new ImmutableMap.Builder<String, Object>()
                                .put("col1", ImmutableMap.of("k1", "v1", "k2", "10"))
                                .put("col2", "3.14")
                                .build()));

        try (DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration("CSVWriter_flow1")
                        .getInstanceFactory()
                        .newInstance(
                                newInstance -> {
                                    newInstance.setValue("dataSource", mockDataSource);
                                    newInstance.setValue("records", records);
                                })) {
            instance.execute();

            records.clear();
            records.add(
                    new DataRecord(
                            new ImmutableMap.Builder<String, Object>()
                                    .put("col1", "C")
                                    .put("col2", "D")
                                    .build()));
            instance.setValue("records", records);
            // If we call the value provider again the new records should be appended.
            // ie. we won't open the DataSource again and won't write the column names header.
            instance.execute();
        }

        assertEquals(
                "col1\tcol2\r\n" + "A\tB\r\n" + "{k1=v1, k2=10}\t3.14\r\n" + "C\tD\r\n",
                byteOut.toString(Charset.defaultCharset()));
    }
}
