/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CSVRecordStreamValueProviderTest.java
 */
package dataflow.data;

import static org.junit.Assert.assertEquals;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Before;
import org.junit.Test;

public class CSVRecordStreamValueProviderTest {

    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void testFlow() throws Exception {
        Stream<Map<String, Object>> resultStream =
                (Stream<Map<String, Object>>)
                        testFlow("CSVRecordStreamValueProvider_flow1", Collections.emptyMap());
        List<Map<String, Object>> results = resultStream.collect(Collectors.toList());
        assertEquals(500, results.size());
        assertEquals("avg", results.get(0).get("field"));
        assertEquals("jobTitleHashCode", results.get(0).get("valueType"));
        assertEquals(839943242, results.get(0).get("value"));
        assertEquals(-0.005225271f, results.get(0).get("weight"));
        float[] expectedFactors =
                new float[] {
                    0.030774692f,
                    0.030448996f,
                    0.029739283f,
                    0.02930015f,
                    0.02985974f,
                    0.033203162f,
                    0.030737665f,
                    0.032454584f
                };
        for (int idx = 0; idx < expectedFactors.length; idx++) {
            assertEquals(expectedFactors[idx], ((List) results.get(0).get("factors")).get(idx));
        }
    }

    private Object testFlow(String id, Map<String, Object> input) throws Exception {
        try (DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration(id)
                        .getInstanceFactory()
                        .newInstance(i -> input.forEach(i::setValue))) {
            instance.execute();
            return instance.getOutput();
        }
    }
}
