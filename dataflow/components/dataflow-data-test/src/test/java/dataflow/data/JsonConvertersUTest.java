/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  JsonConvertersUTest.java
 */
package dataflow.data;

import static org.junit.Assert.*;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.registry.TypeRegistry;
import dataflow.core.type.TypeUtil;
import dataflow.core.type.ValueType;
import java.io.IOException;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

public class JsonConvertersUTest {

    @JsonAutoConvert
    public static class TestJson1 {

        private String name;

        @JsonCreator
        public TestJson1(@JsonProperty(value = "name") String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    @JsonAutoConvert
    public static class TestJson2 {

        @JsonProperty private int age;

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }

    @JsonAutoConvert
    public static class TestJson3 {

        private String name;
        private int age;

        @JsonCreator
        public TestJson3(
                @JsonProperty(value = "name") String name, @JsonProperty(value = "age") int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }

    private DataFlowEnvironment env;
    private TypeRegistry typeRegistry;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
        this.typeRegistry = env.getRegistry().getTypeRegistry();
    }

    @Test
    public void testJsonToObject() {
        TestJson1 result =
                TypeUtil.convert("{\"name\": \"myName1\"}", TestJson1.class, typeRegistry);
        ;
        assertEquals("myName1", result.getName());

        TestJson2 result2 = TypeUtil.convert("{\"age\": 20}", TestJson2.class, typeRegistry);
        ;
        assertEquals(20, result2.getAge());
    }

    @Test
    public void testObjectToJson() {
        TestJson1 test1 = new TestJson1("myName1");
        assertEquals(
                "{\"name\":\"myName1\"}",
                TypeUtil.convert(test1, TestJson1.class, String.class, typeRegistry));

        TestJson2 test2 = new TestJson2();
        test2.setAge(20);
        assertEquals(
                "{\"age\":20}",
                TypeUtil.convert(test2, TestJson2.class, String.class, typeRegistry));
    }

    @Test
    public void testObjectToObject() {
        TestJson1 test1 = new TestJson1("myName1");
        TestJson3 converted1 =
                TypeUtil.convert(test1, TestJson1.class, TestJson3.class, typeRegistry);
        assertEquals("myName1", converted1.getName());
        TestJson2 test2 = new TestJson2();
        test2.setAge(20);
        TestJson3 converted2 =
                TypeUtil.convert(test2, TestJson2.class, TestJson3.class, typeRegistry);
        assertNull(converted2.getName());
        assertEquals(20, converted2.getAge());
    }

    @Test
    public void testMapToObject() {
        Map<String, Object> map =
                ImmutableMap.<String, Object>builder()
                        .put("name", "testName")
                        .put("age", 20)
                        .build();
        ValueType hashMapValueType =
                TypeUtil.valueTypeFromString("java.util.HashMap<String, Object>");
        TestJson3 converted2 =
                TypeUtil.convert(
                        map, hashMapValueType, ValueType.of(TestJson3.class), typeRegistry);
        assertEquals("testName", converted2.getName());
        assertEquals(20, converted2.getAge());
    }

    @Test
    public void testComponent() throws Exception {
        DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration("TestJsonConvert")
                        .getInstanceFactory()
                        .newInstance();
        instance.setValue("in1", "myName1");
        instance.execute();
        String name = (String) instance.getOutput();
        assertEquals("myName1", name);
    }
}
