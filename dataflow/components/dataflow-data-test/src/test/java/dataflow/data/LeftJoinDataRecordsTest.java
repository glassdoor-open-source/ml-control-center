/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LeftJoinDataRecordsTest.java
 */
package dataflow.data;

import static org.junit.Assert.assertEquals;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

public class LeftJoinDataRecordsTest {

    @Test
    public void getValue() {
        DataRecords left = new DataRecords();
        left.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 1)
                                .put("name", "Glassdoor")
                                .build()));
        left.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 2)
                                .put("name", "Google")
                                .build()));

        DataRecords right = new DataRecords();
        right.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 1)
                                .put("industry", 5)
                                .build()));
        right.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 2)
                                .put("industry", 7)
                                .build()));

        DataRecords joined = new LeftJoinDataRecords("id", null, null).getValue(left, right);
        assertEquals(2, joined.size());

        assertEquals(3, joined.get(0).size());
        assertEquals(1, joined.get(0).get("id"));
        assertEquals("Glassdoor", joined.get(0).get("name"));
        assertEquals(5, joined.get(0).get("industry"));

        assertEquals(3, joined.get(1).size());
        assertEquals(2, joined.get(1).get("id"));
        assertEquals("Google", joined.get(1).get("name"));
        assertEquals(7, joined.get(1).get("industry"));
    }
}
