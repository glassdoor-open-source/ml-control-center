/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordsTest.java
 */
package dataflow.data;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class DataRecordsTest {

    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        this.objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
    }

    @Test
    public void testJsonSerialization() throws IOException {
        DataRecords records = new DataRecords();
        records.setKeyColumnNames(ImmutableList.of("id"));
        DataRecord record = new DataRecord();
        record.put("id", 1);
        record.put("name", "Jill");
        record.put("location", ImmutableMap.of("postalCode", 90991));
        records.add(record);
        record = new DataRecord();
        record.put("id", 2);
        record.put("name", "Rebecca");
        record.put("location", ImmutableMap.of("postalCode", 43223));
        records.add(record);

        String expectedJson =
                "{\"columnNames\":[\"id\",\"location\",\"name\"],\"keyColumnNames\":[\"id\"],\"records\":[{\"id\":1,\"location\":{\"postalCode\":90991},\"name\":\"Jill\"},{\"id\":2,\"location\":{\"postalCode\":43223},\"name\":\"Rebecca\"}]}";
        assertEquals(expectedJson, objectMapper.writeValueAsString(records));
        DataRecords deserializedRecords = objectMapper.readValue(expectedJson, DataRecords.class);
        assertEquals(records, deserializedRecords);
    }
}
