/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordsProxyUTest.java
 */
package dataflow.data.proxy;

import static org.junit.Assert.assertEquals;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class DataRecordsProxyUTest {

    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void testComponent() throws Exception {
        DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration("TestDataRecordsProxy")
                        .getInstanceFactory()
                        .newInstance();
        DataRecords records = new DataRecords();
        DataRecord record = new DataRecord();
        record.put("field1", "abc");
        records.add(record);
        instance.setValue("in1", records);

        instance.execute();
        assertEquals("abc", instance.getOutput());
        assertEquals(3, records.size());
        assertEquals("abc", records.get(0).get("field1"));
        assertEquals("xyz_abc", records.get(1).get("field1"));
        assertEquals("987", records.get(2).get("field1"));
        assertEquals(456, records.get(2).get("field2"));
    }
}
