/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordProxyUTest.java
 */
package dataflow.data.proxy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.data.DataRecord;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class DataRecordProxyUTest {

    private interface TestProxy_DefaultValue extends DataRecordProxy {

        String getField1();

        String getField1(String defaultValue);

        void setField1(String value, String defaultValue);
    }

    private interface TestProxy_AutoBoxing extends DataRecordProxy {

        Integer getField1();

        void setField1(Integer value);

        int getField2();

        void setField2(int value);
    }

    private interface TestProxy_InvalidGetter_VoidReturn extends DataRecordProxy {

        void getField1();

        void setField1(Integer value);
    }

    private interface TestProxy_InvalidGetter_TooManyParams extends DataRecordProxy {

        Integer getField1(Integer defaultValue, Integer anotherValue);

        void setField1(Integer value);
    }

    private interface TestProxy_InvalidGetter_wrongDefaultType extends DataRecordProxy {

        Integer getField1(String defaultValue);

        void setField1(Integer value);
    }

    private interface TestProxy_InvalidSetter_NoParams extends DataRecordProxy {

        Integer getField1();

        void setField1();
    }

    private interface TestProxy_InvalidSetter_TooManyParams extends DataRecordProxy {

        Integer getField1();

        void setField1(Integer value, Integer defaultValue, Integer anotherValue);
    }

    private interface TestProxy_InvalidSetter_wrongDefaultType extends DataRecordProxy {

        Integer getField1();

        void setField1(Integer value, Number defaultValue);
    }

    private interface TestProxy_InvalidGetterSetterType extends DataRecordProxy {

        String getField1();

        void setField1(Integer value);
    }

    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void test_getAndSet() {
        DataRecord record = new DataRecord();
        record.put("field1", "abc");
        TestDataRecordProxy proxy = DataRecordProxy.create(record, TestDataRecordProxy.class);
        assertEquals("abc", proxy.getField1());
    }

    @Test
    public void test_validate_missingRequiredValue() {
        DataRecord record = new DataRecord();
        TestDataRecordProxy proxy = DataRecordProxy.create(record, TestDataRecordProxy.class);
        try {
            proxy.validate();
            fail("Expected exception was not thrown");
        } catch (DataRecordValidationException e) {
            assertEquals("Missing required column Field1", e.getMessage());
        }
    }

    @Test
    public void test_validate_invalidFieldType() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        TestDataRecordProxy proxy = DataRecordProxy.create(record, TestDataRecordProxy.class);
        try {
            proxy.validate();
            fail("Expected exception was not thrown");
        } catch (DataRecordValidationException e) {
            assertEquals(
                    "Column Field1 has value type java.lang.Integer, but java.lang.String was expected",
                    e.getMessage());
        }
    }

    @Test
    public void test_getRecord() {
        DataRecord record = new DataRecord();
        record.put("field1", "abc");
        TestDataRecordProxy proxy = DataRecordProxy.create(record, TestDataRecordProxy.class);
        assertTrue(record == proxy.getRecord());
    }

    @Test
    public void test_defaultValues() {
        DataRecord record = new DataRecord();
        TestProxy_DefaultValue proxy = DataRecordProxy.create(record, TestProxy_DefaultValue.class);

        assertEquals("abc", proxy.getField1("abc"));
        proxy.setField1(null, "xyz");
        assertEquals("xyz", proxy.getField1());
    }

    @Test
    public void test_autoBoxing() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        record.put("field2", Integer.valueOf(987));
        TestProxy_AutoBoxing proxy = DataRecordProxy.create(record, TestProxy_AutoBoxing.class);

        assertEquals((Integer) 123, proxy.getField1());
        proxy.setField1(456);
        assertEquals((Integer) 456, proxy.getField1());

        assertEquals(987, proxy.getField2());
    }

    @Test
    public void test_invalidGetter_voidReturn() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        try {
            DataRecordProxy.create(record, TestProxy_InvalidGetter_VoidReturn.class);
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals("Getter Field1 must return a value", ex.getMessage());
        }
    }

    @Test
    public void test_invalidGetter_tooManyParams() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        try {
            DataRecordProxy.create(record, TestProxy_InvalidGetter_TooManyParams.class);
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals("Getter Field1 can have at most 1 parameter", ex.getMessage());
        }
    }

    @Test
    public void test_invalidSetter_noParams() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        try {
            DataRecordProxy.create(record, TestProxy_InvalidSetter_NoParams.class);
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals("Setter Field1 must have at least 1 parameter", ex.getMessage());
        }
    }

    @Test
    public void test_invalidSetter_tooManyParams() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        try {
            DataRecordProxy.create(record, TestProxy_InvalidSetter_TooManyParams.class);
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals("Setter Field1 can have at most 2 parameters", ex.getMessage());
        }
    }

    @Test
    public void test_invalidSetter_wrongDefaultType() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        try {
            DataRecordProxy.create(record, TestProxy_InvalidSetter_wrongDefaultType.class);
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals(
                    "Setter Field1 default value parameter type should be java.lang.Integer",
                    ex.getMessage());
        }
    }

    @Test
    public void test_invalidGetter_wrongDefaultType() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        try {
            DataRecordProxy.create(record, TestProxy_InvalidGetter_wrongDefaultType.class);
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals(
                    "Getter Field1 default value parameter type should be java.lang.Integer",
                    ex.getMessage());
        }
    }

    @Test
    public void test_invalidGetterSetterTypes() {
        DataRecord record = new DataRecord();
        record.put("field1", 123);
        try {
            DataRecordProxy.create(record, TestProxy_InvalidGetterSetterType.class);
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            assertEquals("Getter and setter types don't match for column Field1", ex.getMessage());
        }
    }

    @Test
    public void testComponent() throws Exception {
        DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration("TestDataRecordProxy")
                        .getInstanceFactory()
                        .newInstance();
        DataRecord record = new DataRecord();
        record.put("field1", "abc");
        instance.setValue("in1", record);
        instance.execute();
        assertEquals("xyz", instance.getOutput());
        assertEquals("xyz", record.get("field1"));
        assertEquals(123, record.get("field2"));
    }
}
