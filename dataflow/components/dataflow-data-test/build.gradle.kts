

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(libs.googleAutoService)
    testAnnotationProcessor(projects.dataflowCore)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowData)

    api(projects.dataflowCore)
    api(projects.dataflowCodegen)
    api(projects.dataflowComponentCodegen)
    api(projects.dataflowData)
    api(libs.commonsCsv)
    api(libs.micrometerCore)
    api(libs.jacksonDatabind)
    api(libs.jacksonAnnotations)

    testImplementation(libs.guava)
    testImplementation(libs.springTest)
    testImplementation(libs.jsonassert)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.junit)
    testImplementation(libs.compileTesting)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-data-test"
