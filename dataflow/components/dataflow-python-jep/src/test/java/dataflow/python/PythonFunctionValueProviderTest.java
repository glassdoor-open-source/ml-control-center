/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PythonFunctionValueProviderTest.java
 */
package dataflow.python;

import static dataflow.python.PythonFunctionValueProvider.POSITIONAL_ARGS_INPUT_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import jep.SharedInterpreter;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PythonFunctionValueProviderTest {

    @Before
    public void setUp() {
        DataFlowEnvironment env = new DataFlowEnvironment("test", null, null);
        DataFlowExecutionContext.createExecutionContext(env);
        try {
            new SharedInterpreter().close();
        } catch (Throwable t) {
            Assume.assumeNoException(t);
        }
    }

    @Test
    public void sayHello() throws Exception {
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(null, null, "\"Hello World {name}!!\".format", 1)) {
            String actual = null;
            try {
                actual = (String) valueProvider.getValue(ImmutableMap.of("name", "Python")).get();
            } catch (Throwable t) {
                Assume.assumeNoException(t);
            }
            assertEquals("Hello World Python!!", actual);
        }
    }

    @Test
    public void sayHello_missingParam() throws Exception {
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(null, null, "\"Hello World {name}!!\".format", 1)) {
            try {
                valueProvider.getValue(Collections.emptyMap()).get();
                fail("Expected exception was not thrown");
            } catch (Throwable t) {
                // Expected.
                assertEquals("jep.JepException: <class 'KeyError'>: ('name',)", t.getMessage());
            }
        }
    }

    @Test
    public void invalidFunctionExpr() {
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(null, null, "\"Hello World {name}!!\"", 1)) {
            valueProvider.getValue(Collections.emptyMap()).get();
            fail("Expected exception was not thrown");
        } catch (Throwable t) {
            // Expected.
            assertEquals("jep.JepException: pyembed:invoke Invalid callable.", t.getMessage());
        }
    }

    @Test
    public void fnWithPositionalArgs() throws Exception {
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(null, null, "lambda x: f'Hello {x}!'", 1)) {
            assertEquals(
                    "Hello Test!",
                    valueProvider
                            .getValue(
                                    ImmutableMap.of(
                                            POSITIONAL_ARGS_INPUT_NAME, ImmutableList.of("Test")))
                            .get());
        }
    }

    @Test
    public void fnWithDataRecordsParam() throws Exception {
        DataRecords testRecords = new DataRecords();
        DataRecord testRecord = new DataRecord();
        testRecord.put("k1", "v1");
        testRecords.add(testRecord);
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(
                        ImmutableList.of("json", "dataflow.data as dataflow"),
                        null,
                        "lambda x: x.get(0)['k1']",
                        1)) {
            assertEquals(
                    "v1",
                    valueProvider
                            .getValue(
                                    ImmutableMap.of(
                                            POSITIONAL_ARGS_INPUT_NAME,
                                            ImmutableList.of(testRecords)))
                            .get());
        }
    }

    @Test
    public void fnWithDataRecordsReturn() throws Exception {
        String init =
                "def buildRecords():\n"
                        + "\trecords = dataflow.DataRecords()\n"
                        + "\trecords.setColumnNames(['key'])\n"
                        + "\trecords.setKeyColumnNames(['key'])\n"
                        + "\trecord = dataflow.DataRecord({'k1': 'v1'})\n"
                        + "\trecords.add(record)\n"
                        + "\treturn records";
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(
                        ImmutableList.of("json", "dataflow.data as dataflow"),
                        init,
                        "buildRecords",
                        1)) {
            DataRecords result =
                    (DataRecords)
                            valueProvider
                                    .getValue(
                                            ImmutableMap.of(
                                                    POSITIONAL_ARGS_INPUT_NAME, ImmutableList.of()))
                                    .get();
            assertEquals(1, result.size());
            assertEquals(ImmutableList.of("key"), result.getColumnNames());
            assertEquals(ImmutableList.of("key"), result.getKeyColumnNames());
            DataRecord record = result.get(0);
            assertEquals(1, record.size());
            assertEquals("v1", record.get("k1"));
        }
    }

    @Test
    public void fnWithImport() throws Exception {
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(
                        ImmutableList.of("inspect", "json"),
                        null,
                        "lambda: str(inspect.signature(json.dumps))",
                        1)) {
            assertEquals(
                    "(obj, *, skipkeys=False, ensure_ascii=True, check_circular=True, allow_nan=True, cls=None, indent=None, separators=None, default=None, sort_keys=False, **kw)",
                    valueProvider.getValue(Collections.emptyMap()).get());
        }
    }

    @Test
    public void multipleProviders() throws Exception {
        String init1 = "def f1():\n" + "\treturn 'Hello from f1'";
        String init2 = "def f2():\n" + "\treturn 'Hello from f2'";
        try (PythonFunctionValueProvider valueProvider1 =
                        new PythonFunctionValueProvider(null, init1, "f1", 1);
                PythonFunctionValueProvider valueProvider2 =
                        new PythonFunctionValueProvider(null, init2, "f2", 1)) {
            String actual2 = (String) valueProvider2.getValue(Collections.emptyMap()).get();
            String actual1 = (String) valueProvider1.getValue(Collections.emptyMap()).get();
            assertEquals("Hello from f1", actual1);
            assertEquals("Hello from f2", actual2);
        }
    }

    @Test
    public void fnRaiseException() throws Exception {
        String init = "def test():\n" + "\traise Exception('test exception')";
        try (PythonFunctionValueProvider valueProvider =
                new PythonFunctionValueProvider(null, init, "test", 1)) {
            try {
                valueProvider.getValue(Collections.emptyMap()).get();
                fail("Expected exception was not thrown");
            } catch (ExecutionException ex) {
                assertEquals("<class 'Exception'>: test exception", ex.getCause().getMessage());
            }
        }
    }
}
