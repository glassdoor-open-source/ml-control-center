/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PythonFunctionValueProvider.java
 */
package dataflow.python;

import dataflow.core.component.annotation.*;
import java.util.*;
import java.util.concurrent.*;
import jep.SharedInterpreter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent(description = "Provides the value returned by calling a python function")
public class PythonFunctionValueProvider implements AutoCloseable {

    private static final String FN_NAME = "__f";

    public static final String POSITIONAL_ARGS_INPUT_NAME = "_positionalArgs";
    private static final Logger logger = LoggerFactory.getLogger(PythonFunctionValueProvider.class);
    private final ThreadLocal<SharedInterpreter> interpreter;

    private final ExecutorService executor;
    private final String functionExpr;

    @DataFlowConfigurable
    public PythonFunctionValueProvider(
            @DataFlowConfigProperty(required = false, description = "Imports") List<String> imports,
            @DataFlowConfigProperty(required = false, description = "Initialization code")
                    String init,
            @DataFlowConfigProperty(
                            description = "An expression that evaluates to a function to call")
                    String functionExpr,
            @DataFlowConfigProperty(
                            required = false,
                            description = "The maximum number of interpreter threads")
                    Integer threadCount) {
        this.functionExpr = functionExpr;

        this.interpreter =
                ThreadLocal.withInitial(
                        () -> {
                            logger.debug(
                                    "Creating new JEP SharedInterpreter for thread "
                                            + Thread.currentThread().getName());
                            SharedInterpreter sharedInterpreter = new SharedInterpreter();
                            try {
                                if (imports != null) {
                                    for (String importStmt : imports) {
                                        if (!importStmt.startsWith("import ")) {
                                            importStmt = String.format("import %s", importStmt);
                                        }
                                        sharedInterpreter.exec(importStmt);
                                    }
                                }
                                if (init != null) {
                                    sharedInterpreter.exec(init);
                                }
                                sharedInterpreter.exec(FN_NAME + " = " + functionExpr);
                                return sharedInterpreter;
                            } catch (Throwable t) {
                                logger.error("Failed to create Python SharedInterpreter", t);
                                sharedInterpreter.close();
                                logger.error("JEP ERROR: " + t.getMessage());
                                throw t;
                            }
                        });
        threadCount = threadCount == null ? 1 : threadCount;
        executor =
                Executors.newFixedThreadPool(
                        threadCount,
                        runnable ->
                                new Thread(
                                        () -> {
                                            runnable.run();
                                            // Close the interpreter before the thread completes.
                                            SharedInterpreter interpreter =
                                                    PythonFunctionValueProvider.this.interpreter
                                                            .get();
                                            if (interpreter != null) {
                                                logger.debug(
                                                        "Closing JEP SharedInterpreter for thread "
                                                                + Thread.currentThread().getName());
                                                interpreter.close();
                                                PythonFunctionValueProvider.this.interpreter.set(
                                                        null);
                                            }
                                        },
                                        PythonFunctionValueProvider.class.getSimpleName()));
    }

    @OutputValue
    public CompletableFuture<Object> getValue(@InputValues Map<String, Object> inputs) {
        CompletableFuture<Object> future = new CompletableFuture<>();
        executor.submit(
                () -> {
                    try {
                        SharedInterpreter interpreter =
                                PythonFunctionValueProvider.this.interpreter.get();
                        List<Object> positionalArgs =
                                (List<Object>) inputs.get(POSITIONAL_ARGS_INPUT_NAME);
                        Object result;
                        if (positionalArgs != null) {
                            Map<String, Object> keywordArgs = new HashMap<>(inputs);
                            keywordArgs.remove(POSITIONAL_ARGS_INPUT_NAME);
                            result =
                                    interpreter.invoke(
                                            FN_NAME, positionalArgs.toArray(), keywordArgs);
                        } else {
                            result = interpreter.invoke(FN_NAME, inputs);
                        }
                        future.complete(result);
                    } catch (Throwable t) {
                        future.completeExceptionally(t);
                    }
                });
        return future;
    }

    @Override
    public void close() throws Exception {
        executor.shutdown();
        if (!executor.awaitTermination(1, TimeUnit.MINUTES)) {
            logger.warn(
                    PythonFunctionValueProvider.class.getSimpleName()
                            + " interpreter thread did not complete in the allotted time");
        }
    }
}
