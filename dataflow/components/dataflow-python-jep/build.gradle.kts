

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(libs.jep)
    api(projects.dataflowCore)
    api(libs.slf4jApi)
    api(libs.springContext)
    testImplementation(projects.dataflowData)
    testImplementation(libs.guava)
    testImplementation(libs.mockitoCore)
    implementation(projects.dataflowComponentCodegen)
}

group = "dataflow"
description = "dataflow-python-jep"
