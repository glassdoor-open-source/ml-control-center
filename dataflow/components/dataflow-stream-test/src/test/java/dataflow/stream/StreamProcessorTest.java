/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StreamProcessorTest.java
 */
package dataflow.stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.ImmutableList;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class StreamProcessorTest {

    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", new SimpleDependencyInjector(), null);
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void test() throws Exception {
        List<String> inputList = ImmutableList.of("a", "b", "c");
        DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration("streamProcessorTest")
                        .getInstanceFactory()
                        .newInstance(i -> {});
        instance.setValue("stream", inputList.stream());
        List<String> out = Collections.synchronizedList(new ArrayList<>());
        instance.setValue("list", out);
        instance.execute();
        assertEquals(5, out.size());
        assertEquals("before", out.get(0));
        // Items from input list could be in any order since they are added in multiple threads.
        assertTrue(out.containsAll(inputList));
        assertEquals("after", out.get(out.size() - 1));
    }

    @Test
    public void test_beforeStartValueRef() throws Exception {
        List<String> inputList = ImmutableList.of("a", "b", "c");
        DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration("streamProcessorTest_beforeStartValueRef")
                        .getInstanceFactory()
                        .newInstance(i -> {});
        instance.setValue("stream", inputList.stream());
        instance.execute();
        List<String> out = (List<String>) instance.getOutput();

        assertEquals("Unexpected output " + out.toString(), 5, out.size());
        assertEquals("before", out.get(0));
        // Items from input list could be in any order since they are added in multiple threads.
        assertTrue(out.containsAll(inputList));
        assertEquals("after", out.get(out.size() - 1));
    }

    @Test
    public void test_throwException() throws Exception {
        List<String> inputList = ImmutableList.of("a", "b", "c");
        DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration("streamProcessorTest_throwException")
                        .getInstanceFactory()
                        .newInstance(i -> {});
        instance.setValue("stream", inputList.stream());
        List<String> out = Collections.synchronizedList(new ArrayList<>());
        instance.setValue("list", out);
        try {
            instance.execute();
        } catch (Exception e) {
            Throwable rootException = e;
            while (rootException.getCause() != null && rootException.getCause() != rootException) {
                rootException = rootException.getCause();
            }
            assertEquals("Index 5 out of bounds for length 4", rootException.getMessage());
        }
    }
}
