

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.dataflowStream)

    api(projects.dataflowCore)
    api(projects.dataflowRetry)
    api(projects.dataflowTask)
    api(projects.dataflowStream)
    api(libs.guava)
    api(libs.reactiveStreams)
    api(libs.reactorCore)
    api(libs.jacksonAnnotations)
    api(libs.micrometerCore)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.jsonassert)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-stream-test"
