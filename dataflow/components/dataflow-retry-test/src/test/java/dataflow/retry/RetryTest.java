/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RetryTest.java
 */
package dataflow.retry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import java.io.IOException;
import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Test;

public class RetryTest {

    private final SimpleDependencyInjector dependencyInjector = new SimpleDependencyInjector();
    private DataFlowEnvironment env;

    @Before
    public void setUp() throws IOException {
        this.env = new DataFlowEnvironment("test", dependencyInjector, null);

        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void testRetry_retryThenSuccess() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("testFlow_retryThenSuccess");
        instance.execute();
        assertEquals("attempt1,attempt2,success", instance.getOutput());
    }

    @Test
    public void testRetry_retryConstValueProvider() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("testFlow_retryConstValueProvider");
        instance.execute();
        assertEquals("attempt1,attempt2,success", instance.getOutput());
    }

    @Test
    public void testRetry_retryThenFailure() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("testFlow_retryThenFailure");
        try {
            instance.execute();
            fail("expected exception was not thrown");
        } catch (Exception ex) {
            assertEquals("java.lang.RuntimeException: simulating a failure", ex.getMessage());
        }
    }

    @Test
    public void testRetry_defaultRetry() throws Exception {
        // Uses the default retry behavior supplied through dependency injection.
        dependencyInjector.register(
                new Retry(io.github.resilience4j.retry.Retry.ofDefaults("default")));
        DataFlowInstance instance =
                registerAndGetInstance("testFlow_defaultRetry", (newInstance) -> {});
        instance.execute();
        assertEquals("attempt1,attempt2,success", instance.getOutput());
    }

    private DataFlowInstance registerAndGetInstance(String filename) throws Exception {
        return registerAndGetInstance(filename, instance -> {});
    }

    private DataFlowInstance registerAndGetInstance(String id, Consumer<DataFlowInstance> initFn)
            throws Exception {
        return env.getRegistry()
                .getDataFlowRegistration(id)
                .getInstanceFactory()
                .newInstance(initFn);
    }
}
