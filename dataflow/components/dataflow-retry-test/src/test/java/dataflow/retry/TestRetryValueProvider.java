/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestRetryValueProvider.java
 */
package dataflow.retry;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.concurrent.atomic.AtomicInteger;

@DataFlowComponent
public class TestRetryValueProvider {

    private int failureCount;
    private Retry retry;

    @DataFlowConfigurable
    public TestRetryValueProvider(
            @DataFlowConfigProperty int failureCount,
            @DataFlowConfigProperty(required = false) Retry retry) {
        this.failureCount = failureCount;
        this.retry =
                retry != null
                        ? retry
                        : DataFlowExecutionContext.getCurrentExecutionContext()
                                .getDependencyInjector()
                                .getInstance(Retry.class);
        if (this.retry == null) {
            throw new DataFlowConfigurationException("A retry behavior must be defined");
        }
    }

    @OutputValue
    public String getValue() throws Exception {
        StringBuilder builder = new StringBuilder();
        AtomicInteger count = new AtomicInteger(0);
        return retry.executeCallable(
                () -> {
                    builder.append("attempt").append(count.incrementAndGet()).append(",");
                    if (count.get() < failureCount) {
                        throw new RuntimeException("simulating a failure");
                    }
                    builder.append("success");
                    return builder.toString();
                });
    }

    public Retry getRetry() {
        return retry;
    }
}
