

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.dataflowRetry)

    api(projects.dataflowRetry)
    api(projects.dataflowCore)
    api(libs.resilience4jRetry)
    testImplementation(libs.micrometerCore)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.jsonassert)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-retry-test"
