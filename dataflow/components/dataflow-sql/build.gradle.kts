

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)
    
    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(projects.dataflowRetry)
    api(libs.guava)
    api(libs.micrometerCore)
    api(libs.reactiveStreams)
    api(libs.reactorCore)
    api(libs.springJdbc)
    testImplementation(libs.hibernateCore)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.h2)
    implementation(projects.dataflowComponentCodegen)
}

group = "dataflow"
description = "dataflow-sql"
