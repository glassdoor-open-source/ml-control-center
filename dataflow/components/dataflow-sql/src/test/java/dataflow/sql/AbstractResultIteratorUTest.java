/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractResultIteratorUTest.java
 */
package dataflow.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.DataFilter;
import dataflow.data.query.DataQueryValueProvider;
import dataflow.data.query.KeyRangeDataFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractResultIteratorUTest {

    private static final DataRecords TEST_RECORDS_1;
    private static final DataRecords TEST_RECORDS_2;

    static {
        TEST_RECORDS_1 = new DataRecords();
        TEST_RECORDS_1.setKeyColumnNames(ImmutableList.of("id"));
        TEST_RECORDS_1.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 1)
                                .put("name", "jill")
                                .build()));
        TEST_RECORDS_1.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 2)
                                .put("name", "jack")
                                .build()));

        TEST_RECORDS_2 = new DataRecords();
        TEST_RECORDS_2.setKeyColumnNames(ImmutableList.of("id"));
        TEST_RECORDS_2.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 3)
                                .put("name", "james")
                                .build()));
        TEST_RECORDS_2.add(
                new DataRecord(
                        ImmutableMap.<String, Object>builder()
                                .put("id", 4)
                                .put("name", "jenifer")
                                .build()));
    }

    @Mock private DataQueryValueProvider mockQueryValueProvider;

    @Captor private ArgumentCaptor<Map<String, Object>> paramsCaptor;

    @Captor private ArgumentCaptor<List<DataFilter>> filtersCaptor;

    @Before
    public void setUp() {
        when(mockQueryValueProvider.getKeyColumns()).thenReturn(ImmutableList.of("id"));
    }

    @Test
    public void testIteration_noResults() throws InvalidDataException, IOException {
        Map<String, Object> params = new HashMap<>();
        List<DataFilter> filters = new ArrayList<>();
        when(mockQueryValueProvider.query(paramsCaptor.capture(), filtersCaptor.capture()))
                .thenReturn(new DataRecords());
        Iterator<DataRecords> it = createIterator(params, filters, 3, mockQueryValueProvider);
        assertFalse(it.hasNext());
    }

    @Test
    public void testIteration_singleBatch() throws InvalidDataException, IOException {
        Map<String, Object> params = new HashMap<>();
        List<DataFilter> filters = new ArrayList<>();
        DataRecords result = TEST_RECORDS_1;
        when(mockQueryValueProvider.query(paramsCaptor.capture(), filtersCaptor.capture()))
                .thenReturn(result);
        Iterator<DataRecords> it = createIterator(params, filters, 3, mockQueryValueProvider);
        assertTrue(it.hasNext());
        assertEquals(result, it.next());
    }

    @Test
    public void testIteration_multipleBatches() throws InvalidDataException, IOException {
        Map<String, Object> params = new HashMap<>();
        List<DataFilter> filters = new ArrayList<>();
        when(mockQueryValueProvider.query(paramsCaptor.capture(), filtersCaptor.capture()))
                .thenReturn(TEST_RECORDS_1)
                .thenReturn(TEST_RECORDS_2);
        Iterator<DataRecords> it = createIterator(params, filters, 2, mockQueryValueProvider);
        assertTrue(it.hasNext());
        assertEquals(TEST_RECORDS_1, it.next());
        assertTrue(it.hasNext());
        KeyRangeDataFilter keyRangeDataFilter =
                filtersCaptor.getValue().stream()
                        .filter(f -> f instanceof KeyRangeDataFilter)
                        .map(f -> (KeyRangeDataFilter) f)
                        .findFirst()
                        .orElse(null);
        assertEquals(2L, ((Number) keyRangeDataFilter.getFromKey()).longValue());
        assertEquals(TEST_RECORDS_2, it.next());
    }

    @Test
    public void testIteration_error() throws InvalidDataException, IOException {
        Map<String, Object> params = new HashMap<>();
        List<DataFilter> filters = new ArrayList<>();
        when(mockQueryValueProvider.query(paramsCaptor.capture(), filtersCaptor.capture()))
                .thenReturn(TEST_RECORDS_1)
                .thenThrow(new RuntimeException("query error"));
        Iterator<DataRecords> it = createIterator(params, filters, 2, mockQueryValueProvider);
        assertTrue(it.hasNext());
        it.next();
        try {
            it.hasNext();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            // Expected.
        }
    }

    @Test
    public void testIteration_error_withRetry() throws InvalidDataException, IOException {
        Map<String, Object> params = new HashMap<>();
        List<DataFilter> filters = new ArrayList<>();
        when(mockQueryValueProvider.query(paramsCaptor.capture(), filtersCaptor.capture()))
                .thenReturn(TEST_RECORDS_1)
                .thenThrow(new RuntimeException("query error"))
                .thenReturn(TEST_RECORDS_2);
        Iterator<DataRecords> it = createIterator(params, filters, 2, mockQueryValueProvider);
        assertTrue(it.hasNext());
        assertEquals(TEST_RECORDS_1, it.next());
        try {
            it.hasNext();
            fail("Expected exception was not thrown");
        } catch (Exception ex) {
            // Expected.
        }
        assertTrue(it.hasNext());
        assertEquals(TEST_RECORDS_2, it.next());
    }

    protected abstract Iterator<DataRecords> createIterator(
            Map<String, Object> paramValues,
            List<DataFilter> filters,
            int batchSize,
            DataQueryValueProvider valueProvider);
}
