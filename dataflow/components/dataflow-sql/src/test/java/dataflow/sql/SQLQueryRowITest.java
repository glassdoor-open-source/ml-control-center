/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryRowITest.java
 */
package dataflow.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.google.common.collect.ImmutableMap;
import dataflow.core.type.IntegerValue;
import dataflow.data.DataRecord;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/** Integration tests for {@link SQLQueryRow} that use an embedded H2 instance. */
@RunWith(MockitoJUnitRunner.class)
public class SQLQueryRowITest extends SQLQueryValueProviderBaseITest {

    @Test
    public void getValue_singleRow() throws ExecutionException, InterruptedException {
        SQLQueryRow valueProvider = buildValueProvider(DEFAULT_KEY_COLUMN_NAMES);
        CompletableFuture<DataRecord> futureResult = valueProvider.getValue(TEST_INPUT1);

        DataRecord result = futureResult.get();
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_query() throws ExecutionException, InterruptedException {
        SQLQueryRow valueProvider =
                buildValueProvider("SELECT * FROM users", "postalcode = :postalcode");
        CompletableFuture<DataRecord> futureResult =
                valueProvider.getValue(
                        ImmutableMap.<String, Object>builder().put("postalcode", 94111).build());

        DataRecord result = futureResult.get();
        assertEquals(TEST_NAME2, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_concurrentRequests()
            throws ExecutionException, InterruptedException {
        SQLQueryRow valueProvider = buildValueProvider(DEFAULT_KEY_COLUMN_NAMES);
        CompletableFuture<DataRecord> futureResult1 = valueProvider.getValue(TEST_INPUT1);
        CompletableFuture<DataRecord> futureResult2 = valueProvider.getValue(TEST_INPUT2);

        DataRecord result1 = futureResult1.get();
        assertEquals(TEST_NAME, result1.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result1.get(POSTAL_CODE_COLUMN));
        DataRecord result2 = futureResult2.get();
        assertEquals(TEST_NAME2, result2.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result2.get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_singleRow_noResult() throws ExecutionException, InterruptedException {
        SQLQueryRow valueProvider = buildValueProvider(DEFAULT_KEY_COLUMN_NAMES);
        CompletableFuture<DataRecord> futureResult =
                valueProvider.getValue(TEST_INPUT_NON_EXISTENT_USER);
        assertNull(futureResult.get());
    }

    @Test
    public void getValue_singleRow_allColumns() throws ExecutionException, InterruptedException {
        SQLQueryRow valueProvider = buildValueProvider(DEFAULT_KEY_COLUMN_NAMES);
        CompletableFuture<DataRecord> futureResult = valueProvider.getValue(TEST_INPUT1);
        DataRecord result = futureResult.get();
        assertEquals(1, result.get(USER_ID_COLUMN));
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    /** Tests that {@link IntegerValue} is converted to integer using the . */
    @Test
    public void getValue_integerValueKeyValue() throws ExecutionException, InterruptedException {
        SQLQueryRow valueProvider = buildValueProvider(DEFAULT_KEY_COLUMN_NAMES);
        CompletableFuture<DataRecord> futureResult =
                valueProvider.getValue(
                        ImmutableMap.<String, Object>builder()
                                .put(USER_ID_COLUMN, new IntegerValue(1))
                                .build());

        DataRecord result = futureResult.get();
        assertEquals(TEST_NAME, result.get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(POSTAL_CODE_COLUMN));
    }

    static SQLQueryRow buildValueProvider(String baseQuery, String whereClause) {
        return new SQLQueryRow(
                baseQuery,
                whereClause,
                null,
                DEFAULT_KEY_COLUMN_NAMES,
                null,
                null,
                LAST_UPDATED_COLUMN,
                null,
                null,
                null,
                null,
                null,
                null,
                null);
    }

    static SQLQueryRow buildValueProvider(List<String> keyColNames) {
        return new SQLQueryRow(
                "SELECT * FROM users",
                "user_id = :user_id",
                null,
                keyColNames,
                null,
                "user_id ASC",
                LAST_UPDATED_COLUMN,
                null,
                null,
                null,
                null,
                null,
                null,
                null);
    }
}
