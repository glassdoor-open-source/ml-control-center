/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryStreamITest.java
 */
package dataflow.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/** Integration tests for {@link SQLQueryRow} that use an embedded H2 instance. */
@RunWith(MockitoJUnitRunner.class)
public class SQLQueryStreamITest extends SQLQueryValueProviderBaseITest {

    @Test
    public void getValue_batch_full() throws ExecutionException, InterruptedException {
        SQLQueryStream valueProvider =
                buildValueProvider("SELECT * from users", null, "user_id ASC");

        CompletableFuture<Stream<DataRecords>> futureResult =
                valueProvider.getValue(Collections.emptyMap());
        Stream<DataRecords> result = futureResult.get();
        List<DataRecord> rows =
                result.flatMap((Function<DataRecords, Stream<DataRecord>>) DataRecords::stream)
                        .collect(Collectors.toList());

        int fromKey = Integer.MIN_VALUE;
        assertEquals(15, rows.size());
        for (DataRecord row : rows) {
            int userId = (int) row.get(USER_ID_COLUMN);
            assertTrue(fromKey < userId);
            fromKey = userId;
        }

        // Attempt to close stream, which closes transaction.
        result.close();
    }

    @Test
    public void getValue_preQueryStatement() throws ExecutionException, InterruptedException {
        List<String> preQueryStmts = Collections.singletonList("SET @seed=.5");
        SQLQueryStream valueProvider =
                new SQLQueryStream(
                        "SELECT random(@seed) as VALUE",
                        null,
                        null,
                        preQueryStmts,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
        CompletableFuture<Stream<DataRecords>> futureResult =
                valueProvider.getValue(Collections.emptyMap());

        Stream<DataRecords> stream = futureResult.get();
        DataRecords result = stream.toList().get(0);

        assertEquals(1, result.size());
        assertEquals(1, result.getColumnNames().size());
        assertEquals(0.7308781907032909, result.get(0).get("VALUE"));

        // Attempt to close stream, which closes transaction.
        stream.close();
    }

    static SQLQueryStream buildValueProvider(
            String baseQuery, String whereClause, String orderByClause) {
        return new SQLQueryStream(
                baseQuery,
                null,
                whereClause,
                null,
                DEFAULT_KEY_COLUMN_NAMES,
                null,
                orderByClause,
                LAST_UPDATED_COLUMN,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);
    }
}
