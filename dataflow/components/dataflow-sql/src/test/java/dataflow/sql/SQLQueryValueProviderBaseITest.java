/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryValueProviderBaseITest.java
 */
package dataflow.sql;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.data.DataRecord;
import java.util.List;
import java.util.Set;
import org.junit.After;
import org.junit.Before;
import org.mockito.Mock;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

/** Integration tests for {@link SQLQueryValueProvider} that use an embedded H2 instance. */
public class SQLQueryValueProviderBaseITest {

    protected static final String USER_ID_COLUMN = "user_id";
    protected static final String NAME_COLUMN = "name";
    static final String POSTAL_CODE_COLUMN = "postalcode";
    protected static final String LAST_UPDATED_COLUMN = "lastUpdated";

    static final Set<String> ALL_COLUMN_NAMES =
            ImmutableSet.of(USER_ID_COLUMN, NAME_COLUMN, POSTAL_CODE_COLUMN, LAST_UPDATED_COLUMN);

    protected static final List<String> DEFAULT_KEY_COLUMN_NAMES = ImmutableList.of(USER_ID_COLUMN);

    static final String TEST_NAME = "John";
    static final Integer TEST_POSTAL_CODE = 94941;
    static final String TEST_NAME2 = "Jim";
    static final Integer TEST_POSTAL_CODE2 = 94111;

    static final DataRecord TEST_INPUT1 =
            new DataRecord(ImmutableMap.<String, Object>builder().put(USER_ID_COLUMN, 1).build());
    static final DataRecord TEST_INPUT2 =
            new DataRecord(ImmutableMap.<String, Object>builder().put(USER_ID_COLUMN, 2).build());
    static final DataRecord TEST_INPUT_NON_EXISTENT_USER =
            new DataRecord(
                    ImmutableMap.<String, Object>builder().put(USER_ID_COLUMN, 1001).build());

    private EmbeddedDatabase db;

    @Mock private DataFlowInstance mockInstance;

    @Before
    public void setUp() {
        db =
                new EmbeddedDatabaseBuilder()
                        .setType(EmbeddedDatabaseType.H2)
                        .addScript("schema.sql")
                        .addScript("test-data.sql")
                        .build();
        SQLDatabaseManager dbManager = new SQLDatabaseManager();
        dbManager.registerDataSource("test", db);

        DataFlowEnvironment env =
                new DataFlowEnvironment("test", new SimpleDependencyInjector(dbManager), null);
        DataFlowExecutionContext executionContext =
                DataFlowExecutionContext.createExecutionContext(env);
        executionContext.setInstance(mockInstance);
    }

    @After
    public void cleanUp() {
        db.shutdown();
        DataFlowExecutionContext.setCurrentExecutionContext(null);
    }
}
