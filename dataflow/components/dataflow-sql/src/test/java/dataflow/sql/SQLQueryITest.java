/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryITest.java
 */
package dataflow.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/** Integration tests for {@link SQLQueryRow} that use an embedded H2 instance. */
@RunWith(MockitoJUnitRunner.class)
public class SQLQueryITest extends SQLQueryValueProviderBaseITest {

    @Test
    public void getValue_multiRow() throws ExecutionException, InterruptedException {
        SQLQuery valueProvider = buildValueProvider("SELECT * from users", null);
        CompletableFuture<DataRecords> futureResult =
                valueProvider.getValue(Collections.emptyMap());

        DataRecords result = futureResult.get();
        assertEquals(15, result.size());
        assertEquals(1, result.getKeyColumnNames().size());
        assertEquals(USER_ID_COLUMN, result.getKeyColumnNames().get(0));
        assertEquals(4, result.getColumnNames().size());
        ALL_COLUMN_NAMES.forEach(
                colName -> assertTrue("Missing column " + colName, result.hasColumn(colName)));

        assertEquals(TEST_NAME, result.get(0).get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE, result.get(0).get(POSTAL_CODE_COLUMN));
        assertEquals(TEST_NAME2, result.get(1).get(NAME_COLUMN));
        assertEquals(TEST_POSTAL_CODE2, result.get(1).get(POSTAL_CODE_COLUMN));
    }

    @Test
    public void getValue_multiRow_noResult() throws ExecutionException, InterruptedException {
        SQLQuery valueProvider = buildValueProvider("SELECT * from users", "user_id = 1001");
        CompletableFuture<DataRecords> futureResult =
                valueProvider.getValue(Collections.emptyMap());
        DataRecords result = futureResult.get();
        assertEquals(0, result.size());
    }

    @Test
    public void getValue_missingKeyColumn() throws InterruptedException {
        SQLQuery valueProvider = buildValueProvider("SELECT name,postalcode FROM users", null);
        CompletableFuture<DataRecords> futureResult =
                valueProvider.getValue(Collections.emptyMap());

        try {
            futureResult.get();
            fail("expected exception was not thrown");
        } catch (ExecutionException ex) {
            // Expected.
            assertTrue(ex.getCause() instanceof InvalidDataException);
        }
    }

    @Test
    public void getValue_preQueryStatement() throws ExecutionException, InterruptedException {
        List<String> preQueryStmts = Collections.singletonList("SET @seed=.5");
        SQLQuery valueProvider =
                new SQLQuery(
                        "SELECT random(@seed) as VALUE",
                        null,
                        null,
                        null,
                        null,
                        null,
                        preQueryStmts,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
        CompletableFuture<DataRecords> futureResult =
                valueProvider.getValue(Collections.emptyMap());

        DataRecords result = futureResult.get();
        assertEquals(1, result.size());
        assertEquals(1, result.getColumnNames().size());

        assertEquals(0.7308781907032909, result.get(0).get("VALUE"));
    }

    static SQLQuery buildValueProvider(String baseQuery, String whereClause) {
        return buildValueProvider(baseQuery, whereClause, null);
    }

    static SQLQuery buildValueProvider(
            String baseQuery, String whereClause, List<String> prequeryStatements) {
        return new SQLQuery(
                baseQuery,
                null,
                whereClause,
                DEFAULT_KEY_COLUMN_NAMES,
                null,
                null,
                prequeryStatements,
                LAST_UPDATED_COLUMN,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);
    }
}
