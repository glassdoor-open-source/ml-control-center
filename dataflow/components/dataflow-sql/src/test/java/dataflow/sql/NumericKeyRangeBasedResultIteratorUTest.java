/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  NumericKeyRangeBasedResultIteratorUTest.java
 */
package dataflow.sql;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import dataflow.data.DataRecords;
import dataflow.data.query.DataFilter;
import dataflow.data.query.DataQueryValueProvider;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@RunWith(MockitoJUnitRunner.class)
public class NumericKeyRangeBasedResultIteratorUTest extends AbstractResultIteratorUTest {

    private static final String TEST_SELECT_MAX_ID_QUERY = "SELECT MAX(id)";

    @Mock private NamedParameterJdbcTemplate mockJdbcTemplate;

    @Captor private ArgumentCaptor<ResultSetExtractor<Long>> resultsExtractor;

    @Before
    public void setUp() {
        when(mockJdbcTemplate.query(eq(TEST_SELECT_MAX_ID_QUERY), resultsExtractor.capture()))
                .thenAnswer((Answer<Long>) invocationOnMock -> 10L);
    }

    @Override
    protected Iterator<DataRecords> createIterator(
            Map<String, Object> paramValues,
            List<DataFilter> filters,
            int batchSize,
            DataQueryValueProvider valueProvider) {
        return new NumericKeyRangeBasedResultIterator(
                paramValues,
                filters,
                0L,
                batchSize,
                TEST_SELECT_MAX_ID_QUERY,
                mockJdbcTemplate,
                valueProvider);
    }
}
