/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLDataQueryValueProviderITest.java
 */
package dataflow.sql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.ImmutableList;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.DataQueryValueProvider;
import dataflow.data.query.KeyRangeDataFilter;
import dataflow.data.query.LastUpdateTimeFilter;
import dataflow.data.query.MaxRowCountDataFilter;
import dataflow.data.query.MultiKeyDataFilter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/** Integration tests for {@link SQLQueryRow} that use an embedded H2 instance. */
@RunWith(MockitoJUnitRunner.class)
public class SQLDataQueryValueProviderITest extends SQLQueryValueProviderBaseITest {

    @Test
    public void getEstimatedRowCount() {
        DataQueryValueProvider valueProvider = buildValueProvider("SELECT * from users");
        Optional<Long> count =
                valueProvider.getEstimatedResultCount(Collections.emptyMap(), ImmutableList.of());
        assertEquals(15, count.get().intValue());
    }

    @Test
    public void getValue_multiRow_withMaxRowCount() throws InvalidDataException, IOException {
        DataQueryValueProvider valueProvider = buildValueProvider("SELECT * from users");

        DataRecords result =
                valueProvider.query(
                        Collections.emptyMap(),
                        ImmutableList.of(
                                new KeyRangeDataFilter(3, false, null, false),
                                new MaxRowCountDataFilter(10)));
        assertEquals(10, result.size());
        // Verify that the results are returned by user id.
        Integer userId = 3;
        for (DataRecord record : result) {
            assertTrue((Integer) record.get(USER_ID_COLUMN) > userId);
            userId = (Integer) record.get(USER_ID_COLUMN);
        }
    }

    @Test
    public void testQueryForStream_multiRow_withMaxRowCount() throws IOException {
        DataQueryValueProvider valueProvider = buildValueProvider("SELECT * from users");

        Stream<DataRecords> stream =
                valueProvider.queryForStream(
                        Collections.emptyMap(),
                        ImmutableList.of(
                                new KeyRangeDataFilter(3, false, null, false),
                                new MaxRowCountDataFilter(10)),
                        3);
        List<DataRecord> results = stream.flatMap(DataRecords::stream).collect(Collectors.toList());
        assertEquals(10, results.size());
        // Verify that the results are returned by user id.
        Integer userId = 3;
        for (DataRecord record : results) {
            assertTrue((Integer) record.get(USER_ID_COLUMN) > userId);
            userId = (Integer) record.get(USER_ID_COLUMN);
        }
    }

    @Test
    public void getValue_specificIds() throws InvalidDataException, IOException {
        DataQueryValueProvider valueProvider = buildValueProvider("SELECT * from users");

        DataRecords result =
                valueProvider.query(
                        Collections.emptyMap(),
                        ImmutableList.of(new MultiKeyDataFilter(ImmutableList.of(3, 5, 7, 9))));
        assertEquals(4, result.size());
        assertEquals("Heather", result.get(0).get(NAME_COLUMN));
        assertEquals("Michelle", result.get(1).get(NAME_COLUMN));
        assertEquals("Cynthia", result.get(2).get(NAME_COLUMN));
        assertEquals("Daniel", result.get(3).get(NAME_COLUMN));
    }

    @Test
    public void getValue_incremental() throws ParseException, InvalidDataException, IOException {
        DataQueryValueProvider valueProvider = buildValueProvider("SELECT * from users");

        Date lastUpdateTime = new SimpleDateFormat("dd-MMM-yyyy").parse("01-jan-2015");
        DataRecords result =
                valueProvider.query(
                        Collections.emptyMap(),
                        ImmutableList.of(new LastUpdateTimeFilter(lastUpdateTime)));
        assertEquals(3, result.size());
        assertEquals("Jeremy", result.get(0).get(NAME_COLUMN));
        assertEquals("Harry", result.get(1).get(NAME_COLUMN));
        assertEquals("Jason", result.get(2).get(NAME_COLUMN));
    }

    static SQLDataQueryValueProvider buildValueProvider(String baseQuery) {
        return new SQLDataQueryValueProvider(
                baseQuery,
                null,
                null,
                DEFAULT_KEY_COLUMN_NAMES,
                null,
                null,
                null,
                LAST_UPDATED_COLUMN,
                null,
                null,
                null,
                null,
                null,
                1,
                null,
                null,
                null,
                null,
                null,
                null);
    }
}
