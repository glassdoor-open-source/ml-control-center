/*
 *
 *  * Copyright 2021-2024 Glassdoor, Inc.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 *
 */

/*
 * Copyright 2021-2022 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  test-data.sql
 */

INSERT INTO users VALUES (1, 'John', 94941, '2012-09-17 18:47:52');
INSERT INTO users VALUES (2, 'Jim', 94111, '2012-09-17 18:47:52');
INSERT INTO users VALUES (3, 'Heather', 95050, '2012-09-17 18:47:52');
INSERT INTO users VALUES (4, 'Terry', 19355, '2012-09-17 18:47:52');
INSERT INTO users VALUES (5, 'Michelle', 30701, '2012-09-17 18:47:52');
INSERT INTO users VALUES (6, 'Sara', 19438, '2012-09-17 18:47:52');
INSERT INTO users VALUES (7, 'Cynthia', 70806, '2012-09-17 18:47:52');
INSERT INTO users VALUES (8, 'Jeremy', 17011, '2015-09-17 18:47:52');
INSERT INTO users VALUES (9, 'Daniel', 60120, '2012-09-17 18:47:52');
INSERT INTO users VALUES (10, 'Raymond', 36605, '2012-09-17 18:47:52');
INSERT INTO users VALUES (11, 'Harry', 85365, '2015-09-17 18:47:52');
INSERT INTO users VALUES (12, 'Denise', 19082, '2012-09-17 18:47:52');
INSERT INTO users VALUES (13, 'Diane', 32703, '2012-09-17 18:47:52');
INSERT INTO users VALUES (14, 'Melissa', 11779, '2012-09-17 18:47:52');
INSERT INTO users VALUES (15, 'Jason', 37343, '2015-09-17 18:47:52');