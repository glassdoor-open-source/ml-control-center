/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  KeysFilterQueryDecorator.java
 */
package dataflow.sql;

import com.google.common.base.Joiner;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeysFilterQueryDecorator implements SQLQueryDecorator {

    private static final String IN_KV_PARAM_NAME_PREFIX = "__KV";

    private List<?> keys;

    @DataFlowConfigurable
    public KeysFilterQueryDecorator(@DataFlowConfigProperty List<?> keys) {
        this.keys = keys;
    }

    @Override
    public void apply(SQLQueryInfo queryInfo) {
        queryInfo.getWhereClauses().addAll(getWhereClauses(queryInfo));
        queryInfo.getParamValues().putAll(getParamValues(queryInfo));
    }

    public void setKeys(List<?> keys) {
        this.keys = keys;
    }

    private List<String> getWhereClauses(SQLQueryInfo queryInfo) {
        List<String> keyColumns = queryInfo.getKeyColumns();
        if (keyColumns == null || keyColumns.isEmpty()) {
            throw new DataFlowConfigurationException(
                    "Key columns must be specified with the keys filter");
        }
        StringBuilder inKeys = new StringBuilder();
        inKeys.append("(");
        inKeys.append(Joiner.on(", ").join(keyColumns));
        inKeys.append(")");
        inKeys.append(" IN (");
        for (int keyNum = 1; keyNum <= keys.size(); keyNum++) {
            if (keyNum > 1) {
                inKeys.append(", ");
            }
            inKeys.append("(");
            for (int keyPartNum = 1; keyPartNum <= keyColumns.size(); keyPartNum++) {
                if (keyPartNum > 1) {
                    inKeys.append(", ");
                }
                inKeys.append(
                        String.format(":%s_%d_%d", IN_KV_PARAM_NAME_PREFIX, keyNum, keyPartNum));
            }
            inKeys.append(")");
        }
        inKeys.append(")");
        return Collections.singletonList(inKeys.toString());
    }

    private Map<String, Object> getParamValues(SQLQueryInfo queryInfo) {
        List<String> keyColumns = queryInfo.getKeyColumns();
        Map<String, Object> params = new HashMap<>();
        if (keys != null && !keys.isEmpty() && keyColumns != null) {
            for (int keyValNum = 1; keyValNum <= keys.size(); keyValNum++) {
                Object key = keys.get(keyValNum - 1);
                for (int keyColNum = 1; keyColNum <= keyColumns.size(); keyColNum++) {
                    params.put(
                            String.format(
                                    "%s_%d_%d", IN_KV_PARAM_NAME_PREFIX, keyValNum, keyColNum),
                            SQLDataQueryValueProvider.getKeyColumnValue(key, keyColNum - 1));
                }
            }
        }
        return params;
    }
}
