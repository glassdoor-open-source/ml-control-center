/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LastUpdateTimeFilterQueryDecorator.java
 */
package dataflow.sql;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.exception.DataFlowConfigurationException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class LastUpdateTimeFilterQueryDecorator implements SQLQueryDecorator {

    private static final String LAST_UPDATED_TIME_PARAM_NAME = "__LAST_UPDATED_TIME";

    private Date lastUpdateTime;

    @DataFlowConfigurable
    public LastUpdateTimeFilterQueryDecorator(@DataFlowConfigProperty Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    @Override
    public void apply(SQLQueryInfo queryInfo) {
        queryInfo.getWhereClauses().addAll(getWhereClauses(queryInfo));
        queryInfo.getParamValues().putAll(getParamValues());
    }

    public void setUpdatedSinceDate(Date updatedSinceTime) {
        this.lastUpdateTime = updatedSinceTime;
    }

    private List<String> getWhereClauses(SQLQueryInfo queryInfo) {
        String lastUpdatedTimeColumn = queryInfo.getLastUpdateTimeColumn();
        if (lastUpdatedTimeColumn == null) {
            throw new DataFlowConfigurationException(
                    "lastUpdatedTimeColumn must be specified in order to filter by last update time");
        }
        return Collections.singletonList(
                lastUpdatedTimeColumn + " >= :" + LAST_UPDATED_TIME_PARAM_NAME);
    }

    private Map<String, Object> getParamValues() {
        return Collections.singletonMap(
                LAST_UPDATED_TIME_PARAM_NAME, new Timestamp(lastUpdateTime.getTime()));
    }
}
