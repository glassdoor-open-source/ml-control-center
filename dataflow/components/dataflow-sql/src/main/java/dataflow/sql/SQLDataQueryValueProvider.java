/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLDataQueryValueProvider.java
 */
package dataflow.sql;

import static dataflow.core.util.MetricsUtil.getMetricName;
import static dataflow.core.util.MetricsUtil.tagsToStringArray;
import static dataflow.retry.Retry.executeWithRetry;
import static dataflow.sql.SQLDatabaseIterationMode.KEY_BASED;
import static dataflow.sql.SQLDatabaseIterationMode.NUMERIC_KEY_RANGE_BASED;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Iterators;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.type.WrapperUtil;
import dataflow.core.util.StringUtil;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.DataFilter;
import dataflow.data.query.DataQueryValueProvider;
import dataflow.data.query.KeyRangeDataFilter;
import dataflow.data.query.LastUpdateTimeFilter;
import dataflow.data.query.MaxRowCountDataFilter;
import dataflow.data.query.MultiKeyDataFilter;
import dataflow.retry.Retry;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

public class SQLDataQueryValueProvider implements DataQueryValueProvider {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final int DEFAULT_FETCH_SIZE = 100;
    private static final Pattern SELECT_PATTERN = Pattern.compile("(?i)(?s)SELECT (.+?) FROM .*");

    private String baseQuery;
    private String countBaseQuery;
    private final String whereClause;
    private final String orderByClause;
    private final List<String> preQueryStatements;
    private final List<String> keyColumns;
    private final List<String> recordsKeyColumns;
    private final String lastUpdateTimeColumn;
    private final List<SQLQueryDecorator> queryDecorators;
    protected final Retry retry;
    private final int fetchSize;
    protected final int batchSize;
    private final Integer maxRowCount;
    private boolean orderByKeyIfNotSpecified = false;
    private boolean requireOrderBy = false;
    private final SQLDatabaseIterationMode iterationMode;
    private final String maxKeyQuery;
    private final Object fromKey;

    private Counter queryCounter;
    private Timer queryTimer;
    private Counter errorCounter;

    protected final TransactionTemplate transactionTemplate;
    protected final PlatformTransactionManager txManager;
    protected final NamedParameterJdbcTemplate jdbcTemplate;
    protected static final ExecutorService queryStreamThreadPool = Executors.newCachedThreadPool();

    public SQLDataQueryValueProvider(
            String baseQuery,
            String countBaseQuery,
            String whereClause,
            List<String> keyColumns,
            List<String> recordsKeyColumns,
            String orderByClause,
            List<String> preQueryStatements,
            String lastUpdateTimeColumn,
            List<SQLQueryDecorator> decorators,
            String clusterId,
            Object fromKey,
            Integer fetchSize,
            Integer maxRowCount,
            Integer batchSize,
            DataSource dataSource,
            SQLDatabaseIterationMode iterationMode,
            String maxKeyQuery,
            Retry retry,
            String metricNamePrefix,
            Map<String, String> metricTags) {
        DataFlowExecutionContext executionContext =
                DataFlowExecutionContext.getCurrentExecutionContext();
        DataFlowInstance instance =
                executionContext != null ? executionContext.getInstance() : null;
        this.keyColumns =
                keyColumns != null ? new ArrayList<>(keyColumns) : Collections.emptyList();
        this.recordsKeyColumns = recordsKeyColumns != null ? recordsKeyColumns : this.keyColumns;
        this.whereClause = whereClause;
        this.orderByClause = orderByClause;
        this.preQueryStatements =
                preQueryStatements != null
                        ? new ArrayList<>(preQueryStatements)
                        : Collections.emptyList();
        this.lastUpdateTimeColumn = lastUpdateTimeColumn;
        this.iterationMode = iterationMode;
        if (iterationMode == KEY_BASED && Strings.isNullOrEmpty(orderByClause)) {
            throw new DataFlowConfigurationException(
                    "orderByClause is required for " + KEY_BASED + " iteration");
        }
        if (iterationMode == NUMERIC_KEY_RANGE_BASED) {
            if (fromKey == null) {
                throw new DataFlowConfigurationException(
                        "fromKey is required for " + NUMERIC_KEY_RANGE_BASED + " iteration");
            }
            if (fromKey instanceof Integer) {
                fromKey = ((Integer) fromKey).longValue();
            }
            if (!(fromKey instanceof Long)) {
                throw new DataFlowConfigurationException(
                        "fromKey must be an integer or long value for "
                                + NUMERIC_KEY_RANGE_BASED
                                + " iteration");
            }
            if (maxKeyQuery == null) {
                throw new DataFlowConfigurationException(
                        "maxKeyQuery is required for " + NUMERIC_KEY_RANGE_BASED + " iteration");
            }
        }
        this.fromKey = fromKey;
        this.maxKeyQuery = maxKeyQuery;
        this.queryDecorators = decorators != null ? new ArrayList<>(decorators) : new ArrayList<>();

        if (retry == null) {
            retry = executionContext.getDependencyInjector().getInstance(Retry.class);
        }

        this.retry = Retry.clone(retry);
        if (dataSource == null) {
            SQLDatabaseManager dbManager =
                    executionContext.getDependencyInjector().getInstance(SQLDatabaseManager.class);
            if (dbManager == null) {
                throw new DataFlowConfigurationException(
                        "Unable to inject cluster manager. "
                                + "Please verify that the dependency injector is configured properly.");
            }
            dataSource =
                    (clusterId != null)
                            ? dbManager.getDataSource(clusterId)
                            : dbManager.getDefaultDataSource();
        }

        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.txManager = new DataSourceTransactionManager(dataSource);
        this.transactionTemplate = new TransactionTemplate(this.txManager);

        if (fetchSize == null) {
            fetchSize = DEFAULT_FETCH_SIZE;
        }
        this.fetchSize = fetchSize;
        jdbcTemplate.getJdbcTemplate().setFetchSize(fetchSize);

        this.batchSize = batchSize != null ? batchSize : 1;
        this.maxRowCount = maxRowCount;
        if (maxRowCount != null) {
            jdbcTemplate.getJdbcTemplate().setMaxRows(maxRowCount);
        }
        this.baseQuery = baseQuery;
        this.countBaseQuery = countBaseQuery;

        MeterRegistry meterRegistry =
                executionContext != null
                        ? executionContext.getDependencyInjector().getInstance(MeterRegistry.class)
                        : null;
        if (meterRegistry != null) {
            Map<String, String> tags = new HashMap<>();
            if (instance != null) {
                tags.putAll(instance.getMetricTagsForComponent(this));
            }
            if (clusterId != null) {
                tags.put("clusterId", clusterId);
            }
            if (metricTags != null) {
                tags.putAll(metricTags);
            }
            metricNamePrefix =
                    metricNamePrefix != null ? metricNamePrefix : getClass().getSimpleName();
            queryCounter =
                    meterRegistry.counter(
                            getMetricName(metricNamePrefix, "queryCounter", instance),
                            tagsToStringArray(tags));
            queryTimer =
                    meterRegistry.timer(
                            getMetricName(metricNamePrefix, "queryTimer", instance),
                            tagsToStringArray(tags));
            errorCounter =
                    meterRegistry.counter(
                            getMetricName(metricNamePrefix, "errorCounter", instance),
                            tagsToStringArray(tags));
            Counter retryCounter =
                    meterRegistry.counter(
                            getMetricName(metricNamePrefix, "retryCounter", instance),
                            tagsToStringArray(tags));
            if (this.retry != null) {
                this.retry.getEventPublisher().onRetry(event -> retryCounter.increment());
            }
        }
    }

    @Override
    public DataRecords query(Map<String, Object> paramValues, List<DataFilter> filters)
            throws InvalidDataException, IOException {
        long startTime = System.currentTimeMillis();
        List<SQLQueryDecorator> filterDecorators = getQueryDecoratorsForFilters(filters);
        Map<String, ?> unwrappedInputValues = WrapperUtil.unwrapMapValues(paramValues);
        SQLQueryInfo queryInfo = buildQueryInfo(unwrappedInputValues, filterDecorators);
        String query = buildQuery(queryInfo);
        MapSqlParameterSource paramSource = buildParamSource(queryInfo.getParamValues());

        if (queryCounter != null) {
            queryCounter.increment();
        }

        List<Map<String, Object>> results;
        try {
            logger.debug("Running SQL Query: {}", query);
            results =
                    executeWithRetry(
                            retry,
                            () ->
                                    transactionTemplate.execute(
                                            status -> {
                                                for (String preQueryStmt :
                                                        queryInfo.getPreQueryStatements()) {
                                                    jdbcTemplate.execute(
                                                            preQueryStmt,
                                                            paramSource,
                                                            PreparedStatement::execute);
                                                }
                                                return jdbcTemplate.queryForList(
                                                        query, paramSource);
                                            }));

            if (queryTimer != null) {
                queryTimer.record(System.currentTimeMillis() - startTime, TimeUnit.MILLISECONDS);
            }
        } catch (Exception e) {
            if (errorCounter != null) {
                errorCounter.increment();
            }
            throw new IOException("Error executing query: " + query, e);
        }
        DataRecords records = createDataRecords(results);
        if (!records.validateKeyColumns()) {
            if (errorCounter != null) {
                errorCounter.increment();
            }
            throw new InvalidDataException(
                    String.format(
                            "Fetched records are missing key column(s) %s: %s",
                            keyColumns, records));
        }
        return records;
    }

    public Stream<DataRecords> queryForStream(Map<String, Object> paramValues) throws IOException {
        return queryForStream(paramValues, Collections.emptyList(), batchSize);
    }

    @Override
    public Stream<DataRecords> queryForStream(
            Map<String, Object> paramValues, List<DataFilter> filters, int batchSize) {
        if (iterationMode == KEY_BASED) {
            return StreamSupport.stream(
                    Spliterators.spliteratorUnknownSize(
                            new KeyBasedResultIterator(
                                    paramValues, filters, fromKey, batchSize, this),
                            Spliterator.ORDERED),
                    false);
        } else if (iterationMode == NUMERIC_KEY_RANGE_BASED) {
            return StreamSupport.stream(
                    Spliterators.spliteratorUnknownSize(
                            new NumericKeyRangeBasedResultIterator(
                                    paramValues,
                                    filters,
                                    (Long) fromKey,
                                    fetchSize,
                                    maxKeyQuery,
                                    jdbcTemplate,
                                    this),
                            Spliterator.ORDERED),
                    false);
        }

        // Iterate using a ResultSet.
        if (queryCounter != null) {
            queryCounter.increment();
        }
        List<SQLQueryDecorator> filterDecorators = getQueryDecoratorsForFilters(filters);
        Map<String, ?> unwrappedInputValues = WrapperUtil.unwrapMapValues(paramValues);
        SQLQueryInfo queryInfo = buildQueryInfo(unwrappedInputValues, filterDecorators);
        String query = buildQuery(queryInfo);
        MapSqlParameterSource paramSource = buildParamSource(queryInfo.getParamValues());
        logger.debug("Running SQL Query: {}", query);

        Lock lock = new ReentrantLock();

        AtomicReference<Object> returnValue = new AtomicReference<>();
        Condition returnValueCondition = lock.newCondition();

        AtomicReference<Boolean> closed = new AtomicReference<>();
        Condition closedCondition = lock.newCondition();

        queryStreamThreadPool.submit(
                () -> {
                    transactionTemplate.execute(
                            status -> {
                                try {
                                    for (String preQueryStmt : queryInfo.getPreQueryStatements()) {
                                        jdbcTemplate.execute(
                                                preQueryStmt,
                                                paramSource,
                                                PreparedStatement::execute);
                                    }

                                    Stream<Map<String, Object>> resultStream =
                                            jdbcTemplate.queryForStream(
                                                    query, paramSource, new ColumnMapRowMapper());

                                    returnValue.set(
                                            StreamSupport.stream(
                                                            Spliterators.spliteratorUnknownSize(
                                                                    Iterators.partition(
                                                                            resultStream.iterator(),
                                                                            batchSize),
                                                                    Spliterator.ORDERED),
                                                            false)
                                                    .map(
                                                            SQLDataQueryValueProvider.this
                                                                    ::createDataRecords)
                                                    .onClose(
                                                            () -> {
                                                                // When the stream is closed, close
                                                                // the jdbc result stream and signal
                                                                // that the stream is closed.
                                                                try {
                                                                    resultStream.close();
                                                                    closed.set(true);
                                                                } finally {
                                                                    signal(lock, closedCondition);
                                                                }
                                                            }));
                                    // Signal that the return value is ready.
                                    signal(lock, returnValueCondition);

                                    // Have this thread wait until the stream is closed.
                                    waitFor(lock, closedCondition, closed);
                                } catch (Throwable e) {
                                    if (errorCounter != null) {
                                        errorCounter.increment();
                                    }
                                    logger.warn("Error executing query: " + query, e);
                                    returnValue.set(e);
                                    // Signal that the return value is ready.
                                    signal(lock, returnValueCondition);
                                }
                                return null;
                            });
                });

        try {
            Object retVal = waitFor(lock, returnValueCondition, returnValue);
            if (retVal instanceof Throwable) {
                throw new RuntimeException((Throwable) retVal);
            }
            return (Stream<DataRecords>) retVal;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> getKeyColumns() {
        return keyColumns;
    }

    private <V> V waitFor(Lock lock, Condition condition, AtomicReference<V> ref)
            throws InterruptedException {
        V result = ref.get();
        long startTime = System.currentTimeMillis();
        while (result == null) {
            lock.lock();
            try {
                if (!condition.await(5, TimeUnit.MINUTES)) {
                    long waitTimeMinutes = (System.currentTimeMillis() - startTime) / 1000 / 60;
                    logger.warn(
                            "Waiting for sql result stream to be closed. "
                                    + "waitTime="
                                    + waitTimeMinutes
                                    + " min; "
                                    + SQLDataQueryValueProvider.this.getClass().getSimpleName()
                                    + ":"
                                    + System.identityHashCode(SQLDataQueryValueProvider.this));
                }
                result = ref.get();
            } finally {
                lock.unlock();
            }
        }
        return result;
    }

    private void signal(Lock lock, Condition condition) {
        lock.lock();
        try {
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Optional<Long> getEstimatedResultCount(
            Map<String, Object> paramValues, List<DataFilter> filters) {
        if (countBaseQuery != null && countBaseQuery.isEmpty()) {
            // Count support has been explicitly disabled.
            return Optional.empty();
        }
        try {
            List<SQLQueryDecorator> filterDecorators = getQueryDecoratorsForFilters(filters);
            Map<String, ?> unwrappedInputValues = WrapperUtil.unwrapMapValues(paramValues);
            SQLQueryInfo queryInfo = buildQueryInfo(unwrappedInputValues, filterDecorators);
            if (countBaseQuery != null) {
                queryInfo.setBaseQuery(countBaseQuery);
            } else {
                // Try to build a query to select count from the base query. This may not work in
                // all
                // cases.
                Matcher matcher = SELECT_PATTERN.matcher(baseQuery);
                if (matcher.matches()) {
                    queryInfo.setBaseQuery(
                            String.format(
                                    "SELECT COUNT(*) %s", baseQuery.substring(matcher.end(1))));
                } else {
                    return Optional.empty();
                }
            }
            // Ordering is not needed for a count query.
            queryInfo.setOrderByClause(null);
            boolean ignoreRequireOverride = true;
            String query = buildQuery(queryInfo, ignoreRequireOverride);
            MapSqlParameterSource paramSource = buildParamSource(queryInfo.getParamValues());
            Long count = jdbcTemplate.queryForObject(query, paramSource, Long.class);
            if (count != null && count >= 0) {
                if (countBaseQuery == null) {
                    // We were able successfully get a count we can use this baseQuery in the
                    // future.
                    countBaseQuery = queryInfo.getBaseQuery();
                }
                return Optional.of(count);
            }
        } catch (Exception ex) {
            logger.warn("Unable to get estimated result count", ex);
        }
        return Optional.empty();
    }

    public void setOrderByKeyIfNotSpecified(boolean orderByKeyIfNotSpecified) {
        this.orderByKeyIfNotSpecified = orderByKeyIfNotSpecified;
    }

    public void setRequireOrderBy(boolean requireOrderBy) {
        this.requireOrderBy = requireOrderBy;
    }

    public void setBaseQuery(String baseQuery) {
        this.baseQuery = baseQuery;
    }

    public boolean isFilterSupported(DataFilter filter) {
        if (filter.getClass() == MultiKeyDataFilter.class
                || filter.getClass() == KeyRangeDataFilter.class
                || filter.getClass() == MaxRowCountDataFilter.class) {
            return true;
        } else if (filter.getClass() == LastUpdateTimeFilter.class
                && lastUpdateTimeColumn != null) {
            return true;
        }
        return false;
    }

    private List<SQLQueryDecorator> getQueryDecoratorsForFilters(List<DataFilter> filters) {
        jdbcTemplate.getJdbcTemplate().setMaxRows(maxRowCount != null ? maxRowCount : -1);
        jdbcTemplate.getJdbcTemplate().setFetchSize(fetchSize);
        List<SQLQueryDecorator> filterDecorators = new ArrayList<>();
        for (DataFilter filter : filters) {
            if (filter.getClass() == MultiKeyDataFilter.class) {
                filterDecorators.add(
                        new KeysFilterQueryDecorator(((MultiKeyDataFilter) filter).getKeys()));
            } else if (filter.getClass() == KeyRangeDataFilter.class) {
                KeyRangeDataFilter keyRangeFilter = (KeyRangeDataFilter) filter;
                filterDecorators.add(
                        new KeyRangeFilterQueryDecorator(
                                keyRangeFilter.getFromKey(), keyRangeFilter.isFromKeyInclusive(),
                                keyRangeFilter.getToKey(), keyRangeFilter.isToKeyInclusive()));
            } else if (filter.getClass() == LastUpdateTimeFilter.class) {
                if (!isFilterSupported(filter)) {
                    throw new DataFlowConfigurationException(
                            "lastUpdateTimeColumn must be set in order to use a LastUpdateTimeFilter");
                }
                filterDecorators.add(
                        new LastUpdateTimeFilterQueryDecorator(
                                ((LastUpdateTimeFilter) filter).getLastUpdateTime()));
            } else if (filter.getClass() == MaxRowCountDataFilter.class) {
                int rowCount = ((MaxRowCountDataFilter) filter).getMaxRowCount();
                jdbcTemplate.getJdbcTemplate().setMaxRows(rowCount);
                jdbcTemplate.getJdbcTemplate().setFetchSize(rowCount);
            } else {
                throw new IllegalArgumentException(
                        "Unsupported filter type " + filter.getClass().getName());
            }
        }
        return filterDecorators;
    }

    protected DataRecords createDataRecords(List<Map<String, Object>> results) {
        DataRecords records = new DataRecords();
        records.setKeyColumnNames(recordsKeyColumns);
        records.addAll(results.stream().map(DataRecord::new).collect(Collectors.toList()));
        return records;
    }

    private MapSqlParameterSource buildParamSource(Map<String, ?> inputParams) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        for (Map.Entry<String, ?> entry : inputParams.entrySet()) {
            Object value = entry.getValue();
            paramSource.addValue(entry.getKey(), value);
        }
        return paramSource;
    }

    protected String buildQuery(SQLQueryInfo queryInfo) {
        return buildQuery(queryInfo, false);
    }

    private String buildQuery(SQLQueryInfo queryInfo, boolean ignoreRequireOrderBy) {
        StringBuilder result = new StringBuilder();
        result.append(queryInfo.getBaseQuery());
        if (!queryInfo.getWhereClauses().isEmpty()) {
            result.append(" WHERE ");
            result.append(Joiner.on(" AND ").join(queryInfo.getWhereClauses()));
        }
        if (queryInfo.getOrderByClause() != null && !queryInfo.getOrderByClause().isEmpty()) {
            result.append(" ORDER BY ").append(queryInfo.getOrderByClause());
        } else if (orderByKeyIfNotSpecified) {
            // Order by key by default.
            result.append(" ORDER BY ").append(StringUtil.join(keyColumns, ", "));
        } else if (requireOrderBy && !ignoreRequireOrderBy) {
            throw new DataFlowConfigurationException("An order by clause is required");
        }
        return result.toString();
    }

    private SQLQueryInfo buildQueryInfo(
            Map<String, ?> paramValues, List<SQLQueryDecorator> additionalDecorators) {
        List<String> whereClauses = new ArrayList<>();
        if (!Strings.isNullOrEmpty(whereClause)) {
            whereClauses.add(whereClause);
        }

        SQLQueryInfo queryInfo = new SQLQueryInfo();
        queryInfo.setBaseQuery(baseQuery);
        queryInfo.setPreQueryStatements(preQueryStatements);
        queryInfo.setKeyColumns(keyColumns);
        queryInfo.setLastUpdateTimeColumn(lastUpdateTimeColumn);
        queryInfo.setWhereClauses(whereClauses);
        queryInfo.setOrderByClause(orderByClause);
        queryInfo.setParamValues(
                paramValues != null ? new HashMap<>(paramValues) : new HashMap<>());

        if (fromKey != null) {
            queryInfo.getParamValues().put("_fromKey", fromKey);
        }
        queryInfo.getParamValues().put("_batchSize", fetchSize);
        for (SQLQueryDecorator decorator : queryDecorators) {
            decorator.apply(queryInfo);
        }
        for (SQLQueryDecorator decorator : additionalDecorators) {
            decorator.apply(queryInfo);
        }
        return queryInfo;
    }

    static Object getKeyColumnValue(Object key, int columnIdx) {
        if (key instanceof List) {
            List keyColumnValues = (List) key;
            if (keyColumnValues.size() <= columnIdx) {
                throw new IllegalArgumentException(
                        String.format(
                                "No value for column index %s in key %s",
                                columnIdx, key.toString()));
            }
            return keyColumnValues.get(columnIdx);
        } else if (columnIdx == 0) {
            return key;
        } else {
            throw new IllegalArgumentException(
                    String.format(
                            "No value for column index %d in key %s", columnIdx, key.toString()));
        }
    }
}
