/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryValueProvider.java
 */
package dataflow.sql;

import static dataflow.sql.SQLDatabaseOutputMode.MULTI_ROW;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.retry.Retry;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.sql.DataSource;

@Deprecated
@DataFlowComponent(description = "Provides a value from a SQL query")
public class SQLQueryValueProvider {

    private final SQLDatabaseOutputMode outputMode;
    private final SQLDataQueryValueProvider queryProvider;

    @DataFlowConfigurable
    public SQLQueryValueProvider(
            @DataFlowConfigProperty(
                            description =
                                    "The query to use to get values. Input values will be used to replace named parameters.\n"
                                            + "Example:\\n"
                                            + "select age from products where id = :product_id\\n\\n"
                                            + "In this example the input named product_id will be used to replace :product_id\\n")
                    String baseQuery,
            @DataFlowConfigProperty(
                            required = false,
                            description =
                                    "The base query to get the estimated number of result records")
                    String countBaseQuery,
            @DataFlowConfigProperty(
                            required = false,
                            description =
                                    "The where clause used to filter the results"
                                            + "Input values will be used to replace named parameters.\\n\\n"
                                            + "Example: age >= :min_age")
                    String whereClause,
            @DataFlowConfigProperty(
                            required = false,
                            description = "The names of the columns that form the key of the table")
                    List<String> keyColumns,
            @DataFlowConfigProperty(
                            required = false,
                            description =
                                    "The names of the columns that form the key of output records. Will default to keyColumns if not specified")
                    List<String> recordsKeyColumns,
            @DataFlowConfigProperty(required = false, description = "Order by clause")
                    String orderByClause,
            @DataFlowConfigProperty(
                            required = false,
                            description = "Statements to execute before the query")
                    List<String> preQueryStatements,
            @DataFlowConfigProperty(
                            required = false,
                            description = "The name of the column that holds the last updated time")
                    String lastUpdateTimeColumn,
            @DataFlowConfigProperty(
                            required = false,
                            description = "Decorators that modify the query")
                    List<SQLQueryDecorator> decorators,
            @DataFlowConfigProperty(
                            required = false,
                            description =
                                    "SINGLE_ROW (default)\\n"
                                            + "The provider will output a single map<string, object> instead of a list. If the query "
                                            + "returns multiple rows only the first row is used.\\n\\n"
                                            + "MULTI_ROW\\n"
                                            + "The provider will output a list rows where each row is a map<string, object>\\n\\n"
                                            + "STREAM\\n"
                                            + "The provider will output a stream of rows where each row is a map<string, object>\\n\\n"
                                            + "SINGLE_COLUMN\\n"
                                            + "The provider will output a single value. If the query returns multiple rows only the "
                                            + "first row is used. Only one column can be specified in the columns list.\\n")
                    SQLDatabaseOutputMode outputMode,
            @DataFlowConfigProperty(
                            required = false,
                            description =
                                    "A string that identifies the cluster to use"
                                            + "A cluster with the given id should be registered with the cluster manager")
                    String clusterId,
            @DataFlowConfigProperty(required = false, description = "The starting key")
                    Object fromKey,
            @DataFlowConfigProperty(
                            required = false,
                            description = "The number of rows returned in a batch")
                    Integer fetchSize,
            @DataFlowConfigProperty(
                            required = false,
                            description = "The maximum number of rows to return")
                    Integer maxRowCount,
            @DataFlowConfigProperty(required = false) Integer batchSize,
            @DataFlowConfigProperty(required = false) DataSource dataSource,
            @DataFlowConfigProperty(required = false) SQLDatabaseIterationMode iterationMode,
            @DataFlowConfigProperty(
                            required = false,
                            description =
                                    "A query to get the maximum key if using the KEY_BASED iteration mode")
                    String maxKeyQuery,
            @DataFlowConfigProperty(required = false) Retry retry,
            @DataFlowConfigProperty(required = false) String metricNamePrefix,
            @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        this.queryProvider =
                new SQLDataQueryValueProvider(
                        baseQuery,
                        countBaseQuery,
                        whereClause,
                        keyColumns,
                        recordsKeyColumns,
                        orderByClause,
                        preQueryStatements,
                        lastUpdateTimeColumn,
                        decorators,
                        clusterId,
                        fromKey,
                        fetchSize,
                        maxRowCount,
                        batchSize,
                        dataSource,
                        iterationMode,
                        maxKeyQuery,
                        retry,
                        metricNamePrefix,
                        metricTags);
        this.outputMode = outputMode != null ? outputMode : MULTI_ROW;
    }

    @OutputValue
    public CompletableFuture<Object> getValue(
            @InputValues(
                            description =
                                    "The param values. The name of each param should match a placeholder in the "
                                            + "baseQuery or whereClause.")
                    Map<String, Object> paramValues) {
        CompletableFuture<Object> resultFuture = new CompletableFuture<>();
        DataFlowExecutionContext.getCurrentExecutionContext()
                .getEnvironment()
                .executeAsync(
                        () -> {
                            try {
                                Object result = executeQuery(paramValues);
                                resultFuture.complete(result);
                            } catch (Exception ex) {
                                resultFuture.completeExceptionally(ex);
                            }
                        },
                        getClass().getSimpleName());
        return resultFuture;
    }

    private Object executeQuery(Map<String, Object> params) throws Exception {
        if (outputMode == SQLDatabaseOutputMode.STREAM) {
            return queryProvider.queryForStream(params);
        }
        DataRecords result = queryProvider.query(params);
        switch (outputMode) {
            case SINGLE_COLUMN:
                if (result.size() > 0 && !result.get(0).isEmpty()) {
                    return result.get(0).values().iterator().next();
                } else {
                    return null;
                }
            case SINGLE_ROW:
                return result.size() > 0 ? result.get(0) : null;
            case MULTI_ROW:
                if (!result.validateKeyColumns()) {
                    throw new InvalidDataException(
                            String.format(
                                    "Fetched records are missing key column(s) %s: %s",
                                    queryProvider.getKeyColumns(), result));
                }
                return result;
        }
        throw new IllegalArgumentException("Invalid output mode " + outputMode);
    }
}
