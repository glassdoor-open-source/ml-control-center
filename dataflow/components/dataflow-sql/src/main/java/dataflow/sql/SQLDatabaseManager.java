/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLDatabaseManager.java
 */
package dataflow.sql;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

/**
 * A manager for SQL database {@link DataSource} objects. Other classes can get a registered
 * DataSource given its id.
 */
public class SQLDatabaseManager {

    private String defaultDataSourceId;
    private final Map<String, DataSource> idDataSourceMap = new HashMap<>();

    /** Sets the id of the DataSource that will be returned by {@link #getDefaultDataSource()} */
    public void setDefaultDataSourceId(String defaultDataSourceId) {
        this.defaultDataSourceId = defaultDataSourceId;
    }

    /** Registers a DataSource with the given id. */
    public void registerDataSource(String id, DataSource dataSource) {
        idDataSourceMap.put(id, dataSource);
    }

    /** Returns the DataSource with the given id. */
    public DataSource getDataSource(String id) {
        DataSource dataSource = idDataSourceMap.get(id);
        if (dataSource == null) {
            throw new IllegalArgumentException("DataSource " + id + " is not registered");
        }
        return dataSource;
    }

    /** Returns the default DataSource or null if no default DataSource is specified. */
    public DataSource getDefaultDataSource() {
        if (idDataSourceMap.size() == 1) {
            return idDataSourceMap.values().iterator().next();
        }
        DataSource dataSource = idDataSourceMap.get(defaultDataSourceId);
        if (dataSource == null) {
            throw new IllegalStateException("No default DataSource specified");
        }
        return dataSource;
    }
}
