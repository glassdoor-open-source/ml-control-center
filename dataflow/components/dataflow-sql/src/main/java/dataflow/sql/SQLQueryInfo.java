/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLQueryInfo.java
 */
package dataflow.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SQLQueryInfo {

    private List<String> preQueryStatements = new ArrayList<>();
    private String baseQuery;
    private List<String> keyColumns;
    private String lastUpdateTimeColumn;
    private List<String> whereClauses;
    private Map<String, Object> paramValues;
    private String orderByClause;

    public List<String> getPreQueryStatements() {
        return preQueryStatements;
    }

    public void setPreQueryStatements(List<String> preQueryStatements) {
        this.preQueryStatements = preQueryStatements;
    }

    public String getBaseQuery() {
        return baseQuery;
    }

    public void setBaseQuery(String baseQuery) {
        this.baseQuery = baseQuery;
    }

    public List<String> getKeyColumns() {
        return keyColumns;
    }

    public void setKeyColumns(List<String> keyColumns) {
        this.keyColumns = keyColumns;
    }

    public String getLastUpdateTimeColumn() {
        return lastUpdateTimeColumn;
    }

    public void setLastUpdateTimeColumn(String lastUpdateTimeColumn) {
        this.lastUpdateTimeColumn = lastUpdateTimeColumn;
    }

    public List<String> getWhereClauses() {
        return whereClauses;
    }

    public void setWhereClauses(List<String> whereClauses) {
        this.whereClauses = whereClauses;
    }

    public Map<String, Object> getParamValues() {
        return paramValues;
    }

    public void setParamValues(Map<String, Object> paramValues) {
        this.paramValues = paramValues;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }
}
