/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SQLDatabaseIterationMode.java
 */
package dataflow.sql;

import java.sql.ResultSet;

public enum SQLDatabaseIterationMode {

    /** Iterates through the records using the SQL {@link ResultSet}. */
    RESULT_SET,

    /**
     * Iterates through the records using key values. The results must be ordered. For the first
     * query a batch of records is requested. For subsequent batches the key of the last record in
     * the previous batch is used as the offset.
     */
    KEY_BASED,

    /** Iterates through the records using numeric key ranges. A max key query must be specified. */
    NUMERIC_KEY_RANGE_BASED
}
