/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecords.java
 */
package dataflow.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataRecords implements Iterable<DataRecord> {

    private List<String> keyColumnNames;
    private List<String> columnNames;
    private List<DataRecord> records = new ArrayList<>();

    public List<String> getKeyColumnNames() {
        return keyColumnNames;
    }

    public DataRecords() {}

    public DataRecords(DataRecords records) {
        this.keyColumnNames =
                records.keyColumnNames != null ? new ArrayList<>(records.keyColumnNames) : null;
        this.columnNames =
                records.columnNames != null ? new ArrayList<>(records.columnNames) : null;
        this.records =
                records.records != null
                        ? records.records.stream().map(DataRecord::new).collect(Collectors.toList())
                        : null;
    }

    @JsonProperty("keyColumnNames")
    public void setKeyColumnNames(List<String> keyColumnNames) {
        this.keyColumnNames = keyColumnNames != null ? new ArrayList<>(keyColumnNames) : null;
    }

    public void setKeyColumnNames(String... keyColumnNames) {
        setKeyColumnNames(Arrays.asList(keyColumnNames));
    }

    public List<String> getColumnNames() {
        if (columnNames == null && !isEmpty()) {
            columnNames = new ArrayList<>();
            Set<String> addedColumns = new HashSet<>();
            forEach(
                    record ->
                            record.keySet()
                                    .forEach(
                                            colName -> {
                                                String colNameLowercase = colName.toLowerCase();
                                                if (!addedColumns.contains(colNameLowercase)) {
                                                    columnNames.add(colName);
                                                    addedColumns.add(colNameLowercase);
                                                }
                                            }));
        }
        return columnNames != null ? columnNames : Collections.emptyList();
    }

    @JsonIgnore
    public boolean isEmpty() {
        return records.isEmpty();
    }

    public boolean contains(Object o) {
        return records.contains(o);
    }

    public boolean hasKeyColumn(String name) {
        if (keyColumnNames == null) {
            return false;
        }
        String nameLowercase = name.toLowerCase();
        return keyColumnNames.stream()
                .anyMatch(colName -> nameLowercase.equals(colName.toLowerCase()));
    }

    public boolean hasColumn(String name) {
        if (columnNames == null) {
            return false;
        }
        String nameLowercase = name.toLowerCase();
        return columnNames.stream()
                .anyMatch(colName -> nameLowercase.equals(colName.toLowerCase()));
    }

    @JsonProperty("columnNames")
    public void setColumnNames(List<String> columnNames) {
        Set<String> colsSet = new HashSet<>();
        if (columnNames != null) {
            this.columnNames = new ArrayList<>(columnNames);
            columnNames.forEach(c -> colsSet.add(c.toLowerCase()));
        } else {
            this.columnNames = null;
        }
        for (DataRecord record : records) {
            record.entrySet().removeIf(entry -> !colsSet.contains(entry.getKey().toLowerCase()));
        }
    }

    public void setColumnNames(String... columnNames) {
        setColumnNames(Arrays.asList(columnNames));
    }

    @JsonIgnore
    public Object getKey(DataRecord record) {
        List<String> keyColNames = getKeyColumnNames();
        if (record == null || keyColNames.isEmpty()) {
            return null;
        }
        Object key;
        if (keyColNames.size() == 1) {
            key = record.get(keyColNames.get(0));
        } else {
            List<Object> keyComponents = new ArrayList<>();
            keyColNames.forEach(keyCol -> keyComponents.add(record.get(keyCol)));
            key = keyComponents;
        }
        return key;
    }

    @JsonIgnore
    public List<Object> getKeys() {
        return stream().map(this::getKey).collect(Collectors.toList());
    }

    @JsonIgnore
    public boolean validateKeyColumns() {
        if (isEmpty() && getColumnNames().isEmpty()) {
            return true;
        }

        for (String keyCol : keyColumnNames) {
            boolean found = false;
            for (String col : getColumnNames()) {
                if (keyCol.equalsIgnoreCase(col)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }

            if (stream().anyMatch(record -> !record.containsKey(keyCol))) {
                return false;
            }
        }
        return true;
    }

    public Stream<DataRecord> stream() {
        return records.stream();
    }

    public Stream<DataRecord> parallelStream() {
        return records.parallelStream();
    }

    public DataRecord get(int index) {
        return records.get(index);
    }

    public DataRecord set(int index, DataRecord element) {
        return records.set(index, element);
    }

    public void add(int index, DataRecord element) {
        records.add(index, element);
    }

    public DataRecord remove(int index) {
        return records.remove(index);
    }

    public int indexOf(Object o) {
        return records.indexOf(o);
    }

    public int lastIndexOf(Object o) {
        return records.lastIndexOf(o);
    }

    public ListIterator<DataRecord> listIterator() {
        return records.listIterator();
    }

    public ListIterator<DataRecord> listIterator(int index) {
        return records.listIterator(index);
    }

    public List<DataRecord> subList(int fromIndex, int toIndex) {
        return records.subList(fromIndex, toIndex);
    }

    public int size() {
        return records.size();
    }

    public List<DataRecord> getRecords() {
        return records;
    }

    public void setRecords(List<DataRecord> records) {
        this.records = records;
    }

    public Iterator<DataRecord> iterator() {
        return records.iterator();
    }

    public Object[] toArray() {
        return records.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return records.toArray(a);
    }

    public boolean add(DataRecord dataRecord) {
        return records.add(dataRecord);
    }

    public boolean remove(Object o) {
        return records.remove(o);
    }

    public boolean containsAll(Collection<?> c) {
        return records.containsAll(c);
    }

    public boolean addAll(Collection<? extends DataRecord> c) {
        return records.addAll(c);
    }

    public boolean addAll(int index, Collection<? extends DataRecord> c) {
        return records.addAll(index, c);
    }

    public boolean removeAll(Collection<?> c) {
        return records.removeAll(c);
    }

    public boolean removeIf(Predicate<? super DataRecord> filter) {
        return records.removeIf(filter);
    }

    public boolean retainAll(Collection<?> c) {
        return records.retainAll(c);
    }

    public void replaceAll(UnaryOperator<DataRecord> operator) {
        records.replaceAll(operator);
    }

    public void sort(Comparator<? super DataRecord> c) {
        records.sort(c);
    }

    public void clear() {
        records.clear();
    }

    public void forEach(Consumer<? super DataRecord> action) {
        records.forEach(action);
    }

    public Spliterator<DataRecord> spliterator() {
        return records.spliterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataRecords records1 = (DataRecords) o;
        return Objects.equals(keyColumnNames, records1.keyColumnNames)
                && Objects.equals(columnNames, records1.columnNames)
                && Objects.equals(records, records1.records);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keyColumnNames, columnNames, records);
    }

    @Override
    public String toString() {
        StringBuilder recordsStr = new StringBuilder();
        int len = Math.min(4, records.size());
        for (int cnt = 1; cnt <= len; cnt++) {
            if (cnt > 1) {
                recordsStr.append(", ");
            }
            DataRecord record = records.get(cnt - 1);
            recordsStr.append("[");
            if (keyColumnNames != null) {
                for (String key : keyColumnNames) {
                    recordsStr.append(key).append(":").append(record.get(key)).append(" ");
                }
            }
            recordsStr.append(record.toString()).append("]");
        }
        return "DataRecords{"
                + "keyColumnNames="
                + keyColumnNames
                + ", columnNames="
                + columnNames
                + ", records="
                + recordsStr
                + '}';
    }
}
