/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordProxyConverters.java
 */
package dataflow.data.proxy;

import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.ValueType;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;

public class DataRecordProxyConverters {

    public static class DataRecord_to_Proxy_converter
            extends AbstractValueTypeConverter<DataRecord, DataRecordProxy> {

        public DataRecord_to_Proxy_converter() {
            super(DataRecord.class, DataRecordProxy.class);
        }

        @Override
        public DataRecordProxy convert(DataRecord value, ValueType toValueType) {
            if (value == null) {
                return null;
            }
            try {
                Class<? extends DataRecordProxy> proxyClass =
                        (Class<? extends DataRecordProxy>) Class.forName(toValueType.getType());
                return DataRecordProxy.create(value, proxyClass);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(
                        String.format(
                                "Unable to convert from %s to %s, the target proxy type is undefined",
                                value.getClass().getName(), toValueType));
            }
        }
    }

    public static class Proxy_to_DataRecord_converter
            extends AbstractValueTypeConverter<DataRecordProxy, DataRecord> {

        public Proxy_to_DataRecord_converter() {
            super(DataRecordProxy.class, DataRecord.class);
        }

        @Override
        public DataRecord convert(DataRecordProxy value, ValueType toValueType) {
            if (value == null) {
                return null;
            }
            return value.getRecord();
        }
    }

    public static class DataRecord_Proxy_to_Proxy_converter
            extends AbstractValueTypeConverter<DataRecordProxy, DataRecordProxy> {

        public DataRecord_Proxy_to_Proxy_converter() {
            super(DataRecordProxy.class, DataRecordProxy.class);
        }

        @Override
        public DataRecordProxy convert(DataRecordProxy value, ValueType toValueType) {
            if (value == null) {
                return null;
            }
            try {
                Class<? extends DataRecordProxy> proxyClass =
                        (Class<? extends DataRecordProxy>) Class.forName(toValueType.getType());
                return DataRecordProxy.create(value.getRecord(), proxyClass);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(
                        String.format(
                                "Unable to convert from %s to %s, the target proxy type is undefined",
                                value.getClass().getName(), toValueType));
            }
        }
    }

    public static class DataRecords_to_Proxy_converter
            extends AbstractValueTypeConverter<DataRecords, DataRecordsProxy> {

        public DataRecords_to_Proxy_converter() {
            super(DataRecords.class, DataRecordsProxy.class);
        }

        @Override
        public DataRecordsProxy convert(DataRecords value, ValueType toValueType) {
            if (value == null) {
                return null;
            }
            if (toValueType.getTypeGenericParams().isEmpty()) {
                throw new RuntimeException(
                        String.format(
                                "Unable to convert from %s to %s, the target proxy type is undefined",
                                value.getClass().getName(), toValueType));
            }
            ValueType proxyType = toValueType.getTypeGenericParams().get(0);
            return new DataRecordsProxy(value, proxyType.getTypeClass());
        }
    }

    public static class Proxy_to_DataRecords_converter
            extends AbstractValueTypeConverter<DataRecordsProxy, DataRecords> {

        public Proxy_to_DataRecords_converter() {
            super(DataRecordsProxy.class, DataRecords.class);
        }

        @Override
        public DataRecords convert(DataRecordsProxy value, ValueType toValueType) {
            if (value == null) {
                return null;
            }
            return value.getDataRecords();
        }
    }

    public static class DataRecords_Proxy_to_Proxy_converter
            extends AbstractValueTypeConverter<DataRecordsProxy, DataRecordsProxy> {

        public DataRecords_Proxy_to_Proxy_converter() {
            super(DataRecordsProxy.class, DataRecordsProxy.class);
        }

        @Override
        public DataRecordsProxy convert(DataRecordsProxy value, ValueType toValueType) {
            if (value == null) {
                return null;
            }
            if (toValueType.getTypeGenericParams().isEmpty()) {
                throw new RuntimeException(
                        String.format(
                                "Unable to convert from %s to %s, the target proxy type is undefined",
                                value.getClass().getName(), toValueType));
            }
            ValueType proxyType = toValueType.getTypeGenericParams().get(0);
            return new DataRecordsProxy(value.getDataRecords(), proxyType.getTypeClass());
        }
    }
}
