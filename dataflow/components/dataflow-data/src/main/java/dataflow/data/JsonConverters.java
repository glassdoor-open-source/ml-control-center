/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  JsonConverters.java
 */
package dataflow.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import dataflow.core.type.ValueType;
import dataflow.core.type.ValueTypeConverter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * Converters that can convert a string to/from a json serializable object or from one json
 * serializable object type to another. The json serializable object classes should have the {@link
 * JsonAutoConvert} annotation to indicate that this conversion is allowed.
 */
public final class JsonConverters {

    private static final ValueType STRING_VALUE_TYPE = new ValueType(String.class);

    /** Converter that can convert a string to a json serializable object */
    public static final class Json_to_Object_converter
            implements ValueTypeConverter<String, Object> {

        private final ObjectMapper objectMapper = new ObjectMapper();

        @Override
        public Object convert(String value, ValueType toValueType) {
            try {
                return objectMapper.readValue(
                        value.getBytes(StandardCharsets.UTF_8), toValueType.getTypeClass());
            } catch (IOException e) {
                throw new RuntimeException(
                        String.format("Unable to deserialize json to %s: %s", toValueType, value),
                        e);
            }
        }

        @Override
        public boolean canConvert(ValueType fromValueType, ValueType toValueType) {
            return fromValueType.equals(STRING_VALUE_TYPE)
                    && canJsonAutoConvert(toValueType.getTypeClass());
        }
    }

    /** Converter that can convert a map to a json serializable object */
    public static final class Map_to_Object_converter
            implements ValueTypeConverter<Map<?, ?>, Object> {

        private final ObjectMapper objectMapper = new ObjectMapper();

        @Override
        public Object convert(Map<?, ?> value, ValueType toValueType) {
            try {
                return objectMapper.convertValue(value, toValueType.getTypeClass());
            } catch (Exception e) {
                throw new RuntimeException(
                        String.format("Unable to deserialize map to %s: %s", toValueType, value),
                        e);
            }
        }

        @Override
        public boolean canConvert(ValueType fromValueType, ValueType toValueType) {
            return Map.class.isAssignableFrom(fromValueType.getTypeClass())
                    && canJsonAutoConvert(toValueType.getTypeClass());
        }
    }

    /** Converter that can convert a json serializable object to a json string. */
    public static final class Object_to_Json_converter
            implements ValueTypeConverter<Object, String> {

        private final ObjectMapper objectMapper = new ObjectMapper();

        @Override
        public String convert(Object value, ValueType toValueType) {
            try {
                return objectMapper.writeValueAsString(value);
            } catch (IOException e) {
                throw new RuntimeException(
                        String.format("Unable to deserialize json to %s: %s", toValueType, value),
                        e);
            }
        }

        @Override
        public boolean canConvert(ValueType fromValueType, ValueType toValueType) {
            return toValueType.equals(STRING_VALUE_TYPE)
                    && canJsonAutoConvert(fromValueType.getTypeClass());
        }
    }

    /**
     * Converter that can convert a json serializable object to another json serializable object.
     */
    public static final class Json_Object_to_Object_converter
            implements ValueTypeConverter<Object, Object> {

        private final ObjectMapper objectMapper = new ObjectMapper();

        @Override
        public Object convert(Object value, ValueType toValueType) {
            try {
                String json = objectMapper.writeValueAsString(value);
                return objectMapper.readValue(
                        json.getBytes(StandardCharsets.UTF_8), toValueType.getTypeClass());
            } catch (IOException e) {
                throw new RuntimeException(
                        String.format("Unable to deserialize json to %s: %s", toValueType, value),
                        e);
            }
        }

        @Override
        public boolean canConvert(ValueType fromValueType, ValueType toValueType) {
            return canJsonAutoConvert(toValueType.getTypeClass())
                    && canJsonAutoConvert(fromValueType.getTypeClass());
        }
    }

    private static boolean canJsonAutoConvert(Class<?> c) {
        return c.getAnnotation(JsonAutoConvert.class) != null;
    }
}
