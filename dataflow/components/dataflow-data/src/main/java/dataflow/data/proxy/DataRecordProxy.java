/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordProxy.java
 */
package dataflow.data.proxy;

import static dataflow.data.proxy.DataRecordProxy.ProxyInvocationHandler.getColumnName;
import static dataflow.data.proxy.DataRecordProxy.ProxyInvocationHandler.isGetter;
import static dataflow.data.proxy.DataRecordProxy.ProxyInvocationHandler.isSetter;

import dataflow.data.DataRecord;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * Creates a proxy object backed by a {@link DataRecord} whose getter and setter methods are
 * implemented by getting/setting the corresponding column in the DataRecord. The proxy class should
 * be defined as an interface with methods with the following form for the columns:
 *
 * <p>ValueType get"ColumnName"() ValueType get"ColumnName"(ValueType defaultValue) void
 * set"ColumnName"(ValueType newValue) void set"ColumnName"(ValueType newValue, ValueType
 * defaultValue)
 *
 * <p>where "ColumnName" would be the name of the column and ValueType is the type of the column.
 * These methods can be annotated with the {@link Column} annotation to mark certain columns as
 * required.
 *
 * <p>For example:
 *
 * <p>public interface ExampleDataRecordProxy extends DataRecordProxy {
 *
 * <p>&#64;Column(required = true) int getAge(); void setAge(String value); }
 *
 * <p>Proxy instances can be created using the {@link DataRecordProxy#create(DataRecord, Class)}
 * method.
 *
 * <p>The proxy methods update the proxied DataRecord instance so after a call to a set method the
 * DataRecord column will be changed and subsequent calls to the get method will return the set
 * value.
 *
 * <p>The proxy provides a {@link #validate()} method to ensure that all required columns are
 * present and the types are consistent.
 */
public interface DataRecordProxy {

    final class ProxyInvocationHandler implements InvocationHandler {
        private final Class<? extends DataRecordProxy> proxyClass;
        private final DataRecord record;

        private ProxyInvocationHandler(
                Class<? extends DataRecordProxy> proxyClass, DataRecord record) {
            this.proxyClass = proxyClass;
            this.record = record;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("getRecord")) {
                return record;
            }
            if (method.getName().equals("validate")) {
                validate();
                return null;
            }

            if (isGetter(method)) {
                Object value = record.get(getColumnName(method));
                if (value == null && args != null && args.length > 0) {
                    // If the value is null return the default value.
                    value = args[0];
                }
                return value;
            } else if (isSetter(method)) {
                Object value = args[0];
                if (value == null && args.length > 1) {
                    // If the value is null set to the default value.
                    value = args[1];
                }
                record.put(getColumnName(method), value);
                return null;
            }
            return method.invoke(proxy, args);
        }

        /**
         * Validates that all required columns are present in the {@link DataRecord} and that the
         * values are compatible with the proxy method param/return value types.
         *
         * @throws DataRecordValidationException if the validation constraints are not met.
         */
        private void validate() throws DataRecordValidationException {
            for (Method method : proxyClass.getMethods()) {
                if (isGetter(method)) {
                    String columnName = getColumnName(method);
                    Column columnAnnotation = method.getAnnotation(Column.class);
                    Object value = record.get(columnName);
                    if (columnAnnotation != null) {
                        if (columnAnnotation.required() && value == null) {
                            throw new DataRecordValidationException(
                                    String.format("Missing required column %s", columnName));
                        }
                    }
                    if (value != null) {
                        if (!method.getReturnType().isAssignableFrom(value.getClass())) {
                            throw new DataRecordValidationException(
                                    String.format(
                                            "Column %s has value type %s, but %s was expected",
                                            columnName,
                                            value.getClass().getName(),
                                            method.getReturnType().getName()));
                        }
                    }
                }
            }
        }

        static boolean isGetter(Method method) {
            return method.getName().startsWith("get");
        }

        static boolean isSetter(Method method) {
            return method.getName().startsWith("set");
        }

        static String getColumnName(Method method) {
            return method.getName().substring(3);
        }
    }

    default DataRecord getRecord() {
        // Implemented by the proxy.
        return null;
    }

    /** See {@link ProxyInvocationHandler#validate()} */
    default void validate() throws DataRecordValidationException {
        // Implemented by the proxy.
    }

    static <T extends DataRecordProxy> T create(DataRecord record, Class<T> proxyClass) {
        validateProxyClass(proxyClass);
        return (T)
                Proxy.newProxyInstance(
                        proxyClass.getClassLoader(),
                        new Class<?>[] {proxyClass},
                        new ProxyInvocationHandler(proxyClass, record));
    }

    static <T extends DataRecordProxy> T create(Class<T> proxyClass) {
        validateProxyClass(proxyClass);
        return (T)
                Proxy.newProxyInstance(
                        proxyClass.getClassLoader(),
                        new Class<?>[] {proxyClass},
                        new ProxyInvocationHandler(proxyClass, new DataRecord()));
    }

    static <T extends DataRecordProxy> void validateProxyClass(Class<T> proxyClass) {
        for (Method method : proxyClass.getMethods()) {
            if (isGetter(method)) {
                String columnName = getColumnName(method);
                if (method.getReturnType() == Void.class || method.getReturnType() == void.class) {
                    throw new IllegalArgumentException(
                            "Getter " + columnName + " must return a value");
                }
                if (method.getParameterCount() > 1) {
                    throw new IllegalArgumentException(
                            "Getter " + columnName + " can have at most 1 parameter");
                }
                if (method.getParameterCount() == 1) {
                    Class<?> defaultValueType = method.getParameterTypes()[0];
                    if (!method.getReturnType().isAssignableFrom(defaultValueType)) {
                        throw new IllegalArgumentException(
                                String.format(
                                        "Getter %s default value parameter type should be %s",
                                        columnName, method.getReturnType().getName()));
                    }
                }

                Method setter =
                        Arrays.stream(proxyClass.getMethods())
                                .filter(m -> m.getName().equalsIgnoreCase("set" + columnName))
                                .findFirst()
                                .orElse(null);
                if (setter != null
                        && setter.getParameterCount() > 0
                        && !method.getReturnType()
                                .isAssignableFrom(setter.getParameterTypes()[0])) {
                    throw new IllegalArgumentException(
                            "Getter and setter types don't match for column " + columnName);
                }
            } else if (isSetter(method)) {
                String columnName = getColumnName(method);
                if (method.getParameterCount() < 1) {
                    throw new IllegalArgumentException(
                            "Setter " + columnName + " must have at least 1 parameter");
                }
                if (method.getParameterCount() > 2) {
                    throw new IllegalArgumentException(
                            "Setter " + columnName + " can have at most 2 parameters");
                }
                Class<?> firstParamType = method.getParameterTypes()[0];
                if (method.getParameterTypes().length > 1
                        && !firstParamType.isAssignableFrom(method.getParameterTypes()[1])) {
                    throw new IllegalArgumentException(
                            String.format(
                                    "Setter %s default value parameter type should be %s",
                                    columnName, firstParamType.getName()));
                }
            }
        }
    }
}
