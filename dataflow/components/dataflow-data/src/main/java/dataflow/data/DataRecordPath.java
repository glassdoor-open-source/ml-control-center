/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordPath.java
 */
package dataflow.data;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import java.util.Objects;

/**
 * Specifies a the path to a value in a {@link DataRecord}. The path consists of the name of the
 * column that holds the value and optionally the dotted path of the data within that column if the
 * column value is json or a map of maps. For example the path a1.b1 has the value 7 in the
 * following example:
 *
 * <pre>
 * {
 *     "a1": {
 *         "b1": 7
 *     },
 *     "a2": 5
 * }
 * </pre>
 *
 * If one of the values along a path is a list then the path refers to all values in the list. eg.
 *
 * <pre>
 * {
 *     "a1": {
 *         "b1": [{color: "red"}, {color: "green"}, {color: "blue"}]
 *     },
 *     "a2": 5
 * }
 * </pre>
 *
 * a1.b1.color refers to the values red, green, and blue
 */
public class DataRecordPath {

    public static final String PATH_SEPARATOR = ".";

    private final String columnName;
    private final String path;

    @DataFlowConfigurable
    public DataRecordPath(
            @DataFlowConfigProperty String columnName,
            @DataFlowConfigProperty(required = false) String path) {
        this.columnName = columnName;
        this.path = path;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getPath() {
        return path;
    }

    public String[] getPathElements() {
        if (path == null || path.length() == 0) {
            return new String[0];
        }
        return path.split(PATH_SEPARATOR);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataRecordPath that = (DataRecordPath) o;
        return Objects.equals(getColumnName(), that.getColumnName())
                && Objects.equals(getPath(), that.getPath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getColumnName(), getPath());
    }

    @Override
    public String toString() {
        return "DataRecordPath{"
                + "columnName='"
                + columnName
                + '\''
                + ", path='"
                + path
                + '\''
                + '}';
    }
}
