/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  KeyRangeDataFilter.java
 */
package dataflow.data.query;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;

public class KeyRangeDataFilter implements DataFilter {

    private Object fromKey;
    private boolean fromKeyInclusive;
    private Object toKey;
    private boolean toKeyInclusive;

    @DataFlowConfigurable
    public KeyRangeDataFilter(
            @DataFlowConfigProperty Object fromKey,
            @DataFlowConfigProperty boolean fromKeyInclusive,
            @DataFlowConfigProperty Object toKey,
            @DataFlowConfigProperty boolean toKeyInclusive) {
        this.fromKey = fromKey;
        this.fromKeyInclusive = fromKeyInclusive;
        this.toKey = toKey;
        this.toKeyInclusive = toKeyInclusive;
    }

    public Object getFromKey() {
        return fromKey;
    }

    public boolean isFromKeyInclusive() {
        return fromKeyInclusive;
    }

    public Object getToKey() {
        return toKey;
    }

    public boolean isToKeyInclusive() {
        return toKeyInclusive;
    }
}
