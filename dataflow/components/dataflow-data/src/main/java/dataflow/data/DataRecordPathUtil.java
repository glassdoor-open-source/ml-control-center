/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordPathUtil.java
 */
package dataflow.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class DataRecordPathUtil {

    public interface Visitor {

        void visit(Object value, String path);
    }

    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Gets the value with the given path within the given root object. If the root object is a
     * string then an attempt will be made to parse the string as json to a map of maps.
     *
     * <p>{@see DataRecordPath} for more information on how paths are defined.
     *
     * @throws IllegalArgumentException if the path refers to multiple values
     */
    public static <T> T getValue(Object root, String path) {
        List<T> values = getAllValues(root, path);
        if (values.isEmpty()) {
            return null;
        }
        if (values.size() > 1) {
            throw new IllegalArgumentException("Found multiple values with path: " + path);
        }
        return values.get(0);
    }

    /**
     * Gets the values with the given path within the given root object. If the root object is a
     * string then an attempt will be made to parse the string as json to a map of maps.
     *
     * <p>{@see DataRecordPath} for more information on how paths are defined.
     */
    public static <T> List<T> getAllValues(Object root, String path) {
        List<T> values = new ArrayList<>();
        visit(root, path, (value, valuePath) -> values.add((T) value));
        return values;
    }

    /** Recursively visits the root object and all of its children. */
    public static void visit(Object root, Visitor visitor) {
        visit(root, Integer.MAX_VALUE, visitor);
    }

    /** Recursively visits the root object and all its children up to the given maximum depth. */
    public static void visit(Object root, int maxDepth, Visitor visitor) {
        if (root instanceof String) {
            // Attempt to parse as json.
            try {
                root = jsonStringToObject((String) root);
            } catch (IOException e) {
            }
        }
        visit(root, new ArrayDeque<>(), 0, maxDepth, visitor);
    }

    private static void visit(
            Object root, Deque<String> pathElements, int depth, int maxDepth, Visitor visitor) {
        if (root == null) {
            return;
        }
        if (depth >= maxDepth) {
            visitor.visit(root, getPath(pathElements));
            return;
        }

        if (root instanceof Map) {
            Map map = (Map) root;
            map.forEach(
                    (k, v) -> {
                        pathElements.addLast(k.toString());
                        visit(v, pathElements, depth + 1, maxDepth, visitor);
                        pathElements.removeLast();
                    });
        } else if (root instanceof Iterable) {
            Iterable iterable = (Iterable) root;
            iterable.forEach(v -> visit(v, pathElements, depth + 1, maxDepth, visitor));
        } else {
            visitor.visit(root, getPath(pathElements));
        }
    }

    private static void visit(Object root, String path, Visitor visitor) {
        if (DataRecordPath.PATH_SEPARATOR.equals(path)) {
            visitor.visit(root, path);
            return;
        }
        if (root instanceof String) {
            // Attempt to parse as json.
            try {
                root = jsonStringToObject((String) root);
            } catch (IOException e) {
                throw new IllegalArgumentException("Unable to locate value with path: " + path);
            }
        }
        if (path == null) {
            visitor.visit(root, DataRecordPath.PATH_SEPARATOR);
            return;
        }
        String[] pathElements = path.split(Pattern.quote(DataRecordPath.PATH_SEPARATOR));
        visit(root, pathElements, 0, visitor);
    }

    private static void visit(
            Object root, String[] pathElements, int pathElementIdx, Visitor visitor) {
        if (root == null) {
            return;
        }
        if (pathElementIdx > pathElements.length - 1) {
            visitor.visit(root, getPath(pathElements, pathElementIdx));
            return;
        }
        if (root instanceof Map) {
            Map map = (Map) root;
            visit(map.get(pathElements[pathElementIdx]), pathElements, pathElementIdx + 1, visitor);
        } else if (root instanceof Iterable) {
            Iterable iterable = (Iterable) root;
            for (Object item : iterable) {
                visit(item, pathElements, pathElementIdx, visitor);
            }
        } else {
            throw new IllegalArgumentException(
                    "Unable to locate value with path: "
                            + getPath(pathElements, pathElements.length));
        }
    }

    private static Object jsonStringToObject(String json) throws IOException {
        JsonNode node = objectMapper.readTree(json);
        if (node.isArray()) {
            return objectMapper.convertValue(node, List.class);
        } else {
            return objectMapper.convertValue(node, Map.class);
        }
    }

    private static String getPath(Deque<String> pathElements) {
        StringBuilder path = new StringBuilder();
        pathElements.forEach(
                pathElement -> {
                    if (path.length() > 0) {
                        path.append(DataRecordPath.PATH_SEPARATOR);
                    }
                    path.append(pathElement);
                });
        return path.toString();
    }

    private static String getPath(String[] pathElements, int pathElementIdx) {
        StringBuilder path = new StringBuilder();
        int len = Math.min(pathElements.length - 1, pathElementIdx);
        for (int idx = 0; idx <= len; idx++) {
            if (idx > 0) {
                path.append(DataRecordPath.PATH_SEPARATOR);
            }
            path.append(pathElements[idx]);
        }
        return path.toString();
    }
}
