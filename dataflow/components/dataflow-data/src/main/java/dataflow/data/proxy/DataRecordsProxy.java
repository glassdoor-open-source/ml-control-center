/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataRecordsProxy.java
 */
package dataflow.data.proxy;

import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import java.util.AbstractList;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * A proxy for multiple DataRecords that acts as a List of the {@link DataRecordProxy} type. This
 * proxy is backed by the DataRecords instance so any changes to this proxy are immediately
 * reflected in the DataRecords instance.
 *
 * <p>This class maintains a cache of DataRecordProxy instances for the individual records. The size
 * of this cache can be configured using {@link #maxProxyCacheSize}.
 *
 * @param <T> The DataRecordProxy type
 */
public class DataRecordsProxy<T extends DataRecordProxy> extends AbstractList<T> {

    private static final int DEFAULT_MAX_PROXY_CACHE_SIZE = 1000;

    private final int maxProxyCacheSize;
    private final DataRecords records;
    private final Map<DataRecord, T> proxyCache = new IdentityHashMap<>();
    private final Class<T> recordProxyClass;

    public DataRecordsProxy(DataRecords records, Class<T> recordProxyClass) {
        this(records, recordProxyClass, DEFAULT_MAX_PROXY_CACHE_SIZE);
    }

    public DataRecordsProxy(DataRecords records, Class<T> recordProxyClass, int maxProxyCacheSize) {
        this.records = records;
        this.recordProxyClass = recordProxyClass;
        this.maxProxyCacheSize = maxProxyCacheSize;
    }

    public DataRecords getDataRecords() {
        return records;
    }

    @Override
    public T get(int index) {
        return getRecordProxy(records.get(index));
    }

    @Override
    public T set(int index, T element) {
        DataRecord previous = records.set(index, element.getRecord());
        proxyCache.put(element.getRecord(), element);
        T retVal = getRecordProxy(previous);
        proxyCache.remove(previous);
        ensureProxyCacheSize();
        return retVal;
    }

    @Override
    public void add(int index, T element) {
        records.add(index, element.getRecord());
        proxyCache.put(element.getRecord(), element);
        ensureProxyCacheSize();
    }

    @Override
    public T remove(int index) {
        DataRecord removedRecord = records.remove(index);
        T retVal = getRecordProxy(removedRecord);
        proxyCache.remove(removedRecord);
        return retVal;
    }

    @Override
    public int size() {
        return records.size();
    }

    private T getRecordProxy(DataRecord record) {
        T retVal = proxyCache.get(record);
        if (retVal != null) {
            return retVal;
        }
        retVal = DataRecordProxy.create(record, recordProxyClass);
        proxyCache.put(record, retVal);
        ensureProxyCacheSize();
        return retVal;
    }

    private void ensureProxyCacheSize() {
        Iterator<Entry<DataRecord, T>> it = proxyCache.entrySet().iterator();
        while (it.hasNext() && proxyCache.size() > maxProxyCacheSize) {
            it.remove();
            it.next();
        }
    }
}
