/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CSVWriter.java
 */
package dataflow.data;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.datasource.DataSource;
import dataflow.core.exception.DataFlowConfigurationException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

@DataFlowComponent
public class CSVWriter implements AutoCloseable {

    private List<String> columnNames;
    private final CSVPrinter csvPrinter;
    private boolean firstRecord = true;

    @DataFlowConfigurable
    public CSVWriter(
            @DataFlowConfigProperty DataSource dataSource,
            @DataFlowConfigProperty(required = false) List<String> columnNames,
            @DataFlowConfigProperty(required = false) String delimiter) {
        CSVFormat csvFormat = CSVFormat.DEFAULT;
        if (delimiter != null) {
            if (delimiter.length() != 1) {
                throw new DataFlowConfigurationException("Delimiter must be a single character");
            }
            csvFormat = csvFormat.withDelimiter(delimiter.charAt(0));
        }
        if (columnNames != null && !columnNames.isEmpty()) {
            csvFormat =
                    csvFormat
                            .withFirstRecordAsHeader()
                            .withHeader(columnNames.toArray(new String[0]));
            this.columnNames = columnNames;
        } else {
            this.columnNames = null;
        }

        try {
            PrintStream writer = new PrintStream(dataSource.getOutputStream());
            this.csvPrinter = csvFormat.print(writer);
        } catch (IOException ex) {
            throw new RuntimeException("Unable to write to " + dataSource.getPath(), ex);
        }
    }

    @OutputValue
    public DataRecords getValue(@InputValue DataRecords records) throws IOException {
        if (firstRecord) {
            firstRecord = false;
            if (columnNames == null
                    && records.getColumnNames() != null
                    && !records.getColumnNames().isEmpty()) {
                // If the columnNames were not explicitly configured try to get the column names
                // from the first set of records that are written.
                columnNames = records.getColumnNames();
            }
            if (columnNames != null) {
                csvPrinter.printRecord(columnNames);
            }
        }
        for (DataRecord record : records) {
            if (columnNames != null) {
                for (String columnName : columnNames) {
                    csvPrinter.print(record.get(columnName));
                }
                csvPrinter.println();
            } else {
                csvPrinter.printRecord(record.values());
            }
        }
        return records;
    }

    @Override
    public void close() throws Exception {
        csvPrinter.close();
    }
}
