/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LeftJoinDataRecords.java
 */
package dataflow.data;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@DataFlowComponent
public class LeftJoinDataRecords {

    // TODO(thorntonv): Evaluate using a library such as tablesaw
    //  (https://github.com/jtablesaw/tablesaw) to avoid the need to implement these joins.
    private final String leftJoinColumn;
    private final String rightJoinColumn;

    @DataFlowConfigurable
    public LeftJoinDataRecords(
            @DataFlowConfigProperty(
                            required = false,
                            description =
                                    "The column to use for the join. This column will be used for both the left and right records")
                    String joinColumn,
            @DataFlowConfigProperty(
                            required = false,
                            description = "The column in the left records to use for the join")
                    String leftJoinColumn,
            @DataFlowConfigProperty(
                            required = false,
                            description = "The column in the right records to use for the join")
                    String rightJoinColumn) {
        if (leftJoinColumn == null) {
            leftJoinColumn = joinColumn;
        }
        if (rightJoinColumn == null) {
            rightJoinColumn = joinColumn;
        }
        if (leftJoinColumn == null || rightJoinColumn == null) {
            throw new DataFlowConfigurationException(
                    "Either joinColumn or leftJoinColumn and rightJoinColumn must be specified");
        }
        this.leftJoinColumn = leftJoinColumn;
        this.rightJoinColumn = rightJoinColumn;
    }

    @OutputValue
    public DataRecords getValue(@InputValue DataRecords left, @InputValue DataRecords right) {
        if (left == null || left.isEmpty() || right == null || right.isEmpty()) {
            return left;
        }
        DataRecords joinedRecords = new DataRecords();
        joinedRecords.setKeyColumnNames(left.getKeyColumnNames());
        joinedRecords.setColumnNames(
                mergeColumnNames(left.getColumnNames(), right.getColumnNames()));

        Map<Object, List<DataRecord>> rightJoinMap = buildJoinMap(right, rightJoinColumn);
        for (DataRecord leftRecord : left) {
            Object leftJoinValue = leftRecord.get(leftJoinColumn);
            if (leftJoinValue == null) {
                joinedRecords.add(leftRecord);
            } else {
                List<DataRecord> rightMatches = rightJoinMap.get(leftJoinValue);
                if (rightMatches == null || rightMatches.isEmpty()) {
                    joinedRecords.add(leftRecord);
                } else {
                    for (DataRecord rightRecord : rightMatches) {
                        DataRecord newRecord = new DataRecord();
                        newRecord.putAll(rightRecord);
                        newRecord.putAll(leftRecord);
                        joinedRecords.add(newRecord);
                    }
                }
            }
        }
        return joinedRecords;
    }

    private List<String> mergeColumnNames(List<String> leftCols, List<String> rightCols) {
        List<String> merged = new ArrayList<>(leftCols);
        Set<String> mergedSet = new HashSet<>(leftCols);
        rightCols.stream()
                .filter(col -> !mergedSet.contains(col))
                .forEach(
                        col -> {
                            merged.add(col);
                            mergedSet.add(col);
                        });
        return merged;
    }

    /** Maps values of the specified column to the records that have that value */
    private Map<Object, List<DataRecord>> buildJoinMap(DataRecords records, String colName) {
        Map<Object, List<DataRecord>> map = new HashMap<>();
        records.stream()
                .filter(r -> r.get(colName) != null)
                .forEach(r -> map.computeIfAbsent(r.get(colName), k -> new ArrayList<>()).add(r));
        return map;
    }
}
