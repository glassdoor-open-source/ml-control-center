

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(libs.googleAutoService)
    annotationProcessor(projects.dataflowCodegen)
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCore)

    api(projects.dataflowCore)
    api(projects.dataflowCodegen)
    api(projects.dataflowComponentCodegen)

    api(libs.commonsCsv)
    api(libs.micrometerCore)
    api(libs.jacksonDatabind)
    api(libs.jacksonAnnotations)
    api(libs.jacksonDataformatYaml)
}

group = "dataflow"
description = "dataflow-data"
