

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(projects.dataflowRetry)
    api(projects.dataflowSql)
    api(libs.postgresql)
    api(libs.slf4jApi)
    api(libs.springJdbc)
    api(libs.springContext)
    testImplementation(libs.guava)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.otjPgEmbedded)
    implementation(projects.dataflowComponentCodegen)
}

group = "dataflow"
description = "dataflow-postgres"
