/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  PostgreSQLData.java
 */
package dataflow.postgres;

import static dataflow.core.util.StringUtil.join;

import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.DataFilter;
import dataflow.data.query.MultiKeyDataFilter;
import dataflow.retry.Retry;
import dataflow.sql.SQLDataQueryValueProvider;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.sql.DataSource;
import org.postgresql.util.PGobject;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import reactor.core.publisher.Flux;

public class PostgreSQLData {

    private static final Logger logger = LoggerFactory.getLogger(PostgreSQLData.class);

    private static final int DEFAULT_FETCH_SIZE = 100;
    private final String LAST_UPDATE_TIME_COL_NAME;
    private final Map<String, ColumnType> columnNameTypeMap;
    private final String tableName;
    private final DataSource dataSource;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final SQLDataQueryValueProvider sqlQueryValueProvider;
    private final List<String> keyColumnNames;
    private final List<String> columnNames;

    private static final List<String> LAST_UPDATED_TIME_COL_PATTERNS =
            List.of("^md_updatedatetime$", "^md_update_date_time$", "^md_lastUpdateTime$");

    public PostgreSQLData(
            List<String> preQueryStatements,
            List<String> keyColumnNames,
            List<String> columnNames,
            List<ColumnType> columnTypes,
            String tableName,
            NamedParameterJdbcTemplate jdbcTemplate,
            DataSource dataSource) {
        this.keyColumnNames = keyColumnNames;
        this.columnNames = columnNames;
        this.tableName = tableName;
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = dataSource;

        // Check if input column names contain LastUpdatedDateTimeColumn.
        String fetchedLastUpdatedColumnName = fetchLastUpdatedTimestampColumn(columnNames);
        LAST_UPDATE_TIME_COL_NAME =
                fetchedLastUpdatedColumnName.isEmpty()
                        ? "lastUpdateTime"
                        : fetchedLastUpdatedColumnName;

        Retry noRetry = new Retry(1, true, null, null, null, null);
        this.sqlQueryValueProvider =
                new SQLDataQueryValueProvider(
                        "",
                        null,
                        null,
                        keyColumnNames,
                        null,
                        getOrderByClause(),
                        preQueryStatements,
                        LAST_UPDATE_TIME_COL_NAME,
                        null,
                        null,
                        null,
                        DEFAULT_FETCH_SIZE,
                        null,
                        null,
                        dataSource,
                        null,
                        null,
                        noRetry,
                        null,
                        null);
        this.columnNameTypeMap = new HashMap<>();
        for (int idx = 0; idx < columnNames.size(); idx++) {
            String columnName = columnNames.get(idx);
            ColumnType columnType = columnTypes.get(idx);
            columnNameTypeMap.put(columnName, columnType);
        }
    }

    private String fetchLastUpdatedTimestampColumn(List<String> columnNames) {
        for (String columnName : columnNames) {
            for (String updateTimeColPattern : LAST_UPDATED_TIME_COL_PATTERNS) {
                // Compile and match the pattern
                Pattern pattern = Pattern.compile(updateTimeColPattern);
                Matcher matcher = pattern.matcher(columnName);
                if (matcher.matches()) {
                    return columnName;
                }
            }
        }
        return "";
    }

    public DataRecord read(DataKey key) throws IOException {
        DataRecords records = read(Collections.singletonList(key));
        return !records.isEmpty() ? records.get(0) : null;
    }

    public DataRecords read(List<DataKey> keys) throws IOException {
        return read(Collections.singletonList(new MultiKeyDataFilter(keys)), columnNames);
    }

    public DataRecords read(List<DataFilter> filters, List<String> columns) throws IOException {
        if (columns == null) {
            columns = columnNames;
        }
        StringBuilder baseQuery = new StringBuilder();
        appendBaseQuery(baseQuery, columns);

        sqlQueryValueProvider.setBaseQuery(baseQuery.toString());
        DataRecords records;
        try {
            records = sqlQueryValueProvider.query(Collections.emptyMap(), filters);
            for (DataRecord record : records) {
                for (String col : columns) {
                    if (columnNameTypeMap.get(col) == ColumnType.JSON) {
                        // Unwrap the PGObject for json columns.
                        PGobject jsonObject = (PGobject) record.get(col);
                        record.put(col, jsonObject.getValue());
                    }
                }
            }
        } catch (InvalidDataException e) {
            throw new RuntimeException(
                    String.format("Fetched records are missing key column(s) %s", keyColumnNames));
        }
        records.setColumnNames(columns);
        return records;
    }

    public Stream<DataRecords> readStream(
            List<DataFilter> filters, final List<String> columnsToRead, int batchSize) {
        List<String> columns = columnsToRead != null ? columnsToRead : columnNames;
        StringBuilder baseQuery = new StringBuilder();
        appendBaseQuery(baseQuery, columns);

        sqlQueryValueProvider.setBaseQuery(baseQuery.toString());
        Stream<DataRecords> recordStream =
                sqlQueryValueProvider.queryForStream(Collections.emptyMap(), filters, batchSize);
        return recordStream.map(
                records -> {
                    for (DataRecord record : records) {
                        for (String col : columns) {
                            if (columnNameTypeMap.get(col) == ColumnType.JSON) {
                                // Unwrap the PGObject for json columns.
                                PGobject jsonObject = (PGobject) record.get(col);
                                record.put(col, jsonObject.getValue());
                            }
                        }
                    }
                    records.setColumnNames(columns);
                    return records;
                });
    }

    public CompletableFuture<DataRecords> readAsync(
            List<DataFilter> filters, List<String> columns, ExecutorService executor) {
        CompletableFuture<DataRecords> completableFuture = new CompletableFuture<>();
        executor.submit(
                () -> {
                    try {
                        completableFuture.complete(read(filters, columns));
                    } catch (Throwable e) {
                        logger.warn("Error reading data", e);
                        completableFuture.completeExceptionally(e);
                    }
                });
        return completableFuture;
    }

    public void write(DataRecord record) throws IOException, InvalidDataException {
        DataRecords records = new DataRecords();
        records.setKeyColumnNames(keyColumnNames);
        List<String> filteredColumnNames =
                columnNames.stream()
                        .filter(colName -> !colName.equalsIgnoreCase(LAST_UPDATE_TIME_COL_NAME))
                        .filter(record::containsKey)
                        .collect(Collectors.toList());
        records.setColumnNames(filteredColumnNames);
        records.add(record);
        write(records);
    }

    public void write(DataRecords records) throws IOException, InvalidDataException {
        logger.debug("Writing records to database table: {}", tableName);
        logger.debug("Writing records with columns: {}", records.getColumnNames());
        if (records.isEmpty()) {
            return;
        }
        if (!checkKeyColumns(records)) {
            throw new RuntimeException(
                    new InvalidDataException(
                            String.format(
                                    "Unable to write to table %s. Records are missing key column(s) %s: %s",
                                    this, keyColumnNames, records)));
        }
        List<String> columns = new ArrayList<>();
        Set<String> recordColumnsLowercase =
                records.getColumnNames().stream()
                        .map(String::toLowerCase)
                        .collect(Collectors.toSet());
        for (String colName : columnNames) {
            if (colName.equalsIgnoreCase(LAST_UPDATE_TIME_COL_NAME)) {
                continue;
            }
            if (recordColumnsLowercase.contains(colName.toLowerCase())) {
                columns.add(colName);
            }
        }
        StringBuilder sql = new StringBuilder();
        sql.append(String.format("INSERT INTO %s (", quote(tableName)));
        join(sql, columns, ", ");
        sql.append(", " + LAST_UPDATE_TIME_COL_NAME + ") VALUES ");

        sql.append("(");
        sql.append(join(columns.stream().map(n -> "?").collect(Collectors.toList()), ", "));
        sql.append(", current_timestamp");
        sql.append(")");

        sql.append(" ON CONFLICT (");
        join(sql, keyColumnNames, ", ");
        sql.append(") DO UPDATE ");
        sql.append("SET ");
        boolean first = true;
        for (String columnName : columns) {
            if (!first) {
                sql.append(", ");
            }
            first = false;
            sql.append(String.format("%s=EXCLUDED.%s", columnName, columnName));
        }
        sql.append(", " + LAST_UPDATE_TIME_COL_NAME + "=EXCLUDED." + LAST_UPDATE_TIME_COL_NAME);
        logger.debug("SQL CALL: {}", sql);
        jdbcTemplate
                .getJdbcTemplate()
                .batchUpdate(
                        sql.toString(),
                        new BatchPreparedStatementSetter() {

                            public void setValues(PreparedStatement ps, int idx)
                                    throws SQLException {
                                try {
                                    setPreparedStatementParamValues(records.get(idx), columns, ps);
                                } catch (InvalidDataException e) {
                                    throw new RuntimeException(e);
                                }
                            }

                            public int getBatchSize() {
                                return records.size();
                            }
                        });
    }

    private void setPreparedStatementParamValues(
            DataRecord record, List<String> columns, PreparedStatement ps)
            throws InvalidDataException, SQLException {

        int paramIdx = 1;
        for (String columnName : columns) {
            ColumnType type = columnNameTypeMap.get(columnName);

            Object value = DataTypeConversionUtil.convertTypeIfNeeded(record.get(columnName), type);

            if (columnNameTypeMap.get(columnName) == ColumnType.CHARACTER_ARRAY) {
                if (value instanceof java.util.List) {
                    List<?> list = (List<?>) value; // Cast to a raw list
                    boolean allStrings = list.stream().allMatch(item -> item instanceof String);

                    if (allStrings) {
                        // Obtain connection from DataSource
                        Connection connection = dataSource.getConnection();
                        value = connection.createArrayOf("VARCHAR", list.toArray());
                    }
                }
            }

            if (columnNameTypeMap.get(columnName) == ColumnType.JSON) {
                PGobject jsonObject = new PGobject();
                jsonObject.setType("json");
                try {
                    jsonObject.setValue(String.valueOf(value));
                } catch (SQLException ex) {
                    throw new InvalidDataException("Unable to set json column: " + value, ex);
                }
                value = jsonObject;
            }
            if (value instanceof String) {
                String valueStr = (String) value;
                if (valueStr.indexOf(0) >= 0) {
                    // Postgres does not allow null characters in strings.
                    value = valueStr.replace("\0", "");
                }
            }
            if (value != null
                    && (value instanceof List
                            || value instanceof Set
                            || value.getClass().isArray())) {
                ps.setObject(paramIdx++, value, Types.ARRAY);
            } else {
                ps.setObject(paramIdx++, value, type.getSqlType());
            }
        }
    }

    public void write(Publisher<DataRecord> records) throws IOException {
        throw new UnsupportedOperationException();
    }

    public void write(Publisher<DataRecord> publisher, int batchSize) throws IOException {
        Flux.from(publisher)
                .buffer(batchSize)
                .subscribe(
                        recordList -> {
                            DataRecords records = new DataRecords();
                            records.setKeyColumnNames(keyColumnNames);
                            records.setColumnNames(columnNames);
                            records.addAll(recordList);
                            try {
                                write(records);
                            } catch (IOException | InvalidDataException e) {
                                throw new RuntimeException(e);
                            }
                        });
    }

    public Publisher<DataRecord> readAll() throws IOException {
        // TODO(thorntonv): Implement method.
        throw new UnsupportedOperationException();
    }

    private boolean checkKeyColumns(DataRecords records) {
        DataRecords recordsCopy = new DataRecords();
        recordsCopy.addAll(records.getRecords());
        recordsCopy.setKeyColumnNames(keyColumnNames);
        return recordsCopy.validateKeyColumns();
    }

    private void appendBaseQuery(StringBuilder sql, List<String> columns) {
        if (columns == null || columns.isEmpty()) {
            throw new IllegalArgumentException("Columns must not be null");
        }
        sql.append("SELECT ");
        join(sql, columns, ", ");
        sql.append(String.format(" FROM %s ", quote(tableName)));
    }

    private String getOrderByClause() {
        StringJoiner joiner = new StringJoiner(", ");
        keyColumnNames.forEach(joiner::add);
        return joiner.toString();
    }

    static String quote(String str) {
        return String.format("\"%s\"", str);
    }

    static List<String> quote(List<String> strings) {
        return strings.stream().map(PostgreSQLData::quote).collect(Collectors.toList());
    }

    public void close() {}
}
