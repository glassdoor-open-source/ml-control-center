/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ColumnType.java
 */
package dataflow.postgres;

import java.sql.Types;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum ColumnType {
    BOOLEAN(Types.BOOLEAN, "bool", "boolean"),
    INTEGER(Types.INTEGER, "int", "integer"),
    LONG(Types.BIGINT, "long"),
    FLOAT(Types.FLOAT, "float"),
    DOUBLE(Types.DOUBLE, "double"),
    STRING(Types.VARCHAR, "string"),
    DATE(Types.DATE, "date"),
    DATETIME(Types.TIMESTAMP, "datetime"),
    JSON(Types.OTHER, "json"),
    OBJECT(Types.OTHER, "object"),
    CHARACTER_ARRAY(Types.ARRAY, "character_array", "char_array");

    private final int sqlType;
    private final Set<String> identifiers;

    ColumnType(int sqlType, String... identifiers) {
        this.sqlType = sqlType;
        this.identifiers = Arrays.stream(identifiers).collect(Collectors.toSet());
    }

    public static ColumnType forName(String name) {
        if (name == null) {
            return null;
        }
        name = name.trim().toLowerCase();
        for (ColumnType type : ColumnType.values()) {
            if (type.identifiers.contains(name)) {
                return type;
            }
        }
        return null;
    }

    public int getSqlType() {
        return sqlType;
    }
}
