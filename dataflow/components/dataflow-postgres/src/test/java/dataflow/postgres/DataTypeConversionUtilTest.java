/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataTypeConversionUtilTest.java
 */
package dataflow.postgres;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.data.InvalidDataException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;

public class DataTypeConversionUtilTest {

    @Before
    public void setUp() throws IOException {
        SimpleDependencyInjector dependencyInjector = new SimpleDependencyInjector();
        DataFlowEnvironment env = new DataFlowEnvironment("test", dependencyInjector, null);
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();

        discoveryService.register(discoveryConfig, env);
    }

    @Test
    public void convertTypeIfNeeded_stringToDate() throws InvalidDataException {
        assertEquals(
                Date.from(ZonedDateTime.parse("2017-08-23T23:24:39.339Z").toInstant()),
                DataTypeConversionUtil.convertTypeIfNeeded(
                        "2017-08-23T23:24:39.339Z", ColumnType.DATETIME));
        assertEquals(
                Date.from(LocalDateTime.parse("2020-10-17T22:05:52").toInstant(ZoneOffset.UTC)),
                DataTypeConversionUtil.convertTypeIfNeeded(
                        "2020-10-17T22:05:52", ColumnType.DATETIME));
        assertEquals(
                Date.from(LocalDate.parse("2020-10-17").atStartOfDay().toInstant(ZoneOffset.UTC)),
                DataTypeConversionUtil.convertTypeIfNeeded("2020-10-17", ColumnType.DATETIME));

        try {
            DataTypeConversionUtil.convertTypeIfNeeded("2020-10-17 22:05:52", ColumnType.DATETIME);
            fail("Expected exception was not thrown");
        } catch (Exception ignored) {
        }
    }
}
