

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.dataflowRest)

    api(projects.dataflowCore)
    api(projects.dataflowComponentCore)
    api(projects.dataflowRetry)
    api(projects.dataflowRest)
    api(libs.snakeyaml)
    api(libs.httpclient)
    api(libs.micrometerCore)
    api(libs.springContext)
    api(libs.springBeans)
    api(libs.springWeb)
    api(libs.jacksonCore)
    api(libs.jacksonAnnotations)
    api(libs.jacksonDatabind)
    api(libs.jacksonJaxrsJsonProvider)
    api(libs.jacksonDatatypeThreetenbp)
    api(libs.javaxAnnotationApi)
    testImplementation(libs.guava)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.jsonassert)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-rest-test"
