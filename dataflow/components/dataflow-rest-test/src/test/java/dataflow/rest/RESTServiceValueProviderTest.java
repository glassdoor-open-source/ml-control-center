/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  RESTServiceValueProviderTest.java
 */
package dataflow.rest;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.function.Consumer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

public class RESTServiceValueProviderTest {

    private DataFlowEnvironment env;
    private MockRestServiceServer mockServer;

    @Before
    public void setUp() throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        RESTServiceManager serviceManager = new RESTServiceManager();
        serviceManager.register("test", "http://localhost:8080", restTemplate);

        SimpleDependencyInjector dependencyInjector = new SimpleDependencyInjector();
        dependencyInjector.register(serviceManager);
        this.env = new DataFlowEnvironment("test", dependencyInjector, null);
        DataFlowDiscoveryConfigParser discoveryConfigParser = new DataFlowDiscoveryConfigParser();
        DataflowDiscoveryConfig discoveryConfig =
                discoveryConfigParser.parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);

        this.mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void test1_simplePost() throws Exception {
        mockServer
                .expect(ExpectedCount.once(), requestTo(new URI("http://localhost:8080/users")))
                .andExpect(method(HttpMethod.POST))
                .andExpect(
                        content().json("{\"name\":\"Tom\", \"location\":{\"postalCode\":60120}}"))
                .andRespond(
                        withStatus(HttpStatus.OK)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body("{\"result\": \"success\"}"));
        DataFlowInstance instance = registerAndGetInstance("test1_simplePost");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test2_simpleGet() throws Exception {
        mockServer
                .expect(
                        ExpectedCount.once(),
                        requestTo(new URI("http://localhost:8080/users/count")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withStatus(HttpStatus.OK)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body("{\"result\": \"success\"}"));
        DataFlowInstance instance = registerAndGetInstance("test2_simpleGet");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test3_getWithRequestParams() throws Exception {
        mockServer
                .expect(
                        ExpectedCount.once(),
                        requestTo(new URI("http://localhost:8080/users?userId=123")))
                .andExpect(method(HttpMethod.GET))
                .andExpect(queryParam("userId", "123"))
                .andRespond(
                        withStatus(HttpStatus.OK)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body("{\"result\": \"success\"}"));
        DataFlowInstance instance = registerAndGetInstance("test3_getWithQueryParams");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test4_getWithUrlParams() throws Exception {
        mockServer
                .expect(
                        ExpectedCount.once(),
                        requestTo(new URI("http://localhost:8080/users/123/details")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(
                        withStatus(HttpStatus.OK)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body("{\"result\": \"success\"}"));
        DataFlowInstance instance =
                registerAndGetInstance("test4_getWithUrlParams", i -> i.setValue("userId", 123));
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test5_getWithHeaders() throws Exception {
        mockServer
                .expect(ExpectedCount.once(), requestTo(new URI("http://localhost:8080/users")))
                .andExpect(method(HttpMethod.GET))
                .andExpect(header("cacheEnabled", "false"))
                .andRespond(
                        withStatus(HttpStatus.OK)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body("{\"result\": \"success\"}"));
        DataFlowInstance instance = registerAndGetInstance("test5_getWithHeaders");
        instance.execute();
        mockServer.verify();
        Map<String, Object> result = (Map<String, Object>) instance.getOutput();
        assertEquals(1, result.size());
        assertEquals("success", result.get("result"));
    }

    @Test
    public void test6_postReturnsCustomType() throws Exception {
        mockServer
                .expect(ExpectedCount.once(), requestTo(new URI("http://localhost:8080/users")))
                .andExpect(method(HttpMethod.POST))
                .andExpect(
                        content().json("{\"name\":\"Tom\", \"location\":{\"postalCode\":60120}}"))
                .andRespond(
                        withStatus(HttpStatus.OK)
                                .contentType(MediaType.APPLICATION_JSON)
                                .body("{\"id\": \"5\",\"name\":\"Tom\",\"postalCode\":60120}"));
        DataFlowInstance instance = registerAndGetInstance("test6_postReturnsCustomType");
        instance.execute();
        mockServer.verify();
        User result = (User) instance.getOutput();
        assertEquals((Integer) 5, result.getId());
        assertEquals("Tom", result.getName());
        assertEquals("60120", result.getPostalCode());
    }

    private DataFlowInstance registerAndGetInstance(String id) throws Exception {
        return registerAndGetInstance(id, i -> {});
    }

    private DataFlowInstance registerAndGetInstance(String id, Consumer<DataFlowInstance> initFn)
            throws Exception {
        return env.getRegistry()
                .getDataFlowRegistration(id)
                .getInstanceFactory()
                .newInstance(initFn);
    }
}
