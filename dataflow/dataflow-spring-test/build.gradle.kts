

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.dataflowSpring)

    api(projects.dataflowSpring)
    api(projects.dataflowCore)
    api(libs.springContext)
    api(libs.guava)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-spring-test"
