/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurationTest.java
 */
package dataflow.spring;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataFlowTestConfiguration.class)
public class DataFlowConfigurationTest {

    @Autowired private DataFlowEnvironment env;

    @Autowired private TestImpl testImpl;

    @Before
    public void setUp() {}

    @Test
    public void testWiring() {
        assertNotNull(env);
    }

    @Test
    public void testDependencyInjection() throws Exception {
        DataFlowInstance instance = registerAndGetInstance("dependencyInjectionTest");
        instance.execute();
        TestImpl injectedImpl = (TestImpl) instance.getOutput();
        assertSame(injectedImpl, testImpl);
    }

    private DataFlowInstance registerAndGetInstance(String id) throws Exception {
        return env.getRegistry()
                .getDataFlowRegistration(id)
                .getInstanceFactory()
                .newInstance(i -> {});
    }
}
