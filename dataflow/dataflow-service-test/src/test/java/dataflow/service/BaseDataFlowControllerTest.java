/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BaseDataFlowControllerTest.java
 */
package dataflow.service;

import static org.junit.Assert.*;
import static org.mockito.Answers.RETURNS_MOCKS;

import com.google.common.collect.ImmutableMap;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.spring.DataFlowConfiguration;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataFlowConfiguration.class)
public class BaseDataFlowControllerTest {

    @Autowired private DataFlowEnvironment env;

    @MockBean(answer = RETURNS_MOCKS)
    private MeterRegistry mockMeterRegistry;

    @Test
    public void testRequest() throws Exception {
        TestDataFlowController controller = new TestDataFlowController(env);
        Map<String, Object> req =
                ImmutableMap.<String, Object>builder()
                        .put("value1", "123")
                        .put("value2", ImmutableMap.of("field1", "3.14"))
                        .build();
        Map<String, Object> result = controller.process("testFlow", null, req).getBody();

        assertNotNull(result.get("executionId"));
        assertEquals(123, result.get("myValue"));
        assertEquals(3.14, result.get("field1"));
    }
}
