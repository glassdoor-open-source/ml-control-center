

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowService)
    api(projects.dataflowCore)
    api(projects.dataflowRetry)
    api(libs.springBootStarterWeb)
    api(libs.micrometerCore)
    testImplementation(projects.dataflowSpring)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-service-test"
