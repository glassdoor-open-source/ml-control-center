/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SpringDependencyInjector.java
 */
package dataflow.spring;

import dataflow.core.engine.DependencyInjector;
import org.springframework.context.ApplicationContext;

/**
 * A {@link DependencyInjector} implementation that inject dependencies from the spring application
 * context.
 */
public class SpringDependencyInjector implements DependencyInjector {

    private final ApplicationContext appContext;

    public SpringDependencyInjector(ApplicationContext appContext) {
        this.appContext = appContext;
    }

    @Override
    public <T> T getInstance(Class<T> instanceClass) {
        return appContext.getBean(instanceClass);
    }

    @Override
    public <T> T injectDependencies(T obj) {
        appContext.getAutowireCapableBeanFactory().autowireBean(obj);
        return obj;
    }
}
