

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(libs.googleAutoService)
    annotationProcessor(projects.dataflowComponentCodegen)

    api(projects.dataflowCodegenUtil)
    api(projects.dataflowComponentCodegen)

    api(libs.slf4jApi)
    api(libs.snakeyaml)
    api(libs.reflections)

    testImplementation(libs.junit)
    testImplementation(libs.guava)
    testImplementation(libs.mockitoCore)
}

group = "dataflow"
description = "dataflow-core"
