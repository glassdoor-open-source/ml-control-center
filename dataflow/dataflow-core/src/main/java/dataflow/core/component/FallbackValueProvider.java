/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FallbackValueProvider.java
 */
package dataflow.core.component;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import java.util.List;

/**
 * A ValueProvider that returns the primary value if it is available, otherwise the first available
 * value from the list of fallback values is returned.
 */
@DataFlowComponent
public class FallbackValueProvider {

    @DataFlowConfigurable
    public FallbackValueProvider() {}

    @OutputValue
    public Object getValue(@InputValue Object value, @InputValue List fallbackValues) {
        if (value != null) {
            return value;
        }
        return fallbackValues.stream().filter(item -> item != null).findFirst().orElse(null);
    }
}
