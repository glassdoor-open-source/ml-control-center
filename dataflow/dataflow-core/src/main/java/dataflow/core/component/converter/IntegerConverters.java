/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntegerConverters.java
 */
package dataflow.core.component.converter;

import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.ValueType;

public class IntegerConverters {

    public static final class Integer_to_IntegerValue_converter
            extends AbstractValueTypeConverter<Integer, IntegerValue> {

        public Integer_to_IntegerValue_converter() {
            super(Integer.class, IntegerValue.class);
        }

        public IntegerValue convert(Integer input, ValueType toValueType) {
            return input != null ? new IntegerValue(input) : new IntegerValue();
        }
    }

    public static final class Integer_to_String_converter
            extends AbstractValueTypeConverter<Integer, String> {

        public Integer_to_String_converter() {
            super(Integer.class, String.class);
        }

        public String convert(Integer input, ValueType toValueType) {
            return String.valueOf(input);
        }
    }

    public static final class Short_to_Integer_converter
            extends AbstractValueTypeConverter<Short, Integer> {

        public Short_to_Integer_converter() {
            super(Short.class, Integer.class);
        }

        public Integer convert(Short input, ValueType toValueType) {
            return input.intValue();
        }
    }
}
