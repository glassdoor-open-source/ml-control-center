/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StringConverters.java
 */
package dataflow.core.component.converter;

import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.DoubleValue;
import dataflow.core.type.FloatValue;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;
import dataflow.core.type.ValueType;
import dataflow.core.type.ValueTypeConverter;
import dataflow.core.util.ArrayUtil;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unchecked")
public class StringConverters {

    public static final class String_to_Double_converter
            extends AbstractValueTypeConverter<String, Double> {

        public String_to_Double_converter() {
            super(String.class, Double.class);
        }

        @Override
        public Double convert(final String value, ValueType toValueType) {
            return Double.valueOf(value);
        }
    }

    public static final class String_to_DoubleArray_converter
            extends AbstractValueTypeConverter<String, double[]> {

        public String_to_DoubleArray_converter() {
            super(String.class, double[].class);
        }

        @Override
        public double[] convert(final String value, ValueType toValueType) {
            return ArrayUtil.doubleArrayFromString(value);
        }
    }

    public static final class String_to_DoubleValue_converter
            extends AbstractValueTypeConverter<String, DoubleValue> {

        public String_to_DoubleValue_converter() {
            super(String.class, DoubleValue.class);
        }

        @Override
        public DoubleValue convert(final String value, ValueType toValueType) {
            DoubleValue result = new DoubleValue();
            result.fromString(value);
            return result;
        }
    }

    public static final class String_to_Enum_converter implements ValueTypeConverter<String, Enum> {

        private static final ValueType FROM_TYPE = new ValueType(String.class);

        @Override
        public Enum convert(final String value, ValueType toValueType) {
            try {
                Class<Enum> toClass = (Class<Enum>) Class.forName(toValueType.getType());
                return Enum.valueOf(toClass, value);
            } catch (ClassNotFoundException e) {
                throw new IllegalArgumentException("Invalid class " + toValueType, e);
            }
        }

        @Override
        public boolean canConvert(ValueType fromValueType, ValueType toValueType) {
            if (FROM_TYPE.equals(fromValueType)) {
                try {
                    return Enum.class.isAssignableFrom(Class.forName(toValueType.getType()));
                } catch (ClassNotFoundException ignored) {
                }
            }
            return false;
        }
    }

    public static final class String_to_Float_converter
            extends AbstractValueTypeConverter<String, Float> {

        public String_to_Float_converter() {
            super(String.class, Float.class);
        }

        @Override
        public Float convert(final String value, final ValueType toType) {
            return Float.parseFloat(value);
        }
    }

    public static final class String_to_FloatValue_converter
            extends AbstractValueTypeConverter<String, FloatValue> {

        public String_to_FloatValue_converter() {
            super(String.class, FloatValue.class);
        }

        @Override
        public FloatValue convert(final String value, ValueType toValueType) {
            FloatValue result = new FloatValue();
            result.fromString(value);
            return result;
        }
    }

    public static final class String_to_Integer_converter
            extends AbstractValueTypeConverter<String, Integer> {

        public String_to_Integer_converter() {
            super(String.class, Integer.class);
        }

        @Override
        public Integer convert(final String value, ValueType toValueType) {
            return Integer.parseInt(value);
        }
    }

    public static final class String_to_IntegerValue_converter
            extends AbstractValueTypeConverter<String, IntegerValue> {

        public String_to_IntegerValue_converter() {
            super(String.class, IntegerValue.class);
        }

        @Override
        public IntegerValue convert(final String value, ValueType toValueType) {
            IntegerValue result = new IntegerValue();
            result.fromString(value);
            return result;
        }
    }

    public static final class String_to_Long_converter
            extends AbstractValueTypeConverter<String, Long> {

        public String_to_Long_converter() {
            super(String.class, Long.class);
        }

        @Override
        public Long convert(final String value, ValueType toValueType) {
            return Long.parseLong(value);
        }
    }

    public static final class String_to_LongValue_converter
            extends AbstractValueTypeConverter<String, LongValue> {

        public String_to_LongValue_converter() {
            super(String.class, LongValue.class);
        }

        @Override
        public LongValue convert(final String value, ValueType toValueType) {
            LongValue result = new LongValue();
            result.fromString(value);
            return result;
        }
    }

    public static final class String_to_Date_converter
            extends AbstractValueTypeConverter<String, Date> {

        public String_to_Date_converter() {
            super(String.class, Date.class);
        }

        @Override
        public Date convert(final String value, ValueType toValueType) {
            try {
                return Date.from(ZonedDateTime.parse(value).toInstant());
            } catch (DateTimeParseException ignored) {
            }
            try {
                return Date.from(LocalDateTime.parse(value).atZone(ZoneOffset.UTC).toInstant());
            } catch (DateTimeParseException ignored) {
            }
            try {
                return Date.from(LocalDate.parse(value).atStartOfDay(ZoneOffset.UTC).toInstant());
            } catch (DateTimeParseException ignored) {
            }
            throw new RuntimeException("Unable to parse date string " + value);
        }
    }

    public static final class String_to_List_converter
            extends AbstractValueTypeConverter<String, List> {

        private static final String DELIMETER = ",";

        public String_to_List_converter() {
            super(String.class, List.class);
        }

        @Override
        public List convert(String value, ValueType toValueType) {
            List retVal = new ArrayList<>();
            if (value.startsWith("[")) {
                value = value.substring(1);
            }
            if (value.endsWith("]")) {
                value = value.substring(0, value.length() - 1);
            }
            if (value != null && !value.isBlank()) {
                ValueType itemType = toValueType.getTypeGenericParams().get(0);
                ValueTypeConverter converter =
                        DataFlowExecutionContext.getCurrentExecutionContext()
                                .getEnvironment()
                                .getRegistry()
                                .getTypeRegistry()
                                .getConverter(new ValueType(String.class), itemType);
                for (String itemStr : value.split(DELIMETER)) {
                    retVal.add(converter.convert(itemStr, itemType));
                }
            }
            return retVal;
        }
    }
}
