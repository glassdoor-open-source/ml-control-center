/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ListConverters.java
 */
package dataflow.core.component.converter;

import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.ValueType;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public class ListConverters {

    public static final class List_to_Set_converter extends AbstractValueTypeConverter<List, Set> {

        public List_to_Set_converter() {
            super(List.class, Set.class);
        }

        @Override
        public Set convert(final List list, ValueType toValueType) {
            Set set = new HashSet();
            set.addAll(list);
            return set;
        }
    }
}
