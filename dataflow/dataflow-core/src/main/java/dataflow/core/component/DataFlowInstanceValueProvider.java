/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstanceValueProvider.java
 */
package dataflow.core.component;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.ThreadLocalDataFlowInstance;
import java.util.Map;

@DataFlowComponent(description = "Provides a DataFlowInstance for a given DataFlow id.")
public class DataFlowInstanceValueProvider implements AutoCloseable {

    private final ThreadLocalDataFlowInstance dataFlowInstance;

    @DataFlowConfigurable
    public DataFlowInstanceValueProvider(
            @DataFlowConfigProperty String dataFlowId,
            @DataFlowConfigProperty(required = false) Map<String, Object> values) {
        DataFlowInstance parentInstance =
                DataFlowExecutionContext.getCurrentExecutionContext().getInstance();
        this.dataFlowInstance =
                ThreadLocalDataFlowInstance.create(
                        dataFlowId,
                        parentInstance,
                        newInstance -> {
                            if (values != null) {
                                values.forEach(newInstance::setValue);
                            }
                        });
    }

    @OutputValue
    public DataFlowInstance getValue(@InputValues Map<String, Object> values) {
        if (values != null) {
            values.forEach(dataFlowInstance::setValue);
        }
        return dataFlowInstance;
    }

    @Override
    public void close() throws Exception {
        dataFlowInstance.close();
    }
}
