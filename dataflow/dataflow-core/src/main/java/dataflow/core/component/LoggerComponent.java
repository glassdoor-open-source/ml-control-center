/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LoggerComponent.java
 */
package dataflow.core.component;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.Arrays;
import java.util.Map;
import org.slf4j.LoggerFactory;

@DataFlowComponent(typeName = "Logger")
public class LoggerComponent {

    private static final String[] LEVELS = {"trace", "debug", "info", "warn", "error"};

    private final boolean enabled;
    private final org.slf4j.Logger logger;
    private final String level;

    @DataFlowConfigurable
    public LoggerComponent(
            @DataFlowConfigProperty(required = false) String name,
            @DataFlowConfigProperty(required = false) String level,
            @DataFlowConfigProperty(required = false) Boolean enabled) {
        if (name == null || name.isBlank()) {
            name = LoggerComponent.class.getSimpleName();
        }
        logger = LoggerFactory.getLogger(name);
        this.level = level != null ? level.toLowerCase() : "warn";
        if (Arrays.stream(LEVELS).noneMatch(l -> l.equalsIgnoreCase(this.level))) {
            throw new DataFlowConfigurationException(
                    "Invalid log level " + level + " should be one of " + Arrays.toString(LEVELS));
        }
        this.enabled = enabled != null ? enabled : Boolean.TRUE;
    }

    @OutputValue
    public Void getValue(@InputValues Map<String, Object> input) {
        if (!enabled) {
            return null;
        }
        switch (level) {
            case "trace":
                if (logger.isTraceEnabled()) {
                    logger.trace(String.valueOf(input));
                }
            case "debug":
                if (logger.isDebugEnabled()) {
                    logger.debug(String.valueOf(input));
                }
                break;
            case "info":
                if (logger.isInfoEnabled()) {
                    logger.info(String.valueOf(input));
                }
                break;
            case "warn":
                if (logger.isWarnEnabled()) {
                    logger.warn(String.valueOf(input));
                }
                break;
            case "error":
                if (logger.isErrorEnabled()) {
                    logger.error(String.valueOf(input));
                }
                break;
        }
        return null;
    }
}
