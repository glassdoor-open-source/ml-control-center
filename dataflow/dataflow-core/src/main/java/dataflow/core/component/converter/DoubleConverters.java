/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DoubleConverters.java
 */
package dataflow.core.component.converter;

import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.DoubleValue;
import dataflow.core.type.ValueType;

public class DoubleConverters {

    public static class Double_to_DoubleValue_converter
            extends AbstractValueTypeConverter<Double, DoubleValue> {

        public Double_to_DoubleValue_converter() {
            super(Double.class, DoubleValue.class);
        }

        @Override
        public DoubleValue convert(final Double value, ValueType toValueType) {
            return value != null ? new DoubleValue(value) : new DoubleValue();
        }
    }
}
