/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  StreamForEach.java
 */
package dataflow.core.component.stream;

import static dataflow.core.config.DataFlowConfig.DEFAULT_ITEM_VALUE_NAME;

import dataflow.core.component.annotation.*;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import java.util.stream.Stream;

@DataFlowComponent
public class StreamForEach {

    private final boolean parallel;
    private final String itemValueName;

    @DataFlowConfigurable
    public StreamForEach(
            @DataFlowConfigProperty(
                            required = false,
                            description = "The name of the item value in the mapping DataFlow")
                    String itemValueName,
            @DataFlowConfigProperty(required = false) Boolean parallel) {
        this.itemValueName = itemValueName != null ? itemValueName : DEFAULT_ITEM_VALUE_NAME;
        this.parallel = parallel != null ? parallel : false;
    }

    @OutputValue
    public Void getValue(@InputValue Stream<?> stream, @InputValue DataFlowInstance doFlow) {
        if (parallel) {
            // TODO(thorntonv): Support parallel execution outside of the common fork-join pool.
            stream = stream.parallel();
        }
        final DataFlowExecutionContext executionContext =
                DataFlowExecutionContext.getCurrentExecutionContext();
        if (executionContext == null) {
            throw new RuntimeException("Execution context is not set");
        }
        stream.forEach(
                item ->
                        executionContext.execute(
                                () -> {
                                    doFlow.setValue(itemValueName, item);
                                    doFlow.execute();
                                    return doFlow.getOutput();
                                }));
        return null;
    }
}
