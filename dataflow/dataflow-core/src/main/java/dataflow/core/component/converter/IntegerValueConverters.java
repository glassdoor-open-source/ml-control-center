/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IntegerValueConverters.java
 */
package dataflow.core.component.converter;

import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.ValueType;

public class IntegerValueConverters {

    public static final class IntegerValue_to_Integer_converter
            extends AbstractValueTypeConverter<IntegerValue, Integer> {

        public IntegerValue_to_Integer_converter() {
            super(IntegerValue.class, Integer.class);
        }

        @Override
        public Integer convert(final IntegerValue value, ValueType toValueType) {
            return value != null ? value.toInt() : null;
        }
    }

    public static final class IntegerValue_to_String_converter
            extends AbstractValueTypeConverter<IntegerValue, String> {

        public IntegerValue_to_String_converter() {
            super(IntegerValue.class, String.class);
        }

        @Override
        public String convert(final IntegerValue value, ValueType toValueType) {
            return value != null ? value.toString() : null;
        }
    }
}
