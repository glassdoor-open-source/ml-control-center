/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowValueProvider.java
 */
package dataflow.core.component;

import static dataflow.core.engine.DataFlowExecutionContext.getCurrentExecutionContext;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.ThreadLocalDataFlowInstance;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DataFlowComponent(
        description = "Provides the output value of the dataflow with the given id and parameters.")
public class DataFlowValueProvider {

    private static final Logger logger = LoggerFactory.getLogger(DataFlowValueProvider.class);

    private final String dataFlowId;

    @DataFlowConfigurable
    public DataFlowValueProvider(@DataFlowConfigProperty String dataFlowId) {
        this.dataFlowId = dataFlowId;
    }

    @OutputValue
    public CompletableFuture<Object> getValue(@InputValues Map<String, Object> values) {
        final DataFlowInstance parentInstance = getCurrentExecutionContext().getInstance();
        CompletableFuture<Object> futureResult = new CompletableFuture<>();
        getCurrentExecutionContext()
                .getEnvironment()
                .executeAsync(
                        () -> {
                            DataFlowInstance dataFlowInstance = null;
                            try {
                                dataFlowInstance =
                                        ThreadLocalDataFlowInstance.create(
                                                dataFlowId,
                                                parentInstance,
                                                newInstance -> {
                                                    if (values != null) {
                                                        values.forEach(newInstance::setValue);
                                                    }
                                                });

                                dataFlowInstance.execute();
                                futureResult.complete(dataFlowInstance.getOutput());
                            } catch (Throwable e) {
                                futureResult.completeExceptionally(e);
                            } finally {
                                if (dataFlowInstance != null) {
                                    try {
                                        dataFlowInstance.close();
                                    } catch (Exception e) {
                                        logger.warn(
                                                "Exception while closing dataflow instance "
                                                        + dataFlowId,
                                                e);
                                    }
                                }
                            }
                        },
                        getClass().getSimpleName() + "-" + dataFlowId);

        return futureResult;
    }
}
