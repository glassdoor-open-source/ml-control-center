/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LongConverters.java
 */
package dataflow.core.component.converter;

import dataflow.core.type.AbstractValueTypeConverter;
import dataflow.core.type.LongValue;
import dataflow.core.type.ValueType;

public class LongConverters {

    public static final class Long_to_LongValue_converter
            extends AbstractValueTypeConverter<Long, LongValue> {

        public Long_to_LongValue_converter() {
            super(Long.class, LongValue.class);
        }

        public LongValue convert(Long input, ValueType toValueType) {
            return input != null ? new LongValue(input) : new LongValue();
        }
    }

    public static final class Long_to_String_converter
            extends AbstractValueTypeConverter<Long, String> {

        public Long_to_String_converter() {
            super(Long.class, String.class);
        }

        public String convert(Long input, ValueType toValueType) {
            return String.valueOf(input);
        }
    }
}
