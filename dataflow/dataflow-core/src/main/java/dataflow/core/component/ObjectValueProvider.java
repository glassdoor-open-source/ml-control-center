/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ObjectValueProvider.java
 */
package dataflow.core.component;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent(description = "A ValueProvider that provides a given value.")
public class ObjectValueProvider {

    private Object value;

    @DataFlowConfigurable
    public ObjectValueProvider(
            @DataFlowConfigProperty(description = "The value that will be provided")
                    final Object value) {
        this.value = value;
    }

    @OutputValue
    public Object getValue() {
        return value;
    }
}
