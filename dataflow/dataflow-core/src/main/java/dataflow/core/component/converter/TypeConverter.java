/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TypeConverter.java
 */
package dataflow.core.component.converter;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.registry.TypeRegistry;
import dataflow.core.type.TypeUtil;
import dataflow.core.type.ValueType;

@DataFlowComponent(
        description =
                "A component that can be used to convert a value from one type to another using the\n"
                        + " * {@link ValueTypeConverter}s registered in the dataflow environment.")
public class TypeConverter {

    private final ValueType toType;
    private final TypeRegistry typeRegistry;

    @DataFlowConfigurable
    public TypeConverter(@DataFlowConfigProperty String toType) {
        this.toType = TypeUtil.valueTypeFromString(toType);
        this.typeRegistry =
                DataFlowExecutionContext.getCurrentExecutionContext()
                        .getEnvironment()
                        .getRegistry()
                        .getTypeRegistry();
    }

    @OutputValue
    public Object getValue(@InputValue Object value) {
        return TypeUtil.convert(value, toType, typeRegistry);
    }
}
