/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ListItemValueProvider.java
 */
package dataflow.core.component.collection;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@DataFlowComponent
public class ListItemValueProvider {

    private int index;

    @DataFlowConfigurable
    public ListItemValueProvider(@DataFlowConfigProperty int index) {
        this.index = index;
    }

    @OutputValue
    public Object getValue(@InputValue List<Object> list) {
        if (index >= list.size()) {
            throw new IndexOutOfBoundsException(
                    String.format("Invalid index: %d, list size: %d", index, list.size()));
        }
        if (list instanceof ArrayList) {
            ArrayList<Object> arrayList = (ArrayList<Object>) list;
            return arrayList.get(index);
        } else {
            Iterator<Object> it = list.iterator();
            for (int cnt = 1; cnt <= index && it.hasNext(); cnt++) {
                it.next();
            }
            return it.hasNext() ? it.next() : null;
        }
    }
}
