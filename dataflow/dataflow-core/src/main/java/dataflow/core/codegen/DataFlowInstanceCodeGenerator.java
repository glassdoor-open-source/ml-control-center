/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstanceCodeGenerator.java
 */
package dataflow.core.codegen;

import static dataflow.codegen.util.annotation.AnnotationProcessorUtil.javaEscape;
import static dataflow.core.codegen.CodeGenerationUtil.getInProgressAsyncInputIds;
import static dataflow.core.codegen.CodeGenerationUtil.getValueReferenceExpression;
import static dataflow.core.codegen.CodeGenerationUtil.getVarName;
import static dataflow.core.codegen.CodeGenerationUtil.hasCodeGenerator;
import static dataflow.core.codegen.CodeGenerationUtil.isConstant;
import static dataflow.core.codegen.CodeGenerationUtil.isProvidedValue;
import static dataflow.core.codegen.CodeGenerationUtil.isSimpleConstant;
import static dataflow.core.codegen.CodeGenerationUtil.setValueStmt;
import static dataflow.core.codegen.CodeGenerationUtil.valueIsSetExpr;
import static dataflow.core.util.DataFlowConfigUtil.getSortedConfigs;

import dataflow.core.codegen.DataFlowCodeGenerationContext.ComponentCodeGenerationContext;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.AbstractDataFlowInstance;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.type.ValueType;
import dataflow.core.util.IndentPrintWriter;
import dataflow.core.util.StringUtil;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

/** Generates the code for a DataFlow instance. */
public class DataFlowInstanceCodeGenerator {

    private static final String[] IMPORTS =
            new String[] {
                DataFlowInstance.class.getName(),
                AbstractDataFlowInstance.class.getName(),
                DataFlowEnvironment.class.getName(),
                DataFlowConfig.class.getName(),
                DataFlowExecutionException.class.getName(),
                DataFlowExecutionContext.class.getName(),
                ValueType.class.getName(),
                "java.util.*",
                "java.util.function.*",
                "java.util.concurrent.*",
                "java.util.stream.*"
            };

    private final DataFlowRegistry registry;

    public DataFlowInstanceCodeGenerator(final DataFlowRegistry registry) {
        this.registry = registry;
    }

    public static String getDefaultClassName(DataFlowConfig dataFlowConfig) {
        return dataFlowConfig.getId().substring(0, 1).toUpperCase()
                + dataFlowConfig.getId().substring(1)
                + "DataFlow";
    }

    public String generateSource(
            DataFlowConfig dataFlowConfig,
            String rawYamlConfig,
            DataFlowInstanceCodeGenConfig codeGenConfig) {
        StringWriter stringWriter = new StringWriter();
        IndentPrintWriter out = new IndentPrintWriter(new PrintWriter(stringWriter));

        DataFlowCodeGenerationContext context = createContext(dataFlowConfig, codeGenConfig);

        out.printf("package %s;%n", codeGenConfig.getPackageName());
        out.println();
        writeImports(out, context);
        out.printf(
                "public class %s extends AbstractDataFlowInstance {%n",
                context.getInstanceClassName());
        out.indent();

        writeFields(out, rawYamlConfig, context);
        writeConstructor(out, context);
        writeInitMethod(out, context);
        writeGetComponentsMethod(out, context);
        writeSetValueMethod(out, context);
        writeGetValuesMethod(out, context);
        writeGetValueMethod(out, context);
        writeGetOutputTypeMethod(out, context);
        writeGetValueTypeMethod(out, context);
        writeExecuteMethod(out, context);
        writeGetOutputMethod(out, context);
        writeUpdateAvailabilityMethod(out, context);
        writeCloseMethod(out, context);
        out.unindent();
        out.println("}");
        return stringWriter.toString();
    }

    private void writeImports(IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        TreeSet<String> imports = new TreeSet<>(Arrays.asList(IMPORTS));
        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            ComponentCodeGenerator codeGenerator = getCodeGenerator(componentConfig);
            imports.addAll(codeGenerator.getImports());
        }
        for (String importStr : imports) {
            out.printf("import %s;%n", importStr);
        }
        out.println();
    }

    private void writeFields(
            IndentPrintWriter out, String rawYamlConfig, DataFlowCodeGenerationContext context) {
        out.println(
                "public static final int ENVIRONMENT_VERSION = "
                        + DataFlowEnvironment.VERSION
                        + ";");

        String parentId = context.getDataFlowConfig().getParentId();
        if (parentId == null) {
            String rawYamlBase64 = Base64.getEncoder().encodeToString(rawYamlConfig.getBytes());
            out.println("// Base 64 encoded versions of the configs used to generate this code");
            out.println("public static final String YAML_CONFIG = \"" + rawYamlBase64 + "\";");
            out.println(
                    "public static final Map<String, String> IMPORT_CONFIGS = new HashMap<>();");
            out.println("static {");
            out.indent();
            context.getDataFlowConfig()
                    .getImportConfigs()
                    .forEach(
                            (key, value) -> {
                                String importRawYamlBase64 =
                                        Base64.getEncoder().encodeToString(value.getBytes());
                                out.println(
                                        "IMPORT_CONFIGS.put(\""
                                                + key
                                                + "\", \""
                                                + importRawYamlBase64
                                                + "\");");
                            });
            out.unindent();
            out.println("}");
        }
        out.println(
                "public static final String PARENT_ID = "
                        + (parentId != null ? ("\"" + parentId + "\"") : "null")
                        + ";");

        out.println(
                "private final Map<String, Runnable> componentInitRunnables = new HashMap<>();");

        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            String id = componentConfig.getId();
            ComponentCodeGenerationContext componentContext =
                    context.getComponentContext(componentConfig);
            if (isProvidedValue(componentConfig)) {
                DefaultComponentCodeGenerator.writeValueField(out, id, context);
            } else if (!isSimpleConstant(componentConfig)) {
                ComponentCodeGenerator codeGenerator = getCodeGenerator(componentConfig);
                codeGenerator.writeFields(out, componentContext);
            } else {
                Object value = componentConfig.getProperties().get("value");
                String valueString = String.valueOf(value);
                if (value instanceof String) {
                    valueString = "\"" + value + "\"";
                }
                out.printf(
                        "private volatile %s %s_value = %s;%n",
                        context.getFieldType(id), getVarName(id), valueString);
            }

            // Write the available and finished flags.
            out.printf("private boolean %s_available = true;%n", getVarName(id));
            if (!isSimpleConstant(componentConfig)) {
                out.printf(
                        "private volatile boolean %s_finished = false;%n",
                        componentContext.getVarNamePrefix());
            } else {
                out.printf(
                        "private volatile boolean %s_finished = true;%n",
                        componentContext.getVarNamePrefix());
            }
        }

        out.println();
    }

    private void writeConstructor(IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.printf(
                "%s(DataFlowEnvironment env, Executor executor, DataFlowConfig dataFlowConfig, Consumer<DataFlowInstance> initFn) {%n",
                context.getInstanceClassName());
        out.indent();

        out.printf("super(dataFlowConfig, env, executor);\n");
        // Add the future holders
        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            String id = componentConfig.getId();
            ComponentCodeGenerationContext componentContext =
                    context.getComponentContext(componentConfig);
            if (componentContext.isAsync()) {
                out.printf(
                        "futureHolders.put(\"%s\", new CompletableFutureHolder(\"%s\"));%n",
                        id, id);
            }
        }

        out.println("initFn.accept(this);");
        out.println("init();");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeInitMethod(IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public void init() {");
        out.indent();

        out.println("createExecutionContext();");
        out.println("try {");
        out.indent();

        // Call the init method for each component.
        context.getSortedConfigs().stream()
                .filter(v -> v.getType() != null && !isSimpleConstant(v))
                .forEach(v -> out.printf("%s_init();%n", getVarName(v.getId())));

        out.println("updateAvailability();");

        out.unindent();
        out.println("} finally { DataFlowExecutionContext.popExecutionContext(); }");

        out.unindent();
        out.println("}");
        out.println();

        // Write the component init methods.
        context.getSortedConfigs().stream()
                .filter(v -> v.getType() != null && !isSimpleConstant(v))
                .forEach(v -> writeInitComponentMethod(out, v, context));
    }

    private void writeInitComponentMethod(
            IndentPrintWriter out,
            ComponentConfig componentConfig,
            DataFlowCodeGenerationContext context) {
        ComponentCodeGenerationContext componentContext =
                context.getComponentContext(componentConfig);
        String namePrefix = componentContext.getVarNamePrefix();
        ComponentCodeGenerator codeGenerator = getCodeGenerator(componentConfig);

        out.printf("private void %s_init() {%n", namePrefix);
        out.indent();

        // A list of condition expressions that must be satisfied before initializing the component.
        List<String> initConditions = new ArrayList<>();

        // If the component has properties that are dependent on the output of other components then
        // those
        // components must be finished before this component can be initialized.
        componentContext.getComponentConfig().getPropertyValueProviders().values().stream()
                .filter(v -> !(isProvidedValue(v) || isSimpleConstant(v)))
                .forEach(
                        v ->
                                initConditions.add(
                                        String.format("%s_finished", getVarName(v.getId()))));

        String initConditionExpr = StringUtil.join(initConditions, " && ");
        if (!initConditionExpr.isEmpty()) {
            out.printf("if(%s) {%n", initConditionExpr);
            out.indent();
        }
        codeGenerator.writeConstructorStatements(out, componentContext);
        if (!initConditionExpr.isEmpty()) {
            // Only need to update availability if the init was conditional. For unconditional
            // component inits
            // availability is updated at the end of the init method.
            out.println("updateAvailability();");
            out.unindent();
            out.println("}");
        }
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeSetValueMethod(IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public void setValue(String id, Object value) {");
        out.indent();

        out.println("switch(id) {");
        out.indent();
        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            if (componentConfig.isInternal() || isConstant(componentConfig)) {
                continue;
            }
            String id = componentConfig.getId();
            String varNamePrefix = getVarName(id);
            boolean async = false;
            if (!isProvidedValue(componentConfig)) {
                ComponentCodeGenerationContext componentContext =
                        context.getComponentContext(componentConfig);
                async = componentContext.isAsync();
            }
            String fieldTypeName =
                    context.getTypeRegistry()
                            .getValueTypeClassName(context.getFieldType(id))
                            .orElse(context.getFieldType(id));
            // If the field type is object there is no need to type cast it.
            String typeCast =
                    !fieldTypeName.equals(Object.class.getName())
                            ? String.format("(%s)", fieldTypeName)
                            : "";

            out.printf("case \"%s\":%n", id);
            out.indent();
            out.printf("%s_value = %svalue;%n", varNamePrefix, typeCast);
            if (async) {
                out.printf(
                        "setComponentFuture(\"%s\", CompletableFuture.completedFuture(value));%n",
                        id);
            }

            // The value is set so it is finished.
            out.printf("%s_finished = true;%n", varNamePrefix);

            // Set the component to null for any config that has this output as a properties input.
            // The components that are set to null will be initialized at the end of this method if
            // possible, otherwise they will be lazily re-initialized during flow execution.
            context.getSortedConfigs().stream()
                    .filter(
                            cfg ->
                                    cfg.getPropertyValueProviders()
                                            .containsKey(componentConfig.getId()))
                    .forEach(
                            cfg -> {
                                out.printf("%s_component = null;%n", getVarName(cfg.getId()));
                                // Defer trying the initialize method until this method is ready to
                                // finish.
                                out.printf(
                                        "componentInitRunnables.put(\"%s\", () -> %s_init());%n",
                                        getVarName(cfg.getId()), getVarName(cfg.getId()));
                            });

            // Any config that has this config as a dependency needs to be marked as not finished so
            // that
            // it can be re-computed based on the changed value on the next execute call.
            Map<String, ComponentConfig> outputComponents = new HashMap<>();
            addOutputComponentsRecursive(componentConfig, outputComponents);
            outputComponents
                    .values()
                    .forEach(
                            v -> {
                                if (context.getComponentContext(v) == null) {
                                    return;
                                }
                                out.printf("%s_finished = false;%n", getVarName(v.getId()));
                                // Set the component to null for any config that has this output as
                                // a properties input.
                                // The components that are set to null will be initialized at the
                                // end of this method if
                                // possible, otherwise they will be lazily re-initialized during
                                // flow execution.
                                context.getSortedConfigs().stream()
                                        .filter(
                                                cfg ->
                                                        cfg.getPropertyValueProviders()
                                                                .containsKey(v.getId()))
                                        .forEach(
                                                cfg -> {
                                                    out.printf(
                                                            "%s_component = null;%n",
                                                            getVarName(cfg.getId()));
                                                    // Defer trying the initialize method until this
                                                    // method is ready to finish.
                                                    out.printf(
                                                            "componentInitRunnables.put(\"%s\", () -> %s_init());%n",
                                                            getVarName(cfg.getId()),
                                                            getVarName(cfg.getId()));
                                                });
                            });

            out.println("break;");
            out.unindent();
        }
        out.unindent();
        out.println("}");
        out.println("updateAvailability();");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeGetValuesMethod(
            IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public Map<String, Object> getValues() {");
        out.indent();
        out.println("Map<String, Object> values = new HashMap<>();");
        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            String id = componentConfig.getId();
            out.printf("values.put(\"%s\", %s_value);%n", id, getVarName(id));
        }
        out.println("return values;");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeGetValueMethod(IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public Object getValue(String id) {");
        out.indent();
        out.println("switch(id) {");
        out.indent();
        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            String id = componentConfig.getId();
            if (!isConstant(componentConfig)) {
                out.printf("case \"%s\":%n", id);
                out.indent();
                out.printf("return %s_value;%n", getVarName(id));
                out.unindent();
            }
        }
        out.unindent();
        // end switch
        out.println("}");
        out.println("return null;");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeGetValueTypeMethod(
            IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public String getValueType(String id) {");
        out.indent();
        out.println("switch(id) {");
        out.indent();
        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            String id = componentConfig.getId();
            if (!isConstant(componentConfig)) {
                String type = context.getOutputType(id);
                if (type != null) {
                    out.printf("case \"%s\":%n", id);
                    out.indent();
                    out.printf("return \"%s\";%n", type);
                    out.unindent();
                }
            }
        }
        out.unindent();
        // end switch
        out.println("}");
        out.println("return null;");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeGetOutputTypeMethod(
            IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public String getOutputType() {");
        out.indent();
        ComponentCodeGenerationContext outputContext =
                context.getComponentContext(context.getDataFlowConfig().getOutput());
        out.printf("return \"%s\";%n", outputContext.getFieldType());
        out.unindent();
        out.println("}");
        out.println();
    }

    private void addOutputComponentsRecursive(
            ComponentConfig componentConfig, Map<String, ComponentConfig> outputComponents) {
        outputComponents.putAll(componentConfig.getOutputComponents());
        componentConfig
                .getOutputComponents()
                .values()
                .forEach(v -> addOutputComponentsRecursive(v, outputComponents));
    }

    private void writeGetComponentsMethod(
            IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public Map<String, Object> getComponents() {");
        out.indent();
        out.println("Map<String, Object> components = new LinkedHashMap<>();");

        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            if (!isProvidedValue(componentConfig)
                    && !isSimpleConstant(componentConfig)
                    && !hasCodeGenerator(componentConfig, context.getComponentRegistry())) {
                out.printf(
                        "components.put(\"%s\", %s_component);%n",
                        componentConfig.getId(), getVarName(componentConfig.getId()));
            }
        }
        out.println("return components;");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeExecuteMethod(IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public void execute() throws DataFlowExecutionException {");
        out.indent();
        out.println("DataFlowExecutionContext executionContext = createExecutionContext();");
        if (context.isEventsEnabled() || context.isTimeoutEnabled()) {
            out.println("this.startTimeMillis = System.currentTimeMillis();");
            out.println("fireDataFlowExecutionStartedEvent();");
        }
        out.println("try {");
        out.indent();
        out.println("componentInitRunnables.values().forEach(runnable -> runnable.run());");
        out.println("if(!componentInitRunnables.isEmpty()) updateAvailability();");
        out.println("componentInitRunnables.clear();");
        out.println("clearExecutedFutures();");
        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            if (!isProvidedValue(componentConfig) && !isSimpleConstant(componentConfig)) {
                ComponentCodeGenerationContext componentContext =
                        context.getComponentContext(componentConfig);

                String rawConfigComment =
                        String.valueOf(componentConfig.getRawConfig()).replaceAll("\\R", "");
                out.printf(
                        "// Component %s: %s%n", componentConfig.getIdWithPath(), rawConfigComment);

                if (!context.isIncrementalEnabled()) {
                    out.printf(
                            "if(!%s_available) {%n",
                            componentContext.getVarNamePrefix(),
                            componentContext.getVarNamePrefix());
                    out.indent();

                    List<String[]> availabilityExprReasonList =
                            getAvailabilityExprReasonList(componentConfig);
                    for (String[] availabilityExprReason : availabilityExprReasonList) {
                        String expr = availabilityExprReason[0];
                        String reason = availabilityExprReason[1];
                        out.printf("if(!(%s))%n", expr);
                        out.indent();
                        out.printf(
                                "throw new DataFlowExecutionException(\"Component %s %s is missing a required dependency: %s\");%n",
                                componentConfig.getIdWithPath(),
                                javaEscape(String.valueOf(componentConfig.getRawConfig())),
                                reason);
                        out.unindent();
                    }
                    out.unindent();
                    out.println("}");
                }

                // Only execute the component if it is available and is not already finished.
                out.printf(
                        "if(%s_available && !%s_finished) {%n",
                        componentContext.getVarNamePrefix(), componentContext.getVarNamePrefix());
                out.indent();

                if (!componentContext.isAsync()) {
                    writeExecuteSync(out, context, componentContext);
                } else {
                    writeExecuteAsync(out, context, componentContext);
                }
                // End if(available && !finished).
                out.unindent();
                out.println("}");
            }
        }

        ComponentConfig outputConfig = context.getDataFlowConfig().getOutput();

        out.println("waitForExecutedFutures();");

        out.println("this.endTimeMillis = System.currentTimeMillis();");
        out.println("fireDataFlowExecutionFinishedEvent();");
        out.unindent();
        out.println("} catch(Throwable t) {");
        out.indent();
        if (outputConfig.shouldFailOnError()) {
            out.println("this.endTimeMillis = System.currentTimeMillis();");
            out.println("fireDataFlowExecutionErrorEvent(t);");
            out.println("cancelExecutedFutures();");
            out.println("throw new DataFlowExecutionException(t);");
        } else {
            out.println("// shouldFailOnError = false for " + outputConfig.getIdWithPath());
        }
        out.unindent();
        out.println("} finally {");
        out.indent();
        out.println("DataFlowExecutionContext.popExecutionContext();");
        out.unindent();
        out.println("}");

        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeExecuteSync(
            IndentPrintWriter out,
            DataFlowCodeGenerationContext context,
            ComponentCodeGenerationContext componentContext) {
        ComponentConfig componentConfig = componentContext.getComponentConfig();
        ComponentCodeGenerator codeGenerator = getCodeGenerator(componentConfig);
        String varNamePrefix = componentContext.getVarNamePrefix();

        // Wait for any in progress async inputs to be ready.
        List<String> inputIds = getInProgressAsyncInputIds(componentContext);
        if (!inputIds.isEmpty()) {
            // Only wait if at least one of the async inputs is not finished.
            List<String> futureFinishedExprList =
                    getInProgressAsyncInputIds(componentContext).stream()
                            .map(id -> "!" + getVarName(id) + "_finished")
                            .collect(Collectors.toList());
            out.printf("if(%s) {%n", StringUtil.join(futureFinishedExprList, " || "));
            out.indent();

            getInProgressAsyncInputIds(componentContext)
                    .forEach(inputId -> out.printf("waitFor(\"%s\");%n", inputId));
            out.unindent();
            out.println("}");

            // Remove inputs from the in progress list since this is a synchronous wait.
            context.getInProgressAsyncComponentIds()
                    .removeAll(componentConfig.getDependencies().keySet());
        }

        if (!componentConfig.shouldFailOnError() || componentContext.isEventsEnabled()) {
            // Enclose the execute statement in a try/catch if fallback on error or events are
            // enabled.

            if (componentContext.isEventsEnabled()) {
                out.printf(
                        "fireComponentExecutionStartedEvent(\"%s\");%n", componentConfig.getId());
            }
            out.println("try {");
            out.indent();
            codeGenerator.writeExecuteStatements(out, componentContext);
            if (componentContext.isEventsEnabled()) {
                out.printf(
                        "fireComponentExecutionFinishedEvent(\"%s\");%n", componentConfig.getId());
            }
            out.unindent();
            out.println("} catch (Throwable t) {");
            out.indent();

            // Clear the value for the failed component.
            out.print(setValueStmt(componentContext, "null", componentContext.getOutputType()));

            if (componentContext.isEventsEnabled()) {
                out.printf(
                        "fireComponentExecutionErrorEvent(\"%s\", t);%n", componentConfig.getId());
            }
            if (componentConfig.shouldFailOnError()) {
                out.println("throw t;");
            } else {
                out.println("// shouldFailOnError = false for " + componentConfig.getIdWithPath());
            }
            out.unindent();
            out.println("}");
        } else {
            codeGenerator.writeExecuteStatements(out, componentContext);
        }

        out.printf("%s_finished = true;%n", varNamePrefix);

        // This component has finished executing. If there are any components that have a property
        // that is
        // dependent on this value then it may be possible to initialize them now.
        writeInitComponentsWithDependentProperties(componentConfig.getId(), out, context);
    }

    private void writeInitComponentsWithDependentProperties(
            String id, IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        // Initialize any components that have properties dependent on this value.
        context.getSortedConfigs().stream()
                .filter(v -> v.getPropertyValueProviders().containsKey(id))
                .forEach(v -> out.printf("%s_init();%n", getVarName(v.getId())));
    }

    private void writeExecuteAsync(
            IndentPrintWriter out,
            DataFlowCodeGenerationContext context,
            ComponentCodeGenerationContext componentContext) {
        ComponentConfig componentConfig = componentContext.getComponentConfig();
        ComponentCodeGenerator codeGenerator = getCodeGenerator(componentConfig);
        String id = componentConfig.getId();
        String varNamePrefix = componentContext.getVarNamePrefix();

        out.println("{");

        List<String> inputIds =
                getInProgressAsyncInputIds(componentContext).stream()
                        .map(s -> "\"" + s + "\"")
                        .collect(Collectors.toList());

        // Wait for all inputs to complete.
        out.printf(
                "CompletableFuture<?> %s_future = allOf(%s);%n",
                varNamePrefix, StringUtil.join(inputIds, ", "));

        if (componentContext.isEventsEnabled()) {
            out.printf("fireComponentExecutionStartedEvent(\"%s\");%n", id);
        }

        // Write the completable future for the call to the getValue method.
        writeComponentCompletableFuture(out, componentContext, codeGenerator);

        // When the async execution is complete.
        out.printf("%s_future = %s_future.thenAccept((value) -> {%n", varNamePrefix, varNamePrefix);
        out.indent();
        // Set the execution context of the thread to the current execution context.
        out.println("DataFlowExecutionContext.setCurrentExecutionContext(executionContext);");
        // TODO(thorntonv): Do we need to attempt to convert this value?
        out.printf("%s_value = (%s) value;%n", varNamePrefix, componentContext.getFieldType());
        out.printf("%s_finished = true;%n", varNamePrefix);
        if (componentContext.isEventsEnabled()) {
            out.printf("fireComponentExecutionFinishedEvent(\"%s\");%n", id);
        }
        writeInitComponentsWithDependentProperties(componentConfig.getId(), out, context);

        out.unindent();
        out.println("});");

        out.printf("setComponentFuture(\"%s\", %s_future);%n", id, varNamePrefix);
        out.println("}");

        // Add this component to the list of in progress async components.
        context.getInProgressAsyncComponentIds().add(componentConfig.getId());
    }

    private void writeComponentCompletableFuture(
            IndentPrintWriter out,
            ComponentCodeGenerationContext componentContext,
            ComponentCodeGenerator codeGenerator) {
        DataFlowComponentMetadata componentMetadata = componentContext.getComponentMetadata();
        String varNamePrefix = componentContext.getVarNamePrefix();
        if (componentMetadata.isAsync() && componentMetadata.hasAsyncGet()) {
            // The component has an async getValue method so use the completable future returned by
            // that method.
            writeAsyncGetCompletableFuture(out, componentContext, codeGenerator);
        } else if (componentMetadata.isAsync()) {
            // The getValue method is synchronous, but asynchronous execution is enabled for the
            // component so
            // execute it in a separate thread.
            writeAsyncThreadCompletableFuture(out, componentContext, codeGenerator);
        } else {
            // The getValue method and component are synchronous. incorporate the synchronous
            // getValue call into
            // the completable future chain.
            writeSynchronousCompletableFuture(out, componentContext, codeGenerator);
        }
        if (!componentContext.getComponentConfig().shouldFailOnError()) {
            // If fail on error is disabled then swallow any exceptions that are thrown and continue
            // executing the chain.
            out.printf(
                    "%s_future = %s_future.handle((v, e) -> {return v;}); // shouldFailOnError = false%n",
                    varNamePrefix, varNamePrefix);
        } else if (componentContext.isEventsEnabled()) {
            // When the future is complete fire an event if there was an error.
            out.printf("%s_future = %s_future.handle((v, e) -> {%n", varNamePrefix, varNamePrefix);
            out.indent();
            out.println("if (e != null) {");
            out.indent();
            out.printf(
                    "fireComponentExecutionErrorEvent(\"%s\", (Throwable) e);%n",
                    componentContext.getId());
            out.println("throw new RuntimeException(e);");
            out.unindent();
            out.println("}");
            out.println("return v;");
            out.unindent();
            out.println("});");
        }
    }

    private void writeAsyncGetCompletableFuture(
            IndentPrintWriter out,
            ComponentCodeGenerationContext componentContext,
            ComponentCodeGenerator codeGenerator) {
        // If the component has an async get method then chain it after the inputs are available.
        String varNamePrefix = componentContext.getVarNamePrefix();
        out.printf(
                "%s_future = %s_future.thenComposeAsync(value -> {%n",
                varNamePrefix, varNamePrefix);
        out.indent();
        out.println("DataFlowExecutionContext.setCurrentExecutionContext(executionContext);");

        out.printf(
                "if(%s) return CompletableFuture.completedFuture(value);%n",
                valueIsSetExpr("value", componentContext));

        out.println("try {");
        {
            out.indent();

            // Code generator will write return statement.
            codeGenerator.writeExecuteStatements(out, componentContext);
            out.unindent();
        }

        out.println("} catch(Exception t) { throw new RuntimeException(t); }");
        out.unindent();
        out.println("}, executor);");
    }

    private void writeAsyncThreadCompletableFuture(
            IndentPrintWriter out,
            ComponentCodeGenerationContext componentContext,
            ComponentCodeGenerator codeGenerator) {
        // The component should run async, but it doesn't have an async get method so run it in a
        // separate thread using the executor.
        String varNamePrefix = componentContext.getVarNamePrefix();
        out.printf(
                "%s_future = %s_future.thenApplyAsync(value -> {%n", varNamePrefix, varNamePrefix);
        out.indent();

        out.println("try {");
        out.indent();

        // Set the execution context of the thread to the current execution context.
        out.println("DataFlowExecutionContext.setCurrentExecutionContext(executionContext);");

        out.printf("if(%s) return value;%n", valueIsSetExpr("value", componentContext));
        codeGenerator.writeExecuteStatements(out, componentContext);
        out.printf("return %s_value;%n", varNamePrefix);

        out.unindent();
        out.println("} finally {");
        out.indent();
        out.println("DataFlowExecutionContext.setCurrentExecutionContext(null);");
        out.println("}");

        out.unindent();
        out.println("}, executor);");
    }

    private void writeSynchronousCompletableFuture(
            IndentPrintWriter out,
            ComponentCodeGenerationContext componentContext,
            ComponentCodeGenerator codeGenerator) {
        // The component is synchronous, but either this is a fallback of an async component or at
        // least one
        // of the fallback components is async so we need to incorporate the synchronous operation
        // into the chain.
        String varNamePrefix = componentContext.getVarNamePrefix();
        out.printf(
                "%s_future = %s_future.thenComposeAsync(value -> {%n",
                varNamePrefix, varNamePrefix);
        out.indent();
        out.println("DataFlowExecutionContext.setCurrentExecutionContext(executionContext);");
        if (componentContext.isEventsEnabled()) {
            out.printf("fireComponentExecutionFinishedEvent(\"%s\");%n", componentContext.getId());
        }
        out.printf(
                "if(%s) return CompletableFuture.completedFuture(value);%n",
                valueIsSetExpr("value", componentContext));
        codeGenerator.writeExecuteStatements(out, componentContext);
        out.printf("return CompletableFuture.completedFuture(%s_value);%n", varNamePrefix);
        out.unindent();
        out.println("}, executor);");
    }

    private void writeGetOutputMethod(
            IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        ComponentCodeGenerationContext componentContext =
                context.getComponentContext(context.getDataFlowConfig().getOutput());
        out.printf("public %s getOutput() {%n", componentContext.getFieldType());
        out.indent();
        out.printf("return %s;%n", getValueReferenceExpression(componentContext));
        out.unindent();
        out.println("}");
        out.println();
    }

    private List<String[]> getAvailabilityExprReasonList(ComponentConfig componentConfig) {
        String varNamePrefix = getVarName(componentConfig.getId());
        // A list of expressions that must be true if the component is available.
        List<String[]> availableExprList = new ArrayList<>();
        // Add checks that each input component is available.
        availableExprList.addAll(
                componentConfig.getDependencies().values().stream()
                        .filter(v -> !isSimpleConstant(v))
                        .map(
                                v ->
                                        new String[] {
                                            getVarName(v.getId()) + "_available", // Expression
                                            String.format(
                                                    "%s is not available",
                                                    v.getIdWithPath()) // Reason
                                        })
                        .toList());
        // Add checks that each provided input value is set (finished).
        availableExprList.addAll(
                componentConfig.getInput().values().stream()
                        .filter(CodeGenerationUtil::isProvidedValue)
                        .map(
                                v ->
                                        new String[] {
                                            getVarName(v.getId()) + "_finished", // Expression
                                            String.format(
                                                    "%s is not set", v.getIdWithPath()) // Reason
                                        })
                        .toList());

        //        if (!componentConfig.getPropertyValueProviders().isEmpty()) {
        //            // If the component has property values that are dependent on other components
        // then
        //            // this component must not be null for it to be available.
        //            availableExprList.add(
        //                    new String[] {
        //                        String.format("%s_component != null", varNamePrefix), //
        // Expression
        //                        String.format(
        //                                "%s has missing property value",
        //                                componentConfig.getIdWithPath()) // Reason
        //                    });
        //        }

        // TODO(thorntonv): Update to add hasIsAvailableMethod to the component metadata instead of
        // using reflection
        //  on the instantiated class.
        //        Class<?> componentClass =
        // context.getComponentRegistry().getComponentClass(componentConfig.getType());
        //        try {
        //            if (componentClass.getMethod("isAvailable") != null) {
        //                // If the component has a method named isAvailable then call that method
        // as well.
        //                availableExprList.add(String.format("%s_component.isAvailable()",
        // varNamePrefix));
        //            }
        //        } catch (NoSuchMethodException ignored) {}

        return availableExprList;
    }

    private void writeUpdateAvailabilityMethod(
            IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public void updateAvailability() {");
        out.indent();

        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            if (componentConfig == null
                    || isProvidedValue(componentConfig)
                    || isSimpleConstant(componentConfig)) {
                continue;
            }
            String varNamePrefix = getVarName(componentConfig.getId());

            List<String> availableExprList =
                    getAvailabilityExprReasonList(componentConfig).stream()
                            .map(exprReason -> exprReason[0])
                            .collect(Collectors.toList());

            String componentAvailableExpr = StringUtil.join(availableExprList, " && ");
            if (componentAvailableExpr.isEmpty()) {
                componentAvailableExpr = "true";
            }
            out.printf("%s_available = %s;%n", varNamePrefix, componentAvailableExpr);
        }
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeCloseMethod(IndentPrintWriter out, DataFlowCodeGenerationContext context) {
        out.println("public void close() {");
        out.indent();
        out.println("super.close();");

        for (ComponentConfig componentConfig : context.getSortedConfigs()) {
            if (isProvidedValue(componentConfig) || isConstant(componentConfig)) {
                continue;
            }
            ComponentCodeGenerationContext componentContext =
                    context.getComponentContext(componentConfig);
            ComponentCodeGenerator codeGenerator = getCodeGenerator(componentConfig);
            out.println("try {");
            out.indent();
            codeGenerator.writeCloseStatements(out, componentContext);
            out.unindent();
            out.println("} catch(Exception ex) {");
            out.indent();
            out.println("fireDataFlowExecutionErrorEvent(ex);");
            out.unindent();
            out.println("}");
        }
        out.unindent();
        out.println("}");
    }

    private ComponentCodeGenerator getCodeGenerator(ComponentConfig componentConfig) {
        ComponentCodeGenerator codeGenerator =
                registry.getComponentRegistry()
                        .getComponentCodeGenerator(componentConfig.getType());
        return codeGenerator != null ? codeGenerator : new DefaultComponentCodeGenerator();
    }

    private DataFlowCodeGenerationContext createContext(
            DataFlowConfig dataFlowConfig, DataFlowInstanceCodeGenConfig codeGenConfig) {
        List<ComponentConfig> sortedConfigs = getSortedConfigs(dataFlowConfig);
        return new DataFlowCodeGenerationContext(
                dataFlowConfig, sortedConfigs, codeGenConfig, registry);
    }
}
