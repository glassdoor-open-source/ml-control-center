/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueConverterKey.java
 */
package dataflow.core.registry;

import dataflow.core.type.TypeUtil;
import dataflow.core.type.ValueType;
import java.util.Objects;

public final class ValueConverterKey {

    private ValueType fromType;
    private ValueType toType;

    public ValueConverterKey(final Class<?> fromType, final Class<?> toType) {
        this(TypeUtil.valueTypeFromClass(fromType), TypeUtil.valueTypeFromClass(toType));
    }

    public ValueConverterKey(final ValueType fromType, final ValueType toType) {
        this.fromType = fromType;
        this.toType = toType;
    }

    public ValueType getFromType() {
        return fromType;
    }

    public ValueType getToType() {
        return toType;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ValueConverterKey that = (ValueConverterKey) o;
        return Objects.equals(fromType, that.fromType) && Objects.equals(toType, that.toType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromType, toType);
    }

    @Override
    public String toString() {
        return "ValueConverterRegistrationKey{"
                + "fromType="
                + fromType
                + ", toType="
                + toType
                + '}';
    }
}
