/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowRegistry.java
 */
package dataflow.core.registry;

import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.DataFlowInstanceFactory;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DataFlowRegistry {

    private final TypeRegistry typeRegistry = new TypeRegistry();
    private final ComponentRegistry componentRegistry = new ComponentRegistry();
    private final Map<String, DataFlowRegistration> dataFlowRegistrationMap =
            new ConcurrentHashMap<>();

    public DataFlowRegistration getDataFlowRegistration(String dataFlowId) {
        if (dataFlowId == null) {
            throw new IllegalArgumentException(
                    "Attempted to get dataflow registration with a null id");
        }
        DataFlowRegistration registration = dataFlowRegistrationMap.get(dataFlowId);
        if (registration == null) {
            // Check for an id with timestamp match.
            registration =
                    dataFlowRegistrationMap.values().stream()
                            .filter(reg -> reg.getDataFlowConfig().getId().equals(dataFlowId))
                            .findAny()
                            .orElse(null);
            if (registration == null) {
                throw new IllegalArgumentException("DataFlow " + dataFlowId + " is not registered");
            }
        }
        return registration;
    }

    public void registerDataFlow(
            DataFlowConfig dataFlowConfig, DataFlowInstanceFactory instanceFactory) {
        dataFlowRegistrationMap.put(
                dataFlowConfig.getId(),
                new DataFlowRegistration(dataFlowConfig.getId(), dataFlowConfig, instanceFactory));
    }

    public DataFlowRegistration unregisterDataFlow(String id) {
        return dataFlowRegistrationMap.remove(id);
    }

    public Map<String, DataFlowRegistration> getRegisteredDataFlows() {
        return Collections.unmodifiableMap(dataFlowRegistrationMap);
    }

    public ComponentRegistry getComponentRegistry() {
        return componentRegistry;
    }

    public TypeRegistry getTypeRegistry() {
        return typeRegistry;
    }
}
