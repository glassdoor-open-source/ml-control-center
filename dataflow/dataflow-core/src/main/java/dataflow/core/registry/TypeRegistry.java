/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TypeRegistry.java
 */
package dataflow.core.registry;

import dataflow.core.type.BooleanValue;
import dataflow.core.type.DoubleValue;
import dataflow.core.type.FloatValue;
import dataflow.core.type.IntegerValue;
import dataflow.core.type.LongValue;
import dataflow.core.type.TypeUtil;
import dataflow.core.type.ValueType;
import dataflow.core.type.ValueTypeConverter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Contains information about the set of types that are available for use and provides type
 * conversion functions.
 */
public class TypeRegistry {

    public static final ValueType STRING_VALUE_TYPE =
            new ValueType(String.class.getName(), Collections.EMPTY_LIST);

    private static final Pattern CLASS_ARRAY_PATTERN = Pattern.compile("\\[+L(.*);");
    private static final Pattern PRIMITIVE_ARRAY_PATTERN = Pattern.compile("\\[+([BCDFIJSZ])");

    private final Map<String, String> valueTypeClassNameMap = new HashMap<>();
    private final Map<String, Class<?>> primitiveClassNameMap = new HashMap<>();
    private final List<ValueTypeConverter<?, ?>> converters = new ArrayList<>();
    private final Map<ValueConverterKey, ValueTypeConverter<?, ?>> convertersCache =
            new HashMap<>();

    public TypeRegistry() {
        // Classes that can be referenced using their simple name.
        Class<?>[] simpleNameClasses =
                new Class<?>[] {
                    Object.class, Integer.class, IntegerValue.class, Long.class, LongValue.class,
                            Float.class,
                    FloatValue.class, Double.class, DoubleValue.class, Boolean.class,
                            BooleanValue.class, String.class,
                    List.class, Stream.class
                };
        Arrays.stream(simpleNameClasses)
                .forEach(
                        simpleNameClass -> {
                            valueTypeClassNameMap.put(
                                    simpleNameClass.getSimpleName(), simpleNameClass.getName());
                            valueTypeClassNameMap.put(
                                    simpleNameClass.getSimpleName().toLowerCase(),
                                    simpleNameClass.getName());
                        });
        valueTypeClassNameMap.put("int", Integer.class.getName());
        primitiveClassNameMap.put("int", Integer.class);
        valueTypeClassNameMap.put("long", Long.class.getName());
        primitiveClassNameMap.put("long", Long.class);
        valueTypeClassNameMap.put("float", Float.class.getName());
        primitiveClassNameMap.put("float", Float.class);
        valueTypeClassNameMap.put("double", Double.class.getName());
        primitiveClassNameMap.put("double", Double.class);
        valueTypeClassNameMap.put("bool", Boolean.class.getName());
        primitiveClassNameMap.put("bool", Boolean.class);
        valueTypeClassNameMap.put("boolean", Boolean.class.getName());
        primitiveClassNameMap.put("boolean", Boolean.class);
        valueTypeClassNameMap.put("byte", Byte.class.getName());
        primitiveClassNameMap.put("byte", Byte.class);
        valueTypeClassNameMap.put("char", Character.class.getName());
        primitiveClassNameMap.put("char", Character.class);
    }

    public <FROM_TYPE, TO_TYPE> void register(
            ValueTypeConverter<FROM_TYPE, TO_TYPE> converterFunction) {
        converters.add(converterFunction);
    }

    public boolean canConvert(final String fromType, final String toType) {
        if (fromType.equals(toType)) {
            return true;
        }
        return getConverter(fromType, toType) != null;
    }

    public <T, R> boolean canConvert(Class<T> fromType, Class<R> toType) {
        return canConvert(fromType.getName(), toType.getName());
    }

    public Class<?> getValueTypeClass(final String valueTypeParam) {
        try {
            if (primitiveClassNameMap.containsKey(valueTypeParam)) {
                return primitiveClassNameMap.get(valueTypeParam);
            }
            return Class.forName(getValueTypeClassName(valueTypeParam).orElse(valueTypeParam));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<String> getValueTypeClassName(final String valueTypeParam) {
        if ("?".equalsIgnoreCase(valueTypeParam)) {
            return Optional.of(Object.class.getName());
        }

        String type = valueTypeParam;
        // Check for an entry that exactly matches the given type.
        String valueTypeClassName = valueTypeClassNameMap.get(type);
        if (valueTypeClassName != null) {
            return Optional.of(valueTypeClassName);
        }
        // If the type is an array then try to find the type of the elements.
        boolean isArray = false;
        Matcher matcher = CLASS_ARRAY_PATTERN.matcher(type);
        if (type.contains("]")) {
            type = type.substring(0, type.indexOf('['));
            isArray = true;
        } else if (matcher.matches()) {
            // Drop [L and ;
            type = matcher.group(1);
            isArray = true;
        } else if (PRIMITIVE_ARRAY_PATTERN.matcher(type).matches()) {
            // The type is already a valid class name.
            return Optional.of(type);
        }

        type = TypeUtil.removeGenericTypes(type);
        valueTypeClassName = valueTypeClassNameMap.get(type);
        if (valueTypeClassName != null) {
            if (!isArray) {
                return Optional.of(valueTypeClassName);
            } else if (primitiveClassNameMap.containsKey(valueTypeClassName)) {
                return Optional.of(
                        Array.newInstance(primitiveClassNameMap.get(valueTypeClassName), 0)
                                .getClass()
                                .getName());
            } else if ((matcher = CLASS_ARRAY_PATTERN.matcher(valueTypeParam)).matches()) {
                return Optional.of(valueTypeParam.replace(matcher.group(1), valueTypeClassName));
            }
        }
        try {
            Class<?> valueTypeClass = Class.forName(type);
            if (isArray) {
                valueTypeClass = Array.newInstance(valueTypeClass, 0).getClass();
                return Optional.of(valueTypeClass.getName());
            }
        } catch (ClassNotFoundException e) {
        }
        return Optional.empty();
    }

    public Class<?>[] getGenericTypeClasses(final String valueType) {
        String[] genericTypes = TypeUtil.getGenericTypes(valueType);
        Class<?>[] genericTypeClasses = new Class<?>[genericTypes.length];
        for (int idx = 0; idx < genericTypes.length; idx++) {
            genericTypeClasses[idx] = getValueTypeClass(genericTypes[idx]);
        }
        return genericTypeClasses;
    }

    public ValueTypeConverter<?, ?> getConverter(final ValueType from, final ValueType to) {
        final ValueType fromType = resolveClasses(from);
        final ValueType toType = resolveClasses(to);

        if (fromType.getTypeClass() == null) {
            throw new RuntimeException(
                    String.format(
                            "Unable to convert from %s to %s, unable to resolve from type",
                            from, to));
        }
        if (toType.getTypeClass() == null) {
            throw new RuntimeException(
                    String.format(
                            "Unable to convert from %s to %s, unable to resolve to type",
                            from, to));
        }
        final ValueConverterKey key = new ValueConverterKey(fromType, toType);
        ValueTypeConverter<?, ?> converter = convertersCache.get(key);
        if (converter != null) {
            return converter;
        }

        converter =
                converters.stream()
                        .filter(c -> c.canConvert(fromType, toType))
                        .findFirst()
                        .orElse(null);
        if (converter != null) {
            // Cache the converter so that it can be found more quickly in the future.
            convertersCache.put(key, converter);
        }
        return converter;
    }

    public ValueTypeConverter<?, ?> getConverter(String fromType, String toType) {
        return getConverter(
                TypeUtil.valueTypeFromString(fromType), TypeUtil.valueTypeFromString(toType));
    }

    //
    //    public boolean isAssignable(ValueType fromType, ValueType toType) throws
    // ClassNotFoundException {
    //        fromType = resolveClasses(fromType);
    //        toType = resolveClasses(toType);
    //        Class<?> fromTypeClass = Class.forName(fromType.getType());
    //        Class<?> toTypeClass = Class.forName(toType.getType());
    //        if (toTypeClass.isAssignableFrom(fromTypeClass) &&
    //                fromType.getTypeGenericParams().size() ==
    // toType.getTypeGenericParams().size()) {
    //
    //        }
    //
    //        return false;
    //    }

    public ValueType resolveClasses(ValueType valueType) {
        String typeClassName =
                getValueTypeClassName(valueType.getType()).orElse(valueType.getType());
        List<ValueType> genericTypeParams =
                valueType.getTypeGenericParams().stream()
                        .map(this::resolveClasses)
                        .collect(Collectors.toList());
        return new ValueType(typeClassName, genericTypeParams);
    }
}
