/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TypeUtil.java
 */
package dataflow.core.type;

import dataflow.core.registry.TypeRegistry;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings({"WeakerAccess", "unused"})
public class TypeUtil {

    private static final Map<String, String> PRIMITIVE_TYPE_OBJECT_TYPE_MAP = new HashMap<>();

    private static final Pattern GENERIC_PATTERN = Pattern.compile(".*?<(.*)>");

    static {
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(boolean.class.getName(), Boolean.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(int.class.getName(), Integer.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(long.class.getName(), Long.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(float.class.getName(), Float.class.getName());
        PRIMITIVE_TYPE_OBJECT_TYPE_MAP.put(double.class.getName(), Double.class.getName());
    }

    public static <T, R> R convert(
            T object, Class<T> fromType, Class<R> toType, TypeRegistry typeRegistry) {
        return convert(
                object,
                valueTypeFromString(fromType.getName()),
                valueTypeFromString(toType.getName()),
                typeRegistry);
    }

    public static <T, R> R convert(T object, Class<R> toType, TypeRegistry typeRegistry) {
        return convert(
                object,
                valueTypeFromString(object.getClass().getName()),
                valueTypeFromString(toType.getName()),
                typeRegistry);
    }

    public static <T, R> R convert(T object, ValueType toValueType, TypeRegistry typeRegistry) {
        if (object == null) {
            return null;
        }
        return convert(
                object,
                valueTypeFromString(object.getClass().getName()),
                toValueType,
                typeRegistry);
    }

    public static <T, R> R convert(
            final T object,
            final ValueType fromValueType,
            final ValueType toValueType,
            TypeRegistry typeRegistry) {
        if (typeRegistry
                .resolveClasses(fromValueType)
                .equals(typeRegistry.resolveClasses(toValueType))) {
            return (R) object;
        }

        ValueTypeConverter converter = typeRegistry.getConverter(fromValueType, toValueType);
        if (converter == null) {
            // Fallback using the type of the object.
            ValueType objectValueType = ValueType.of(object);
            if (typeRegistry
                    .resolveClasses(objectValueType)
                    .equals(typeRegistry.resolveClasses(toValueType))) {
                return (R) object;
            }
            converter = typeRegistry.getConverter(objectValueType, toValueType);
        }
        if (converter != null) {
            return (R) converter.convert(object, toValueType);
        }

        Class<?> toValueClass = typeRegistry.getValueTypeClass(toValueType.getType());
        if (toValueClass.isAssignableFrom(object.getClass())) {
            // If the object is assignable to the target type then don't convert.
            return (R) object;
        }

        throw new IllegalArgumentException(
                "No converter registered to convert from "
                        + object.getClass().getName()
                        + " to "
                        + toValueType);
    }

    public static String getPackageName(String className) {
        int lastDot = className.lastIndexOf('.');
        return lastDot > 0 ? className.substring(0, lastDot) : "";
    }

    public static boolean isPrimitiveType(final String type) {
        return PRIMITIVE_TYPE_OBJECT_TYPE_MAP.containsKey(type);
    }

    public static String primitiveTypeToObjectType(String type) {
        return PRIMITIVE_TYPE_OBJECT_TYPE_MAP.getOrDefault(type, type);
    }

    public static String getSimpleClassName(String className) {
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            className = className.substring(lastDot + 1);
        }
        className = removeGenericTypes(className);
        return className;
    }

    public static String removeGenericTypes(String type) {
        int genericTypeStart = type.indexOf('<');
        int genericTypeEnd = type.lastIndexOf('>');
        if (genericTypeStart > 0 && genericTypeEnd > genericTypeStart) {
            type = type.substring(0, genericTypeStart) + type.substring(genericTypeEnd + 1);
        }
        return type;
    }

    public static String[] getGenericTypes(String type) {
        Matcher matcher = GENERIC_PATTERN.matcher(type);
        if (matcher.matches()) {
            String genericTypes = matcher.group(1);
            if (!genericTypes.trim().isEmpty()) {
                List<String> typesList = new ArrayList<>();
                int depth = 0;
                StringBuilder typeBuilder = new StringBuilder();
                for (char ch : genericTypes.toCharArray()) {
                    if (ch == '<') {
                        depth++;
                    } else if (ch == '>') {
                        depth--;
                    } else if (depth == 0 && (ch == ',')) {
                        typesList.add(typeBuilder.toString().trim());
                        typeBuilder.setLength(0);
                        continue;
                    }
                    typeBuilder.append(ch);
                }
                if (typeBuilder.length() > 0) {
                    typesList.add(typeBuilder.toString().trim());
                }
                return typesList.toArray(new String[] {});
            }
        }
        return new String[0];
    }

    public static ValueType valueTypeFromClass(Class<?> typeClass, Class<?>... genericTypes) {
        return new ValueType(
                typeClass.getName(),
                Arrays.stream(genericTypes)
                        .map(t -> new ValueType(t.getName(), Collections.emptyList()))
                        .collect(Collectors.toList()));
    }

    public static ValueType valueTypeFromString(String valueTypeStr) {
        String type = TypeUtil.removeGenericTypes(valueTypeStr);
        String[] genericTypeStrings = TypeUtil.getGenericTypes(valueTypeStr);
        List<ValueType> genericParams =
                Arrays.stream(genericTypeStrings)
                        .map(TypeUtil::valueTypeFromString)
                        .collect(Collectors.toList());
        return new ValueType(type, genericParams);
    }
}
