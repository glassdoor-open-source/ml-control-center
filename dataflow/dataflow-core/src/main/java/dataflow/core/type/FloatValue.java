/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FloatValue.java
 */
package dataflow.core.type;

import java.util.Objects;

/** A mutable wrapper for primitive float values. */
@SuppressWarnings("unused")
public class FloatValue {

    public static final String TYPE = "float";

    private float value;
    private boolean set = false;

    public FloatValue() {}

    public FloatValue(float value) {
        setValue(value);
    }

    public String getType() {
        return TYPE;
    }

    public FloatValue setValue(float value) {
        this.value = value;
        set = true;
        return this;
    }

    public FloatValue setValue(Float value) {
        if (value != null) {
            setValue(value.floatValue());
        } else {
            clear();
        }
        return this;
    }

    public FloatValue setValue(FloatValue value) {
        if (value != null) {
            this.value = value.value;
            this.set = value.set;
        } else {
            clear();
        }
        return this;
    }

    public int toInt() {
        return (int) value;
    }

    public long toLong() {
        return (long) value;
    }

    public float toFloat() {
        return value;
    }

    public void fromFloat(float value) {
        this.value = value;
        set = true;
    }

    public double toDouble() {
        return value;
    }

    public boolean toBoolean() {
        return value != 0.0f;
    }

    public void fromString(final String str) {
        value = Float.parseFloat(str);
        set = true;
    }

    public String toString() {
        return String.valueOf(value);
    }

    public boolean isSet() {
        return set;
    }

    public void clear() {
        set = false;
        value = 0.0f;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final FloatValue that = (FloatValue) o;
        return Float.compare(that.value, value) == 0 && set == that.set;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, set);
    }
}
