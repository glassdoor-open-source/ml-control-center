/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractValueTypeConverter.java
 */
package dataflow.core.type;

/** A function from converting objects of one type to another */
public abstract class AbstractValueTypeConverter<FROM_TYPE, TO_TYPE>
        implements ValueTypeConverter<FROM_TYPE, TO_TYPE> {

    private final Class<?> fromClass;
    private final ValueType fromType;
    private final Class<?> toClass;
    private final ValueType toType;

    protected AbstractValueTypeConverter(Class<FROM_TYPE> fromType, Class<TO_TYPE> toType) {
        this(new ValueType(fromType), new ValueType(toType));
    }

    protected AbstractValueTypeConverter(ValueType fromType, ValueType toType) {
        this.fromType = fromType;
        this.fromClass = fromType.getTypeClass();
        this.toType = toType;
        this.toClass = toType.getTypeClass();
    }

    public boolean canConvert(ValueType fromValueType, ValueType toValueType) {
        if (fromType.equals(fromValueType) && toType.equals(toValueType)) {
            return true;
        }
        return fromClass.isAssignableFrom(fromValueType.getTypeClass())
                && toClass.isAssignableFrom(toValueType.getTypeClass());
    }
}
