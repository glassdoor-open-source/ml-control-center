/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DoubleValue.java
 */
package dataflow.core.type;

import java.util.Objects;

/** A mutable wrapper for primitive double values. */
@SuppressWarnings("unused")
public class DoubleValue {

    public static final String TYPE = "double";

    private double value;
    private boolean set = false;

    public DoubleValue() {}

    public DoubleValue(double value) {
        setValue(value);
    }

    public String getType() {
        return TYPE;
    }

    public DoubleValue setValue(double value) {
        this.value = value;
        set = true;
        return this;
    }

    public DoubleValue setValue(Double value) {
        if (value != null) {
            setValue(value.doubleValue());
        } else {
            clear();
        }
        return this;
    }

    public DoubleValue setValue(DoubleValue value) {
        if (value != null) {
            this.value = value.value;
            this.set = value.set;
        } else {
            clear();
        }
        return this;
    }

    public int toInt() {
        return (int) value;
    }

    public void fromInt(int value) {
        this.value = value;
        set = true;
    }

    public long toLong() {
        return (long) value;
    }

    public void fromLong(long value) {
        this.value = value;
        set = true;
    }

    public void fromFloat(float value) {
        this.value = value;
        set = true;
    }

    public double toDouble() {
        return value;
    }

    public void fromDouble(double value) {
        this.value = value;
        set = true;
    }

    public boolean toBoolean() {
        return value != 0.0d;
    }

    public void fromBoolean(boolean value) {
        this.value = value ? 1.0 : 0.0;
        set = true;
    }

    public void fromString(final String str) {
        value = Double.parseDouble(str);
        set = true;
    }

    public String toString() {
        return String.valueOf(value);
    }

    public boolean isSet() {
        return set;
    }

    public void clear() {
        set = false;
        value = 0.0d;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final DoubleValue that = (DoubleValue) o;
        return Double.compare(that.value, value) == 0 && set == that.set;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, set);
    }
}
