/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MapUtil.java
 */
package dataflow.core.util;

import java.util.Map;

public class MapUtil {

    public static <K, V> Map.Entry<K, V> getEntry(K key, Map<K, V> map) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (key.equals(entry.getKey())) {
                return entry;
            }
        }
        throw new IllegalArgumentException(key + " is not present in map: " + map.toString());
    }

    /**
     * Performs a deep merge of the overrides map into the given map. Values in the overrides map
     * will replace corresponding values in the original map. If the value for a key in both the
     * overrides and original map are maps then those maps will be merged using this method.
     */
    public static <K, V> void deepMerge(Map<K, V> map, Map<K, V> overrides) {
        overrides.forEach(
                (k, v) -> {
                    V originalValue = map.get(k);
                    if (originalValue instanceof Map && v instanceof Map) {
                        deepMerge((Map<K, V>) originalValue, (Map<K, V>) v);
                    } else {
                        map.put(k, v);
                    }
                });
    }
}
