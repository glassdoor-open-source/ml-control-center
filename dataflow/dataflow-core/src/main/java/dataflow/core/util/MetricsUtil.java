/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MetricsUtil.java
 */
package dataflow.core.util;

import dataflow.core.component.metadata.metrics.DataFlowCounterMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowDistributionMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowGaugeMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowTimerMetricMetadata;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class MetricsUtil {

    private interface MetricFactory<T> {
        T create(String metricName, Iterable<Tag> tags, MeterRegistry meterRegistry);
    }

    /**
     * Returns a new {@link Counter} or {Optional.empty()} if metrics support is not configured.
     *
     * @param metricMetadata Metadata for the component metric
     * @param componentInstance Instance of the component the metric is for
     * @param metricNamePrefix An optional prefix to add to the metric name. Will default to the
     *     component class name if this is null
     * @param additionalTags Additional tags to add to the metric.
     * @return The counter instance
     */
    public static Optional<Counter> createCounter(
            DataFlowCounterMetricMetadata metricMetadata,
            Object componentInstance,
            String metricNamePrefix,
            Map<String, String> additionalTags) {
        return createMetric(
                metricMetadata,
                componentInstance,
                metricNamePrefix,
                additionalTags,
                (metricName, tags, meterRegistry) -> meterRegistry.counter(metricName, tags));
    }

    /**
     * Returns a new {@link Timer} or {Optional.empty()} if metrics support is not configured.
     *
     * @param metricMetadata Metadata for the component metric
     * @param componentInstance Instance of the component the metric is for
     * @param metricNamePrefix An optional prefix to add to the metric name. Will default to the
     *     component class name if this is null
     * @param additionalTags Additional tags to add to the metric.
     * @return The timer instance
     */
    public static Optional<Timer> createTimer(
            DataFlowTimerMetricMetadata metricMetadata,
            Object componentInstance,
            String metricNamePrefix,
            Map<String, String> additionalTags) {
        return createMetric(
                metricMetadata,
                componentInstance,
                metricNamePrefix,
                additionalTags,
                (metricName, tags, meterRegistry) -> meterRegistry.timer(metricName, tags));
    }

    /**
     * Returns a new {@link DistributionSummary} or {Optional.empty()} if metrics support is not
     * configured.
     *
     * @param metricMetadata Metadata for the component metric
     * @param componentInstance Instance of the component the metric is for
     * @param metricNamePrefix An optional prefix to add to the metric name. Will default to the
     *     component class name if this is null
     * @param additionalTags Additional tags to add to the metric.
     * @return The DistributionSummary instance
     */
    public static Optional<DistributionSummary> createDistribution(
            DataFlowDistributionMetricMetadata metricMetadata,
            Object componentInstance,
            String metricNamePrefix,
            Map<String, String> additionalTags) {
        return createMetric(
                metricMetadata,
                componentInstance,
                metricNamePrefix,
                additionalTags,
                (metricName, tags, meterRegistry) -> meterRegistry.summary(metricName, tags));
    }

    /**
     * Returns a new gauge or {Optional.empty()} if metrics support is not configured.
     *
     * @param stateObj The state object that the gauge will be created from
     * @param valueFn A function to get the gauge value from the state object
     * @param metricMetadata Metadata for the component metric
     * @param componentInstance Instance of the component the metric is for
     * @param metricNamePrefix An optional prefix to add to the metric name. Will default to the
     *     component class name if this is null
     * @param additionalTags Additional tags to add to the metric.
     * @return The DistributionSummary instance
     */
    public static <T> Optional<T> createGauge(
            T stateObj,
            ToDoubleFunction<T> valueFn,
            DataFlowGaugeMetricMetadata metricMetadata,
            Object componentInstance,
            String metricNamePrefix,
            Map<String, String> additionalTags) {
        return createMetric(
                metricMetadata,
                componentInstance,
                metricNamePrefix,
                additionalTags,
                (metricName, tags, meterRegistry) ->
                        meterRegistry.gauge(metricName, tags, stateObj, valueFn));
    }

    private static <T> Optional<T> createMetric(
            DataFlowMetricMetadata metricMetadata,
            Object componentInstance,
            String metricNamePrefix,
            Map<String, String> additionalTags,
            MetricFactory<T> metricFactory) {
        DataFlowExecutionContext executionContext =
                DataFlowExecutionContext.getCurrentExecutionContext();
        DataFlowInstance instance = executionContext.getInstance();
        MeterRegistry meterRegistry =
                executionContext.getDependencyInjector().getInstance(MeterRegistry.class);
        if (meterRegistry == null) {
            return Optional.empty();
        }
        Map<String, String> tags = new HashMap<>();
        if (instance != null) {
            tags.putAll(instance.getMetricTagsForComponent(componentInstance));
        }
        if (additionalTags != null) {
            tags.putAll(additionalTags);
        }
        metricNamePrefix =
                metricNamePrefix != null
                        ? metricNamePrefix
                        : componentInstance.getClass().getSimpleName();
        return Optional.of(
                metricFactory.create(
                        getMetricName(metricNamePrefix, metricMetadata.getName(), instance),
                        toTags(tags),
                        meterRegistry));
    }

    public static String getMetricName(
            String metricNamePrefix, String metricName, DataFlowInstance instance) {
        StringBuilder finalName = new StringBuilder();
        if (instance != null) {
            appendToMetricName(instance.getMetricNamePrefix(), finalName);
        }
        appendToMetricName(metricNamePrefix, finalName);
        appendToMetricName(metricName, finalName);
        return finalName.toString();
    }

    public static void appendToMetricName(String name, StringBuilder metricNameBuilder) {
        if (name != null && !name.trim().isEmpty()) {
            if (metricNameBuilder.length() > 0) {
                metricNameBuilder.append(".");
            }
            metricNameBuilder.append(name);
        }
    }

    public static List<Tag> toTags(Map<String, String> tags) {
        return tags.entrySet().stream()
                .map(e -> Tag.of(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    public static String[] tagsToStringArray(Map<String, String> tags) {
        String[] array = new String[tags.size() * 2];
        int idx = 0;
        for (Map.Entry<String, String> tag : tags.entrySet()) {
            array[idx++] = tag.getKey();
            array[idx++] = tag.getValue();
        }
        return array;
    }
}
