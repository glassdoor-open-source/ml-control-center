/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigUtil.java
 */
package dataflow.core.util;

import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.exception.DataFlowConfigurationException;
import java.util.*;

public class DataFlowConfigUtil {

    public static List<ComponentConfig> getSortedConfigs(DataFlowConfig dataFlowConfig) {
        // TODO(thorntonv): This can be optimized by re-ordering to start executing async components
        //  as early as possible.
        ComponentConfig output = new ComponentConfig();
        output.setId("finalOutput-" + UUID.randomUUID());

        Map<String, ComponentConfig> outputDependencies = new LinkedHashMap<>();
        outputDependencies.put(dataFlowConfig.getOutput().getId(), dataFlowConfig.getOutput());
        // Add the sinks as dependencies of the output so they are included in the topological sort.
        dataFlowConfig
                .getSinks()
                .forEach(id -> outputDependencies.put(id, dataFlowConfig.getComponents().get(id)));
        dataFlowConfig
                .getProvidedValueIds()
                .forEach(id -> outputDependencies.put(id, dataFlowConfig.getComponents().get(id)));
        outputDependencies.entrySet().removeIf(e -> e.getValue().isDetached());

        List<ComponentConfig> sortedConfigs =
                GraphUtil.topologicalSort(
                        output,
                        v -> {
                            if (v == output) {
                                return outputDependencies.values();
                            }
                            Map<String, ComponentConfig> deps =
                                    new LinkedHashMap<>(v.getDependencies());
                            deps.entrySet().removeIf(e -> e.getValue().isDetached());
                            return deps.values();
                        });
        sortedConfigs.remove(output);
        List<String> abstractConfigIds =
                sortedConfigs.stream()
                        .filter(ComponentConfig::isAbstract)
                        .map(
                                componentConfig ->
                                        String.format(
                                                "%s%s",
                                                componentConfig.getIdWithPath(),
                                                componentConfig.getRawConfig()))
                        .toList();
        if (!abstractConfigIds.isEmpty()) {
            throw new DataFlowConfigurationException(
                    abstractConfigIds
                            + " is abstract and should not be used as an input or output");
        }
        return sortedConfigs;
    }
}
