/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BuilderUtil.java
 */
package dataflow.core.util;

import static dataflow.core.type.TypeUtil.convert;
import static dataflow.core.type.TypeUtil.valueTypeFromString;

import dataflow.core.config.RawConfigurableObjectConfig;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.type.ValueType;
import java.util.*;

public class BuilderUtil {

    public static Object buildObject(
            String type,
            Map<String, Object> map,
            Map<String, Object> instanceValues,
            DataFlowRegistry registry) {

        return registry.getComponentRegistry()
                .getPropertyObjectBuilder(type)
                .build(map, instanceValues);
    }

    /**
     * Builds a list using a list from a parsed configuration file. New configured instances will be
     * created for any items in the list that are {@link
     * dataflow.core.component.annotation.DataFlowConfigurable} objects. An attempt will be made to
     * convert items to the type of the output list if needed.
     *
     * @param list the list from the parsed configuration file
     * @param itemClassName the item class of the output list
     * @param instanceValues component values for the dataflow instance.
     * @return the built list
     */
    public static List buildListObject(
            List list,
            String itemClassName,
            Map<String, Object> instanceValues,
            DataFlowRegistry registry) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        List result = new ArrayList();
        for (Object item : list) {
            if (item instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig obj = (RawConfigurableObjectConfig) item;
                item = buildObject(obj.getType(), obj, instanceValues, registry);
            }
            ValueType itemType = valueTypeFromString(itemClassName);
            result.add(
                    convert(
                            item,
                            valueTypeFromString(item.getClass().getName()),
                            itemType,
                            registry.getTypeRegistry()));
        }
        return result;
    }

    /**
     * Builds a map using a map from a parsed configuration file. New configured instances will be
     * created for any keys or values in the map that are {@link
     * dataflow.core.component.annotation.DataFlowConfigurable} objects. An attempt will be made to
     * convert keys and values to the types of the output map if needed.
     *
     * @param map the map from the parsed configuration file
     * @param keyClassName the key class of the output map
     * @param valueClassName the value class of the output map
     * @param instanceValues component values for the dataflow instance.
     * @return the built map
     */
    public static Map buildMapObject(
            Map<?, ?> map,
            String keyClassName,
            String valueClassName,
            Map<String, Object> instanceValues,
            DataFlowRegistry registry) {
        if (map == null || map.isEmpty()) {
            return Collections.emptyMap();
        }
        Map result = new HashMap();
        for (Map.Entry entry : map.entrySet()) {
            Object key = entry.getKey();
            if (key instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig obj = (RawConfigurableObjectConfig) key;
                key = buildObject(obj.getType(), obj, instanceValues, registry);
            }
            ValueType keyType = valueTypeFromString(keyClassName);
            key =
                    convert(
                            key,
                            valueTypeFromString(key.getClass().getName()),
                            keyType,
                            registry.getTypeRegistry());

            Object value = entry.getValue();
            if (value instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig obj = (RawConfigurableObjectConfig) value;
                value = buildObject(obj.getType(), obj, instanceValues, registry);
            }
            ValueType valueType = valueTypeFromString(valueClassName);
            value =
                    convert(
                            value,
                            valueTypeFromString(value.getClass().getName()),
                            valueType,
                            registry.getTypeRegistry());

            result.put(key, value);
        }
        return result;
    }
}
