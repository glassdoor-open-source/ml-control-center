/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DependencyInjector.java
 */
package dataflow.core.engine;

/**
 * An interface for the dependency injection framework. Typically this will be implemented using
 * Spring one of the other commonly used dependency injection frameworks.
 */
public interface DependencyInjector {

    /** Get an instance of the specified class. */
    <T> T getInstance(Class<T> instanceClass);

    /** Injects dependencies into the given object. */
    <T> T injectDependencies(T obj);
}
