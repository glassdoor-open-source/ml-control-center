/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AbstractDataFlowInstance.java
 */
package dataflow.core.engine;

import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.RawConfigurableObjectConfig;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.util.BuilderUtil;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** An abstract base class for dynamically generated {@link DataFlowInstance} implementations. */
@SuppressWarnings("unchecked")
public abstract class AbstractDataFlowInstance implements DataFlowInstance {

    protected static final class CompletableFutureHolder {

        private final String componentId;
        private CompletableFuture<?> future;

        public CompletableFutureHolder(String componentId) {
            this.componentId = componentId;
        }

        public String getComponentId() {
            return componentId;
        }

        public <T> CompletableFuture<T> getFuture() {
            return (CompletableFuture<T>) future;
        }

        public void setFuture(CompletableFuture<?> future) {
            this.future = future;
        }
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    protected final String instanceId = UUID.randomUUID().toString();
    protected final DataFlowEnvironment environment;
    protected final Executor executor;
    protected final DataFlowConfig dataFlowConfig;
    protected long startTimeMillis;
    protected long endTimeMillis;
    protected long maxExecutionTimeMillis;
    protected final Map<String, CompletableFutureHolder> futureHolders = new HashMap<>();
    protected DataFlowInstance parentInstance;

    private String metricNamePrefix;
    private Map<String, String> metricTags;
    private final Map<Object, Map<String, String>> componentMetricTags = new IdentityHashMap<>();
    private final Map<Object, String> componentIdMap = new IdentityHashMap<>();
    private final AtomicBoolean canceled = new AtomicBoolean(false);
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public AbstractDataFlowInstance(
            DataFlowConfig dataFlowConfig,
            final DataFlowEnvironment environment,
            final Executor executor) {
        this.dataFlowConfig = dataFlowConfig;
        this.environment = environment;
        this.executor = executor;
        this.maxExecutionTimeMillis = environment.getMaxExecutionTimeMillis();
    }

    public String getInstanceId() {
        return instanceId;
    }

    public long getMaxExecutionTimeMillis() {
        return maxExecutionTimeMillis;
    }

    public void setMaxExecutionTimeMillis(final long maxExecutionTimeMillis) {
        this.maxExecutionTimeMillis = maxExecutionTimeMillis;
    }

    public DataFlowConfig getDataFlowConfig() {
        return dataFlowConfig;
    }

    public DataFlowEnvironment getEnvironment() {
        return environment;
    }

    public <T> void forEachComponent(Class<T> componentClass, Consumer<T> function) {
        getComponents().values().stream()
                .filter(v -> componentClass.isAssignableFrom(v.getClass()))
                .map(v -> (T) v)
                .forEach(function);
        updateAvailability();
    }

    public void forEachComponent(BiConsumer<String, Object> function) {
        getComponents().forEach(function);
        updateAvailability();
    }

    public void updateAvailability() {}

    @Override
    public void setMetricNamePrefix(String metricNamePrefix) {
        this.metricNamePrefix = metricNamePrefix;
    }

    @Override
    public String getMetricNamePrefix() {
        if (metricNamePrefix != null) {
            return metricNamePrefix;
        }
        return parentInstance != null ? parentInstance.getMetricNamePrefix() : null;
    }

    @Override
    public void setMetricTags(Map<String, String> tags) {
        this.metricTags = tags != null ? new HashMap<>(tags) : new HashMap<>();
    }

    @Override
    public Map<String, String> getMetricTags() {
        Map<String, String> mergedTags = new HashMap<>();
        if (parentInstance != null && parentInstance.getMetricTags() != null) {
            mergedTags.putAll(parentInstance.getMetricTags());
        }
        if (metricTags != null) {
            mergedTags.putAll(metricTags);
        }
        return mergedTags;
    }

    @Override
    public void reset() {
        init();
    }

    public DataFlowConfig getConfig() {
        return dataFlowConfig;
    }

    @Override
    public void cancel() {
        canceled.set(true);
    }

    @Override
    public boolean isCanceled() {
        return canceled.get();
    }

    @Override
    public void close() {
        closed.set(true);
    }

    @Override
    protected void finalize() throws Throwable {
        // TODO(thorntonv): Finalize is now deprecated so this check should be done another way.
        try {
            if (!closed.get()) {
                logger.warn(
                        "Instance {} for DataFlow {} was not closed",
                        getInstanceId(),
                        getDataFlowConfig().getId());
                close();
            }
        } finally {
            super.finalize();
        }
    }

    protected DataFlowExecutionContext createExecutionContext() {
        DataFlowExecutionContext context =
                DataFlowExecutionContext.createExecutionContext(environment);
        context.setInstance(this);

        DataFlowExecutionContext parentContext = context.getParentContext();
        if (parentContext != null) {
            this.parentInstance = parentContext.getInstance();
        }
        return context;
    }

    public abstract Map<String, Object> getComponents();

    protected abstract void init();

    public synchronized Map<String, String> getMetricTagsForComponent(Object component) {
        Map<String, String> tags = componentMetricTags.get(component);
        if (tags == null) {
            tags = new HashMap<>();
            tags.put("dataFlowId", dataFlowConfig.getId());
            String componentId = getComponentId(component);
            tags.put("componentId", componentId != null ? componentId : "unknown");

            String componentType = null;
            if (componentId != null) {
                ComponentConfig componentConfig = dataFlowConfig.getComponents().get(componentId);
                if (componentConfig != null) {
                    componentType = componentConfig.getType();
                }
            }
            tags.put("componentType", componentType != null ? componentType : "unknown");
            tags.putAll(getMetricTags());
            componentMetricTags.put(component, tags);
        }
        return tags;
    }

    public synchronized String getComponentId(Object component) {
        String componentId = componentIdMap.get(component);
        if (componentId != null) {
            return componentId;
        }
        for (Map.Entry<String, Object> entry : getComponents().entrySet()) {
            if (entry.getValue() == component) {
                componentIdMap.put(component, entry.getKey());
                return entry.getKey();
            }
        }
        return null;
    }

    protected void setComponentFuture(String componentId, CompletableFuture<?> future) {
        CompletableFutureHolder holder = futureHolders.get(componentId);
        if (holder == null) {
            throw new RuntimeException("Invalid component id " + componentId);
        }
        holder.future = future;
    }

    protected void clearExecutedFutures() {
        futureHolders
                .values()
                .forEach(
                        f -> {
                            f.future = null;
                        });
    }

    protected void cancelExecutedFutures() {
        futureHolders.values().stream()
                .filter(f -> f.future != null)
                .map(CompletableFutureHolder::getFuture)
                .filter(f -> !f.isDone())
                .forEach(f -> f.cancel(true));
    }

    protected void waitForExecutedFutures() {
        futureHolders
                .values()
                .forEach(
                        futureHolder -> {
                            try {
                                if (futureHolder != null && futureHolder.future != null) {
                                    waitFor(futureHolder);
                                }
                            } catch (InterruptedException
                                    | ExecutionException
                                    | TimeoutException e) {
                                logger.warn(
                                        "Error while waiting for result from component "
                                                + futureHolder.getComponentId(),
                                        e);
                                throw new RuntimeException(e);
                            }
                        });
    }

    protected CompletableFuture<?> allOf(String... componentIds) {
        return CompletableFuture.allOf(
                Arrays.stream(componentIds)
                        .map(futureHolders::get)
                        .filter(holder -> holder != null && holder.getFuture() != null)
                        .map(holder -> holder.future)
                        .toArray(CompletableFuture[]::new));
    }

    protected void waitFor(String componentId)
            throws ExecutionException, InterruptedException, TimeoutException {
        waitFor(futureHolders.get(componentId));
    }

    protected <T> T waitFor(CompletableFutureHolder futureHolder)
            throws InterruptedException, ExecutionException, TimeoutException {
        if (futureHolder == null || futureHolder.getFuture() == null) {
            return null;
        }
        if (startTimeMillis == 0) {
            startTimeMillis = System.currentTimeMillis();
        }
        long waitStartTime = System.currentTimeMillis();
        while (true) {
            long millisRemaining =
                    maxExecutionTimeMillis - (System.currentTimeMillis() - startTimeMillis);
            if (millisRemaining <= 0) {
                throw new TimeoutException(
                        "Timeout waiting for result for component "
                                + futureHolder.getComponentId());
            }

            try {
                long timeToWaitMillis = Math.min(millisRemaining, 60 * 1000);
                return futureHolder.<T>getFuture().get(timeToWaitMillis, TimeUnit.MILLISECONDS);
            } catch (TimeoutException ex) {
                String elapsedTime =
                        Duration.ofMillis(System.currentTimeMillis() - waitStartTime).toString();
                String remainingTime = Duration.ofMillis(millisRemaining).toString();

                logger.warn(
                        "Waiting for result from component "
                                + futureHolder.getComponentId()
                                + "; elapsed="
                                + elapsedTime
                                + "; remaining="
                                + remainingTime);
            } catch (InterruptedException | ExecutionException ex) {
                logger.warn(
                        "Error while waiting for result from component "
                                + futureHolder.getComponentId(),
                        ex);
                throw ex;
            }
        }
    }

    protected Object buildConstant(Object value) {
        if (value instanceof RawConfigurableObjectConfig) {
            RawConfigurableObjectConfig rawConfig = (RawConfigurableObjectConfig) value;
            return buildObject(rawConfig.getType(), rawConfig, getValues());
        } else {
            return value;
        }
    }

    protected Object buildObject(
            String type, Map<String, Object> map, Map<String, Object> instanceValues) {
        // replaceValueReferences(map, instanceValues);
        map = buildInternalObjects(map, instanceValues, environment.getRegistry());
        return BuilderUtil.buildObject(type, map, instanceValues, environment.getRegistry());
    }

    protected Map<String, Object> buildInternalObjects(
            Map<String, Object> map,
            Map<String, Object> instanceValues,
            DataFlowRegistry registry) {
        if (map == null || map.isEmpty()) {
            return map;
        }
        Map<String, Object> newMap = new LinkedHashMap<>();
        map.forEach(
                (key, value) -> {
                    if (value instanceof Map) {
                        Map<String, Object> valueMap =
                                buildInternalObjects(
                                        (Map<String, Object>) value, instanceValues, registry);
                        if (value instanceof RawConfigurableObjectConfig) {
                            RawConfigurableObjectConfig obj = (RawConfigurableObjectConfig) value;
                            value =
                                    BuilderUtil.buildObject(
                                            obj.getType(), valueMap, instanceValues, registry);
                        } else {
                            value = valueMap;
                        }
                    } else if (value instanceof Iterable) {
                        Collection newCollection =
                                value instanceof Set ? new LinkedHashSet() : new ArrayList();
                        ((Iterable<?>) value)
                                .forEach(
                                        v -> {
                                            if (v instanceof RawConfigurableObjectConfig) {
                                                RawConfigurableObjectConfig obj =
                                                        (RawConfigurableObjectConfig) v;
                                                Object newObj =
                                                        BuilderUtil.buildObject(
                                                                obj.getType(),
                                                                obj,
                                                                instanceValues,
                                                                registry);
                                                newCollection.add(newObj);
                                            } else {
                                                newCollection.add(v);
                                            }
                                        });
                        value = newCollection;
                    }
                    newMap.put(key, value);
                });
        return newMap;
    }

    protected void fireDataFlowExecutionStartedEvent() {
        environment.fireDataFlowExecutionStartedEvent(this);
    }

    protected void fireDataFlowExecutionFinishedEvent() {
        environment.fireDataFlowExecutionFinishedEvent(this);
    }

    protected void fireDataFlowExecutionErrorEvent(Throwable error) {
        environment.fireDataFlowExecutionErrorEvent(this, error);
    }

    protected void fireComponentExecutionStartedEvent(String componentId) {
        environment.fireComponentExecutionStartedEvent(this, componentId, startTimeMillis);
    }

    protected void fireComponentExecutionFinishedEvent(String componentId) {
        environment.fireComponentExecutionFinishedEvent(this, componentId, startTimeMillis);
    }

    protected void fireComponentExecutionErrorEvent(String componentId, Throwable error) {
        environment.fireComponentExecutionErrorEvent(this, componentId, startTimeMillis, error);
    }
}
