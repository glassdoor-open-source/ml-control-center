/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  SimpleDependencyInjector.java
 */
package dataflow.core.engine;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A simple {@link dataflow.core.engine.DependencyInjector} implementation. This is used for testing
 * and as a placeholder when no other implementation is specified.
 */
public class SimpleDependencyInjector implements DependencyInjector {

    private final Map<Class<?>, Object> classInstanceMap;

    public SimpleDependencyInjector(Object... dependencies) {
        this.classInstanceMap =
                Arrays.stream(dependencies).collect(Collectors.toMap(Object::getClass, dep -> dep));
    }

    public void register(Object dependency) {
        classInstanceMap.put(dependency.getClass(), dependency);
    }

    @Override
    public <T> T getInstance(final Class<T> instanceClass) {
        return (T) classInstanceMap.get(instanceClass);
    }

    @Override
    public <T> T injectDependencies(final T obj) {
        return obj;
    }
}
