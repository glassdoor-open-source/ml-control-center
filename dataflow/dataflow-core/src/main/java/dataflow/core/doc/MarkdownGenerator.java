/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MarkdownGenerator.java
 */
package dataflow.core.doc;

import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.component.metadata.DataFlowConfigurableObjectPropertyMetadata;
import dataflow.core.util.IndentPrintWriter;
import dataflow.core.util.StringUtil;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MarkdownGenerator {

    public static String generateComponentDoc(DataFlowComponentMetadata metadata) {
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        try (PrintWriter pw = new PrintWriter(byteOut)) {
            dataflow.core.util.IndentPrintWriter out = new dataflow.core.util.IndentPrintWriter(pw);
            out.printf("# %s%n", metadata.getTypeName());
            if (metadata.getDescription() != null && !metadata.getDescription().isBlank()) {
                out.println(metadata.getDescription());
            }

            // Properties.
            out.println();
            out.printf("## Properties%n");
            List<DataFlowConfigurableObjectPropertyMetadata> propertiesMetadata =
                    metadata.getConfigurableObjectMetadata().getPropertyMetadata();
            List<Map<String, String>> propertyMetadataTable =
                    propertiesMetadata.stream()
                            .map(
                                    propMetadata -> {
                                        Map<String, String> values = new HashMap<>();
                                        values.put("Name", propMetadata.getName());
                                        values.put(
                                                "Type",
                                                prettyRawTypeName(propMetadata.getRawType()));
                                        values.put(
                                                "Required",
                                                String.valueOf(propMetadata.isRequired()));
                                        values.put("Description", propMetadata.getDescription());
                                        return values;
                                    })
                            .toList();
            if (!propertiesMetadata.isEmpty()) {
                generateTable(
                        new String[] {"Name", "Type", "Required", "Description"},
                        propertyMetadataTable,
                        true,
                        out);
            } else {
                out.println("None");
            }

            // Inputs.
            out.println();
            out.printf("## Inputs%n");
            List<Map<String, String>> inputMetadataTable =
                    metadata.getInputMetadata().stream()
                            .map(
                                    inputMetadata -> {
                                        Map<String, String> values = new HashMap<>();
                                        values.put("Name", inputMetadata.getName());
                                        values.put(
                                                "Type",
                                                prettyRawTypeName(inputMetadata.getRawType()));
                                        values.put(
                                                "Required",
                                                String.valueOf(inputMetadata.isRequired()));
                                        values.put("Description", inputMetadata.getDescription());
                                        return values;
                                    })
                            .toList();

            if (!inputMetadataTable.isEmpty()) {
                generateTable(
                        new String[] {"Name", "Type", "Required", "Description"},
                        inputMetadataTable,
                        true,
                        out);
            } else {
                out.println("None");
            }
            // Output.
            out.println();
            out.printf("## Output%n");
            Map<String, String> outputMetadataTable = new HashMap<>();
            outputMetadataTable.put(
                    "Type", prettyRawTypeName(metadata.getOutputMetadata().getRawType()));
            outputMetadataTable.put("Description", metadata.getOutputMetadata().getDescription());
            if (!StringUtil.isNullOrEmpty(metadata.getOutputMetadata().getDescription())) {
                generateTable(
                        new String[] {"Type", "Description"},
                        Collections.singletonList(outputMetadataTable),
                        true,
                        out);
            } else {
                out.println(outputMetadataTable.get("Type"));
            }

            // Metrics.
            // Inputs.
            out.println();
            out.printf("## Metrics%n");
            List<Map<String, String>> metricsMetadataTable =
                    metadata.getMetricsMetadata().stream()
                            .map(
                                    inputMetadata -> {
                                        Map<String, String> values = new HashMap<>();
                                        values.put("Name", inputMetadata.getName());
                                        values.put("Type", inputMetadata.getType().name());
                                        values.put("Description", inputMetadata.getDescription());
                                        return values;
                                    })
                            .toList();
            if (!metricsMetadataTable.isEmpty()) {
                generateTable(
                        new String[] {"Name", "Type", "Description"},
                        metricsMetadataTable,
                        true,
                        out);
            } else {
                out.println("None");
            }
        }

        return byteOut.toString(Charset.defaultCharset());
    }

    public static void generateTable(
            String[] allColumnNames,
            List<Map<String, String>> values,
            boolean removeEmptyColumns,
            IndentPrintWriter printWriter) {
        List<String> columnNames = Arrays.stream(allColumnNames).collect(Collectors.toList());
        if (removeEmptyColumns) {
            columnNames.removeIf(
                    colName ->
                            values.stream()
                                    .allMatch(v -> StringUtil.isNullOrEmpty(v.get(colName))));
        }

        printWriter.println("| " + StringUtil.join(columnNames, " | ") + " |");

        // All columns left aligned for now :---
        printWriter.println(
                "| :"
                        + StringUtil.join(
                                columnNames.stream()
                                        .map(colName -> colName.replaceAll(".", "-"))
                                        .collect(Collectors.toList()),
                                " | ")
                        + " |");
        values.forEach(
                v -> {
                    List<String> colValues =
                            columnNames.stream()
                                    .map(v::get)
                                    .map(MarkdownGenerator::formatTableValue)
                                    .toList();
                    printWriter.println("| " + StringUtil.join(colValues, " | ") + " |");
                });
    }

    private static String prettyRawTypeName(String rawTypeName) {
        for (String packageName :
                new String[] {
                    "java.util.",
                    "java.lang.",
                    "java.io.",
                    "java.util.stream",
                    "dataflow.core.engine.",
                    "dataflow.data.",
                    "dataflow.sql.",
                    "dataflow.core.datasource."
                }) {
            rawTypeName = rawTypeName.replace(packageName, "");
        }
        rawTypeName = rawTypeName.replace("<", "\\<");
        rawTypeName = rawTypeName.replace(",", ", ");
        return rawTypeName;
    }

    private static String formatTableValue(String val) {
        val = val.replace("\n", "<br>");
        val = val.replace("\\n", "<br>");
        val = val.replace("|", "\\|");
        return val;
    }
}
