/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ReferenceDocGenerator.java
 */
package dataflow.core.doc;

import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.environment.DataFlowEnvironment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;

public class ReferenceDocGenerator {

    public static void generate(
            String basePath,
            List<DataFlowConfig> dataFlowConfigs,
            DataFlowEnvironment env,
            boolean clean)
            throws IOException {
        if (clean && new File(basePath).exists()) {
            // Remove any existing generated files.
            Files.walk(Path.of(basePath))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }

        // Component documentation
        for (DataFlowComponentMetadata componentMetadata :
                env.getRegistry().getComponentRegistry().getAllComponentMetadata()) {
            String path = componentMetadata.getComponentPackageName().replace('.', '/');
            File docFilePath =
                    new File(
                            String.format(
                                    "%s/components/%s/%s.md",
                                    basePath, path, componentMetadata.getTypeName()));
            docFilePath.getParentFile().mkdirs();
            try (BufferedWriter writer =
                    new BufferedWriter(new FileWriter(docFilePath.getAbsolutePath()))) {
                writer.write(MarkdownGenerator.generateComponentDoc(componentMetadata));
            }
        }

        // DataFlow dot files.
        for (DataFlowConfig dataFlowConfig : dataFlowConfigs) {
            String dotFile = DotFileGenerator.generateDOTFile(dataFlowConfig);
            File dotFilePath =
                    new File(String.format("%s/dataflow/%s.dot", basePath, dataFlowConfig.getId()));
            dotFilePath.getParentFile().mkdirs();
            try (BufferedWriter writer =
                    new BufferedWriter(new FileWriter(dotFilePath.getAbsolutePath()))) {
                writer.write(dotFile);
            }
        }
    }
}
