/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MemoryCache.java
 */
package dataflow.core.cache;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

/** A {@link Cache} implementation that uses a hash map in memory. */
public class MemoryCache<K, V> implements Cache<K, V> {

    private final Map<K, V> cache = new ConcurrentHashMap<>();

    @Override
    public V get(K key, Callable<? extends V> loader) throws ExecutionException {
        V value = cache.get(key);
        if (value == null) {
            try {
                value = loader.call();
                cache.put(key, value);
            } catch (Throwable t) {
                throw new ExecutionException("Unable to load value into cache", t);
            }
        }
        return value;
    }
}
