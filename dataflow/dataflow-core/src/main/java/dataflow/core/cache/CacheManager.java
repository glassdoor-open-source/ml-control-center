/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CacheManager.java
 */
package dataflow.core.cache;

import dataflow.core.exception.DataFlowConfigurationException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CacheManager {

    private final Map<String, Cache<?, ?>> idCacheMap = new ConcurrentHashMap<>();

    public Cache<?, ?> getCache(String id) {
        Cache<?, ?> cache = idCacheMap.get(id);
        if (cache == null) {
            throw new DataFlowConfigurationException("No cache with id " + id + " is registered");
        }
        return cache;
    }

    public void register(String id, Cache<?, ?> cache) {
        idCacheMap.put(id, cache);
    }
}
