/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ClassloaderJavaFileManager.java
 */
package dataflow.core.compiler;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.*;
import java.util.jar.JarEntry;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;

/**
 * A {@link javax.tools.JavaFileManager} for files loaded using a specified class loader. This is
 * needed because the {@link javax.tools.JavaCompiler} is not able to find classes in fat jars that
 * contain other jars (such as the jars created for spring boot apps). This file manager makes those
 * classes visible to the compiler by using the provided class loader to find the class files / jars
 * in the class path.
 */
public class ClassloaderJavaFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {

    private static final String CLASS_EXTENSION = ".class";
    private static final String BASE_MODULE = "SYSTEM_MODULES[java.base]";

    private final ClassLoader classLoader;
    private final StandardJavaFileManager standardFileManager;

    public ClassloaderJavaFileManager(
            ClassLoader classLoader, StandardJavaFileManager standardFileManager) {
        super(standardFileManager);
        this.classLoader = classLoader;
        this.standardFileManager = standardFileManager;
    }

    @Override
    public String inferBinaryName(Location location, JavaFileObject file) {
        if (file instanceof ClassloaderJavaFileObject) {
            return ((ClassloaderJavaFileObject) file).binaryName();
        } else {
            return standardFileManager.inferBinaryName(location, file);
        }
    }

    @Override
    public boolean hasLocation(Location location) {
        return location == StandardLocation.CLASS_PATH
                || location == StandardLocation.PLATFORM_CLASS_PATH;
    }

    @Override
    public Iterable<JavaFileObject> list(
            Location location, String packageName, Set<Kind> kinds, boolean recurse)
            throws IOException {

        if (location == StandardLocation.PLATFORM_CLASS_PATH
                || BASE_MODULE.equals(location.getName())) {
            return standardFileManager.list(location, packageName, kinds, recurse);
        } else if (location == StandardLocation.CLASS_PATH && kinds.contains(Kind.CLASS)) {

            if (packageName.startsWith("java") || packageName.startsWith("com.sun")) {
                return standardFileManager.list(location, packageName, kinds, recurse);
            } else {
                List<JavaFileObject> files = new ArrayList<>();
                standardFileManager.list(location, packageName, kinds, recurse).forEach(files::add);
                if (files.isEmpty()) {
                    files.addAll(findUsingClassLoader(packageName));
                }
                return files;
            }
        }
        return Collections.emptyList();
    }

    @Override
    public int isSupportedOption(String option) {
        return -1;
    }

    public List<JavaFileObject> findUsingClassLoader(String packageName) throws IOException {
        String javaPackageName = packageName.replaceAll("\\.", "/");

        List<JavaFileObject> result = new ArrayList<>();

        Enumeration<URL> packageResources = classLoader.getResources(javaPackageName);
        while (packageResources.hasMoreElements()) {
            URL packageResource = packageResources.nextElement();
            result.addAll(listPackageResource(packageName, packageResource));
        }

        return result;
    }

    private Collection<JavaFileObject> listPackageResource(
            String packageName, URL packageResourceUrl) {
        File packageResourceFile = new File(packageResourceUrl.getFile());
        if (packageResourceFile.isDirectory()) {
            return listDirectory(packageName, packageResourceFile);
        } else {
            return listJar(packageResourceUrl);
        }
    }

    private List<JavaFileObject> listJar(URL packageResourceUrl) {
        List<JavaFileObject> files = new ArrayList<>();
        try {
            String jarUri = packageResourceUrl.toExternalForm();
            int pos = jarUri.lastIndexOf('!');
            jarUri = jarUri.substring(0, pos);

            JarURLConnection jarConn = (JarURLConnection) packageResourceUrl.openConnection();
            String rootEntryName = jarConn.getEntryName();
            int rootEndPos = rootEntryName.length() + 1;

            Enumeration<JarEntry> entries = jarConn.getJarFile().entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                String name = entry.getName();
                if (name.startsWith(rootEntryName)
                        && name.indexOf('/', rootEndPos) == -1
                        && name.endsWith(CLASS_EXTENSION)) {
                    URI uri = URI.create(jarUri + "!/" + name);
                    String binaryName = name.replaceAll("/", ".");
                    binaryName = binaryName.replaceAll(CLASS_EXTENSION + "$", "");
                    files.add(new ClassloaderJavaFileObject(binaryName, uri));
                }
            }
            return files;
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error listing contents of jar file " + packageResourceUrl, e);
        }
    }

    private List<JavaFileObject> listDirectory(String packageName, File directory) {
        List<JavaFileObject> files = new ArrayList<>();
        File[] children = directory.listFiles();
        if (children != null) {
            for (File child : children) {
                if (child.isFile()) {
                    if (child.getName().endsWith(CLASS_EXTENSION)) {
                        String binaryName = packageName + "." + child.getName();
                        binaryName = binaryName.replaceAll(CLASS_EXTENSION + "$", "");
                        files.add(new ClassloaderJavaFileObject(binaryName, child.toURI()));
                    }
                }
            }
        }
        return files;
    }
}
