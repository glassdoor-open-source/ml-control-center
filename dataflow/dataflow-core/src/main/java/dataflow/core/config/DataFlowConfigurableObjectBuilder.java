/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigurableObjectBuilder.java
 */
package dataflow.core.config;

import dataflow.core.engine.DataFlowExecutionContext;
import java.util.Map;

/**
 * An interface for builders that build a DataFlowConfigurable object from configuration properties.
 */
public interface DataFlowConfigurableObjectBuilder {

    /**
     * Build the object
     *
     * @param configProperties the configuration properties
     * @param instanceValues the values from the DataFlow instance
     * @return the built object
     */
    Object build(Map<String, Object> configProperties, Map<String, Object> instanceValues);

    default <T> T injectDependencies(T obj) {
        return DataFlowExecutionContext.getCurrentExecutionContext()
                .getDependencyInjector()
                .injectDependencies(obj);
    }
}
