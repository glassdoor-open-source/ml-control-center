/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ComponentConfig.java
 */
package dataflow.core.config;

import dataflow.core.exception.DataFlowConfigurationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings({"WeakerAccess", "unchecked", "unused"})
public class ComponentConfig {

    /**
     * If any fields are changed here check if {@link #setExtendedConfig(ComponentConfig)} and
     * {@link #overrideWith(ComponentConfig)} need to be updated.
     */
    private String id;

    private String type;
    // The original type of the component. Can be different from type if the component has an
    // override or mock that changes the type.
    private String originalType;
    // Specifies if this component is a provided value. Provided values are set by the caller before
    // executing the pipeline.
    private boolean providedValue;
    private String extendsId;
    private String overridesId;
    private String mocksId;
    private Map<String, Object> properties = new LinkedHashMap<>();
    private final Map<String, ComponentConfig> propertyValueProviders = new LinkedHashMap<>();
    private final Map<String, ComponentConfig> input = new LinkedHashMap<>();
    private final Map<String, ComponentConfig> output = new LinkedHashMap<>();
    private Map<String, String> inputTypes = new LinkedHashMap<>();
    private String outputType;
    private Boolean failOnError = true;
    private Map<String, Object> rawConfig = new LinkedHashMap<>();
    private boolean internal = false;

    // Specifies if the component is detached.
    // When overriding a component the inputs of the overridden component are detached if they
    // are no longer used.
    private boolean detached = false;
    private boolean sink = false;
    private boolean _abstract = false;
    private Boolean eventsEnabled = null;
    private ComponentConfig parentComponentConfig;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String getId() {
        return id;
    }

    public String getIdWithPath() {
        if (parentComponentConfig == null) {
            return getId();
        }
        return parentComponentConfig.getIdWithPath() + "." + getId();
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public Map<String, ComponentConfig> getInput() {
        return input;
    }

    public Map<String, ComponentConfig> getDependencies() {
        Map<String, ComponentConfig> inputs = new LinkedHashMap<>(getInput());
        inputs.putAll(getPropertyValueProviders());
        return inputs;
    }

    public boolean shouldFailOnError() {
        return failOnError != null && failOnError;
    }

    public void setFailOnError(Boolean failOnError) {
        this.failOnError = failOnError;
    }

    public Map<String, Object> getRawConfig() {
        return rawConfig;
    }

    public void setId(final String id) {
        if (id != null && id.isBlank()) {
            throw new DataFlowConfigurationException("Component id can not be blank");
        }
        this.id = id;
    }

    public void setProperties(final Map<String, Object> properties) {
        this.properties = properties;
    }

    public Map<String, ComponentConfig> getPropertyValueProviders() {
        return propertyValueProviders;
    }

    public void addPropertyValueProvider(ComponentConfig componentConfig) {
        propertyValueProviders.put(componentConfig.getId(), componentConfig);
        componentConfig.addOutput(this);
    }

    public void removePropertyValueProvider(ComponentConfig componentConfig) {
        propertyValueProviders.remove(componentConfig.getId(), componentConfig);
        componentConfig.removeOutput(getId());
    }

    private void addOutput(ComponentConfig componentConfig) {
        output.put(componentConfig.getId(), componentConfig);
        if (detached) {
            // Reattach the component.
            getDependencies().values().forEach(dep -> dep.addOutput(this));
            detached = false;
        }
    }

    private void removeOutput(String componentId) {
        output.remove(componentId);
        if (output.isEmpty()) {
            // This component has no more outputs. Mark it as detached to indicate that
            // it is not used.
            if (!detached) {
                detached = true;
                // Remove this component from its dependencies.
                getDependencies().values().forEach(dep -> dep.removeOutput(getId()));
            }
        }
    }

    public void setPropertyValueProviders(Map<String, ComponentConfig> providersMap) {
        this.propertyValueProviders.forEach((key, value) -> value.removeOutput(getId()));
        this.propertyValueProviders.clear();
        providersMap.values().forEach(this::addPropertyValueProvider);
    }

    public void setInput(final Map<String, ComponentConfig> input) {
        this.input.forEach((id, inputConfig) -> inputConfig.removeOutput(getId()));
        this.input.clear();
        input.forEach(this::setInput);
    }

    public void setInput(String id, ComponentConfig input) {
        this.input.put(id, input);
        input.addOutput(this);
    }

    public void removeInput(String id, ComponentConfig input) {
        this.input.remove(id);
        input.removeOutput(getId());
    }

    /** Returns the components that have this component as a direct input. */
    public Map<String, ComponentConfig> getOutputComponents() {
        return output;
    }

    public void setRawConfig(final Map<String, Object> rawConfig) {
        this.rawConfig = rawConfig;
    }

    public String getType() {
        return type;
    }

    public String getOriginalType() {
        return originalType != null ? originalType : type;
    }

    public void setType(final String type) {
        if (type != null && type.isBlank()) {
            throw new DataFlowConfigurationException("Component type can not be blank");
        }
        if (originalType == null) {
            // Set the original type before updating the type.
            originalType = this.type != null ? this.type : type;
        }
        this.type = type;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setAbstract(boolean _abstract) {
        this._abstract = _abstract;
    }

    public boolean isAbstract() {
        return _abstract;
    }

    public void setSink(boolean sink) {
        this.sink = sink;
    }

    public boolean isSink() {
        return sink;
    }

    public boolean isDetached() {
        return detached;
    }

    public void overrideWith(ComponentConfig overrideConfig) {
        // Intentionally skipping id field.
        // Intentionally skipping overridesId field

        setType(overrideConfig.type);
        // Intentionally leaving originalType unchanged.
        this.extendsId = overrideConfig.extendsId;
        this.properties = overrideConfig.properties;

        // Call the set methods to ensure that the component graph structure is properly maintained.
        setPropertyValueProviders(overrideConfig.propertyValueProviders);
        setInput(overrideConfig.input);

        overrideConfig.output.forEach((key, value) -> addOutput(value));

        this.inputTypes.clear();
        this.inputTypes.putAll(overrideConfig.inputTypes);
        this.outputType = overrideConfig.outputType;
        this.failOnError = overrideConfig.failOnError;
        this.sink = overrideConfig.sink;
        this.internal = overrideConfig.internal;
        this._abstract = overrideConfig._abstract;
        this.eventsEnabled = overrideConfig.eventsEnabled;
        this.parentComponentConfig = overrideConfig.parentComponentConfig;
        this.rawConfig.clear();
        this.rawConfig.putAll(overrideConfig.rawConfig);
    }

    public void setExtendedConfig(ComponentConfig extendedConfig)
            throws DataFlowConfigurationException {
        if (this.type != null
                && !this.type.equalsIgnoreCase(extendedConfig.type)
                && !this.type.equalsIgnoreCase("Component")
                && !this.type.equalsIgnoreCase("ValueProvider")
                && !this.type.equalsIgnoreCase("Action")
                && this.getOverridesId() == null) {
            throw new DataFlowConfigurationException(
                    String.format(
                            "Invalid type %s for component %s. It should be %s or Component",
                            this.type, id, extendedConfig.type));
        }
        if (extendedConfig == null) {
            throw new DataFlowConfigurationException(
                    "Invalid extended config id for component " + getId());
        }
        if (this.getOverridesId() == null && this.getMocksId() == null) {
            // Change the type to the extended config type unless this is a override or mock.
            setType(extendedConfig.getType());
        }

        if (extendedConfig.properties != null) {
            extendMapRecursive(properties, extendedConfig.properties);
        }
        extendedConfig.propertyValueProviders.forEach(
                (k, v) -> {
                    propertyValueProviders.putIfAbsent(k, v);
                    v.getOutputComponents().putIfAbsent(getId(), this);
                });
        if (extendedConfig.input != null) {
            extendedConfig.input.forEach(
                    (k, v) -> {
                        input.putIfAbsent(k, v);
                        v.getOutputComponents().put(getId(), this);
                    });
        }
        if (extendedConfig.inputTypes != null) {
            extendedConfig.inputTypes.forEach(
                    (k, v) -> {
                        inputTypes.putIfAbsent(k, v);
                    });
        }
        // Skip extending output because this config will be connected independently to other nodes
        // in the graph.

        if (outputType == null) {
            outputType = extendedConfig.outputType;
        }
        if (failOnError == null) {
            failOnError = extendedConfig.failOnError;
        }
        extendMapRecursive(rawConfig, extendedConfig.rawConfig);
        if (eventsEnabled == null) {
            eventsEnabled = extendedConfig.eventsEnabled;
        }
    }

    private void extendMapRecursive(Map map, Map extendedMap) {
        Set<Map.Entry> entrySet = extendedMap.entrySet();
        for (Map.Entry entry : entrySet) {
            Object key = entry.getKey();
            Object extendedValue = entry.getValue();
            boolean containsKey = map.containsKey(key);
            if (extendedValue instanceof RawComponentConfig) {
                RawComponentConfig innerConfig =
                        containsKey
                                ? (RawComponentConfig) map.get(key)
                                : new RawComponentConfig(
                                        ((RawComponentConfig) extendedValue).getType(),
                                        new LinkedHashMap<>());
                extendMapRecursive(innerConfig, (Map) extendedValue);
                map.put(key, innerConfig);
            } else if (extendedValue instanceof RawConfigurableObjectConfig) {
                RawConfigurableObjectConfig innerConfig =
                        containsKey
                                ? (RawConfigurableObjectConfig) map.get(key)
                                : new RawConfigurableObjectConfig(
                                        ((RawConfigurableObjectConfig) extendedValue).getType(),
                                        new LinkedHashMap<>());
                extendMapRecursive(innerConfig, (Map) extendedValue);
                map.put(key, innerConfig);
            } else if (extendedValue instanceof Map) {
                Map innerMap = containsKey ? (Map) map.get(key) : new LinkedHashMap();
                extendMapRecursive(innerMap, (Map) extendedValue);
                map.put(key, innerMap);
            } else if (!containsKey && extendedValue instanceof List) {
                List innerList = new ArrayList();
                extendListRecursive(innerList, (List) extendedValue);
                map.put(key, innerList);
            } else if (!containsKey) {
                map.put(entry.getKey(), entry.getValue());
            }
        }
    }

    private void extendListRecursive(List list, List extendedList) {
        for (Object extendedValue : extendedList) {
            if (extendedValue instanceof Map) {
                Map innerMap = new LinkedHashMap();
                extendMapRecursive(innerMap, (Map) extendedValue);
                list.add(innerMap);
            } else if (extendedValue instanceof List) {
                List innerList = new ArrayList();
                extendListRecursive(innerList, (List) extendedValue);
                list.add(innerList);
            } else {
                list.add(extendedValue);
            }
        }
    }

    public String getExtendsId() {
        return extendsId;
    }

    public void setExtendsId(final String extendsId) {
        if (extendsId != null && extendsId.isBlank()) {
            throw new DataFlowConfigurationException("extends can not be blank");
        }
        this.extendsId = extendsId;
    }

    public String getOverridesId() {
        return overridesId;
    }

    public void setOverridesId(String overridesId) {
        if (overridesId != null && overridesId.isBlank()) {
            throw new DataFlowConfigurationException("overrides can not be blank");
        }
        this.overridesId = overridesId;
    }

    public String getMocksId() {
        return mocksId;
    }

    public void setMocksId(String mocksId) {
        if (mocksId != null && mocksId.isBlank()) {
            throw new DataFlowConfigurationException("mocks can not be blank");
        }
        this.mocksId = mocksId;
    }

    public Boolean isEventsEnabled() {
        return eventsEnabled;
    }

    public void setEventsEnabled(final boolean eventsEnabled) {
        this.eventsEnabled = eventsEnabled;
    }

    public Map<String, String> getInputTypes() {
        return inputTypes;
    }

    public void setInputTypes(final Map<String, String> inputTypes) {
        this.inputTypes = inputTypes;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(final String outputType) {
        if (outputType != null && outputType.isBlank()) {
            throw new DataFlowConfigurationException("outputType can not be blank");
        }
        this.outputType = outputType;
    }

    public String summaryString() {
        return getIdWithPath() + getRawConfig();
    }

    @Override
    public String toString() {
        return "ComponentConfig {"
                + "id='"
                + id
                + '\''
                + ", type='"
                + type
                + '\''
                + ", originalType='"
                + originalType
                + '\''
                + ", extendsId='"
                + extendsId
                + '\''
                + ", overridesId='"
                + overridesId
                + '\''
                + ", mocksId='"
                + mocksId
                + '\''
                + ", properties="
                + properties
                + ", propertyValueProviders="
                + emptyIfNull(propertyValueProviders).keySet()
                + ", input="
                + emptyIfNull(input).keySet()
                + ", output="
                + emptyIfNull(output).keySet()
                + ", inputTypes="
                + inputTypes.keySet()
                + ", outputType='"
                + outputType
                + '\''
                + ", failOnError="
                + failOnError
                + ", abstract="
                + _abstract
                + ", internal="
                + internal
                + ", providedValue="
                + providedValue
                + ", eventsEnabled="
                + eventsEnabled
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ComponentConfig that = (ComponentConfig) o;
        return isInternal() == that.isInternal()
                && isSink() == that.isSink()
                && _abstract == that._abstract
                && Objects.equals(getId(), that.getId())
                && Objects.equals(getType(), that.getType())
                && Objects.equals(getExtendsId(), that.getExtendsId())
                && Objects.equals(getOverridesId(), that.getOverridesId())
                && Objects.equals(getMocksId(), that.getMocksId())
                && Objects.equals(getProperties(), that.getProperties())
                && Objects.equals(
                        emptyIfNull(getPropertyValueProviders()).keySet(),
                        emptyIfNull(that.getPropertyValueProviders()).keySet())
                && Objects.equals(
                        emptyIfNull(getInput()).keySet(), emptyIfNull(that.getInput()).keySet())
                && Objects.equals(emptyIfNull(output).keySet(), emptyIfNull(that.output).keySet())
                && Objects.equals(getInputTypes(), that.getInputTypes())
                && Objects.equals(getOutputType(), that.getOutputType())
                && Objects.equals(failOnError, that.failOnError)
                && Objects.equals(isEventsEnabled(), that.isEventsEnabled())
                && providedValue == that.providedValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getId(),
                getType(),
                getExtendsId(),
                getOverridesId(),
                getMocksId(),
                getProperties(),
                emptyIfNull(getPropertyValueProviders()).keySet(),
                emptyIfNull(getInput()).keySet(),
                emptyIfNull(output).keySet(),
                getInputTypes(),
                getOutputType(),
                failOnError,
                getRawConfig(),
                isInternal(),
                isSink(),
                isProvidedValue(),
                _abstract,
                isEventsEnabled());
    }

    private <K, V> Map<K, V> emptyIfNull(Map<K, V> map) {
        return map != null ? map : Collections.emptyMap();
    }

    public void setParentComponentConfig(ComponentConfig parentComponentConfig) {
        this.parentComponentConfig = parentComponentConfig;
    }

    public ComponentConfig getParentComponentConfig() {
        return parentComponentConfig;
    }

    public void setProvidedValue(boolean provided) {
        this.providedValue = provided;
    }

    public boolean isProvidedValue() {
        return providedValue;
    }
}
