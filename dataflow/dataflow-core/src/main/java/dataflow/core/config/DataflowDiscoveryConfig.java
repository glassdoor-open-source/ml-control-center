/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataflowDiscoveryConfig.java
 */
package dataflow.core.config;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DataflowDiscoveryConfig {

    private Set<String> componentPackages;
    private Set<String> excludeComponentPackages;
    private Set<String> includeComponentClasses;
    private Set<String> excludeComponentClasses;
    private Set<String> dataFlowConfigPaths;
    private Set<String> environmentConfigPaths;

    public Set<String> getComponentPackages() {
        return componentPackages != null
                ? Collections.unmodifiableSet(componentPackages)
                : Collections.emptySet();
    }

    public Set<String> getExcludeComponentPackages() {
        return excludeComponentPackages != null
                ? Collections.unmodifiableSet(excludeComponentPackages)
                : Collections.emptySet();
    }

    public Set<String> getIncludeComponentClasses() {
        return includeComponentClasses != null
                ? Collections.unmodifiableSet(includeComponentClasses)
                : Collections.emptySet();
    }

    public Set<String> getExcludeComponentClasses() {
        return excludeComponentClasses != null
                ? Collections.unmodifiableSet(excludeComponentClasses)
                : Collections.emptySet();
    }

    public Set<String> getDataFlowConfigPaths() {
        return dataFlowConfigPaths != null
                ? Collections.unmodifiableSet(dataFlowConfigPaths)
                : Collections.emptySet();
    }

    public Set<String> getEnvironmentConfigPaths() {
        return environmentConfigPaths != null
                ? Collections.unmodifiableSet(environmentConfigPaths)
                : Collections.emptySet();
    }

    public void setComponentPackages(Set<String> componentPackages) {
        this.componentPackages = new HashSet<>(componentPackages);
    }

    public void setExcludeComponentPackages(Set<String> excludeComponentPackages) {
        this.excludeComponentPackages = new HashSet<>(excludeComponentPackages);
    }

    public void setIncludeComponentClasses(Set<String> includeComponentClasses) {
        this.includeComponentClasses = new HashSet<>(includeComponentClasses);
    }

    public void setExcludeComponentClasses(Set<String> excludeComponentClasses) {
        this.excludeComponentClasses = new HashSet<>(excludeComponentClasses);
    }

    public void setDataFlowConfigPaths(Set<String> dataFlowConfigPaths) {
        this.dataFlowConfigPaths = new HashSet<>(dataFlowConfigPaths);
    }

    public void setEnvironmentConfigPaths(Set<String> environmentConfigPaths) {
        this.environmentConfigPaths = new HashSet<>(environmentConfigPaths);
    }
}
