/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfig.java
 */
package dataflow.core.config;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class DataFlowConfig {

    public static final String DEFAULT_ITEM_VALUE_NAME = "item";

    private String id;
    private final Map<String, ComponentConfig> components;
    private ComponentConfig output;
    private boolean incrementalMode;
    private boolean eventsEnabled;
    // Map of import resource name to import config yaml.
    private final Map<String, String> importConfigs;

    private String parentId;
    private Map<String, DataFlowConfig> childDataFlows;

    public DataFlowConfig(
            final String id,
            final Map<String, ComponentConfig> components,
            final ComponentConfig output,
            Map<String, String> importConfigs,
            String parentId,
            final Map<String, DataFlowConfig> childDataFlows,
            final boolean incrementalMode,
            final boolean eventsEnabled) {
        this.id = id;
        this.components = new LinkedHashMap<>(components);
        this.output = output;
        this.importConfigs = new LinkedHashMap<>(importConfigs);
        this.parentId = parentId;
        this.childDataFlows = new LinkedHashMap<>(childDataFlows);
        this.incrementalMode = incrementalMode;
        this.eventsEnabled = eventsEnabled;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Map<String, ComponentConfig> getComponents() {
        return components;
    }

    public ComponentConfig getOutput() {
        return output;
    }

    public Map<String, String> getImportConfigs() {
        return importConfigs;
    }

    public Map<String, DataFlowConfig> getChildDataFlows() {
        return childDataFlows;
    }

    public boolean eventsEnabled() {
        return eventsEnabled;
    }

    public Set<String> getProvidedValueIds() {
        return components.values().stream()
                .filter(ComponentConfig::isProvidedValue)
                .map(ComponentConfig::getId)
                .collect(Collectors.toSet());
    }

    public Set<String> getSinks() {
        return components.values().stream()
                .filter(ComponentConfig::isSink)
                .filter(v -> !v.isAbstract())
                .filter(v -> v != output)
                .map(ComponentConfig::getId)
                .collect(Collectors.toSet());
    }

    public boolean isIncrementalMode() {
        return incrementalMode;
    }

    public boolean isEventsEnabled() {
        return eventsEnabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DataFlowConfig that = (DataFlowConfig) o;
        return incrementalMode == that.incrementalMode
                && eventsEnabled == that.eventsEnabled
                && Objects.equals(id, that.id)
                && Objects.equals(components, that.components)
                && Objects.equals(output, that.output)
                && Objects.equals(importConfigs, importConfigs)
                && Objects.equals(parentId, that.parentId)
                && Objects.equals(childDataFlows, that.childDataFlows);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                id,
                components,
                output,
                incrementalMode,
                eventsEnabled,
                importConfigs,
                parentId,
                childDataFlows);
    }

    @Override
    public String toString() {
        return "DataFlowConfig{"
                + "id='"
                + id
                + '\''
                + ", components="
                + components
                + ", output="
                + output
                + ", incrementalMode="
                + incrementalMode
                + ", eventsEnabled="
                + eventsEnabled
                + ", importConfigs="
                + importConfigs.keySet()
                + ", parentId="
                + parentId
                + ", childDataFlows="
                + childDataFlows
                + '}';
    }
}
