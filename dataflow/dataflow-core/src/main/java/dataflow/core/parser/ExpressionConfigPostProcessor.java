/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ExpressionConfigPostProcessor.java
 */
package dataflow.core.parser;

import static dataflow.core.parser.CoreComponentTypes.EXPRESSION_VALUE_PROVIDER;

import dataflow.core.config.ComponentConfig;
import dataflow.core.config.RawComponentConfig;
import dataflow.core.exception.DataFlowConfigurationException;
import java.io.StringReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.yaml.snakeyaml.Yaml;

public class ExpressionConfigPostProcessor {

    private static final String EXPRESSION_PROPERTY_NAME = "expression";

    private static class ExpressionParseContext {

        ComponentConfig componentConfig;
        StringBuilder expression = new StringBuilder();
        StringBuilder componentIdReference = new StringBuilder();
        StringBuilder componentIdRefOrTypeName = new StringBuilder();
        StringBuilder properties = new StringBuilder();
        StringBuilder inputs = new StringBuilder();
        StringBuilder current = expression;
        boolean inExpr = false;
        // true if this is a reference to an existing value, otherwise a new value provider will be
        // created.
        boolean valueRef = true;

        public ExpressionParseContext(final ComponentConfig componentConfig) {
            this.componentConfig = componentConfig;
        }
    }

    private Yaml yaml = new Yaml();

    void process(DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        List<ComponentConfig> configs = new ArrayList<>(ctx.componentConfigs.values());
        for (ComponentConfig componentConfig : configs) {
            if (isExpressionValueProvider(componentConfig)) {
                processExpressionValueProvider(componentConfig, ctx);
            }
        }
    }

    private boolean isExpressionValueProvider(ComponentConfig componentConfig) {
        return EXPRESSION_VALUE_PROVIDER.equalsIgnoreCase(componentConfig.getType())
                || EXPRESSION_VALUE_PROVIDER.equalsIgnoreCase(componentConfig.getOriginalType());
    }

    private void processExpressionValueProvider(
            ComponentConfig componentConfig, DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        String expr = (String) componentConfig.getProperties().get(EXPRESSION_PROPERTY_NAME);
        if (expr.contains("$(")) {
            throw new DataFlowConfigurationException(
                    "Invalid expression " + expr + " expressions must use @ syntax");
        }
        ExpressionParseContext context = new ExpressionParseContext(componentConfig);
        Deque<ExpressionParseContext> stack = new ArrayDeque<>();
        for (int idx = 0; idx < expr.length(); idx++) {
            try {
                char ch = expr.charAt(idx);
                if (ch == '@') {
                    if (context.inExpr) {
                        // We are already in an expression so start an inner expression
                        stack.addLast(context);
                        context =
                                new ExpressionParseContext(
                                        DataFlowConfigBuilder.getOrCreateComponentConfig(
                                                ctx.idGenerator.generate(
                                                        context.componentConfig.getId() + "_inner"),
                                                componentConfig,
                                                ctx));
                        context.componentConfig.setType(EXPRESSION_VALUE_PROVIDER);
                    }
                    // If characters other than ( follow @ then it is a component id reference or type name.
                    context.current = context.componentIdRefOrTypeName;
                    context.inExpr = true;
                    continue;
                } else if (context.inExpr && ch == '{') {
                    // This is an expression of the form @componentTypeName{properties}(inputs)
                    context.valueRef = false;
                    context.current = context.properties;
                    continue;
                } else if (context.inExpr && ch == '}') {
                    // This is an expression of the form @componentTypeName{properties}(inputs)
                    // Set current to null, next character should be (
                    context.current = null;
                    continue;
                } else if (context.inExpr && ch == '(') {
                    if (!context.componentIdRefOrTypeName.isEmpty()) {
                        // This is an expression of the form @componentTypeName{properties}(inputs)
                        context.valueRef = false;
                        context.current = context.inputs;
                    } else {
                        // This is an expression of the form @(componentId)
                        context.valueRef = true;
                        context.current = context.componentIdReference;
                    }
                    continue;
                } else if (context.inExpr && ch == ')') {
                    finalizeExpression(context, componentConfig, ctx);

                    if (!stack.isEmpty()) {
                        finalizeExpressionValueProviderConfig(context);
                        ExpressionParseContext innerContext = context;
                        context = stack.pop();
                        String refId = innerContext.componentConfig.getId();
                        if (isSimpleValueReferenceExpr(innerContext.componentConfig)) {
                            // The inner expression is simply a reference to a value. Skip
                            // evaluating the inner
                            // expression remove it and just reference the value directly.
                            ctx.componentConfigs.remove(innerContext.componentConfig.getId());
                            innerContext
                                    .componentConfig
                                    .getInput()
                                    .values()
                                    .forEach(
                                            input ->
                                                    input.getOutputComponents()
                                                            .remove(
                                                                    innerContext.componentConfig
                                                                            .getId()));
                            refId =
                                    innerContext
                                            .componentConfig
                                            .getInput()
                                            .keySet()
                                            .iterator()
                                            .next();
                        }
                        context.current.append("$(").append(refId).append(")");
                    }
                    continue;
                }
                if (context.current == null) {
                    throw new RuntimeException("Invalid expression");
                }
                context.current.append(ch);
            } catch (Exception ex) {
                throw new DataFlowConfigurationException(
                        "Error parsing expression id="
                                + componentConfig.getId()
                                + " \""
                                + expr.substring(0, idx)
                                + "\" -> \""
                                + expr.substring(idx)
                                + "\"",
                        ex);
            }
        }

        finalizeExpressionValueProviderConfig(context);
    }

    private void finalizeExpression(
            ExpressionParseContext context,
            ComponentConfig componentConfig,
            DataFlowConfigBuilder.ConfigBuilderContext ctx) {
        ComponentConfig configRef = null;
        if(context.valueRef) {
            // This is an expression of the form @(componentId)
            String id = context.componentIdReference.toString();
            if (id.trim().isEmpty()) {
                throw new DataFlowConfigurationException("Component id not specified for expression");
            }
            configRef = ctx.componentConfigs.get(id);
            if (configRef == null) {
                // The id does not match one of the already defined components, assume it is a provided value.
                RawComponentConfig newConfig = new RawComponentConfig(null, new LinkedHashMap<>());
                newConfig.put("id", id);
                configRef = DataFlowConfigBuilder.buildProvidedValueComponent(newConfig, componentConfig, ctx);
            }
        } else if(context.componentIdRefOrTypeName != null && !context.componentIdRefOrTypeName.isEmpty()) {
            // The expression is a component value expression eg.
            // @testVal1{bins: [.25,.5,.75]}(value: .6)

            // Create a new component config based on the expression.
            Map<String, Object> newConfig = new HashMap<>();
            newConfig.put(
                "id", ctx.idGenerator.generate(context.componentConfig.getId() + "_inner"));

            String componentIdRefOrTypeName = context.componentIdRefOrTypeName.toString();
            ComponentConfig extendedConfigRef = ctx.componentConfigs.get(componentIdRefOrTypeName);
            String type;
            if(extendedConfigRef == null) {
                // componentIdRefOrTypeName is a type name.
                type = componentIdRefOrTypeName;
            } else {
                // componentIdRefOrTypeName was the id of an existing component that we want to extend.
                type = extendedConfigRef.getType();
                newConfig.put("extends", extendedConfigRef.getId());
            }

            Map<String, Object> inputs = toMap(context.inputs.toString());
            if (inputs.size() == 1 && inputs.values().iterator().next() == null) {
                // A single input without an associated name. Use the name value as default.
                inputs.put("value", inputs.keySet().iterator().next());
            }
            newConfig.put("input", inputs);

            Map<String, Object> properties = toMap(context.properties.toString());
            newConfig.putAll(properties);

            RawComponentConfig rawComponentConfig = new RawComponentConfig(type, newConfig);
            configRef = DataFlowConfigBuilder.buildComponent(rawComponentConfig, componentConfig, ctx);

            // This is running in the post-processing step after extends and overrides have been
            // applied
            // to existing components. Call processExtends on this new component since there are
            // cases where
            // this new component extends an existing component.
            DataFlowConfigBuilder.processExtends(configRef, ctx);
        }

        // Reset the context.
        context.inExpr = false;
        context.componentIdReference.setLength(0);
        context.componentIdRefOrTypeName.setLength(0);
        context.inputs.setLength(0);
        context.properties.setLength(0);
        context.expression.append("@(").append(configRef.getId()).append(")");
        context.componentConfig.setInput(configRef.getId(), configRef);
        context.current = context.expression;
    }

    private Map<String, Object> toMap(String str) {
        if (str == null || str.trim().isEmpty()) {
            return Collections.emptyMap();
        }
        return yaml.load(new StringReader("{" + str + "}"));
    }

    private void finalizeExpressionValueProviderConfig(ExpressionParseContext context) {
        context.componentConfig
                .getProperties()
                .put(EXPRESSION_PROPERTY_NAME, context.expression.toString());
    }

    private boolean isSimpleValueReferenceExpr(ComponentConfig expressionConfig) {
        if (!EXPRESSION_VALUE_PROVIDER.equalsIgnoreCase(expressionConfig.getType())) {
            return false;
        }
        String expr = (String) expressionConfig.getProperties().get(EXPRESSION_PROPERTY_NAME);
        return expressionConfig.getInput().size() == 1
                && expr.startsWith("@(")
                && expr.endsWith(")");
    }
}
