/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  CoreComponentTypes.java
 */
package dataflow.core.parser;

public class CoreComponentTypes {

    static final String CONST_VALUE_PROVIDER = "ConstValueProvider";
    static final String OBJECT_VALUE_PROVIDER = "ObjectValueProvider";
    static final String DATAFLOW_INSTANCE_VALUE_PROVIDER = "DataFlowInstanceValueProvider";
    static final String PLACEHOLDER_STRING_VALUE_PROVIDER = "PlaceholderStringValueProvider";
    static final String MAP_ENTRY_VALUE_PROVIDER = "MapEntryValueProvider";
    static final String LIST_VALUE_PROVIDER = "ListValueProvider";

    static final String MAP_VALUE_PROVIDER = "MapValueProvider";

    static final String EXPRESSION_VALUE_PROVIDER = "ExpressionValueProvider";
}
