/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowConfigBuilder.java
 */
package dataflow.core.parser;

import static dataflow.core.config.DataFlowConfig.DEFAULT_ITEM_VALUE_NAME;
import static dataflow.core.parser.CoreComponentTypes.CONST_VALUE_PROVIDER;
import static dataflow.core.parser.CoreComponentTypes.DATAFLOW_INSTANCE_VALUE_PROVIDER;
import static dataflow.core.parser.CoreComponentTypes.LIST_VALUE_PROVIDER;
import static dataflow.core.parser.CoreComponentTypes.MAP_ENTRY_VALUE_PROVIDER;
import static dataflow.core.parser.CoreComponentTypes.MAP_VALUE_PROVIDER;
import static dataflow.core.parser.CoreComponentTypes.OBJECT_VALUE_PROVIDER;
import static dataflow.core.parser.CoreComponentTypes.PLACEHOLDER_STRING_VALUE_PROVIDER;

import dataflow.core.component.metadata.DataFlowComponentInputMetadata;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.component.metadata.DataFlowConfigurableObjectPropertyMetadata;
import dataflow.core.config.ComponentConfig;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.RawComponentConfig;
import dataflow.core.config.RawConfigurableObjectConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.PlaceholderString;
import dataflow.core.engine.ValueReference;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.util.DataFlowConfigUtil;
import dataflow.core.util.GraphUtil;
import io.micrometer.core.instrument.util.IOUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings({"unchecked", "WeakerAccess"})
public class DataFlowConfigBuilder {

    private static final String[] PIPELINE_SECTION_NAME_ALIASES = {"pipeline", "actions"};

    static class IdGenerator {

        private static final String PREFIX = "_";
        private final Map<String, Integer> nameCountMap = new LinkedHashMap<>();

        public String generate(String prefix) {
            prefix = prefix != null ? PREFIX + prefix : PREFIX;
            int count = nameCountMap.getOrDefault(prefix, 0);
            String name = prefix + count;
            nameCountMap.put(prefix, count + 1);
            return name;
        }
    }

    static class ConfigBuilderContext {

        final Set<String> extendedConfigIds = new LinkedHashSet<>();
        ImportResourceProviderWrapper importResourceProviderWrapper;
        boolean incrementalMode;
        boolean eventsEnabled;
        String id;
        final String parentId;
        ComponentConfig output = null;

        final Map<String, ComponentConfig> componentConfigs = new LinkedHashMap<>();
        final Map<String, DataFlowConfig> childDataFlowConfigs = new LinkedHashMap<>();
        final Map<String, ConfigBuilderContext> childDataFlowContexts = new LinkedHashMap<>();
        final IdGenerator idGenerator = new IdGenerator();
        final DataFlowRegistry registry;
        final ExpressionConfigPostProcessor expressionConfigPostProcessor;
        final Set<String> propValueRefDisabledComponentIds = new LinkedHashSet<>();
        final Function<String, InputStream> importResourceProvider;

        private ConfigBuilderContext(
            String parentId,
            DataFlowRegistry registry,
            Function<String, InputStream> importResourceProvider) {
            this.parentId = parentId;
            this.registry = registry;
            this.expressionConfigPostProcessor = new ExpressionConfigPostProcessor();
            this.id = null;
            this.importResourceProvider = importResourceProvider;
        }

        Set<String> getProvidedValueIds() {
            return componentConfigs.values().stream()
                .filter(ComponentConfig::isProvidedValue)
                .map(ComponentConfig::getId)
                .collect(Collectors.toSet());
        }
    }

    static class ImportResourceProviderWrapper implements Function<String, InputStream> {

        private final Map<String, String> importedResources = new LinkedHashMap<>();
        private final Function<String, InputStream> resourceProvider;

        public ImportResourceProviderWrapper(Function<String, InputStream> resourceProvider) {
            this.resourceProvider = resourceProvider;
        }

        @Override
        public InputStream apply(String resourceName) {
            String importYaml = IOUtils.toString(resourceProvider.apply(resourceName));
            importedResources.put(resourceName, importYaml);
            return new ByteArrayInputStream(importYaml.getBytes());
        }

        public Map<String, String> getImportedResources() {
            return importedResources;
        }
    }

    public static final Pattern VALUE_REFERENCE = Pattern.compile("\\$\\(([^)]+?)\\)");
    private static final String PLACEHOLDER_STRING_ID_PREFIX = "placeholderString";

    private final DataFlowRegistry registry;
    private final Function<String, InputStream> importResourceProvider;

    public DataFlowConfigBuilder(
        DataFlowRegistry registry, Function<String, InputStream> importResourceProvider) {
        this.registry = registry;
        this.importResourceProvider = importResourceProvider;
    }

    public DataFlowRegistry getRegistry() {
        return registry;
    }

    public DataFlowConfig build(Map<String, Object> parsedConfig)
        throws DataFlowConfigurationException {
        String parentId = null; // This is the root config.
        ConfigBuilderContext ctx =
            new ConfigBuilderContext(parentId, registry, importResourceProvider);
        build(parsedConfig, ctx);
        // Perform post-processing for the parent and all children before building the
        // final DataFlowConfig objects.
        postProcess(ctx);
        return createDataFlowConfigFromContext(ctx);
    }

    public static void build(Map<String, Object> parsedConfig, ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {

        for (Map.Entry<String, Object> entry : parsedConfig.entrySet()) {
            String key = entry.getKey().toLowerCase();
            if (key.equalsIgnoreCase("DataFlow")) {
                Object dataFlowObj = entry.getValue();
                if (!(dataFlowObj instanceof Map)) {
                    throw new DataFlowConfigurationException(
                        "Invalid DataFlow configuration: " + dataFlowObj);
                }
                buildDataFlowConfig((Map<String, Object>) dataFlowObj, ctx);
            } else {
                buildDataFlowConfig(parsedConfig, ctx);
            }
            return;
        }
        throw new DataFlowConfigurationException(
            "Configuration must have DataFlow or pipeline as a root element");
    }

    private static void buildDataFlowConfig(
        Map<String, Object> dataFlowObj, ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {
        ctx.eventsEnabled = true;
        ctx.incrementalMode = false;

        if (dataFlowObj.getClass() == RawComponentConfig.class) {
            // DataFlow consists of a single component.
            ctx.output = buildComponentForNode(dataFlowObj, null, ctx);
            dataFlowObj = new LinkedHashMap<>();
        }

        ctx.id = (String) dataFlowObj.get("id");
        if (ctx.id != null && ctx.id.isBlank()) {
            throw new DataFlowConfigurationException(
                "Dataflow id can not be blank: " + dataFlowObj);
        }
        if (ctx.id == null) {
            ctx.id = "anonymous_" + UUID.randomUUID().toString().replace("-", "");
        }

        // Process imports this should be done before building any other components.
        ctx.importResourceProviderWrapper =
            new ImportResourceProviderWrapper(ctx.importResourceProvider);
        if (dataFlowObj.get("import") != null) {
            Object importsObj = dataFlowObj.get("import");
            if (importsObj instanceof String) {
                // Accept a single string by converting it to a list.
                importsObj = Collections.singletonList(importsObj);
            }
            if (!(importsObj instanceof Collection)) {
                throw new DataFlowConfigurationException("Invalid import: " + importsObj);
            }
            if (((Collection<?>) importsObj).stream().anyMatch(v -> !(v instanceof String))) {
                throw new DataFlowConfigurationException("Invalid import: " + importsObj);
            }
            List<String> importResourceNames = (List<String>) importsObj;
            for (String importItem : importResourceNames) {
                InputStream importIn = ctx.importResourceProviderWrapper.apply(importItem);
                if (importIn == null) {
                    throw new DataFlowConfigurationException(
                        "Could not find import " + importItem + " on the classpath");
                }
                try (importIn) {
                    Object importConfig =
                        new DataFlowParser(ctx.registry, ctx.importResourceProviderWrapper)
                            .parseRawConfig(importIn);
                    buildComponents(importConfig, ctx);
                } catch (DataFlowParseException e) {
                    throw new DataFlowConfigurationException(
                        "Error while parsing import " + importItem, e);
                } catch (IOException e) {
                    throw new DataFlowConfigurationException(
                        "Error while reading import " + importItem, e);
                }
            }
        }
        for (Map.Entry<String, Object> entry : dataFlowObj.entrySet()) {
            String key = entry.getKey().toLowerCase();
            if (key.equalsIgnoreCase("id")) {
                // Id has already been set.
            } else if (Arrays.stream(PIPELINE_SECTION_NAME_ALIASES)
                .anyMatch(key::equalsIgnoreCase)) {
                buildComponents(entry.getValue(), ctx);
            } else if (key.equalsIgnoreCase("output")) {
                ctx.output = buildComponentForNode(entry.getValue(), null, ctx);
            } else if (key.equalsIgnoreCase("eventsEnabled")) {
                ctx.eventsEnabled = (boolean) entry.getValue();
            } else if (key.equalsIgnoreCase("incrementalMode")) {
                ctx.incrementalMode = (boolean) entry.getValue();
            } else if (key.equalsIgnoreCase("import")) {
                // Imports have already been processed so skip.
            } else {
                Map<String, Object> componentConfig = new LinkedHashMap<>();
                componentConfig.put(entry.getKey(), entry.getValue());
                buildComponentForNode(componentConfig, null, ctx);
            }
        }

        processExtends(ctx);
        processOverrides(ctx);
    }

    static DataFlowConfig createDataFlowConfigFromContext(ConfigBuilderContext ctx) {
        ctx.childDataFlowContexts.forEach(
            (key, value) ->
                ctx.childDataFlowConfigs.put(key, createDataFlowConfigFromContext(value)));

        DataFlowConfig dataFlowConfig =
            new DataFlowConfig(
                ctx.id,
                ctx.componentConfigs,
                ctx.output,
                ctx.importResourceProviderWrapper.getImportedResources(),
                ctx.parentId,
                ctx.childDataFlowConfigs,
                ctx.incrementalMode,
                ctx.eventsEnabled);
        checkForUnusedConfigs(dataFlowConfig);
        return dataFlowConfig;
    }

    static void postProcess(ConfigBuilderContext ctx) {
        ctx.childDataFlowContexts.values().forEach(DataFlowConfigBuilder::postProcess);

        ctx.expressionConfigPostProcessor.process(ctx);

        if (ctx.output == null) {
            List<ComponentConfig> outputs =
                ctx.componentConfigs.values().stream()
                    .filter(componentCfg -> componentCfg.getType() != null)
                    .filter(componentCfg -> !componentCfg.isSink())
                    .filter(
                        componentCfg ->
                            componentCfg.getOutputComponents() != null
                                && componentCfg.getOutputComponents().isEmpty())
                    .toList();
            if (outputs.size() == 1) {
                // There is only one component that could be the output so mark it as the output.
                ctx.output = outputs.get(0);
            } else {
                throw new DataFlowConfigurationException(
                    "Output for dataflow " + ctx.id + " is not defined");
            }
        }
    }

    static void checkForUnusedConfigs(DataFlowConfig dataFlowConfig) {
        List<ComponentConfig> sortedConfigs = DataFlowConfigUtil.getSortedConfigs(dataFlowConfig);
        Map<String, ComponentConfig> unusedConfigs =
            new LinkedHashMap<>(dataFlowConfig.getComponents());
        sortedConfigs.forEach(v -> unusedConfigs.remove(v.getId()));

        List<String> unusedNotAbstractConfigs =
            unusedConfigs.values().stream()
                .filter(v -> !v.isAbstract())
                .filter(v -> v.getType() != null)
                .filter(v -> v.getOverridesId() == null)
                .filter(v -> !v.isDetached())
                .map(
                    componentConfig ->
                        String.format(
                            "%s%s",
                            componentConfig.getIdWithPath(),
                            componentConfig.getRawConfig()))
                .toList();
        if (!unusedNotAbstractConfigs.isEmpty()) {
            throw new DataFlowConfigurationException(
                    unusedNotAbstractConfigs
                            + " is not used as a direct or indirect dependency of the dataflow output. "
                            + "If this component is not used to compute the final output but you would still like it to run add sink: true to the component config. "
                            + " If this component is only used as a base for inheritance add abstract: true to the component config.");
        }
    }

    static void processExtends(ConfigBuilderContext ctx) throws DataFlowConfigurationException {
        Map<String, ComponentConfig> componentConfigs = ctx.componentConfigs;
        for (ComponentConfig config : componentConfigs.values()) {
            processExtends(config, ctx);
        }
    }

    static void processExtends(ComponentConfig config, ConfigBuilderContext ctx) {
        if (config.getExtendsId() == null) {
            // Nothing to extend.
            return;
        }
        // Build the chain of extended configs.
        List<ComponentConfig> extendedConfigs =
            GraphUtil.topologicalSort(
                config,
                extendedConfig -> {
                    ComponentConfig extendsConfig =
                        getComponentConfigWithPath(extendedConfig.getExtendsId(), ctx)
                            .orElse(null);
                    return extendsConfig != null
                        ? Collections.singletonList(extendsConfig)
                        : Collections.emptyList();
                });

        // Extend the configs in order from highest to lowest level.
        for (ComponentConfig extendedConfig : extendedConfigs) {
            if (extendedConfig.getExtendsId() != null
                && !ctx.extendedConfigIds.contains(extendedConfig.getId())) {
                extendConfig(extendedConfig, ctx);
                ctx.extendedConfigIds.add(extendedConfig.getId());
            }
        }
        extendConfig(config, ctx);
    }

    static void processOverrides(ConfigBuilderContext ctx) {
        Map<String, ComponentConfig> componentConfigs = ctx.componentConfigs;

        // Ensure that each component is overridden by at most 1 other component.
        IdentityHashMap<ComponentConfig, ComponentConfig> inverseOverrideMap =
            new IdentityHashMap<>();
        for (ComponentConfig config : componentConfigs.values()) {
            if (config.getOverridesId() == null) {
                continue;
            }
            ComponentConfig overriddenConfig =
                getComponentConfigWithPath(config.getOverridesId(), ctx).orElse(null);
            if (overriddenConfig == null) {
                throw new DataFlowConfigurationException(
                    "No component with id " + config.getOverridesId() + " found for override");
            }
            if (inverseOverrideMap.containsKey(overriddenConfig)) {
                throw new DataFlowConfigurationException(
                    "Component " + overriddenConfig.getId() + " has multiple overrides");
            }
            inverseOverrideMap.put(overriddenConfig, config);
        }

        for (ComponentConfig config : componentConfigs.values()) {
            if (config.getOverridesId() != null) {
                overrideConfig(config, ctx, new LinkedHashSet<>());
            }
        }
    }

    static void overrideConfig(
        ComponentConfig config, ConfigBuilderContext ctx, Set<String> overrideParents) {
        if (config != null && config.getOverridesId() != null) {
            if (overrideParents.contains(config.getId())) {
                throw new DataFlowConfigurationException(
                    "Circular chain of overrides found while attempting to override component "
                        + config.getOverridesId());
            }
            ComponentConfig overriddenConfig =
                getComponentConfigWithPath(config.getOverridesId(), ctx).orElse(null);
            if (overriddenConfig == null) {
                throw new DataFlowConfigurationException(
                    "Unable to override "
                        + config.getOverridesId()
                        + ". The component is not defined.");
            }
            if (overriddenConfig.isAbstract()) {
                throw new DataFlowConfigurationException(
                    "Can not override abstract component " + config.getOverridesId());
            }

            overriddenConfig.overrideWith(config);
            overrideParents.add(config.getId());
            if (overriddenConfig.getOverridesId() != null) {
                overrideConfig(overriddenConfig, ctx, overrideParents);
            }
        }
    }

    static void extendConfig(ComponentConfig config, ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {
        ComponentConfig extendedConfig =
            getComponentConfigWithPath(config.getExtendsId(), ctx).orElse(null);
        if (extendedConfig == null) {
            throw new DataFlowConfigurationException(
                "No component with id "
                    + config.getExtendsId()
                    + " found to extend "
                    + config.getId());
        }
        config.setExtendedConfig(extendedConfig);
    }

    static List<ComponentConfig> buildComponents(Object componentsObj, ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {
        if (!(componentsObj instanceof List)) {
            throw new DataFlowConfigurationException("Components must be a list: " + componentsObj);
        }

        List<ComponentConfig> retVal = new ArrayList<>();
        List componentsList = (List) componentsObj;
        for (Object componentObj : componentsList) {
            if (!(componentObj instanceof RawConfigurableObjectConfig)) {
                throw new DataFlowConfigurationException(
                    "Invalid component configuration. Did you forget to add a tag before the name? "
                        + componentObj);
            }

            retVal.add(buildComponentForNode(componentObj, null, ctx));
        }
        return retVal;
    }

    static ComponentConfig buildComponentForNode(Object node, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        return buildComponentForNode(node, null, parentComponentConfig, ctx);
    }

    /**
     * Builds a component for the given node in the parsed yaml config map.
     *
     * @param node                     the node to build a component for
     * @param rawType                  The rawType of the component output value or null if not known
     * @param parentComponentConfig    the parent component config or null if none
     * @param ctx                      config building context
     * @return the built component config or null if the node is a constant
     */
    static ComponentConfig buildComponentForNode(Object node, String rawType, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        ComponentConfig componentConfig;
        if (rawType != null && rawType.equalsIgnoreCase(DataFlowInstance.class.getName())) {
            componentConfig = buildDataFlowInstanceComponent(node, parentComponentConfig, ctx);
        } else if (node instanceof RawComponentConfig) {
            componentConfig = buildComponent((RawComponentConfig) node, parentComponentConfig, ctx);
        } else if (node instanceof RawConfigurableObjectConfig) {
            componentConfig = buildConfigurableObjectComponent((RawConfigurableObjectConfig) node, parentComponentConfig, ctx);
        } else if (node instanceof Map) {
            componentConfig = buildMapComponent((Map<String, Object>) node, parentComponentConfig, ctx);
        } else if (node instanceof Iterable) {
            componentConfig = buildListComponent((Iterable) node, parentComponentConfig, ctx);
        } else if (node instanceof String) {
            componentConfig = buildStringComponent((String) node, parentComponentConfig, ctx);
        } else {
            componentConfig = buildConstantComponent(node, parentComponentConfig, ctx);
        }
        return componentConfig;
    }

    /**
     * Builds the component for a @link{RawComponentConfig} node.
     */
    static ComponentConfig buildComponent(
        RawComponentConfig rawComponentConfig, ComponentConfig parentComponentConfig,
        ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {

        String type = rawComponentConfig.getType();
        if ("ProvidedValue".equalsIgnoreCase(type)) {
            return buildProvidedValueComponent(rawComponentConfig, parentComponentConfig, ctx);
        }

        DataFlowComponentMetadata componentMetadata = null;
        if(!"Component".equalsIgnoreCase(type) && !"ValueProvider".equalsIgnoreCase(type)) {
            componentMetadata = getComponentMetadata(type, ctx);
            type = componentMetadata.getTypeName();
        } else {
            type = null;
        }

        boolean internal = false;
        String id = (String) rawComponentConfig.get("id");
        if (id == null) {
            String parentIdWithPathPrefix = getParentIdWithPathIdPrefix(parentComponentConfig);
            id = ctx.idGenerator.generate(parentIdWithPathPrefix + type);
            internal = true;
        } else if (id.isBlank()) {
            throw new DataFlowConfigurationException(
                "Component id can not be blank: " + rawComponentConfig);
        }
        ComponentConfig config = getOrCreateComponentConfig(id, parentComponentConfig, ctx);
        config.setType(type);
        config.setProvidedValue(false);
        config.setInternal(internal);
        Map<String, Object> properties = new LinkedHashMap<>();
        config.setRawConfig(rawComponentConfig);
        config.setMocksId((String) rawComponentConfig.get("mocks"));
        if (config.getMocksId() != null) {
            // Mock is same as extending and overriding the mocked component.
            config.setExtendsId(config.getMocksId());
            config.setOverridesId(config.getMocksId());
        }
        for (Map.Entry<String, Object> entry : rawComponentConfig.entrySet()) {
            String key = entry.getKey();
            if (key.equalsIgnoreCase("id")) {
                // Already handled
                continue;
            } else if (key.equalsIgnoreCase("mocks")) {
                // Already handled
                continue;
            } else if (key.equalsIgnoreCase("extends")) {
                config.setExtendsId((String) entry.getValue());
            } else if (key.equalsIgnoreCase("overrides")) {
                config.setOverridesId((String) entry.getValue());
                if (config.getId() == null) {
                    config.setId(
                        ctx.idGenerator.generate(
                            "_overrides_" + config.getOverridesId().replace('.', '_')));
                }
            } else if (key.equalsIgnoreCase("input")) {
                config.setInput(
                    buildComponentInputs(entry.getValue(), componentMetadata, config, ctx));
            } else if (key.equalsIgnoreCase("inputTypes")) {
                if (!(entry.getValue() instanceof Map)) {
                    throw new DataFlowConfigurationException(
                        String.format(
                            "%s inputTypes must be a map but was %s",
                            id, entry.getValue()));
                }
                config.setInputTypes((Map<String, String>) entry.getValue());
            } else if (key.equalsIgnoreCase("outputType")) {
                if (!(entry.getValue() instanceof String)) {
                    throw new DataFlowConfigurationException(
                        String.format(
                            "%s outputType must be a string but was %s",
                            id, entry.getValue()));
                }
                config.setOutputType((String) entry.getValue());
            } else if (key.equalsIgnoreCase("failOnError")) {
                config.setFailOnError((Boolean) entry.getValue());
            } else if (key.equalsIgnoreCase("eventsEnabled")) {
                config.setEventsEnabled((Boolean) entry.getValue());
            } else if (key.equalsIgnoreCase("abstract")) {
                config.setAbstract((Boolean) entry.getValue());
            } else if (key.equalsIgnoreCase("sink")) {
                config.setSink((Boolean) entry.getValue());
            } else if (componentMetadata != null) {
                // Try to determine if this is an input or a property based on the component
                // metadata
                DataFlowConfigurableObjectPropertyMetadata propertyMetadata =
                    componentMetadata.getConfigurableObjectMetadata().getPropertyMetadata(key);
                DataFlowComponentInputMetadata inputMetadata = getInputMetadata(componentMetadata, key);
                if (propertyMetadata != null) {
                    // Assume the entry is a property if property metadata exists
                    String rawType = propertyMetadata.getRawType();
                    if(isConstant(entry.getValue())) {
                        properties.put(key, entry.getValue());
                    } else {
                        ComponentConfig propertyComponent =
                            buildComponentForNode(entry.getValue(), rawType, parentComponentConfig, ctx);
                        properties.put(key, new ValueReference(propertyComponent.getId()));
                        config.addPropertyValueProvider(propertyComponent);
                    }
                } else {
                    String rawType = inputMetadata != null ? inputMetadata.getRawType() : null;
                    ComponentConfig inputComponent = buildComponentForNode(
                        entry.getValue(), rawType, parentComponentConfig, ctx);
                    config.setInput(key, inputComponent);
                }
            } else {
                // There is no component metadata. Assume all entries are properties.
                if(!isConstant(entry.getValue())) {
                    ComponentConfig propertyComponent = buildComponentForNode(entry.getValue(), null, parentComponentConfig, ctx);
                    properties.put(key, new ValueReference(propertyComponent.getId()));
                    config.addPropertyValueProvider(propertyComponent);
                } else {
                    properties.put(key, entry.getValue());
                }
            }
        }
        config.setProperties(properties);
        return config;
    }

    static DataFlowComponentMetadata getComponentMetadata(String type, ConfigBuilderContext ctx) {
        DataFlowComponentMetadata metadata = null;
        try {
            metadata = ctx.registry.getComponentRegistry().getComponentMetadata(type);
        } catch (Exception ex) {
            if (!type.endsWith("ValueProvider") && !type.endsWith("Action")) {
                // Try appending ValueProvider to the type name.
                try {
                    metadata =
                        ctx.registry
                            .getComponentRegistry()
                            .getComponentMetadata(type + "ValueProvider");
                } catch (Exception ex2) {
                    try {
                        // Try appending Action to the type name.
                        metadata =
                            ctx.registry
                                .getComponentRegistry()
                                .getComponentMetadata(type + "Action");
                    } catch (Exception ex3) {
                        throw ex;
                    }
                }
            }
        }
        return metadata;
    }

    static DataFlowComponentInputMetadata getInputMetadata(DataFlowComponentMetadata componentMetadata, String name) {
        if(componentMetadata == null) {
            return null;
        }
        return componentMetadata.getInputMetadata().stream()
            .filter(in -> in.getName().equalsIgnoreCase(name))
            .findAny()
            .orElse(null);
    }

    static ComponentConfig buildProvidedValueComponent(RawComponentConfig rawComponentConfig, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        String id = (String) rawComponentConfig.get("id");
        if (id == null || id.isBlank()) {
            throw new DataFlowConfigurationException("Provided values must have an id");
        }
        ComponentConfig config = getOrCreateComponentConfig(id, parentComponentConfig, ctx);
        config.setType(null);
        config.setProvidedValue(true);
        config.setInternal(false);
        config.setOutputType((String) rawComponentConfig.get("outputType"));
        if(config.getOutputType() == null) {
            config.setOutputType((String) rawComponentConfig.get("type"));
        }
        config.setRawConfig(rawComponentConfig);
        return config;
    }

    /**
     * Builds the inputs map for a component.
     */
    static Map<String, ComponentConfig> buildComponentInputs(
        Object inputObj,
        DataFlowComponentMetadata componentMetadata,
        ComponentConfig parentComponentConfig,
        ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {
        if (!(inputObj instanceof Map)) {
            throw new DataFlowConfigurationException("Input must be a map");
        }

        Map<String, ComponentConfig> retVal = new LinkedHashMap<>();
        Map<String, Object> inputMap = (Map<String, Object>) inputObj;
        for (Map.Entry<String, Object> input : inputMap.entrySet()) {
            String inputName = input.getKey();
            DataFlowComponentInputMetadata inputMetadata = getInputMetadata(componentMetadata, inputName);
            String rawType = inputMetadata != null ? inputMetadata.getRawType() : null;
            retVal.put(
                inputName, buildComponentForNode(input.getValue(), rawType, parentComponentConfig, ctx));
        }
        return retVal;
    }


    static ComponentConfig buildConfigurableObjectComponent(
        final RawConfigurableObjectConfig rawConfigurableObjectConfig,
        final ComponentConfig parentComponentConfig,
        final ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {
        String parentIdWithPath = getParentIdWithPathIdPrefix(parentComponentConfig);
        String newId = (String) rawConfigurableObjectConfig.get("id");
        if (newId == null) {
            String idPrefix = parentIdWithPath + rawConfigurableObjectConfig.getType();
            newId = ctx.idGenerator.generate(idPrefix);
        }
        ComponentConfig config = getOrCreateComponentConfig(newId, parentComponentConfig, ctx);
        config.setType(OBJECT_VALUE_PROVIDER);
        config.setInternal(true);
        config.setProperties(Collections.singletonMap("value", rawConfigurableObjectConfig));
        config.setRawConfig(Collections.singletonMap("value", rawConfigurableObjectConfig));

        rawConfigurableObjectConfig.entrySet().forEach(entry -> {
            if(!isConstant(entry.getValue())) {
                ComponentConfig entryConfig = buildComponentForNode(entry.getValue(), config, ctx);
                entry.setValue(new ValueReference(entryConfig.getId()));
                config.addPropertyValueProvider(entryConfig);
            }
        });
        return config;
    }

    static ComponentConfig buildDataFlowInstanceComponent(
        Object inputObj,
        ComponentConfig parentComponentConfig,
        ConfigBuilderContext ctx) {
        String dataFlowId;
        Set<String> providedValueIds = new LinkedHashSet<>();
        String itemValueName = DEFAULT_ITEM_VALUE_NAME;
        if (inputObj instanceof String) {
            // The value is an id reference.
            dataFlowId = (String) inputObj;
        } else if (inputObj instanceof Map || inputObj instanceof List) {
            // The value is a child DataFlow.
            Map<String, Object> rawChildConfig = new LinkedHashMap<>();
            if (inputObj instanceof Map) {
                rawChildConfig = (Map<String, Object>) inputObj;
            } else {
                List components = (List) inputObj;
                rawChildConfig.put(PIPELINE_SECTION_NAME_ALIASES[0], components);
            }
            dataFlowId = (String) rawChildConfig.get("id");
            String parentId = ctx.id;
            if (ctx.parentId != null) {
                // Start building children's parentId from current ctx
                parentId = ctx.parentId + "_" + parentId;
            }
            if (dataFlowId == null) {
                dataFlowId = parentId + ctx.idGenerator.generate("_childFlow");
            } else if (dataFlowId.isBlank()) {
                throw new DataFlowConfigurationException(
                    "Dataflow id can not be blank: " + rawChildConfig);
            } else {
                dataFlowId = parentId + "_" + dataFlowId;
            }
            Set<String> keys =
                rawChildConfig.keySet().stream()
                    .map(String::toLowerCase)
                    .collect(Collectors.toSet());
            if (Arrays.stream(PIPELINE_SECTION_NAME_ALIASES).anyMatch(keys::contains)) {
                rawChildConfig.put("id", dataFlowId);
            }
            ConfigBuilderContext childCtx =
                new ConfigBuilderContext(
                    parentId, ctx.registry, ctx.importResourceProvider);
            build(rawChildConfig, childCtx);
            childCtx.id = dataFlowId;

            ctx.childDataFlowContexts.put(childCtx.id, childCtx);
            providedValueIds.addAll(childCtx.getProvidedValueIds());
        } else {
            throw new DataFlowConfigurationException(
                "Invalid child data flow definition: "
                    + inputObj
                    + " for component " + parentComponentConfig.getId());
        }
        ComponentConfig componentConfig =
            getOrCreateComponentConfig(
                ctx.idGenerator.generate("_dataFlow" + dataFlowId + "_component"),
                parentComponentConfig,
                ctx);
        componentConfig.setType(DATAFLOW_INSTANCE_VALUE_PROVIDER);
        componentConfig.setInternal(true);
        componentConfig.setProperties(Collections.singletonMap("dataFlowId", dataFlowId));
        providedValueIds.stream()
            .filter(id -> !id.equalsIgnoreCase(itemValueName))
            .map(id -> buildValueReferenceComponent(id, parentComponentConfig, ctx))
            .filter(valRef -> valRef.getType() == null)
            .forEach(valRef -> valRef.setSink(true));
        return componentConfig;
    }

    static ComponentConfig buildListComponent(Iterable list, ComponentConfig parentComponentConfig,
        ConfigBuilderContext ctx) {
        String parentIdWithPathPrefix = getParentIdWithPathIdPrefix(parentComponentConfig);
        ComponentConfig config =
            getOrCreateComponentConfig(
                ctx.idGenerator.generate(parentIdWithPathPrefix + "list"),
                parentComponentConfig,
                ctx);
        config.setType(LIST_VALUE_PROVIDER);
        config.setInternal(true);
        int idx = 0;
        Map<String, Object> properties = new LinkedHashMap<>();
        List<String> itemKeys = new ArrayList<>();
        Map<String, ComponentConfig> inputs = new LinkedHashMap<>();
        for (Object listItem : list) {
            String itemKey = String.valueOf(idx++);
            ComponentConfig itemConfig = buildComponentForNode(listItem, parentComponentConfig, ctx);
            inputs.put(itemKey, itemConfig);
            itemKeys.add(itemKey);
        }
        config.setInput(inputs);
        properties.put("itemKeys", itemKeys);
        config.setProperties(properties);
        return config;
    }
    static ComponentConfig buildMapComponent(Map<String, Object> map, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        String parentIdWithPathPrefix = getParentIdWithPathIdPrefix(parentComponentConfig);
        ComponentConfig config =
            getOrCreateComponentConfig(
                ctx.idGenerator.generate(parentIdWithPathPrefix + "map"),
                parentComponentConfig,
                ctx);
        config.setType(MAP_VALUE_PROVIDER);
        config.setInternal(true);

        Map<String, ComponentConfig> inputs = new LinkedHashMap<>();
        map.forEach((key, value) -> {
            ComponentConfig entryConfig = buildComponentForNode(value, parentComponentConfig, ctx);
            inputs.put(key, entryConfig);
        });
        config.setInput(inputs);
        return config;
    }

    static ComponentConfig buildStringComponent(String str, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        Matcher matcher = VALUE_REFERENCE.matcher(str);
        if (matcher.matches()) {
            return buildValueReferenceComponent(matcher.group(1), parentComponentConfig, ctx);
        } else if (matcher.find(0)) {
            return buildPlaceholderStringComponent(str, parentComponentConfig, ctx);
        } else {
            return buildConstantComponent(str, parentComponentConfig, ctx);
        }
    }

    static ComponentConfig buildValueReferenceComponent(
        String reference, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx)
        throws DataFlowConfigurationException {
        if (reference.contains(".")) {
            return buildMapEntryComponent(reference, parentComponentConfig, ctx);
        } else if (reference.contains("[")) {
            // TODO(thorntonv): Handle list element access in value reference.
            throw new RuntimeException("List element access not implemented");
        } else {
            return getOrCreateComponentConfig(reference, null, ctx);
        }
    }

    static ComponentConfig buildPlaceholderStringComponent(
        String str, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        Optional<ComponentConfig> existingConfig =
            ctx.componentConfigs.values().stream()
                .filter(v -> PLACEHOLDER_STRING_VALUE_PROVIDER.equals(v.getType()))
                .filter(v -> str.equals(v.getProperties().get("placeholderString")))
                .findAny();
        if (existingConfig.isPresent()) {
            return existingConfig.get();
        }
        String parentIdWithPathPrefix = getParentIdWithPathIdPrefix(parentComponentConfig);
        ComponentConfig placeholderStringConfig =
            getOrCreateComponentConfig(
                ctx.idGenerator.generate(
                    parentIdWithPathPrefix + PLACEHOLDER_STRING_ID_PREFIX),
                parentComponentConfig,
                ctx);
        placeholderStringConfig.setType(PLACEHOLDER_STRING_VALUE_PROVIDER);
        placeholderStringConfig.setInternal(true);
        Map<String, Object> properties = new LinkedHashMap<>();
        properties.put("placeholderString", str);
        placeholderStringConfig.setProperties(properties);
        PlaceholderString placeholderString = new PlaceholderString(str);

        Map<String, ComponentConfig> input =
            new HashSet<>(placeholderString.getPlaceholderNames())
                .stream()
                .map(id -> getOrCreateComponentConfig(id, null, ctx))
                .collect(Collectors.toMap(ComponentConfig::getId, v -> v));
        placeholderStringConfig.setInput(input);
        placeholderStringConfig.setRawConfig(properties);
        // Don't try to resolve property value references on the placeholder string property.
        ctx.propValueRefDisabledComponentIds.add(placeholderStringConfig.getId());
        return placeholderStringConfig;
    }

    static ComponentConfig buildMapEntryComponent(
        String reference, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        ComponentConfig mapEntryValueProvider =
            getOrCreateComponentConfig(
                ctx.idGenerator.generate("mapEntry"), parentComponentConfig, ctx);
        mapEntryValueProvider.setInternal(true);
        mapEntryValueProvider.setType(MAP_ENTRY_VALUE_PROVIDER);
        int dotIdx = reference.indexOf('.');
        String valueId = reference.substring(0, dotIdx);
        String key = reference.substring(dotIdx + 1);
        Map<String, Object> properties = new LinkedHashMap<>();
        properties.put("key", key);
        mapEntryValueProvider.setProperties(properties);
        Map<String, ComponentConfig> inputs = new LinkedHashMap<>();
        inputs.put("map", getOrCreateComponentConfig(valueId, parentComponentConfig, ctx));
        mapEntryValueProvider.setInput(inputs);
        return mapEntryValueProvider;
    }

    static ComponentConfig buildConstantComponent(Object value, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        String parentIdWithPath = getParentIdWithPathIdPrefix(parentComponentConfig);
        String idPrefix = parentIdWithPath + "const";
        ComponentConfig config = getOrCreateComponentConfig(ctx.idGenerator.generate(idPrefix), parentComponentConfig, ctx);
        config.setType(CONST_VALUE_PROVIDER);
        config.setInternal(true);
        config.setProperties(Collections.singletonMap("value", value));
        config.setRawConfig(Collections.singletonMap("value", value));
        return config;
    }

    static boolean isConstant(Object obj) {
        if (obj instanceof RawConfigurableObjectConfig) {
            return false;
        } else if (obj instanceof Map) {
            Map map = (Map) obj;
            for (Object value : map.values()) {
                if (!isConstant(value)) {
                    return false;
                }
            }
        } else if(obj instanceof Iterable) {
            Iterable iterable = (Iterable) obj;
            for(Object value : iterable) {
                if(!isConstant(value)) {
                    return false;
                }
            }
        } else if(obj instanceof String) {
            String str = (String) obj;
            Matcher matcher = VALUE_REFERENCE.matcher(str);
            if (matcher.matches()) {
                // Value Reference
                return false;
            } else if (matcher.find(0)) {
                // Placeholder string
                return false;
            }
        }
        return true;
    }

    static String getParentIdWithPathIdPrefix(ComponentConfig parentComponentConfig) {
        return parentComponentConfig != null
                ? parentComponentConfig.getIdWithPath().replace(".", "_").replaceAll("_+", "_")
                        + "_"
                : "";
    }

    static ComponentConfig getOrCreateComponentConfig(
            String id, ComponentConfig parentComponentConfig, ConfigBuilderContext ctx) {
        ComponentConfig config = ctx.componentConfigs.get(id);
        if (config == null) {
            config = new ComponentConfig();
            config.setId(id);
            ctx.componentConfigs.put(id, config);
            config.setParentComponentConfig(parentComponentConfig);
        } else if ((parentComponentConfig != null && config.getParentComponentConfig() != null)
                && config.getParentComponentConfig() != parentComponentConfig) {
            throw new RuntimeException(
                    String.format(
                            "Component %s already has parent %s but %s is requested",
                            id,
                            config.getParentComponentConfig() != null
                                    ? config.getParentComponentConfig().getId()
                                    : "null",
                            parentComponentConfig.getId()));
        }
        if (parentComponentConfig != null) {
            config.setParentComponentConfig(parentComponentConfig);
        }
        return config;
    }

    static Optional<ComponentConfig> getComponentConfigWithPath(
            String path, ConfigBuilderContext ctx) {
        if (path == null) {
            return Optional.empty();
        }
        String[] pathElements = path.split("\\.");
        if (pathElements.length == 1) {
            return Optional.ofNullable(ctx.componentConfigs.get(pathElements[0]));
        }
        ConfigBuilderContext root = ctx.childDataFlowContexts.get(ctx.id + "_" + pathElements[0]);
        return getComponentConfigWithPath(
                Arrays.copyOfRange(pathElements, 1, pathElements.length), root);
    }

    static Optional<ComponentConfig> getComponentConfigWithPath(
            String[] pathElements, ConfigBuilderContext root) {
        if (root == null || pathElements == null || pathElements.length == 0) {
            return Optional.empty();
        }
        ConfigBuilderContext ctx = root;
        for (int idx = 0; idx < pathElements.length - 1; idx++) {
            ctx = ctx.childDataFlowContexts.get(root.id + "_" + pathElements[idx]);
            if (ctx == null) {
                return Optional.empty();
            }
        }
        return Optional.ofNullable(ctx.componentConfigs.get(pathElements[pathElements.length - 1]));
    }
}
