/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowEnvironment.java
 */
package dataflow.core.environment;

import static dataflow.core.codegen.DataFlowInstanceCodeGenerator.getDefaultClassName;

import dataflow.codegen.util.DataFlowVersion;
import dataflow.core.codegen.DataFlowInstanceCodeGenConfig;
import dataflow.core.codegen.DataFlowInstanceCodeGenerator;
import dataflow.core.compiler.CompileException;
import dataflow.core.compiler.JavaCompilerUtil;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.DataFlowInstanceFactory;
import dataflow.core.engine.DependencyInjector;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.registry.DataFlowRegistry;
import io.micrometer.core.instrument.util.IOUtils;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Contains the environment used for executing a dataflow. */
@SuppressWarnings({"unused"})
public class DataFlowEnvironment implements AutoCloseable {

    public static final String DATAFLOW_PACKAGE = "dataflow.core.environment";

    // Update this version when an incompatible change is made to the dataflow code generation
    // or execution code.
    public static final int VERSION = DataFlowVersion.ENVIRONMENT_VERSION;

    private static final Logger logger =
            LoggerFactory.getLogger(DataFlowEnvironment.class.getSimpleName());

    private final String id;
    private final DataFlowRegistry registry;
    private final long maxExecutionTimeMillis;
    private final DependencyInjector dependencyInjector;
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1, r -> new Thread(r, "ScheduledTask"));

    private final List<DataFlowEventHandler> dataFlowEventHandlers =
            Collections.synchronizedList(new ArrayList<>());

    private static final class DefaultDependencyInjector extends SimpleDependencyInjector {

        public DefaultDependencyInjector() {
            super();
        }
    }

    @DataFlowConfigurable
    public DataFlowEnvironment(
            @DataFlowConfigProperty String id,
            @DataFlowConfigProperty(required = false) DependencyInjector dependencyInjector,
            @DataFlowConfigProperty(required = false) Long maxExecutionTimeMillis) {
        this.id = id;
        this.dependencyInjector =
                dependencyInjector != null ? dependencyInjector : new DefaultDependencyInjector();
        this.maxExecutionTimeMillis =
                maxExecutionTimeMillis != null ? maxExecutionTimeMillis : TimeUnit.DAYS.toMillis(1);

        this.registry = new DataFlowRegistry();
    }

    public String getId() {
        return id;
    }

    public DataFlowRegistry getRegistry() {
        return registry;
    }

    public DependencyInjector getDependencyInjector() {
        return dependencyInjector;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    /**
     * @param dataFlowConfigYamlIn Input stream from which the dataflow configuration yaml can be
     *     read.
     * @param id Optional id to use for the new dataflow. Will use the id from the config yaml if
     *     this is null
     */
    public void registerDataFlow(InputStream dataFlowConfigYamlIn, String id)
            throws DataFlowParseException, CompileException {
        String rawYaml = IOUtils.toString(dataFlowConfigYamlIn);
        DataFlowParser parser = new DataFlowParser(getRegistry());
        DataFlowConfig dataFlowConfig =
                parser.parse(new ByteArrayInputStream(rawYaml.getBytes(Charset.defaultCharset())));
        if (id != null) {
            dataFlowConfig.setId(id);
        }
        String className = getDefaultClassName(dataFlowConfig);
        DataFlowInstanceCodeGenerator codeGenerator =
                new DataFlowInstanceCodeGenerator(getRegistry());
        DataFlowInstanceCodeGenConfig codeGenConfig =
                new DataFlowInstanceCodeGenConfig(
                        DATAFLOW_PACKAGE,
                        className,
                        dataFlowConfig.isIncrementalMode(),
                        dataFlowConfig.isEventsEnabled(),
                        true);
        String source = codeGenerator.generateSource(dataFlowConfig, rawYaml, codeGenConfig);
        Class<DataFlowInstance> instanceClass =
                JavaCompilerUtil.compile(
                        codeGenConfig.getPackageName() + "." + codeGenConfig.getClassName(),
                        source,
                        this.getClass().getClassLoader());

        registerDataFlow(instanceClass, dataFlowConfig);
    }

    public void registerDataFlow(Class<? extends DataFlowInstance> dataFlowClass) {
        registerDataFlow(dataFlowClass, loadDataFlowConfig(dataFlowClass));
    }

    private void registerDataFlow(
            Class<? extends DataFlowInstance> dataFlowClass, DataFlowConfig dataFlowConfig) {
        // Check environment version.
        try {
            int envVersion =
                    (Integer) dataFlowClass.getDeclaredField("ENVIRONMENT_VERSION").get(null);
            if (envVersion != VERSION) {
                throw new RuntimeException(
                        "Unable to register dataflow "
                                + dataFlowClass.getSimpleName()
                                + ": The dataflow has an incompatible environment version "
                                + envVersion
                                + ", expected "
                                + VERSION);
            }
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(
                    "Unable to register dataflow "
                            + dataFlowClass.getSimpleName()
                            + ": The dataflow has an incompatible environment version, expected "
                            + VERSION);
        }

        // Register child dataflows
        for (DataFlowConfig childConfig : dataFlowConfig.getChildDataFlows().values()) {
            String childClassName = getDefaultClassName(childConfig);

            try {
                registerDataFlow(
                        (Class<? extends DataFlowInstance>)
                                Class.forName(
                                        dataFlowClass.getPackageName() + "." + childClassName),
                        childConfig);

                logger.info(
                        "Registered child dataflow "
                                + childConfig.getId()
                                + " of "
                                + dataFlowConfig.getId());
            } catch (Throwable e) {
                throw new RuntimeException(
                        "Child dataflow class "
                                + childClassName
                                + " was not found for dataflow "
                                + dataFlowConfig.getId(),
                        e);
            }
        }

        getRegistry()
                .registerDataFlow(
                        dataFlowConfig, createInstanceFactory(dataFlowClass, dataFlowConfig));
        logger.info("Registered dataflow " + dataFlowClass.getName());
    }

    private DataFlowConfig loadDataFlowConfig(Class<? extends DataFlowInstance> dataFlowClass) {
        try {
            byte[] yamlConfig =
                    Base64.getDecoder()
                            .decode(
                                    (String)
                                            dataFlowClass
                                                    .getDeclaredField("YAML_CONFIG")
                                                    .get(null));
            Map<String, String> importConfigsBase64 =
                    (Map<String, String>)
                            dataFlowClass.getDeclaredField("IMPORT_CONFIGS").get(null);
            DataFlowParser parser =
                    new DataFlowParser(
                            getRegistry(),
                            resourceName -> {
                                String importResourceBase64 = importConfigsBase64.get(resourceName);
                                if (importResourceBase64 == null) {
                                    return null;
                                } else {
                                    return new ByteArrayInputStream(
                                            Base64.getDecoder().decode(importResourceBase64));
                                }
                            });
            return parser.parse(new ByteArrayInputStream(yamlConfig));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(
                    "Invalid dataflow class: "
                            + dataFlowClass.getName()
                            + ", Unable to find config yaml in class",
                    e);
        } catch (DataFlowParseException e) {
            throw new RuntimeException(
                    "Error while parsing config for dataflow class " + dataFlowClass.getName(), e);
        }
    }

    private DataFlowInstanceFactory createInstanceFactory(
            Class<? extends DataFlowInstance> dataFlowClass, final DataFlowConfig dataFlowConfig) {
        try {
            final Constructor<? extends DataFlowInstance> constructor =
                    dataFlowClass.getDeclaredConstructor(
                            DataFlowEnvironment.class,
                            Executor.class,
                            DataFlowConfig.class,
                            Consumer.class);
            constructor.setAccessible(true);
            return initFn ->
                    constructor.newInstance(
                            DataFlowEnvironment.this, getExecutor(), dataFlowConfig, initFn);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(
                    "Invalid dataflow class: "
                            + dataFlowClass.getName()
                            + ", Class does not have a constructor with the expected signature.",
                    e);
        }
    }

    public void registerEventHandler(DataFlowEventHandler handler) {
        dataFlowEventHandlers.add(handler);
    }

    public List<DataFlowEventHandler> getEventHandlers() {
        return new ArrayList<>(dataFlowEventHandlers);
    }

    public void unregisterEventHandler(DataFlowEventHandler handler) {
        dataFlowEventHandlers.remove(handler);
    }

    public void fireDataFlowExecutionStartedEvent(DataFlowInstance instance) {
        dataFlowEventHandlers.forEach(handler -> handler.onDataFlowExecutionStarted(instance));
    }

    public void fireDataFlowExecutionFinishedEvent(DataFlowInstance instance) {
        dataFlowEventHandlers.forEach(handler -> handler.onDataFlowExecutionFinished(instance));
    }

    public void fireDataFlowExecutionErrorEvent(DataFlowInstance instance, Throwable error) {
        dataFlowEventHandlers.forEach(handler -> handler.onDataFlowExecutionError(instance, error));
    }

    public void fireComponentExecutionStartedEvent(
            DataFlowInstance instance, String componentId, long startTimeMillis) {
        dataFlowEventHandlers.forEach(
                handler -> handler.onComponentExecutionStarted(instance, componentId));
    }

    public void fireComponentExecutionFinishedEvent(
            DataFlowInstance instance, String componentId, long startTimeMillis) {
        dataFlowEventHandlers.forEach(
                handler -> handler.onComponentExecutionFinished(instance, componentId));
    }

    public void fireComponentExecutionErrorEvent(
            DataFlowInstance instance, String componentId, long startTimeMillis, Throwable error) {
        dataFlowEventHandlers.forEach(
                handler -> handler.onComponentExecutionError(instance, componentId, error));
    }

    public Future<?> executeAsync(Runnable r, String taskName) {
        final DataFlowExecutionContext executionContext =
                DataFlowExecutionContext.getCurrentExecutionContext();
        return executor.submit(
                () -> {
                    Thread.currentThread().setName(taskName);
                    try {
                        DataFlowExecutionContext.setCurrentExecutionContext(executionContext);
                        r.run();
                    } finally {
                        Thread.currentThread().setName(DataFlowEnvironment.class.getSimpleName());
                        DataFlowExecutionContext.setCurrentExecutionContext(null);
                    }
                });
    }

    public ScheduledFuture<?> scheduleAtFixedRate(
            Runnable r, long initialDelay, long period, TimeUnit unit, String threadName) {
        return scheduler.scheduleAtFixedRate(
                () -> {
                    Thread.currentThread().setName(threadName);
                    try {
                        r.run();
                    } finally {
                        Thread.currentThread()
                                .setName(
                                        DataFlowEnvironment.class.getSimpleName()
                                                + "ScheduledTask");
                    }
                },
                initialDelay,
                period,
                unit);
    }

    public long getMaxExecutionTimeMillis() {
        return maxExecutionTimeMillis;
    }

    @Override
    public void close() throws Exception {
        executor.shutdown();
        if (!executor.awaitTermination(1, TimeUnit.MINUTES)) {
            logger.warn("Failed to shutdown the dataflow environment thread pool");
        }
    }
}
