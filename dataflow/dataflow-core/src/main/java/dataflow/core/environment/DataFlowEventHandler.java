/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowEventHandler.java
 */
package dataflow.core.environment;

import dataflow.core.engine.DataFlowInstance;

public interface DataFlowEventHandler {

    void onDataFlowExecutionStarted(DataFlowInstance instance);

    void onDataFlowExecutionFinished(DataFlowInstance instance);

    void onDataFlowExecutionError(DataFlowInstance instance, Throwable error);

    void onComponentExecutionStarted(DataFlowInstance instance, String componentId);

    void onComponentExecutionFinished(DataFlowInstance instance, String componentId);

    void onComponentExecutionError(DataFlowInstance instance, String componentId, Throwable error);
}
