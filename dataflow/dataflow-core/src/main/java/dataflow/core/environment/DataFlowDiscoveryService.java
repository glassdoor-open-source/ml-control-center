/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowDiscoveryService.java
 */
package dataflow.core.environment;

import static dataflow.core.environment.DataFlowEnvironment.DATAFLOW_PACKAGE;

import dataflow.core.codegen.ComponentCodeGenerator;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.component.metadata.DataFlowConfigurableObjectMetadata;
import dataflow.core.config.DataFlowConfigurableObjectBuilder;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.type.ValueTypeConverter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataFlowDiscoveryService {

    private static final Logger logger = LoggerFactory.getLogger(DataFlowDiscoveryService.class);

    public void register(DataflowDiscoveryConfig discoveryConfig, DataFlowEnvironment env) {
        registerComponents(discoveryConfig, env);
        registerDataFlows(discoveryConfig, env);
    }

    public void registerDataFlows(
            DataflowDiscoveryConfig discoveryConfig, DataFlowEnvironment env) {
        Reflections reflections = createReflections();
        reflections.getSubTypesOf(DataFlowInstance.class).stream()
                .filter(Objects::nonNull)
                .filter(c -> c.getPackageName().equals(DATAFLOW_PACKAGE))
                .filter(
                        c -> {
                            try {
                                // Only register top level parent dataflows.
                                String parentId =
                                        (String) c.getDeclaredField("PARENT_ID").get(null);
                                return parentId == null;
                            } catch (IllegalAccessException | NoSuchFieldException e) {
                                logger.warn("Unable to register dataflow " + c.getName(), e);
                                return false;
                            }
                        })
                .toList()
                .forEach(env::registerDataFlow);
    }

    private Reflections createReflections() {
        Set<URL> urls = new HashSet<>();
        urls.addAll(ClasspathHelper.forJavaClassPath());
        urls.addAll(
                ClasspathHelper.forClassLoader(
                        ClassLoader.getSystemClassLoader(), getClass().getClassLoader()));
        return new Reflections(
                new ConfigurationBuilder().setScanners(new SubTypesScanner()).setUrls(urls));
    }

    public void registerComponents(
            DataflowDiscoveryConfig discoveryConfig, DataFlowEnvironment env) {
        final DataFlowRegistry registry = env.getRegistry();
        DataFlowExecutionContext.createExecutionContext(env);
        Reflections reflections = createReflections();
        reflections.getSubTypesOf(DataFlowComponentMetadata.class).stream()
                .map(
                        metadataClass -> {
                            try {
                                return (DataFlowComponentMetadata)
                                        metadataClass.getField("INSTANCE").get(null);
                            } catch (IllegalAccessException | NoSuchFieldException e) {
                                return null;
                            }
                        })
                .filter(Objects::nonNull)
                .filter(metadata -> matches(metadata, discoveryConfig))
                .forEach(
                        metadata -> {
                            try {
                                Class<?> builderClass = Class.forName(metadata.getBuilderClass());
                                DataFlowConfigurableObjectBuilder builder =
                                        (DataFlowConfigurableObjectBuilder)
                                                builderClass
                                                        .getConstructor(DataFlowEnvironment.class)
                                                        .newInstance(new Object[] {env});
                                ComponentCodeGenerator codeGenerator = null;
                                if (metadata.getCodeGeneratorClass() != null) {
                                    Class<?> codeGeneratorClass =
                                            Class.forName(metadata.getCodeGeneratorClass());
                                    codeGenerator =
                                            (ComponentCodeGenerator)
                                                    codeGeneratorClass
                                                            .getConstructor()
                                                            .newInstance();
                                }
                                registry.getComponentRegistry()
                                        .registerComponent(metadata, builder, codeGenerator);
                                logger.debug(
                                        "Registered component " + metadata.getComponentClassName());
                            } catch (Throwable t) {
                                throw new RuntimeException(metadata.toString(), t);
                            }
                        });
        reflections.getSubTypesOf(DataFlowConfigurableObjectBuilder.class).stream()
                .map(
                        builderClass -> {
                            try {
                                return builderClass
                                        .getConstructor(DataFlowEnvironment.class)
                                        .newInstance(env);
                            } catch (InvocationTargetException
                                    | InstantiationException
                                    | NoSuchMethodException
                                    | IllegalAccessException e) {
                                throw new RuntimeException(e);
                            }
                        })
                .forEach(
                        builder -> {
                            try {
                                DataFlowConfigurableObjectMetadata metadata =
                                        (DataFlowConfigurableObjectMetadata)
                                                builder.getClass().getField("METADATA").get(null);
                                registry.getComponentRegistry()
                                        .registerPropertyObjectBuilder(
                                                Class.forName(metadata.getClassName()), builder);
                            } catch (Throwable t) {
                                throw new RuntimeException(t);
                            }
                        });

        reflections.getSubTypesOf(ValueTypeConverter.class).stream()
                .filter(c -> !Modifier.isAbstract(c.getModifiers()))
                .forEach(
                        converterClass -> {
                            Constructor<?> constructor =
                                    Arrays.stream(converterClass.getConstructors())
                                            .filter(c -> c.getParameterCount() == 0)
                                            .findAny()
                                            .orElse(null);
                            if (constructor == null) {
                                throw new RuntimeException(
                                        "Converter "
                                                + converterClass.getSimpleName()
                                                + " must have a public no argument constructor");
                            }
                            try {
                                ValueTypeConverter<?, ?> converter =
                                        (ValueTypeConverter<?, ?>)
                                                constructor.newInstance(
                                                        new Object
                                                                [constructor.getParameterCount()]);
                                registry.getTypeRegistry().register(converter);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
    }

    private boolean matches(
            DataFlowComponentMetadata metadata, DataflowDiscoveryConfig componentDiscoveryConfig) {
        String className = metadata.getClass().getName();
        if (componentDiscoveryConfig.getExcludeComponentClasses().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(className).matches())) {
            return false;
        }
        if (componentDiscoveryConfig.getIncludeComponentClasses().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(className).matches())) {
            return true;
        }
        String packageName = metadata.getClass().getPackageName();
        if (componentDiscoveryConfig.getExcludeComponentPackages().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(packageName).matches())) {
            return false;
        }
        return componentDiscoveryConfig.getComponentPackages().stream()
                .map(Pattern::compile)
                .anyMatch(pattern -> pattern.matcher(packageName).matches());
    }
}
