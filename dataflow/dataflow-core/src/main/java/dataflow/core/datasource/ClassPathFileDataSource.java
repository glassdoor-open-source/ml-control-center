/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ClassPathFileDataSource.java
 */
package dataflow.core.datasource;

import static dataflow.core.datasource.DataSourceUtil.getDataSourcesMatchingGlobPattern;
import static dataflow.core.datasource.DataSourceUtil.normalizePathSeparator;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.jar.JarEntry;
import java.util.stream.Collectors;

/** A {@link DataSource} for a single file on the classpath. */
public class ClassPathFileDataSource implements DataSource {

    /**
     * Returns the DataSources on the classpath that match the given glob pattern. See
     * getDataSourcesMatchingGlobPattern(DataSource, String) for more details on the glob pattern.
     */
    public static List<DataSource> getClassPathDataSourcesMatchingGlobPattern(String globPattern) {
        List<DataSource> retVal = new ArrayList<>();
        for (DataSource root : getClassPathRootDataSources()) {
            try {
                retVal.addAll(getDataSourcesMatchingGlobPattern(root, globPattern));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return retVal;
    }

    /**
     * A {@link DataSource} that wraps classpath {@link DataSource}s which can either be {@link
     * LocalFileDataSource}s of file system directories on the classpath or {@link
     * ZipFileDataSource}s of zip files on the classpath.
     */
    static class ClassPathDataSourceWrapper implements DataSource {

        private final DataSource delegate;
        private final String basePath;

        ClassPathDataSourceWrapper(DataSource delegate, String basePath) {
            this.delegate = delegate;
            this.basePath = basePath;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return delegate.getInputStream();
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return delegate.getOutputStream();
        }

        @Override
        public List<DataSource> getChildren() throws IOException {
            return delegate.getChildren().stream()
                    .map(child -> new ClassPathDataSourceWrapper(child, basePath))
                    .collect(Collectors.toList());
        }

        @Override
        public DataSource getParent() throws IOException {
            DataSource parent = delegate.getParent();
            return parent != null ? new ClassPathDataSourceWrapper(parent, basePath) : null;
        }

        @Override
        public boolean hasChanged() throws IOException {
            return delegate.hasChanged();
        }

        @Override
        public boolean exists() throws IOException {
            return delegate.exists();
        }

        @Override
        public void delete() throws IOException {
            delegate.delete();
        }

        @Override
        public String getName() {
            return delegate.getName();
        }

        @Override
        public String getPath() {
            String path = delegate.getPath();
            if (basePath != null && path.startsWith(basePath)) {
                // Remove the base path from the returned path.
                path = path.substring(basePath.length());
                if (!path.startsWith(DataSource.PATH_SEPARATOR)) {
                    path = DataSource.PATH_SEPARATOR + path;
                }
            }
            return path;
        }

        @Override
        public List<DataSource> getRoots() {
            return getClassPathRootDataSources();
        }

        @Override
        public DataSource createChild(String name, boolean isDirectory) throws IOException {
            return delegate.createChild(name, isDirectory);
        }

        @Override
        public boolean equals(final Object o) {
            return delegate.equals(o);
        }

        @Override
        public int hashCode() {
            return delegate.hashCode();
        }

        public String toString() {
            return delegate.toString();
        }
    }

    public static List<DataSource> getClassPathRootDataSources() {
        final ArrayList<DataSource> retVal = new ArrayList<>();
        final String classPath = System.getProperty("java.class.path", ".");
        final String[] classPathElements = classPath.split(System.getProperty("path.separator"));
        for (final String element : classPathElements) {
            final File file = new File(element);
            if (!file.exists()) {
                // Skip paths that don't exist.
                continue;
            }
            DataSource root;
            if (file.isDirectory()) {
                root = new LocalFileDataSource(file.getAbsolutePath());
            } else {
                root = new ZipFileDataSource(file.getAbsolutePath(), null);
            }
            // Add the DataSource to the list of roots. Set the base path to the absolute path to
            // the file so that
            // paths of the children will be relative to this root.
            retVal.add(new ClassPathDataSourceWrapper(root, root.getPath()));
        }
        return retVal;
    }

    private final String filename;
    private final String path;
    private long lastModified = -1;

    @DataFlowConfigurable
    public ClassPathFileDataSource(@DataFlowConfigProperty String path) {
        if (!path.startsWith(DataSource.PATH_SEPARATOR)) {
            path = DataSource.PATH_SEPARATOR + path;
        }
        this.filename = path;

        // Ensure that the path starts with a leading separator.
        if (!path.startsWith(DataSource.PATH_SEPARATOR)) {
            path = DataSource.PATH_SEPARATOR + path;
        }
        this.path = normalizePathSeparator(path, File.separator);
    }

    @Override
    public InputStream getInputStream() {
        InputStream inputStream = getClass().getResourceAsStream(filename);
        if (inputStream == null) {
            throw new RuntimeException("Unable to load file " + filename + " from the classpath");
        }
        return inputStream;
    }

    @Override
    public OutputStream getOutputStream() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<DataSource> getChildren() {
        return Collections.emptyList();
    }

    @Override
    public DataSource getParent() {
        return null;
    }

    @Override
    public boolean hasChanged() throws IOException {
        long currentLastModified = getLastModified();
        if (currentLastModified != lastModified) {
            lastModified = currentLastModified;
            return true;
        }
        return false;
    }

    @Override
    public boolean exists() throws IOException {
        return getClass().getResourceAsStream(filename) != null;
    }

    @Override
    public void delete() throws IOException {
        throw new UnsupportedOperationException();
    }

    private long getLastModified() throws IOException {
        URL uri = getClass().getResource(filename);
        URLConnection conn = uri.openConnection();
        if (conn instanceof JarURLConnection) {
            JarEntry jarEntry = ((JarURLConnection) conn).getJarEntry();
            if (jarEntry != null) {
                return jarEntry.getTime();
            }
        }
        return conn.getLastModified();
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public List<DataSource> getRoots() {
        return getClassPathRootDataSources();
    }

    @Override
    public DataSource createChild(String name, boolean isDirectory) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ClassPathFileDataSource that = (ClassPathFileDataSource) o;
        return Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }

    @Override
    public String toString() {
        return "ClassPathFileDataSource{"
                + "filename='"
                + filename
                + '\''
                + ", lastModified="
                + lastModified
                + '}';
    }
}
