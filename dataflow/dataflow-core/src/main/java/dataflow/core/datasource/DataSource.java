/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSource.java
 */
package dataflow.core.datasource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Optional;

/** An interface for various sources of data. */
public interface DataSource {

    String PATH_SEPARATOR = "/";

    /**
     * Returns an {@link InputStream} that can be used to read the contents of this {@link
     * DataSource}.
     */
    InputStream getInputStream() throws IOException;

    /**
     * Returns an {@link OutputStream} that can be used to write the contents of this {@link
     * DataSource}.
     */
    OutputStream getOutputStream() throws IOException;

    /**
     * Returns the children of this {@link DataSource} or an empty list if it doesn't have children.
     */
    List<DataSource> getChildren() throws IOException;

    /** Returns the parent of this {@link DataSource} or null if it doesn't have a parent. */
    DataSource getParent() throws IOException;

    /**
     * Returns true if the {@link DataSource} has changed. The first call to this method will return
     * true. Subsequent calls will return true if the DataSource has changed since the previous call
     * to this method.
     */
    boolean hasChanged() throws IOException;

    boolean exists() throws IOException;

    void delete() throws IOException;

    /**
     * Returns the name of the DataSource. If the {@link DataSource} is not a leaf then its name
     * should end with {@link DataSource#PATH_SEPARATOR}.
     */
    default String getName() {
        String path = getPath();
        String[] pathElements = path.split(PATH_SEPARATOR);
        String suffix = "";
        if (path.endsWith(DataSource.PATH_SEPARATOR)) {
            suffix = DataSource.PATH_SEPARATOR;
        }
        return pathElements.length > 0 ? pathElements[pathElements.length - 1] + suffix : suffix;
    }

    /**
     * Returns the full path and name of the DataSource. If the {@link DataSource} is not a leaf
     * then its path should end with {@link DataSource#PATH_SEPARATOR}.
     */
    String getPath();

    /** Returns the top level {@link DataSource}s for this type of {@link DataSource}. */
    List<DataSource> getRoots();

    /**
     * Returns the child with the given name or {@link Optional#empty()} if no child with given name
     * exists.
     */
    default Optional<DataSource> getChild(String name) throws IOException {
        String normalizedName = normalize(name);
        return getChildren().stream()
                .filter(child -> normalize(child.getName()).equals(normalizedName))
                .findAny();
    }

    DataSource createChild(String name, boolean isDirectory) throws IOException;

    static String normalize(String name) {
        if (name == null) {
            return null;
        }
        if (name.startsWith(DataSource.PATH_SEPARATOR)) {
            name = name.substring(1);
        }
        if (name.endsWith(DataSource.PATH_SEPARATOR)) {
            name = name.substring(0, name.length() - 1);
        }
        return name;
    }
}
