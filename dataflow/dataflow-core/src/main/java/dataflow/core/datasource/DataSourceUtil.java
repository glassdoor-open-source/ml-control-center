/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSourceUtil.java
 */
package dataflow.core.datasource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Pattern;

/** {@link DataSource} utility methods. */
public class DataSourceUtil {

    public static String toString(InputStream inputStream) {
        return toString(inputStream, StandardCharsets.UTF_8);
    }

    public static String toString(InputStream inputStream, Charset charset) {
        try (Scanner scanner = new Scanner(inputStream, charset.name())) {
            return scanner.useDelimiter("\\A").next();
        }
    }

    public static Optional<DataSource> getChildWithPath(DataSource root, String path)
            throws IOException {
        return getChildWithPath(root, path, false);
    }

    public static Optional<DataSource> getChildWithPath(
            DataSource root, String path, boolean create) throws IOException {
        DataSource child = root;
        String[] pathElements = path.split(DataSource.PATH_SEPARATOR);
        for (int idx = 0; idx < pathElements.length; idx++) {
            String pathElement = pathElements[idx];
            boolean isDirectory = idx < pathElements.length - 1;
            if (!pathElement.isEmpty()) {
                String childName =
                        isDirectory ? pathElement + DataSource.PATH_SEPARATOR : pathElement;
                DataSource next = child.getChild(childName).orElse(null);
                if (next == null) {
                    if (create) {
                        next = child.createChild(childName, isDirectory);
                    } else {
                        return Optional.empty();
                    }
                }
                child = next;
            }
        }
        return Optional.ofNullable(child);
    }

    /**
     * Returns the path elements from the given path. Parent path elements have {@link
     * DataSource#PATH_SEPARATOR} as a suffix.
     */
    public static List<String> getPathElements(String path) {
        List<String> pathElements = new ArrayList<>();
        StringBuilder pathElement = new StringBuilder();
        for (char ch : path.toCharArray()) {
            pathElement.append(ch);
            if (ch == DataSource.PATH_SEPARATOR.charAt(0)) {
                pathElements.add(pathElement.toString());
                pathElement.setLength(0);
            }
        }
        if (pathElement.length() > 0) {
            pathElements.add(pathElement.toString());
        }
        return pathElements;
    }

    /** Returns the parent path of the given path or null if this path has no parent. */
    public static String getParentPath(String path) {
        return getParentPath(path, DataSource.PATH_SEPARATOR);
    }

    public static String getParentPath(String path, String separator) {
        if (path == null) {
            return null;
        }
        // Remove the trailing separator.
        path =
                path.endsWith(separator)
                        ? path.substring(0, path.length() - separator.length())
                        : path;

        int idx = path.lastIndexOf(separator);
        if (idx > 0) {
            return path.substring(0, idx) + separator;
        }
        return path.isEmpty() || separator.equals(path) ? null : separator;
    }

    public static String appendPath(String path, String name) {
        StringBuilder sb = new StringBuilder();
        path = path != null ? path : "";
        sb.append(path);
        if (!path.endsWith(DataSource.PATH_SEPARATOR)) {
            sb.append(DataSource.PATH_SEPARATOR);
        }
        if (name.startsWith(DataSource.PATH_SEPARATOR)) {
            name = name.substring(DataSource.PATH_SEPARATOR.length());
        }
        sb.append(name);
        return sb.toString();
    }

    /**
     * Normalize the path separator used in the given path from the given separator to the {@link
     * DataSource#PATH_SEPARATOR}.
     *
     * @param path the path to normalize
     * @param pathSeparator the separator to replace with {@link DataSource#PATH_SEPARATOR}
     * @return the path with the normalized separator
     */
    public static String normalizePathSeparator(String path, String pathSeparator) {
        return normalizePathSeparator(path, pathSeparator, DataSource.PATH_SEPARATOR);
    }

    /**
     * Normalize the path separator used in the given path.
     *
     * @param path the path to normalize
     * @param pathSeparator the separator to replace
     * @param newPathSeparator the new path separator to use
     * @return the path with the normalized separator
     */
    public static String normalizePathSeparator(
            String path, String pathSeparator, String newPathSeparator) {
        if (pathSeparator.equals(newPathSeparator)) {
            return path;
        }
        if (path.contains(newPathSeparator)) {
            throw new IllegalArgumentException(
                    String.format(
                            "Unable to normalize path %s. It already contains the new separator character %s",
                            path, newPathSeparator));
        }
        return path.replaceAll(Pattern.quote(pathSeparator), newPathSeparator);
    }

    /**
     * Returns all of the DataSources that match the given glob pattern starting with the given root
     * DataSource. The glob pattern is a string where the * wildcard can be used to match 0 or more
     * characters and the ? wildcard can be used to match a single character. Wildcard matches stop
     * at each {@link DataSource#PATH_SEPARATOR}.
     *
     * <p>For example:
     *
     * <pre>/dir{@literal *}/test-{@literal *}.csv</pre>
     *
     * <p>matches /dir1/test-1.csv /dir/test-a.csv etc.
     */
    public static List<DataSource> getDataSourcesMatchingGlobPattern(
            DataSource root, String globPattern) throws IOException {
        List<DataSource> matchingDataSources = new ArrayList<>();
        Deque<String> pathElements = new ArrayDeque<>(getPathElements(globPattern));
        if (pathElements.getFirst().equals(DataSource.PATH_SEPARATOR)) {
            pathElements.removeFirst();
        }
        if (root.getPath().startsWith(DataSource.PATH_SEPARATOR)
                && !globPattern.startsWith(DataSource.PATH_SEPARATOR)) {
            // Ensure that the pattern starts with the path separator.
            globPattern = DataSource.PATH_SEPARATOR + globPattern;
        }
        getSourcesMatchingPattern(
                root, pathElements, getPatternFromGlob(globPattern), matchingDataSources);
        return matchingDataSources;
    }

    private static void getSourcesMatchingPattern(
            DataSource parent,
            Deque<String> pathElements,
            Pattern globPattern,
            List<DataSource> sources)
            throws IOException {
        if (pathElements.isEmpty()) {
            return;
        }
        String pathElement = pathElements.removeFirst();
        try {
            Pattern namePattern = getPatternFromGlob(pathElement);
            List<DataSource> children;
            if (pathElement.indexOf('*') < 0 && pathElement.indexOf('?') < 0) {
                // The pathElement does not have a glob pattern.
                Optional<DataSource> child = parent.getChild(pathElement);
                children = child.map(Collections::singletonList).orElse(Collections.emptyList());
            } else {
                // The pathElement has a glob pattern.
                children = parent.getChildren();
            }
            for (DataSource child : children) {
                if (globPattern.matcher(child.getPath()).matches()) {
                    sources.add(child);
                } else if (namePattern.matcher(child.getName()).matches()) {
                    getSourcesMatchingPattern(child, pathElements, globPattern, sources);
                }
            }
        } finally {
            pathElements.addFirst(pathElement);
        }
    }

    static Pattern getPatternFromGlob(String glob) {
        return Pattern.compile(
                "^"
                        + Pattern.quote(glob)
                                // ? replacement must come before * replacement because of the
                                // reluctant qualifier on *
                                .replace("?", "\\E[^" + DataSource.PATH_SEPARATOR + "]\\Q")
                                .replace("*", "\\E[^" + DataSource.PATH_SEPARATOR + "]*?\\Q")
                        + "$");
    }
}
