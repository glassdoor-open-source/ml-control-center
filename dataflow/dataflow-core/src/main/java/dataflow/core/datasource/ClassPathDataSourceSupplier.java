/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ClassPathDataSourceSupplier.java
 */
package dataflow.core.datasource;

import static dataflow.core.datasource.ClassPathFileDataSource.getClassPathDataSourcesMatchingGlobPattern;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import java.util.ArrayList;
import java.util.List;

public class ClassPathDataSourceSupplier implements DataSourceSupplier<DataSource> {

    private List<String> paths;

    @DataFlowConfigurable
    public ClassPathDataSourceSupplier(@DataFlowConfigProperty List<String> paths) {
        this.paths = paths;
    }

    @Override
    public List<DataSource> get() {
        List<DataSource> dataSources = new ArrayList<>();
        for (String path : paths) {
            try {
                dataSources.addAll(getClassPathDataSourcesMatchingGlobPattern(path));
            } catch (Exception e) {
                throw new RuntimeException("Error loading classpath resources with path " + path);
            }
        }
        return dataSources;
    }
}
