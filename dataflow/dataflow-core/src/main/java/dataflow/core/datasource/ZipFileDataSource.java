/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ZipFileDataSource.java
 */
package dataflow.core.datasource;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/** A {@link DataSource} for a zip file. */
public class ZipFileDataSource implements DataSource {

    private final String zipFilename;
    private final String zipEntryName;
    private ZipFile zipFile;
    private long lastModified = -1;

    @DataFlowConfigurable
    public ZipFileDataSource(
            @DataFlowConfigProperty String zipFilename,
            @DataFlowConfigProperty(name = "path") String zipEntryName) {
        this.zipFilename = zipFilename;
        this.zipEntryName = zipEntryName;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        if (zipEntryName == null) {
            return new ZipInputStream(new FileInputStream(zipFilename));
        }
        ZipFile zipFile = getZipFile();
        ZipEntry zipEntry = zipFile.getEntry(zipEntryName);
        return zipEntry != null ? zipFile.getInputStream(zipEntry) : null;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns the children of this {@link DataSource}. If the {@link DataSource} is the top level
     * of the zip file then all children are returned. If the {@link DataSource} is an entry in the
     * zip file then all of its children (direct and indirect) children are returned.
     */
    @Override
    public List<DataSource> getChildren() throws IOException {
        List<DataSource> children = new ArrayList<>();
        ZipFile zipFile = getZipFile();
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            String nextEntryName = entries.nextElement().getName();
            if (zipEntryName == null
                    || (nextEntryName.startsWith(zipEntryName)
                            && !nextEntryName.equals(zipEntryName))) {
                children.add(new ZipFileDataSource(zipFilename, nextEntryName));
            }
        }
        return children;
    }

    @Override
    public ZipFileDataSource getParent() {
        return zipEntryName != null ? new ZipFileDataSource(zipFilename, null) : null;
    }

    @Override
    public boolean hasChanged() {
        long currentLastUpdatedTime = getLastModified();
        if (currentLastUpdatedTime != lastModified) {
            lastModified = currentLastUpdatedTime;
            return true;
        }
        return false;
    }

    @Override
    public boolean exists() throws IOException {
        if (!new File(zipFilename).exists()) {
            return false;
        }
        if (zipEntryName == null) {
            return false;
        }
        ZipFile zipFile = getZipFile();
        ZipEntry zipEntry = zipFile.getEntry(zipEntryName);
        return zipEntry != null;
    }

    @Override
    public void delete() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getPath() {
        // Path normalization is not needed because the zip entry name is a valid DataSource path.
        return zipEntryName != null
                ? DataSource.PATH_SEPARATOR + zipEntryName
                : DataSource.PATH_SEPARATOR;
    }

    @Override
    public List<DataSource> getRoots() {
        return Collections.singletonList(new ZipFileDataSource(zipFilename, null));
    }

    @Override
    public DataSource createChild(String name, boolean isDirectory) {
        throw new UnsupportedOperationException();
    }

    private long getLastModified() {
        // Set the zipFile to null to reload.
        zipFile = null;
        try {
            ZipEntry entry = getEntry();
            if (entry != null && entry.getLastModifiedTime() != null) {
                return entry.getLastModifiedTime().toMillis();
            }
        } catch (IOException e) {
            // Try to fallback to the zip file's last modified time.
        }
        File file = new File(zipFilename);
        return file.lastModified();
    }

    private ZipFile getZipFile() throws IOException {
        if (zipFile == null) {
            zipFile = new ZipFile(zipFilename);
        }
        return zipFile;
    }

    private ZipEntry getEntry() throws IOException {
        if (zipEntryName == null) {
            return null;
        }
        return getZipFile().getEntry(zipEntryName);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ZipFileDataSource that = (ZipFileDataSource) o;
        return Objects.equals(zipFilename, that.zipFilename)
                && Objects.equals(zipEntryName, that.zipEntryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zipFilename, zipEntryName);
    }

    @Override
    public String toString() {
        return "ZipFileDataSource{"
                + "zipFilename='"
                + zipFilename
                + '\''
                + ", zipEntryName='"
                + zipEntryName
                + '\''
                + ", lastModified="
                + lastModified
                + '}';
    }
}
