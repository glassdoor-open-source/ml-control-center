/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  LocalFileDataSource.java
 */
package dataflow.core.datasource;

import static dataflow.core.datasource.DataSourceUtil.normalizePathSeparator;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/** A {@link DataSource} for a file on the local file system. */
public class LocalFileDataSource implements DataSource {

    private final File file;
    private final String path;
    private long lastModified = -1;

    @DataFlowConfigurable
    public LocalFileDataSource(@DataFlowConfigProperty String path) {
        this.file = new File(path);
        this.path = getNormalizedPath(file);
    }

    @Override
    public InputStream getInputStream() throws FileNotFoundException {
        return new FileInputStream(file);
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return new FileOutputStream(file);
    }

    public Optional<DataSource> getChild(String name) {
        LocalFileDataSource child =
                new LocalFileDataSource(file.getAbsolutePath() + File.separator + name);
        return child.exists() ? Optional.of(child) : Optional.empty();
    }

    @Override
    public DataSource createChild(String name, boolean isDirectory) throws IOException {
        LocalFileDataSource child =
                new LocalFileDataSource(path + DataSource.PATH_SEPARATOR + name);
        if (isDirectory) {
            child.file.mkdirs();
        }
        return child;
    }

    public boolean exists() {
        return file.exists();
    }

    @Override
    public List<DataSource> getChildren() {
        File[] children = file.listFiles();
        return children != null
                ? Arrays.stream(children)
                        .map(child -> new LocalFileDataSource(child.getAbsolutePath()))
                        .collect(Collectors.toList())
                : Collections.emptyList();
    }

    @Override
    public LocalFileDataSource getParent() {
        File parentFile = file.getParentFile();
        return parentFile.exists() ? new LocalFileDataSource(parentFile.getAbsolutePath()) : null;
    }

    @Override
    public boolean hasChanged() {
        boolean changed = file.lastModified() != lastModified;
        lastModified = file.lastModified();
        return changed;
    }

    @Override
    public void delete() throws IOException {
        file.delete();
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public List<DataSource> getRoots() {
        File[] roots = File.listRoots();
        if (roots == null) {
            return Collections.emptyList();
        }
        return Arrays.stream(roots)
                .map(root -> new LocalFileDataSource(root.getAbsolutePath()))
                .collect(Collectors.toList());
    }

    private static String getNormalizedPath(File file) {
        String path = normalizePathSeparator(file.getAbsolutePath(), File.separator);
        if (file.isDirectory() && !path.endsWith(DataSource.PATH_SEPARATOR)) {
            // If the file is a directory its path should end with the separator.
            path = path + DataSource.PATH_SEPARATOR;
        }
        return path;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final LocalFileDataSource that = (LocalFileDataSource) o;
        return Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }

    @Override
    public String toString() {
        return "LocalFileDataSource{" + "file=" + file + ", lastModified=" + lastModified + '}';
    }
}
