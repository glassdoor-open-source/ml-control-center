/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowParserTest.java
 */
package dataflow.core.parser;

import com.google.common.collect.ImmutableMap;
import dataflow.core.component.metadata.*;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataFlowConfigurableObjectBuilder;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowParseException;
import dataflow.core.parser.mock.*;
import dataflow.core.registry.ComponentRegistry;
import dataflow.core.registry.DataFlowRegistry;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DataFlowParserTest {

    private DataFlowParser parser;

    @Before
    public void setUp() {
        DataFlowRegistry registry = new DataFlowRegistry();
        registerMockComponent(
                "BinnedValueProvider",
                BinnedValueProvider.class,
                ImmutableMap.of("value", "Integer"),
                "Integer",
                registry.getComponentRegistry());
        registerMockComponent(
                "MemoryTableValueProvider",
                MemoryTableValueProvider.class,
                Collections.emptyMap(),
                "Object",
                registry.getComponentRegistry());
        registerMockComponent(
                "LocalFileDataSource",
                LocalFileDataSource.class,
                Collections.emptyMap(),
                "DataSource",
                registry.getComponentRegistry());
        registerMockComponent(
                "MinRowCountValidator",
                MinRowCountValidator.class,
                Collections.emptyMap(),
                "",
                registry.getComponentRegistry());
        registerMockComponent(
                "FallbackValueProvider",
                FallbackValueProvider.class,
                ImmutableMap.<String, String>builder()
                        .put("value", "Object")
                        .put("fallbackValues", "List")
                        .build(),
                "Object",
                registry.getComponentRegistry());
        registerMockComponent(
                "ConstValueProvider",
                ConstValueProvider.class,
                Collections.emptyMap(),
                "Object",
                registry.getComponentRegistry());
        registerMockComponent(
                "ListValueProvider",
                ListValueProvider.class,
                Collections.emptyMap(),
                "List",
                registry.getComponentRegistry());
        this.parser = new DataFlowParser(registry);
    }

    @Test
    public void testParser_flow1() throws DataFlowConfigurationException, DataFlowParseException {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream("test-flow-1.yaml"));
        Assert.assertEquals(
                "DataFlowConfig{id='"
                        + config.getId()
                        + "', components={pclick_binned=ComponentConfig {id='pclick_binned', type='BinnedValueProvider', originalType='BinnedValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={bins=[0, 0.6033935, 0.6284179, 0.64703363, 0.66284174, 0.6801757, 0.70019525, 0.7250976, 0.75732416, 0.8002929]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, providedValue=false, eventsEnabled=null}, pclick=ComponentConfig {id='pclick', type='null', originalType='null', extendsId='null', overridesId='null', mocksId='null', properties={}, propertyValueProviders=[], input=[], output=[pclick_binned], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, providedValue=false, eventsEnabled=null}}, output=ComponentConfig {id='pclick_binned', type='BinnedValueProvider', originalType='BinnedValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={bins=[0, 0.6033935, 0.6284179, 0.64703363, 0.66284174, 0.6801757, 0.70019525, 0.7250976, 0.75732416, 0.8002929]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, providedValue=false, eventsEnabled=null}, incrementalMode=false, eventsEnabled=true, importConfigs=[], parentId=null, childDataFlows={}}",
                config.toString());
    }

    @Test
    public void testParser_flow2() throws DataFlowConfigurationException, DataFlowParseException {
        DataFlowConfig config = parser.parse(getClass().getResourceAsStream("test-flow-2.yaml"));
        Assert.assertEquals(
                "DataFlowConfig{id='test_2', components={ffm_feature=ComponentConfig {id='ffm_feature', type='MemoryTableValueProvider', originalType='MemoryTableValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={keyColumns=[valueType, value], outputColumns=[field, weight, factors], schema=valueType:string, value:integer, field:string, weight:float, factors:float[], outputMode=SINGLE_ROW, sourceFormat=tsv, source={filename=ffm.tsv}, lazyLoad=false, checkIntervalSecs=300, validators=[{minRows=100000}]}, propertyValueProviders=[], input=[], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=true, internal=false, providedValue=false, eventsEnabled=null}, goc_feature=ComponentConfig {id='goc_feature', type='MemoryTableValueProvider', originalType='ValueProvider', extendsId='ffm_feature', overridesId='null', mocksId='null', properties={lazyLoad=true, keyColumns=[valueType, value], outputColumns=[field, weight, factors], schema=valueType:string, value:integer, field:string, weight:float, factors:float[], outputMode=SINGLE_ROW, sourceFormat=tsv, source={filename=ffm.tsv}, checkIntervalSecs=300, validators=[{minRows=100000}]}, propertyValueProviders=[], input=[valueType, value], output=[goc_feature_fallback], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, providedValue=false, eventsEnabled=null}, _goc_feature_const0=ComponentConfig {id='_goc_feature_const0', type='ConstValueProvider', originalType='ConstValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={value=GOC}, propertyValueProviders=[], input=[], output=[goc_feature], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, providedValue=false, eventsEnabled=null}, _goc_feature_const1=ComponentConfig {id='_goc_feature_const1', type='ConstValueProvider', originalType='ConstValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={value=1002}, propertyValueProviders=[], input=[], output=[goc_feature], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, providedValue=false, eventsEnabled=null}, goc_feature_fallback=ComponentConfig {id='goc_feature_fallback', type='FallbackValueProvider', originalType='FallbackValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={}, propertyValueProviders=[], input=[value, fallbackValues], output=[goc_feature_binned], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, providedValue=false, eventsEnabled=null}, _goc_feature_fallback_list0=ComponentConfig {id='_goc_feature_fallback_list0', type='ListValueProvider', originalType='ListValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={itemKeys=[0]}, propertyValueProviders=[], input=[0], output=[goc_feature_fallback], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, providedValue=false, eventsEnabled=null}, _goc_feature_fallback_ConstValueProvider0=ComponentConfig {id='_goc_feature_fallback_ConstValueProvider0', type='ConstValueProvider', originalType='ConstValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={value=5}, propertyValueProviders=[], input=[], output=[_goc_feature_fallback_list0], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=true, providedValue=false, eventsEnabled=null}, goc_feature_binned=ComponentConfig {id='goc_feature_binned', type='BinnedValueProvider', originalType='BinnedValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={bins=[0.25, 0.5, 0.75]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, providedValue=false, eventsEnabled=null}}, output=ComponentConfig {id='goc_feature_binned', type='BinnedValueProvider', originalType='BinnedValueProvider', extendsId='null', overridesId='null', mocksId='null', properties={bins=[0.25, 0.5, 0.75]}, propertyValueProviders=[], input=[value], output=[], inputTypes=[], outputType='null', failOnError=true, abstract=false, internal=false, providedValue=false, eventsEnabled=null}, incrementalMode=false, eventsEnabled=true, importConfigs=[], parentId=null, childDataFlows={}}",
                config.toString());
    }

    private static void registerMockComponent(
            String type,
            Class componentClass,
            Map<String, String> inputs,
            String outputType,
            ComponentRegistry componentRegistry) {
        List<DataFlowComponentInputMetadata> inputMetadata =
                inputs.entrySet().stream()
                        .map(
                                entry ->
                                        new DataFlowComponentInputMetadata(
                                                entry.getKey(),
                                                entry.getValue(),
                                                new String[] {},
                                                true,
                                                ""))
                        .collect(Collectors.toList());
        componentRegistry.registerComponent(
                new DataFlowComponentMetadata(
                        type,
                        "",
                        componentClass.getPackageName(),
                        componentClass.getName(),
                        new DataFlowConfigurableObjectMetadata(componentClass.getName(), ""),
                        inputMetadata,
                        new DataFlowComponentOutputMetadata(outputType, new String[] {}, ""),
                        null,
                        null,
                        false,
                        false,
                        false,
                        Collections.emptyList()),
                new DataFlowConfigurableObjectBuilder() {
                    @Override
                    public Object build(
                            Map<String, Object> configProperties,
                            Map<String, Object> instanceValues) {
                        return new BinnedValueProvider();
                    }
                });
    }
}
