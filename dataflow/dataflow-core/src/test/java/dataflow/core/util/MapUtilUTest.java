/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MapUtilUTest.java
 */
package dataflow.core.util;

import static org.junit.Assert.*;

import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class MapUtilUTest {

    @Test
    public void testDeepMerge() {
        Map<String, Object> original =
                new HashMap<>(
                        ImmutableMap.<String, Object>builder()
                                .put("v1", "abc")
                                .put("v3", "123")
                                .put(
                                        "m1",
                                        new HashMap<>(
                                                ImmutableMap.<String, Object>builder()
                                                        .put("a1", "---")
                                                        .put("a2", "###")
                                                        .build()))
                                .build());
        Map<String, Object> overrides =
                ImmutableMap.<String, Object>builder()
                        .put("v2", "xzy")
                        .put("v3", "987")
                        .put(
                                "m1",
                                new HashMap<>(
                                        ImmutableMap.<String, Object>builder()
                                                .put("a2", "***")
                                                .build()))
                        .put(
                                "m2",
                                new HashMap<>(
                                        ImmutableMap.<String, Object>builder()
                                                .put("b1", "%%%")
                                                .build()))
                        .build();

        MapUtil.deepMerge(original, overrides);

        Map<String, Object> expected =
                ImmutableMap.<String, Object>builder()
                        .put("v1", "abc")
                        .put("v2", "xzy")
                        .put("v3", "987")
                        .put(
                                "m1",
                                new HashMap<>(
                                        ImmutableMap.<String, Object>builder()
                                                .put("a1", "---")
                                                .put("a2", "***")
                                                .build()))
                        .put(
                                "m2",
                                new HashMap<>(
                                        ImmutableMap.<String, Object>builder()
                                                .put("b1", "%%%")
                                                .build()))
                        .build();
        assertEquals(expected, original);
    }
}
