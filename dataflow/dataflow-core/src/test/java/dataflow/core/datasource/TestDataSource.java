/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestDataSource.java
 */
package dataflow.core.datasource;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TestDataSource implements DataSource {

    private String path;
    private String data;
    private DataSource parent;
    private List<DataSource> children = new ArrayList<>();
    private boolean changed;

    public TestDataSource(final String path, final String data) {
        this.path = path;
        this.data = data;
    }

    @Override
    public InputStream getInputStream() {
        return new ByteArrayInputStream(data.getBytes());
    }

    @Override
    public ByteArrayOutputStream getOutputStream() {
        return new ByteArrayOutputStream() {
            @Override
            public void close() throws IOException {
                super.close();
                TestDataSource.this.data = this.toString(Charset.defaultCharset());
            }
        };
    }

    @Override
    public List<DataSource> getChildren() {
        return children;
    }

    public TestDataSource addChild(TestDataSource child) {
        children.add(child);
        child.parent = this;
        return this;
    }

    @Override
    public DataSource getParent() {
        return parent;
    }

    @Override
    public boolean hasChanged() {
        boolean retVal = changed;
        changed = false;
        return retVal;
    }

    @Override
    public boolean exists() throws IOException {
        return true;
    }

    @Override
    public void delete() throws IOException {
        data = "";
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public List<DataSource> getRoots() {
        DataSource parent = this;
        try {
            while (parent.getParent() != null) {
                parent = parent.getParent();
            }
            return Collections.singletonList(parent);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public DataSource createChild(String name, boolean isDirectory) throws IOException {
        TestDataSource child = new TestDataSource(path + DataSource.PATH_SEPARATOR + name, "");
        children.add(child);
        return child;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final TestDataSource that = (TestDataSource) o;
        return Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}
