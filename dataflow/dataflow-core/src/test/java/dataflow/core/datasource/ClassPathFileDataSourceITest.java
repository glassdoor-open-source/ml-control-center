/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ClassPathFileDataSourceITest.java
 */
package dataflow.core.datasource;

import static dataflow.core.datasource.ClassPathFileDataSource.getClassPathDataSourcesMatchingGlobPattern;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class ClassPathFileDataSourceITest {

    private static final String TEST_FILE = "/dataflow/core/datasource/test.txt";

    @Test
    public void testGetClassPathDataSourcesMatchingGlobPattern() throws IOException {
        List<DataSource> matches =
                getClassPathDataSourcesMatchingGlobPattern("dataflow/core/datasource/*.txt");
        assertTrue(matches.size() > 0);
    }

    @Test
    public void testRead() throws IOException {
        ClassPathFileDataSource dataSource = new ClassPathFileDataSource(TEST_FILE);
        byte[] buffer = new byte[512];
        try (InputStream in = dataSource.getInputStream()) {
            int len = in.read(buffer);
            assertEquals("This is a test file", new String(buffer, 0, len));
        }
    }

    @Test
    public void getSourcesMatchingPattern() throws IOException {
        List<DataSource> sources = new ArrayList<>();
        for (DataSource root : new ClassPathFileDataSource("/").getRoots()) {
            sources.addAll(
                    DataSourceUtil.getDataSourcesMatchingGlobPattern(
                            root, "/dataflow/core/datasource/test*.txt"));
        }
        assertTrue(sources.size() > 0);
        for (DataSource dataSource : sources) {
            assertTrue(dataSource instanceof ClassPathFileDataSource.ClassPathDataSourceWrapper);
            assertTrue(dataSource.getPath().startsWith("/dataflow/core/datasource/test"));
            assertTrue(dataSource.getInputStream().read() > 0);
        }
    }

    @Test
    public void testHasChanged() throws IOException {
        ClassPathFileDataSource dataSource = new ClassPathFileDataSource(TEST_FILE);
        assertTrue(dataSource.hasChanged());
        assertFalse(dataSource.hasChanged());
    }

    @Test
    public void testGetParent() {
        ClassPathFileDataSource dataSource = new ClassPathFileDataSource(TEST_FILE);
        assertNull(dataSource.getParent());
    }

    @Test
    public void testEqualsAndHashCode() {
        ClassPathFileDataSource dataSource1 =
                new ClassPathFileDataSource("/dataflow/core/parser/test-flow-1.yaml");
        ClassPathFileDataSource dataSource2 =
                new ClassPathFileDataSource("/dataflow/core/parser/test-flow-1.yaml");
        ClassPathFileDataSource dataSource3 =
                new ClassPathFileDataSource("/dataflow/core/parser/test-flow-2.yaml");
        assertEquals(dataSource1, dataSource1);
        assertEquals(dataSource1, dataSource2);
        assertEquals(dataSource1.hashCode(), dataSource2.hashCode());
        assertNotEquals(dataSource1, dataSource3);
    }

    @Test
    public void testRead_nonExistent() {
        ClassPathFileDataSource dataSource =
                new ClassPathFileDataSource("/dataflow/core/datasource/test-nonExistent.txt");
        try {
            dataSource.getInputStream();
            fail("Expected exception was not thrown");
        } catch (Exception e) {
            // Expected
        }
    }
}
