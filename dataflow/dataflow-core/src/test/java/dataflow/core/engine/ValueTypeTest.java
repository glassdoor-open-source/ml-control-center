/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  ValueTypeTest.java
 */
package dataflow.core.engine;

import static dataflow.core.type.TypeUtil.valueTypeFromString;
import static org.junit.Assert.assertEquals;

import dataflow.core.type.ValueType;
import org.junit.Test;

public class ValueTypeTest {

    @Test
    public void fromString_noGenericTypes() {
        ValueType type = valueTypeFromString("java.util.String");
        assertEquals("java.util.String", type.toString());
    }

    @Test
    public void fromString_singleGenericType() {
        ValueType type = valueTypeFromString("List<Integer>");
        assertEquals("List<Integer>", type.toString());
    }

    @Test
    public void fromString_multipleGenericTypes() {
        ValueType type = valueTypeFromString("Map<String, Float>");
        assertEquals("Map<String,Float>", type.toString());
    }

    @Test
    public void fromString_nestedGenericTypes() {
        ValueType type = valueTypeFromString("java.util.Map<String, List<Double>>");
        assertEquals("java.util.Map<String,List<Double>>", type.toString());
    }

    @Test
    public void fromString_multipleNestedGenericTypes() {
        ValueType type = valueTypeFromString("List<String, Map<Integer, Double>>");
        assertEquals("List<String,Map<Integer,Double>>", type.toString());
    }
}
