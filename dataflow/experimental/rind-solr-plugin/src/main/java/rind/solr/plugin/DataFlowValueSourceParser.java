/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowValueSourceParser.java
 */
package rind.solr.plugin;

import static dataflow.core.type.TypeUtil.convert;
import static dataflow.core.type.TypeUtil.valueTypeFromString;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.registry.TypeRegistry;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.lucene.queries.function.ValueSource;
import org.apache.solr.search.FunctionQParser;
import org.apache.solr.search.SyntaxError;
import org.apache.solr.search.ValueSourceParser;

/**
 * A {@link ValueSourceParser} implementation that uses DataFlow to compute the value for each
 * document. The input parameters are: - The number of input values - name, value, and type strings
 * for each input value - The number of input value sources - name, value source for each input
 * value source
 */
public class DataFlowValueSourceParser extends ValueSourceParser {

    private static final DataFlowEnvironment DEFAULT_ENV;

    static {
        try {
            DEFAULT_ENV = new DataFlowEnvironment("default", new SimpleDependencyInjector(), null);
            DataFlowDiscoveryConfigParser discoveryConfigParser =
                    new DataFlowDiscoveryConfigParser();
            DataflowDiscoveryConfig discoveryConfig =
                    discoveryConfigParser.parse(
                            ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
            DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
            discoveryService.register(discoveryConfig, DEFAULT_ENV);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the DataFlow engine that will be used. The engine can be customized by extending this
     * class and overriding this method.
     */
    protected DataFlowEnvironment getDataFlowEnvironment() {
        return DEFAULT_ENV;
    }

    @Override
    public ValueSource parse(final FunctionQParser functionQParser) throws SyntaxError {
        String dataFlowId = functionQParser.parseArg();

        TypeRegistry typeRegistry = getDataFlowEnvironment().getRegistry().getTypeRegistry();

        Map<String, Object> inputValues = new HashMap<>();
        int inputValueCount = functionQParser.parseInt();
        for (int cnt = 1; cnt <= inputValueCount; cnt++) {
            String name = functionQParser.parseArg();
            String strValue = functionQParser.parseArg();
            String type = functionQParser.parseArg();
            Object value =
                    convert(
                            strValue,
                            TypeRegistry.STRING_VALUE_TYPE,
                            valueTypeFromString(type),
                            typeRegistry);
            inputValues.put(name, value);
        }

        Map<String, ValueSource> inputValueSources = new HashMap<>();
        int inputValueSourceCount = functionQParser.parseInt();
        for (int cnt = 1; cnt <= inputValueSourceCount; cnt++) {
            String name = functionQParser.parseArg();
            ValueSource valueSource = functionQParser.parseValueSource();
            inputValueSources.put(name, valueSource);
        }

        try (DataFlowInstance instance =
                getDataFlowEnvironment()
                        .getRegistry()
                        .getDataFlowRegistration(dataFlowId)
                        .getInstanceFactory()
                        .newInstance(i -> {})) {
            inputValues.forEach(instance::setValue);
            instance.execute();
            return new DataFlowValueSource(
                    instance, functionQParser.getReq().getSchema(), inputValueSources);
        } catch (Exception e) {
            throw new SyntaxError("Invalid data flow " + dataFlowId, e);
        }
    }
}
