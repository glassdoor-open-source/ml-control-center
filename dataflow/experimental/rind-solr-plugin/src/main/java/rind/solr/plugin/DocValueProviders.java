/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DocValueProviders.java
 */
package rind.solr.plugin;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.exception.DataFlowConfigurationException;
import java.io.IOException;
import org.apache.lucene.index.BinaryDocValues;
import org.apache.lucene.index.DocValuesType;
import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.NumericDocValues;
import org.apache.solr.schema.DoubleValueFieldType;
import org.apache.solr.schema.FieldType;
import org.apache.solr.schema.FloatValueFieldType;
import org.apache.solr.schema.IndexSchema;
import org.apache.solr.schema.IntValueFieldType;
import org.apache.solr.schema.LongValueFieldType;
import org.apache.solr.schema.NumericValueFieldType;
import org.apache.solr.schema.SchemaField;

public class DocValueProviders {

    interface DocValueProvider {

        void init(LeafReaderContext readerContext, IndexSchema schema) throws IOException;
    }

    private abstract static class AbstractNumericDocValueProvider implements DocValueProvider {

        private final String field;
        protected NumericDocValues docValues;

        private AbstractNumericDocValueProvider(final String field) {
            this.field = field;
        }

        public boolean isAvailable() {
            return docValues != null;
        }

        public void init(LeafReaderContext readerContext, IndexSchema schema) throws IOException {
            final FieldInfo fieldInfo = readerContext.reader().getFieldInfos().fieldInfo(field);
            if (fieldInfo == null) {
                throw new RuntimeException("Invalid field " + field);
            }

            DocValuesType docValuesType = fieldInfo.getDocValuesType();
            if (docValuesType != DocValuesType.NUMERIC) {
                throw new DataFlowConfigurationException(
                        String.format(
                                "Field %s doc values type should be NUMERIC, but was %s",
                                field, docValuesType));
            }
            SchemaField schemaField = schema.getField(field);
            FieldType schemaFieldType = schemaField.getType();
            if (!getSchemaFieldType().isInstance(schemaFieldType)) {
                throw new DataFlowConfigurationException(
                        String.format(
                                "Field %s type should be an instance of %s, but was %s",
                                field,
                                getSchemaFieldType().getSimpleName(),
                                schemaFieldType.getClass().getSimpleName()));
            }
            docValues = readerContext.reader().getNumericDocValues(field);
        }

        protected abstract Class<? extends NumericValueFieldType> getSchemaFieldType();
    }

    @DataFlowComponent
    public static class IntDocValueProvider extends AbstractNumericDocValueProvider {

        @DataFlowConfigurable
        public IntDocValueProvider(@DataFlowConfigProperty String field) {
            super(field);
        }

        protected Class<? extends NumericValueFieldType> getSchemaFieldType() {
            return IntValueFieldType.class;
        }

        @OutputValue
        public int getValue(@InputValue int docId) {
            return (int) docValues.get(docId);
        }
    }

    @DataFlowComponent
    public static class LongDocValueProvider extends AbstractNumericDocValueProvider {

        @DataFlowConfigurable
        public LongDocValueProvider(@DataFlowConfigProperty String field) {
            super(field);
        }

        @Override
        protected Class<? extends NumericValueFieldType> getSchemaFieldType() {
            return LongValueFieldType.class;
        }

        @OutputValue
        public long getValue(@InputValue int docId) {
            return docValues.get(docId);
        }
    }

    @DataFlowComponent
    public static class FloatDocValueProvider extends AbstractNumericDocValueProvider {

        @DataFlowConfigurable
        public FloatDocValueProvider(@DataFlowConfigProperty String field) {
            super(field);
        }

        @Override
        protected Class<? extends NumericValueFieldType> getSchemaFieldType() {
            return FloatValueFieldType.class;
        }

        @OutputValue
        public float getValue(@InputValue int docId) {
            return Float.intBitsToFloat((int) docValues.get(docId));
        }
    }

    @DataFlowComponent
    public static class DoubleDocValueProvider extends AbstractNumericDocValueProvider {

        @DataFlowConfigurable
        public DoubleDocValueProvider(@DataFlowConfigProperty String field) {
            super(field);
        }

        @Override
        protected Class<? extends NumericValueFieldType> getSchemaFieldType() {
            return DoubleValueFieldType.class;
        }

        @OutputValue
        public double getValue(@InputValue int docId) {
            return Double.longBitsToDouble(docValues.get(docId));
        }
    }

    @DataFlowComponent
    public static class ObjectDocValueProvider implements DocValueProvider {

        private String field;
        private BinaryDocValues docValues;
        private SchemaField schemaField;

        @DataFlowConfigurable
        public ObjectDocValueProvider(@DataFlowConfigProperty final String field) {
            this.field = field;
        }

        @Override
        public void init(final LeafReaderContext readerContext, final IndexSchema schema)
                throws IOException {
            final FieldInfo fieldInfo = readerContext.reader().getFieldInfos().fieldInfo(field);
            if (fieldInfo == null) {
                throw new RuntimeException("Invalid field " + field);
            }
            this.schemaField = schema.getField(field);
            DocValuesType docValuesType = fieldInfo.getDocValuesType();

            switch (docValuesType) {
                case BINARY:
                    this.docValues = readerContext.reader().getBinaryDocValues(field);
                    break;
                case SORTED:
                    this.docValues = readerContext.reader().getSortedDocValues(field);
                    break;
                case SORTED_NUMERIC:
                case SORTED_SET:
                    throw new RuntimeException("Unsupported field type " + schemaField.getType());
                case NONE:
                    this.docValues = null;
                    break;
            }
        }

        @OutputValue
        public Object getValue(@InputValue int docId) {
            return schemaField.getType().toObject(schemaField, docValues.get(docId));
        }
    }

    @DataFlowComponent
    public static class BooleanDocValueProvider implements DocValueProvider {

        private final ObjectDocValueProvider docValueProvider;

        @DataFlowConfigurable
        public BooleanDocValueProvider(@DataFlowConfigProperty final String field) {
            docValueProvider = new ObjectDocValueProvider(field);
        }

        @Override
        public void init(final LeafReaderContext readerContext, final IndexSchema schema)
                throws IOException {
            docValueProvider.init(readerContext, schema);
        }

        @OutputValue
        public boolean getValue(@InputValue int docId) {
            return (Boolean) docValueProvider.getValue(docId);
        }
    }
}
