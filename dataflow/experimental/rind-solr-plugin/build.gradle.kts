

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(libs.guava)
    api(libs.luceneCore)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
    compileOnly(libs.solrCore)
}

group = "dataflow"
description = "rind-solr-plugin"
