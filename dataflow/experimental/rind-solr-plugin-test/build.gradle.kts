

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.rindSolrPlugin)

    api(projects.rindSolrPlugin)
    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(libs.guava)
    api(libs.luceneCore)
    testImplementation(libs.solrTestFramework)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
    compileOnly(libs.solrCore)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "rind-solr-plugin-test"
