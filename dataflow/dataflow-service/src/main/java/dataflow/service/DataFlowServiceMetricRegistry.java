/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowServiceMetricRegistry.java
 */
package dataflow.service;

import dataflow.core.environment.DataFlowEnvironment;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class DataFlowServiceMetricRegistry {

    private record GaugeCacheKey(String name, Iterable<Tag> tags) {}

    private static final Logger logger =
            LoggerFactory.getLogger(DataFlowServiceMetricRegistry.class);

    public static final String DATAFLOW_ERROR_COUNTER = "dataflow_errors_counter";
    public static final String DATAFLOW_HEALTH_CHECK_COUNTER = "dataflow_health_check_counter";

    public static final String DATAFLOW_DURATION_GAUGE = "dataflow_duration_gauge";
    public static final String DATAFLOW_SUCCESS_COUNTER = "dataflow_success_counter";
    public static final String DATAFLOW_SUCCESS_STATUS_GAUGE = "dataflow_success_status_gauge";
    public static final String DATAFLOW_LAST_SUCCESS_GAUGE = "dataflow_last_success_gauge";

    // Gauge cache for storing gauge values
    private final Map<GaugeCacheKey, Long> gaugeCache = new HashMap<>();

    private final MeterRegistry registry;

    public DataFlowServiceMetricRegistry(DataFlowEnvironment env) {
        this.registry = env.getDependencyInjector().getInstance(MeterRegistry.class);
    }

    public void addHealthCheck() {
        incrementCount(DATAFLOW_HEALTH_CHECK_COUNTER, null);
    }

    public void addDataflowException(Throwable t, String dataflowId) {
        incrementCount(
                DATAFLOW_ERROR_COUNTER, Tags.of("type", resolveType(t), "dataflowId", dataflowId));
        setGauge(DATAFLOW_SUCCESS_STATUS_GAUGE, Tags.of("dataFlowId", dataflowId), 0);
    }

    public void addDataflowSuccess(String dataflowId) {
        Tags tags = Tags.of("dataFlowId", dataflowId);
        incrementCount(DATAFLOW_SUCCESS_COUNTER, tags);
        setGauge(DATAFLOW_LAST_SUCCESS_GAUGE, tags, Instant.now().toEpochMilli());
        setGauge(DATAFLOW_SUCCESS_STATUS_GAUGE, tags, 1);
    }

    public void addDataflowDuration(Duration duration, String dataflowId) {
        setGauge(DATAFLOW_DURATION_GAUGE, Tags.of("dataFlowId", dataflowId), duration.getSeconds());
    }

    private void incrementCount(String counterName, Iterable<Tag> tags) {
        if (registry != null) {
            registry.counter(counterName, tags).increment();
        }
    }

    private void setGauge(String gaugeName, Iterable<Tag> tags, long number) {
        if (registry != null) {
            logger.warn("Setting gauge {} with tags {} to {}", gaugeName, tags, number);
            GaugeCacheKey cacheKey = new GaugeCacheKey(gaugeName, tags);
            gaugeCache.put(cacheKey, number);
            registry.gauge(gaugeName, tags, gaugeCache, g -> g.get(cacheKey));
        }
    }

    private String resolveType(Throwable t) {
        return t != null ? t.getClass().getSimpleName() : "NA";
    }
}
