/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowTaskController.java
 */
package dataflow.service;

import dataflow.core.environment.DataFlowEnvironment;
import dataflow.task.DataFlowTaskRunner;
import dataflow.task.TaskTriggerEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DataFlowTaskController {

    private final DataFlowTaskRunner runner;

    @Autowired
    public DataFlowTaskController(DataFlowEnvironment env) {
        runner = new DataFlowTaskRunner(env);
    }

    @RequestMapping(value = "/dataflowTask/trigger", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> trigger(@RequestBody TaskTriggerEvent triggerEvent) {
        try {
            Object result = runner.run(triggerEvent);
            return ResponseEntity.ok().body(result);
        } catch (Exception e) {
            return ResponseEntity.internalServerError()
                    .body("Error running dataflow task " + e.getMessage());
        }
    }
}
