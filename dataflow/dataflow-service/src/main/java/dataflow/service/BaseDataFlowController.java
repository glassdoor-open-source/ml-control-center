/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  BaseDataFlowController.java
 */
package dataflow.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import dataflow.core.config.ComponentConfig;
import dataflow.core.engine.DataFlowInstance;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.type.TypeUtil;
import dataflow.core.type.ValueTypeConverter;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * A controller that will return the result of running the dataflow pipeline with the given id. The
 * request fields will be set as inputs to the pipeline. Value types will be converted if possible
 * when type information is available using the registered dataflow type converters. If no
 * appropriate converter is registered then an attempt will be made to convert the field using the
 * Jackson ObjectMapper. The pipeline output should be a json serializable object.
 */
public class BaseDataFlowController<REQUEST_CLASS, RESPONSE_CLASS> {

    private static final Logger logger = LoggerFactory.getLogger(BaseDataFlowController.class);

    private final DataFlowEnvironment env;
    private final DataFlowServiceMetricRegistry metricRegistry;
    private final ObjectMapper mapper = new ObjectMapper();

    public BaseDataFlowController(DataFlowEnvironment env) {
        this.env = env;
        this.metricRegistry = new DataFlowServiceMetricRegistry(env);
    }

    public ResponseEntity<RESPONSE_CLASS> process(
            String dataFlowId, String executionId, REQUEST_CLASS req) {

        MDC.put("dataflowId", dataFlowId);
        MDC.put("executionId", executionId);

        try {
            // Validate Input request.
            if (!this.validateRequest(req)) {
                createErrorResponse(400, req, null);
            }
        } catch (Exception e) {
            // Validate request function may also simply throw an exception.
            return createErrorResponse(400, req, e);
        }

        // Sanitize request.
        req = this.sanitizeRequest(req);

        // Create dataflow instance and load request as input.
        DataFlowInstance instance;
        try {
            instance = createInstance(dataFlowId, executionId, req);
        } catch (Exception e) {
            return createErrorResponse(500, req, e);
        }

        // Attempt to run instance and record metrics.
        ResponseEntity<RESPONSE_CLASS> response;
        Instant dataflowStart = Instant.now();
        try (instance) {
            logger.debug("Running dataflow {}: req: {}", dataFlowId, req);
            instance.execute();
            response = ResponseEntity.ok((RESPONSE_CLASS) instance.getOutput());
            metricRegistry.addDataflowSuccess(dataFlowId);
        } catch (Exception e) {
            logger.error("Exception thrown when executing dataflow instance.", e);
            metricRegistry.addDataflowException(e, dataFlowId);
            response = createErrorResponse(500, req, e);
        } finally {
            Duration dataflowDuration = Duration.between(dataflowStart, Instant.now());
            metricRegistry.addDataflowDuration(dataflowDuration, dataFlowId);
        }

        logger.debug("Finished dataflow {}", dataFlowId);
        return response;
    }

    private DataFlowInstance createInstance(
            String dataFlowId, String executionId, final REQUEST_CLASS req) throws Exception {

        DataFlowInstance instance =
                env.getRegistry()
                        .getDataFlowRegistration(dataFlowId)
                        .getInstanceFactory()
                        .newInstance(
                                newInstance -> {
                                    if (executionId == null) {
                                        newInstance.setValue(
                                                "executionId", UUID.randomUUID().toString());
                                    } else {
                                        newInstance.setValue("executionId", executionId);
                                    }

                                    // Supply request to instance under key "request"
                                    newInstance.setValue("request", req);

                                    // Return early if req is Map.
                                    if (!(req instanceof Map)) {
                                        return;
                                    }

                                    // In special case that request is a Map, load each key:value
                                    // pair into dataflow as well.
                                    Map reqMap = (Map) req;
                                    reqMap.forEach(
                                            (key, value) -> {
                                                if (!(key instanceof String)) {
                                                    logger.warn(
                                                            "Request was Map but keys were not instances of String.");
                                                    return;
                                                }
                                                ComponentConfig componentConfig =
                                                        newInstance
                                                                .getConfig()
                                                                .getComponents()
                                                                .get(key);
                                                if (value != null
                                                        && componentConfig != null
                                                        && componentConfig.getOutputType()
                                                                != null) {
                                                    // Attempt to convert the request value to the
                                                    // expected type.
                                                    ValueTypeConverter converter =
                                                            env.getRegistry()
                                                                    .getTypeRegistry()
                                                                    .getConverter(
                                                                            value.getClass()
                                                                                    .getName(),
                                                                            componentConfig
                                                                                    .getOutputType());
                                                    if (converter != null) {
                                                        value =
                                                                converter.convert(
                                                                        value,
                                                                        TypeUtil
                                                                                .valueTypeFromString(
                                                                                        componentConfig
                                                                                                .getOutputType()));
                                                    } else {
                                                        // Attempt to convert using the
                                                        // ObjectMapper.
                                                        Class<?> valueClass =
                                                                env.getRegistry()
                                                                        .getTypeRegistry()
                                                                        .getValueTypeClass(
                                                                                componentConfig
                                                                                        .getOutputType());
                                                        value =
                                                                mapper.convertValue(
                                                                        value, valueClass);
                                                    }

                                                    newInstance.setValue((String) key, value);
                                                }
                                            });
                                });

        return instance;
    }

    /**
     * Default function to sanitize a request before feeding into dataflow.yaml.
     *
     * <p>Base function is identity function, although may be overridden by classes inheriting from
     * BaseDataFlowController.
     *
     * @param req Input request that has already been validated by validateRequest
     * @return Sanitized request (identity function in this case)
     */
    public REQUEST_CLASS sanitizeRequest(REQUEST_CLASS req) {
        return req;
    }

    /**
     * Default function to validate request. May throw exception or return False to indicate request
     * is not valid. Base function simply returns true always, although may be overridden by classes
     * inheriting from BaseDataFlowController.
     *
     * @param req Input request that may be invalid.
     * @return If request is valid (Always true for base function)
     * @throws Exception May also be way to indicate request is invalid
     */
    public Boolean validateRequest(REQUEST_CLASS req) throws Exception {
        return true;
    }

    /**
     * Function to handle returning response if an error was encountered while handling a request.
     *
     * @param status The HTTP status, typically 400 or 500.
     * @param request The input request that caused the error.
     * @param exception Caught exception (or null if no exception found)
     * @return Response to return back to user.
     */
    public ResponseEntity<RESPONSE_CLASS> createErrorResponse(
            int status, REQUEST_CLASS request, Exception exception) {
        return ResponseEntity.status(HttpStatus.valueOf(status)).body(null);
    }
}
