

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(projects.dataflowStream)
    api(projects.dataflowComponentCore)
    api(libs.guava)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

group = "dataflow"
description = "dataflow-integration"
