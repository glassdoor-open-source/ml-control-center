

plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(projects.dataflowComponentCodegen)
    testAnnotationProcessor(projects.dataflowCodegen)

    api(projects.dataflowTask)
    api(projects.dataflowCore)
    api(projects.dataflowRetry)
    api(libs.jacksonDatabind)
    api(libs.micrometerCore)
    testImplementation(projects.dataflowSpring)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

tasks.withType<JavaCompile> {
    doFirst {
        options.sourcepath = files("src/test/java") + files("src/test/resources")
    }
}

group = "dataflow"
description = "dataflow-task-test"
