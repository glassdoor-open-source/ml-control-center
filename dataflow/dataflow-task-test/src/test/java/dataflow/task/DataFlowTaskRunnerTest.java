/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowTaskRunnerTest.java
 */
package dataflow.task;

import static org.mockito.Answers.RETURNS_MOCKS;

import dataflow.core.environment.DataFlowEnvironment;
import dataflow.spring.DataFlowConfiguration;
import io.micrometer.core.instrument.MeterRegistry;
import java.util.Map;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataFlowConfiguration.class)
public class DataFlowTaskRunnerTest extends TestCase {

    @Autowired private DataFlowEnvironment env;

    @MockBean(answer = RETURNS_MOCKS)
    private MeterRegistry mockMeterRegistry;

    @Test
    public void testRun() throws Exception {
        DataFlowTaskRunner runner = new DataFlowTaskRunner(env);
        TaskTriggerEvent trigger =
                TaskTriggerEventParser.parse(ClassLoader.getSystemResourceAsStream("trigger.json"));
        Map<String, Object> output = (Map<String, Object>) runner.run(trigger);
        assertEquals(2, output.size());
        assertEquals("dryRun-true", output.get("test1"));
        assertEquals("executionId-abc123", output.get("test2"));
    }
}
