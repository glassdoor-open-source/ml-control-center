

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(libs.googleAutoService)

    api(projects.dataflowCodegenUtil)
    api(projects.dataflowComponentCore)
    api(libs.googleAutoService)
}

group = "dataflow"
description = "dataflow-component-codegen"
