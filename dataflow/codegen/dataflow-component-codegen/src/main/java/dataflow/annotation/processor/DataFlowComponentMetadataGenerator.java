/*
 * Copyright 2021-2025 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponentMetadataGenerator.java
 */
package dataflow.annotation.processor;

import static dataflow.annotation.processor.DataFlowConfigurableObjectBuilderGenerator.newDataFlowConfigurableObjectMetadataStmt;
import static dataflow.codegen.util.TypeUtil.getSimpleClassName;
import static dataflow.codegen.util.annotation.AnnotationProcessorUtil.buildArrayString;
import static dataflow.codegen.util.annotation.AnnotationProcessorUtil.javaEscape;
import static dataflow.codegen.util.annotation.DataFlowComponentMetadataBuilder.getComponentMetadataClassName;

import com.google.auto.service.AutoService;
import dataflow.codegen.util.DataFlowVersion;
import dataflow.codegen.util.annotation.AnnotationProcessorUtil;
import dataflow.codegen.util.annotation.DataFlowComponentMetadataBuilder;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.MetricType;
import dataflow.core.component.metadata.*;
import dataflow.core.component.metadata.metrics.DataFlowCounterMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowDistributionMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowGaugeMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowMetricMetadata;
import dataflow.core.component.metadata.metrics.DataFlowTimerMetricMetadata;
import dataflow.core.util.IndentPrintWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

@SupportedAnnotationTypes("dataflow.core.component.annotation.DataFlowComponent")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
@SuppressWarnings("unused")
public class DataFlowComponentMetadataGenerator extends AbstractProcessor {

    private static final String[] IMPORT_CLASSES =
            new String[] {
                Map.class.getName(),
                HashMap.class.getName(),
                List.class.getName(),
                ArrayList.class.getName(),
                DataFlowComponentMetadata.class.getName(),
                DataFlowConfigurableObjectMetadata.class.getName(),
                DataFlowConfigurableObjectPropertyMetadata.class.getName(),
                DataFlowComponentInputMetadata.class.getName(),
                DataFlowComponentOutputMetadata.class.getName(),
                DataFlowMetricMetadata.class.getName(),
                DataFlowCounterMetricMetadata.class.getName(),
                DataFlowTimerMetricMetadata.class.getName(),
                DataFlowGaugeMetricMetadata.class.getName(),
                DataFlowDistributionMetricMetadata.class.getName(),
                MetricType.class.getName()
            };

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements =
                    roundEnv.getElementsAnnotatedWith(annotation);
            for (Element element : annotatedElements) {
                if (!(element instanceof TypeElement)) {
                    printAndThrowError(
                            "Unexpected element annotated with "
                                    + DataFlowComponent.class.getSimpleName()
                                    + " annotation",
                            element);
                }
                if (!element.getModifiers().contains(Modifier.PUBLIC)) {
                    printAndThrowError(
                            "Only public classes can be annotated with "
                                    + DataFlowComponent.class.getSimpleName()
                                    + " annotation",
                            element);
                }
                if (element.getModifiers().contains(Modifier.ABSTRACT)) {
                    printAndThrowError(
                            "Classes annotated with "
                                    + DataFlowComponent.class.getSimpleName()
                                    + " annotation cannot be abstract",
                            element);
                }
                if (element.getEnclosingElement().getKind() == ElementKind.CLASS
                        && !element.getModifiers().contains(Modifier.STATIC)) {
                    printAndThrowError(
                            "Classes annotated with "
                                    + DataFlowComponent.class.getSimpleName()
                                    + " must be static when nested in another class",
                            element);
                }

                try {
                    writeFile(((TypeElement) element));
                } catch (Exception e) {
                    printAndThrowError(
                            "Error generating component metadata: " + e.getMessage(), element);
                }
            }
        }

        return true;
    }

    private void writeFile(TypeElement componentType) throws IOException {
        DataFlowComponentMetadataBuilder metadataBuilder =
                new DataFlowComponentMetadataBuilder(processingEnv);
        DataFlowComponentMetadata metadata = metadataBuilder.buildMetadata(componentType);

        String metadataClassName = getComponentMetadataClassName(componentType, processingEnv);
        String metadataSimpleClassName = getSimpleClassName(metadataClassName);

        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(metadataClassName);
        try (PrintWriter fileWriter = new PrintWriter(builderFile.openWriter())) {
            IndentPrintWriter out = new IndentPrintWriter(fileWriter);

            out.printf("package %s;%n", metadata.getComponentPackageName());
            out.println();

            writeImports(out);

            out.printf(
                    "public class %s extends DataFlowComponentMetadata {%n",
                    metadataSimpleClassName);

            out.println();
            out.indent();

            writeFields(metadata, metadataSimpleClassName, out);
            writeConstructor(metadata, metadataSimpleClassName, out);

            out.unindent();
            out.println("}");
        }
    }

    private void writeFields(
            DataFlowComponentMetadata metadata,
            String metadataSimpleClassName,
            IndentPrintWriter out) {
        out.println(
                "public static final int ENVIRONMENT_VERSION = "
                        + DataFlowVersion.ENVIRONMENT_VERSION
                        + ";");

        out.printf("public static final String TYPE = \"%s\";%n", metadata.getTypeName());
        out.println("public static final boolean IS_ASYNC = " + metadata.isAsync() + ";");
        out.println("public static final boolean HAS_ASYNC_GET = " + metadata.hasAsyncGet() + ";");
        out.println(
                "public static final boolean HAS_DYNAMIC_INPUT = "
                        + metadata.hasDynamicInput()
                        + ";");
        out.printf(
                "public static final String DESCRIPTION = \"%s\";%n",
                javaEscape(metadata.getDescription()));
        out.printf(
                "public static final String COMPONENT_PACKAGE_NAME = \"%s\";%n",
                metadata.getComponentPackageName());
        out.printf(
                "public static final String COMPONENT_CLASS_NAME = \"%s\";%n",
                metadata.getComponentClassName());

        out.printf(
                "public static final DataFlowConfigurableObjectMetadata CONFIGURABLE_OBJECT_METADATA = %s;\n",
                newDataFlowConfigurableObjectMetadataStmt(
                        metadata.getConfigurableObjectMetadata()));
        out.println(
                "public static final List<DataFlowComponentInputMetadata> INPUT_METADATA = new ArrayList<>();");
        out.println("public static final DataFlowComponentOutputMetadata OUTPUT_METADATA;");
        out.println("public static final String BUILDER_CLASS_NAME;");
        out.println("public static final String CODE_GENERATOR_CLASS_NAME;");

        // Metrics
        // Write the Metrics internal class.
        out.println();
        out.println("public static final class Metrics {");
        out.indent();
        out.println("public static final List<DataFlowMetricMetadata> VALUES = new ArrayList<>();");
        if (metadata.getMetricsMetadata() != null) {
            for (DataFlowMetricMetadata metricMetadata : metadata.getMetricsMetadata()) {
                out.printf(
                        "public static final %s %s;%n",
                        getMetricMetadataType(metricMetadata), getMetricVarName(metricMetadata));

                out.println("static");
                out.println("{");
                out.indent();
                writeMetricMetadataVars(metricMetadata, out);
                String metricVarName = getMetricVarName(metricMetadata);
                out.printf(
                        "%s = new %s(name, description, type);%n",
                        metricVarName, getMetricMetadataType(metricMetadata));
                out.printf("VALUES.add(%s);%n", metricVarName);
                out.unindent();
                out.println("}");
            }
        }

        out.unindent();
        out.println("}");

        // End Metrics

        out.println();
        out.println("static {");
        out.indent();

        for (DataFlowComponentInputMetadata inputMetadata : metadata.getInputMetadata()) {
            out.println("{");
            out.indent();
            writeMetadataVars(
                    inputMetadata.getName(),
                    inputMetadata.getRawType(),
                    true,
                    inputMetadata.getSupportedTypes(),
                    inputMetadata.isRequired(),
                    inputMetadata.getDescription(),
                    out);
            out.println(
                    "INPUT_METADATA.add(new DataFlowComponentInputMetadata("
                            + "name, rawType, supportedTypes, required, description));");
            out.unindent();
            out.println("}");
        }
        out.println();

        DataFlowComponentOutputMetadata outputMetadata = metadata.getOutputMetadata();
        out.println("{");
        out.indent();
        writeMetadataVars(
                null,
                outputMetadata.getRawType(),
                true,
                outputMetadata.getSupportedTypes(),
                true,
                outputMetadata.getDescription(),
                out);
        out.println(
                "OUTPUT_METADATA = new DataFlowComponentOutputMetadata(rawType, supportedTypes, description);");
        out.unindent();
        out.println("}");
        out.println();

        if (metadata.getBuilderClass() != null) {
            out.println("BUILDER_CLASS_NAME = \"" + metadata.getBuilderClass() + "\";");
        } else {
            out.println("BUILDER_CLASS_NAME = null;");
        }
        if (metadata.getCodeGeneratorClass() != null) {
            out.println(
                    "CODE_GENERATOR_CLASS_NAME = \"" + metadata.getCodeGeneratorClass() + "\";");
        } else {
            out.println("CODE_GENERATOR_CLASS_NAME = null;");
        }

        out.unindent();
        out.println("}");

        out.printf(
                "public static final DataFlowComponentMetadata INSTANCE = new %s();",
                metadataSimpleClassName);
        out.println();
    }

    private void writeConstructor(
            DataFlowComponentMetadata metadata, String metadataClassName, IndentPrintWriter out) {
        out.printf("public %s() {%n", metadataClassName);
        out.indent();
        out.println(
                "super(TYPE, DESCRIPTION, COMPONENT_PACKAGE_NAME, COMPONENT_CLASS_NAME, "
                        + "CONFIGURABLE_OBJECT_METADATA, INPUT_METADATA, OUTPUT_METADATA, BUILDER_CLASS_NAME, CODE_GENERATOR_CLASS_NAME, "
                        + "IS_ASYNC, HAS_ASYNC_GET, "
                        + "HAS_DYNAMIC_INPUT, Metrics.VALUES);");
        out.unindent();
        out.println("}");
        out.println();
    }

    private void writeImports(IndentPrintWriter out) {
        for (String importClass : IMPORT_CLASSES) {
            out.printf("import %s;%n", importClass);
        }
        out.println();
    }

    private void writeMetadataVars(
            String name,
            String rawType,
            boolean isEnum,
            Set<String> supportedTypes,
            boolean required,
            String description,
            IndentPrintWriter out) {
        if (name != null) {
            out.printf("String name = \"%s\";%n", name);
        }
        out.printf("String rawType = \"%s\";%n", rawType);
        out.printf("boolean isEnum = %s;%n", isEnum);
        out.printf("String[] supportedTypes = %s;%n", buildArrayString(supportedTypes));
        out.printf("boolean required = %s;%n", String.valueOf(required));
        out.printf("String description = \"%s\";%n", javaEscape(description));
    }

    private void writeMetricMetadataVars(
            DataFlowMetricMetadata metricMetadata, IndentPrintWriter out) {
        if (metricMetadata.getName() != null) {
            out.printf("String name = \"%s\";%n", metricMetadata.getName());
        }
        String description =
                metricMetadata.getDescription() != null ? metricMetadata.getDescription() : "";
        out.printf("String description = \"%s\";%n", javaEscape(description));
        out.printf(
                "MetricType type = MetricType.valueOf(\"%s\");%n", metricMetadata.getType().name());
    }

    public static String getMetricVarName(DataFlowMetricMetadata metricMetadata) {
        // The metric name is checked in the builder to ensure it is a valid variable name.
        return metricMetadata.getName();
    }

    private String getMetricMetadataType(DataFlowMetricMetadata metricMetadata) {
        return switch (metricMetadata.getType()) {
            case COUNTER -> DataFlowCounterMetricMetadata.class.getSimpleName();
            case TIMER -> DataFlowTimerMetricMetadata.class.getSimpleName();
            case GAUGE -> DataFlowGaugeMetricMetadata.class.getSimpleName();
            case DISTRIBUTION_SUMMARY -> DataFlowDistributionMetricMetadata.class.getSimpleName();
        };
    }

    private void printAndThrowError(String msg, Element element) {
        if (element != null) {
            msg = element + ": " + msg;
        }
        AnnotationProcessorUtil.printAndThrowError(msg, element, processingEnv);
    }
}
