/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AnnotationProcessorUtilTest.java
 */
package dataflow.codegen.util;

import static org.junit.Assert.*;

import com.google.common.collect.ImmutableSet;
import dataflow.codegen.util.annotation.AnnotationProcessorUtil;
import org.junit.Test;

public class AnnotationProcessorUtilTest {

    @Test
    public void testJavaEscape() {
        assertEquals("abc\\n123", AnnotationProcessorUtil.javaEscape("abc\n123"));
        assertEquals("abc\\r123", AnnotationProcessorUtil.javaEscape("abc\r123"));
        assertEquals("abc\\b123", AnnotationProcessorUtil.javaEscape("abc\b123"));
        assertEquals("abc\\f123", AnnotationProcessorUtil.javaEscape("abc\f123"));
        assertEquals("abc\\\"123\\\"", AnnotationProcessorUtil.javaEscape("abc\"123\""));
        assertEquals("abc\\t123", AnnotationProcessorUtil.javaEscape("abc\t123"));
        assertEquals("abc\\\\123", AnnotationProcessorUtil.javaEscape("abc\\123"));
        assertEquals("</br>", AnnotationProcessorUtil.javaEscape("</br>"));
    }

    @Test
    public void testBuildArrayString() {
        assertEquals("new String[]{}", AnnotationProcessorUtil.buildArrayString(ImmutableSet.of()));
        assertEquals(
                "new String[]{\"abc\"}",
                AnnotationProcessorUtil.buildArrayString(ImmutableSet.of("abc")));
        assertEquals(
                "new String[]{\"abc\",\"123\",\"xyz\"}",
                AnnotationProcessorUtil.buildArrayString(ImmutableSet.of("abc", "123", "xyz")));
    }
}
