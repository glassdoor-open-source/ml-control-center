/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  AnnotationProcessorUtil.java
 */
package dataflow.codegen.util.annotation;

import java.util.Set;
import java.util.StringJoiner;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

public class AnnotationProcessorUtil {

    public static String javaEscape(String str) {
        str = str.replace("\\", "\\\\");
        str = str.replace("\r", "\\r");
        str = str.replace("\n", "\\n");
        str = str.replace("\b", "\\b");
        str = str.replace("\f", "\\f");
        str = str.replace("\t", "\\t");
        str = str.replace("\"", "\\\"");
        return str;
    }

    public static String quote(String str) {
        if (str == null) {
            return "\"null\"";
        }
        return "\"" + str + "\"";
    }

    public static String buildArrayString(Set<String> values) {
        StringBuilder builder = new StringBuilder("new String[]{");
        StringJoiner joiner = new StringJoiner(",");
        values.forEach(v -> joiner.add(quote(v)));
        builder.append(joiner);
        builder.append("}");
        return builder.toString();
    }

    public static void printAndThrowError(
            String msg, Element element, ProcessingEnvironment processingEnv) {
        if (element != null) {
            msg = element.toString() + ": " + msg;
        }
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, msg, element);
        throw new IllegalStateException(msg);
    }
}
