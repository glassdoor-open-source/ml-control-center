/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  IndentPrintWriter.java
 */
package dataflow.core.util;

import java.io.PrintWriter;

/** A {@link PrintWriter} wrapper that supports indentation. */
public class IndentPrintWriter {

    private final PrintWriter out;
    private int indent = 0;
    private String indentSpaces = "";
    private int spacesPerIndent = 4;

    public IndentPrintWriter(final PrintWriter out) {
        this.out = out;
    }

    public IndentPrintWriter printf(String format, Object... args) {
        out.printf(indent(format), args);
        return this;
    }

    public IndentPrintWriter print(String str) {
        out.print(indent(str));
        return this;
    }

    public IndentPrintWriter println(String str) {
        out.println(indent(str));
        return this;
    }

    public IndentPrintWriter indent() {
        indent++;
        indentSpaces = repeat(" ", indent * spacesPerIndent);
        return this;
    }

    public IndentPrintWriter unindent() {
        indent--;
        indentSpaces = repeat(" ", indent * spacesPerIndent);
        return this;
    }

    public IndentPrintWriter println() {
        out.println();
        return this;
    }

    public void flush() {
        out.flush();
    }

    private String indent(String str) {
        return str.replaceAll("(?m)^", indentSpaces);
    }

    public void printLines(String lines) {
        for (String line : lines.split("\r?\n")) {
            println(line.trim());
        }
    }

    private String repeat(String str, int count) {
        StringBuilder result = new StringBuilder();
        for (int cnt = 1; cnt <= count; cnt++) {
            result.append(str);
        }
        return result.toString();
    }
}
