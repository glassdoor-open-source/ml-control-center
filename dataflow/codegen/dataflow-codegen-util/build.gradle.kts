

plugins {
    id("java-library")
}

dependencies {
    api(projects.dataflowComponentCore)
    api(libs.micrometerCore)
    testImplementation(libs.guava)
    testImplementation(libs.junit)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.springTest)
    testImplementation(libs.springBootTestAutoconfigure)
    testImplementation(libs.jsonassert)
}

group = "dataflow"
description = "dataflow-codegen-util"
