
plugins {
    id("java-library")
}

dependencies {
    testAnnotationProcessor(libs.googleAutoService)
    testAnnotationProcessor(projects.dataflowCodegen)
    testAnnotationProcessor(projects.dataflowComponentCodegen)

    api(projects.dataflowCodegen)
    api(projects.dataflowCodegenUtil)
    api(projects.dataflowComponentCore)

    testImplementation(libs.junit)
    testImplementation(libs.compileTesting)
    testImplementation(projects.dataflowComponentCodegen)
}

group = "dataflow"
description = "dataflow-component-codegen-test"
