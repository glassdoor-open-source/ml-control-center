/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowComponentMetadataGeneratorTest.java
 */
package dataflow.annotation.processor;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import static org.junit.Assert.*;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import org.junit.Test;

public class DataFlowComponentMetadataGeneratorTest {

    @Test
    public void process_validValueProvider() {
        Compilation compilation =
                javac().withProcessors(new DataFlowComponentMetadataGenerator())
                        .compile(JavaFileObjects.forResource("ValidValueProvider.java"));
        assertThat(compilation).succeeded();
    }

    @Test
    public void process_missingGetValueMethod() {
        verifyFailedCompilation(
                "InvalidValueProvider_missingGetValueMethod.java",
                "must have a single public getValue method");
    }

    @Test
    public void process_missingOutputValueAnnotation() {
        verifyFailedCompilation(
                "InvalidValueProvider_missingOutputValueAnnotation.java",
                "must have a single public getValue method");
    }

    @Test
    public void process_privateGetValueMethod() {
        verifyFailedCompilation(
                "InvalidValueProvider_privateGetValueMethod.java",
                "must have a single public getValue method");
    }

    @Test
    public void process_staticGetValueMethod() {
        verifyFailedCompilation(
                "InvalidValueProvider_staticGetValueMethod.java",
                "must have a single public getValue method");
    }

    @Test
    public void process_abstractClass() {
        verifyFailedCompilation("InvalidValueProvider_abstractClass.java", "cannot be abstract");
    }

    @Test
    public void process_missingInputValueAnnotation() {
        verifyFailedCompilation(
                "InvalidValueProvider_missingInputValueAnnotation.java",
                "getValue parameters must be annotated with InputValue or InputValues");
    }

    @Test
    public void process_inputValuesAndOtherParam() {
        verifyFailedCompilation(
                "InvalidValueProvider_inputValuesAndOtherParam.java",
                "InputValues annotated parameter must be the only method parameter");
    }

    @Test
    public void process_duplicateInputName() {
        verifyFailedCompilation(
                "InvalidValueProvider_duplicateInputName.java", "Duplicate input name in1");
    }

    @Test
    public void process_missingConstructor() {
        verifyFailedCompilation(
                "InvalidValueProvider_missingConstructor.java",
                "must have a single public constructor annotated with DataFlowConfigurable");
    }

    @Test
    public void process_missingConstructorAnnotation() {
        verifyFailedCompilation(
                "InvalidValueProvider_missingConstructorAnnotation.java",
                "must have a single public constructor annotated with DataFlowConfigurable");
    }

    @Test
    public void process_nested_not_static() {
        verifyFailedCompilation(
                "InvalidValueProvider_nested_not_static.java",
                "must be static when nested in another class");
    }

    private static void verifyFailedCompilation(String resourceName, String errorMessageSubstring) {
        try {
            Compilation compilation =
                    javac().withProcessors(new DataFlowComponentMetadataGenerator())
                            .compile(JavaFileObjects.forResource(resourceName));
            assertThat(compilation).failed();
            fail("Expected exception was not thrown: " + compilation.diagnostics());
        } catch (Exception ex) {
            assertTrue(ex.getMessage(), ex.getMessage().contains(errorMessageSubstring));
        }
    }
}
