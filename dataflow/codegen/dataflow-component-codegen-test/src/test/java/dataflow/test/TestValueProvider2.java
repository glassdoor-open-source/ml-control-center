/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider2.java
 */
package dataflow.test;

import com.google.common.base.Joiner;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;
import java.util.Map;

@DataFlowComponent
public class TestValueProvider2 {

    private String separator;

    @DataFlowConfigurable
    public TestValueProvider2(@DataFlowConfigProperty String separator) {
        this.separator = separator;
    }

    @OutputValue
    public String getValue(@InputValues Map<String, Object> inputs) {
        return Joiner.on(separator).join(inputs.entrySet());
    }
}
