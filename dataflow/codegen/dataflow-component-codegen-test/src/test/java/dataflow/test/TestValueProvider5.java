/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider5.java
 */
package dataflow.test;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.OutputValue;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@DataFlowComponent()
public class TestValueProvider5 {

    private List<String> strings;

    @DataFlowConfigurable
    public TestValueProvider5(@DataFlowConfigProperty List<String> strings) {
        this.strings = strings;
    }

    @OutputValue
    public CompletableFuture<Integer> getValue() {
        return CompletableFuture.completedFuture(strings.size());
    }

    public List<String> getStrings() {
        return strings;
    }
}
