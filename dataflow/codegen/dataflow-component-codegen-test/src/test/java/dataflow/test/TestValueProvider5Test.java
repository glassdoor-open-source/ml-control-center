/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider5Test.java
 */
package dataflow.test;

import static org.junit.Assert.*;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.environment.DataFlowEnvironment;
import java.util.concurrent.ExecutionException;
import org.junit.Before;
import org.junit.Test;

public class TestValueProvider5Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider5Metadata.INSTANCE;

    private TestValueProvider5Builder builder;

    @Before
    public void setUp() {
        final DataFlowEnvironment env = new DataFlowEnvironment();
        builder = new TestValueProvider5Builder(env);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertTrue(METADATA.isAsync());
        assertTrue(METADATA.hasAsyncGet());

        assertEquals(
                "java.util.List<java.lang.String>",
                METADATA.getConfigurableObjectMetadata()
                        .getPropertyMetadata("strings")
                        .getRawType());
        assertTrue(METADATA.getInputMetadata().isEmpty());

        assertEquals(Integer.class.getName(), METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_listProperty() throws ExecutionException, InterruptedException {
        TestValueProvider5 valueProvider =
                builder.build(
                        ImmutableMap.<String, Object>builder()
                                .put("strings", Lists.newArrayList("a", "b", "c"))
                                .build(),
                        ImmutableMap.<String, Object>builder().build());
        assertEquals((Integer) 3, valueProvider.getValue().get());
    }
}
