/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  TestValueProvider6Test.java
 */
package dataflow.test;

import static org.junit.Assert.*;

import com.google.common.collect.ImmutableMap;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.config.RawConfigurableObjectConfig;
import dataflow.core.engine.ValueReference;
import dataflow.core.environment.DataFlowEnvironment;
import org.junit.Before;
import org.junit.Test;

public class TestValueProvider6Test {

    private static final DataFlowComponentMetadata METADATA = TestValueProvider6Metadata.INSTANCE;

    private DataFlowEnvironment env;
    private TestValueProvider6Builder builder;

    @Before
    public void setUp() {
        env = new DataFlowEnvironment();
        builder = new TestValueProvider6Builder(env);
    }

    @Test
    public void testMetadata() {
        assertFalse(METADATA.hasDynamicInput());
        assertFalse(METADATA.isAsync());
        assertFalse(METADATA.hasAsyncGet());

        assertEquals(
                TestProperty.class.getName(),
                METADATA.getConfigurableObjectMetadata()
                        .getPropertyMetadata("testProperty")
                        .getRawType());
        assertTrue(METADATA.getInputMetadata().isEmpty());

        assertEquals(TestProperty.class.getName(), METADATA.getOutputMetadata().getRawType());
    }

    @Test
    public void testBuilder_customPropertyObject() {
        RawConfigurableObjectConfig rawTestPropertyConfig =
                new RawConfigurableObjectConfig(
                        "TestProperty", ImmutableMap.of("value1", 7, "value2", "str"));

        TestValueProvider6 valueProvider =
                builder.build(
                        ImmutableMap.<String, Object>builder()
                                .put("testProperty", rawTestPropertyConfig)
                                .build(),
                        ImmutableMap.<String, Object>builder().build());
        TestProperty testProperty = valueProvider.getValue();
        assertEquals(7, testProperty.getValue1());
        assertEquals("str", testProperty.getValue2());
    }

    @Test
    public void testBuilder_customPropertyObject_missing() {
        try {
            builder.build(
                    ImmutableMap.<String, Object>builder().build(),
                    ImmutableMap.<String, Object>builder().build());
            fail("Expected exception was not thrown");
        } catch (IllegalArgumentException ex) {
            // Expected.
        }
    }

    @Test
    public void testBuilder_customPropertyObject_valueReference() {
        TestValueProvider6 valueProvider =
                builder.build(
                        ImmutableMap.<String, Object>builder()
                                .put("testProperty", new ValueReference("myVal"))
                                .build(),
                        ImmutableMap.<String, Object>builder()
                                .put("myVal", new TestProperty(9, "test"))
                                .build());

        TestProperty testProperty = valueProvider.getValue();
        assertEquals(9, testProperty.getValue1());
        assertEquals("test", testProperty.getValue2());
    }
}
