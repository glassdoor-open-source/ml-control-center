package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;

@DataFlowComponent
public class InvalidValueProvider_missingOutputValueAnnotation {

    @DataFlowConfigurable
    public InvalidValueProvider_missingOutputValueAnnotation() {
    }

    public String getValue(@InputValue String in1) {
        return null;
    }
}
