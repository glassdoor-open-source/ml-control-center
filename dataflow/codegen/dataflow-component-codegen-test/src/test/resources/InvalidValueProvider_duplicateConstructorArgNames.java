package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_duplicateConstructorArgNames {

    @DataFlowConfigurable
    public InvalidValueProvider_duplicateConstructorArgNames(
            @DataFlowConfigProperty String param1,
            @DataFlowConfigProperty(name = "param1") String param2) {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}
