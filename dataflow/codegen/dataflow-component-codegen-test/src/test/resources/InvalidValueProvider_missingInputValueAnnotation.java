package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_missingInputValueAnnotation {

    @DataFlowConfigurable
    public InvalidValueProvider_missingInputValueAnnotation() {
    }

    @OutputValue
    public String getValue(String in1) {
        return null;
    }
}
