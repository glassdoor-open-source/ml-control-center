package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;

@DataFlowComponent
public class InvalidValueProvider_missingGetValueMethod {

    @DataFlowConfigurable
    public InvalidValueProvider_missingGetValueMethod() {
    }
}
