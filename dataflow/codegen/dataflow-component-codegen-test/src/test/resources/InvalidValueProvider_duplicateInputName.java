package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public class InvalidValueProvider_duplicateInputName {

    @DataFlowConfigurable
    public InvalidValueProvider_duplicateInputName() {
    }

    @OutputValue
    public String getValue(@InputValue String in1, @InputValue(name = "in1") String in2) {
        return null;
    }
}
