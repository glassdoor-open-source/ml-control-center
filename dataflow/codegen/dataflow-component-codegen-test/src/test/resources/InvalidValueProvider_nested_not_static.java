package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.OutputValue;

public class InvalidValueProvider_nested_not_static {

    @DataFlowComponent
    public class InvalidValueProvider_nested_not_static_nested {

        private String param;

        @DataFlowConfigurable
        public InvalidValueProvider_nested_not_static_nested(@DataFlowConfigProperty String param) {
            this.param = param;
        }

        @OutputValue
        public String getValue() {
            return param;
        }
    }
}
