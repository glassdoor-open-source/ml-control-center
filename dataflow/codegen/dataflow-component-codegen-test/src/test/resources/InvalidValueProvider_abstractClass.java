package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;

@DataFlowComponent
public abstract class InvalidValueProvider_abstractClass {

    @DataFlowConfigurable
    public InvalidValueProvider_abstractClass() {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}
