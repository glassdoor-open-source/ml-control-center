package dataflow.test;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import java.util.List;

@DataFlowComponent
public class ValidValueProvider {

    @DataFlowConfigurable
    public ValidValueProvider(@DataFlowConfigProperty String prop1,
            @DataFlowConfigProperty List<String> prop2) {
    }

    @OutputValue
    public String getValue(@InputValue String in1) {
        return null;
    }
}
