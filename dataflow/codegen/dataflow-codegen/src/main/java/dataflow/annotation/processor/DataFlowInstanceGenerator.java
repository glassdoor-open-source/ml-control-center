/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataFlowInstanceGenerator.java
 */
package dataflow.annotation.processor;

import static dataflow.codegen.util.annotation.AnnotationProcessorUtil.printAndThrowError;
import static dataflow.core.environment.DataFlowEnvironment.DATAFLOW_PACKAGE;

import com.google.auto.service.AutoService;
import dataflow.codegen.util.annotation.DataFlowComponentMetadataBuilder;
import dataflow.codegen.util.annotation.DataFlowConfigurableObjectMetadataBuilder;
import dataflow.core.codegen.DataFlowInstanceCodeGenConfig;
import dataflow.core.codegen.DataFlowInstanceCodeGenerator;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.metadata.DataFlowComponentMetadata;
import dataflow.core.component.metadata.DataFlowConfigurableObjectMetadata;
import dataflow.core.config.DataFlowConfig;
import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.doc.ReferenceDocGenerator;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import dataflow.core.parser.DataFlowParser;
import dataflow.core.registry.DataFlowRegistry;
import dataflow.core.util.IndentPrintWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;
import org.yaml.snakeyaml.Yaml;

/** Generates the source code dataflow instances */
@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
@SuppressWarnings("unused")
public class DataFlowInstanceGenerator extends AbstractProcessor {

    private static final String DATAFLOW_REF_BASE_PATH = "dataflow_ref_doc/";

    private final DataFlowEnvironment env =
            new DataFlowEnvironment("base", new SimpleDependencyInjector(), null);

    public DataFlowInstanceGenerator() {
        super();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        DataFlowComponentMetadataBuilder componentMetadataBuilder =
                new DataFlowComponentMetadataBuilder(processingEnv);
        DataFlowConfigurableObjectMetadataBuilder configurableObjectMetadataBuilder =
                new DataFlowConfigurableObjectMetadataBuilder(processingEnv);
        // Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
        roundEnv.getElementsAnnotatedWith(DataFlowComponent.class)
                .forEach(
                        componentElement -> {
                            DataFlowComponentMetadata metadata =
                                    componentMetadataBuilder.buildMetadata(
                                            (TypeElement) componentElement);
                            env.getRegistry()
                                    .getComponentRegistry()
                                    .registerComponent(metadata, null);
                        });
        roundEnv.getElementsAnnotatedWith(DataFlowConfigurable.class)
                .forEach(
                        element -> {
                            DataFlowConfigurableObjectMetadata metadata =
                                    configurableObjectMetadataBuilder.buildMetadata(
                                            (TypeElement) element.getEnclosingElement());
                            env.getRegistry()
                                    .getComponentRegistry()
                                    .registerPropertyObjectBuilder(metadata.getClassName(), null);
                        });

        if (roundEnv.processingOver()) {
            List<DataFlowConfig> dataFlowConfigs = new ArrayList<>();
            DataflowDiscoveryConfig discoveryConfig = null;
            try {
                byte[] dataflowYaml = null;
                try {
                    File dataflowFile =
                            new File(
                                    processingEnv
                                            .getFiler()
                                            .getResource(
                                                    StandardLocation.CLASS_OUTPUT,
                                                    "",
                                                    "dataflow.yaml")
                                            .toUri()
                                            .getPath());
                    if (dataflowFile.exists()) {
                        dataflowYaml =
                                processingEnv
                                        .getFiler()
                                        .getResource(
                                                StandardLocation.CLASS_OUTPUT, "", "dataflow.yaml")
                                        .getCharContent(true)
                                        .toString()
                                        .getBytes(StandardCharsets.UTF_8);
                    } else {
                        dataflowYaml =
                                processingEnv
                                        .getFiler()
                                        .getResource(
                                                StandardLocation.SOURCE_PATH, "", "dataflow.yaml")
                                        .getCharContent(true)
                                        .toString()
                                        .getBytes(StandardCharsets.UTF_8);
                    }
                } catch (Throwable ex) {
                    return false;
                }
                DataFlowDiscoveryConfigParser discoveryConfigParser =
                        new DataFlowDiscoveryConfigParser();
                discoveryConfig =
                        discoveryConfigParser.parse(new ByteArrayInputStream(dataflowYaml));

                DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
                discoveryService.registerComponents(discoveryConfig, env);

                Yaml yaml =
                        new Yaml(
                                new DataFlowParser.ConfigurableObjectConstructor(
                                        env.getRegistry()));
                DataFlowExecutionContext.createExecutionContext(env);
            } catch (Throwable t) {
                t.printStackTrace();
                printAndThrowError(
                        "Error generating dataflow instance classes: " + t.getMessage(),
                        null,
                        processingEnv);
            }

            DataFlowParser parser =
                    new DataFlowParser(
                            env.getRegistry(),
                            resourceName -> {
                                try {
                                    File resourceFile =
                                            new File(
                                                    processingEnv
                                                            .getFiler()
                                                            .getResource(
                                                                    StandardLocation.CLASS_OUTPUT,
                                                                    "",
                                                                    resourceName)
                                                            .toUri()
                                                            .getPath());
                                    if (resourceFile.exists()) {
                                        return processingEnv
                                                .getFiler()
                                                .getResource(
                                                        StandardLocation.CLASS_OUTPUT,
                                                        "",
                                                        resourceName)
                                                .openInputStream();
                                    } else {
                                        return processingEnv
                                                .getFiler()
                                                .getResource(
                                                        StandardLocation.SOURCE_PATH,
                                                        "",
                                                        resourceName)
                                                .openInputStream();
                                    }
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            });
            for (String configSource : discoveryConfig.getDataFlowConfigPaths()) {
                try {
                    String rawYamlConfig;
                    File rawYamlConfigFile =
                            new File(
                                    processingEnv
                                            .getFiler()
                                            .getResource(
                                                    StandardLocation.CLASS_OUTPUT, "", configSource)
                                            .toUri()
                                            .getPath());
                    if (rawYamlConfigFile.exists()) {
                        rawYamlConfig =
                                processingEnv
                                        .getFiler()
                                        .getResource(
                                                StandardLocation.CLASS_OUTPUT, "", configSource)
                                        .getCharContent(true)
                                        .toString();
                    } else {
                        rawYamlConfig =
                                processingEnv
                                        .getFiler()
                                        .getResource(StandardLocation.SOURCE_PATH, "", configSource)
                                        .getCharContent(true)
                                        .toString();
                    }

                    try (InputStream configIn =
                            new ByteArrayInputStream(
                                    rawYamlConfig.getBytes(StandardCharsets.UTF_8))) {
                        DataFlowConfig dataFlowConfig = parser.parse(configIn);

                        dataFlowConfigs.add(dataFlowConfig);
                        writeDataFlowInstanceClass(
                                dataFlowConfig, rawYamlConfig, env.getRegistry());
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                    printAndThrowError(
                            "Error generating dataflow instance class for "
                                    + configSource
                                    + ": "
                                    + t.getMessage(),
                            null,
                            processingEnv);
                }
            }

            try {
                ReferenceDocGenerator.generate(DATAFLOW_REF_BASE_PATH, dataFlowConfigs, env, false);
            } catch (IOException e) {
                e.printStackTrace();
                printAndThrowError(
                        "Error generating dataflow reference documentation: " + e.getMessage(),
                        null,
                        processingEnv);
            }
        }
        return false;
    }

    private void writeDataFlowInstanceClass(
            DataFlowConfig dataFlowConfig, String rawYamlConfig, DataFlowRegistry registry)
            throws IOException {
        DataFlowInstanceCodeGenerator codeGenerator = new DataFlowInstanceCodeGenerator(registry);
        String packageName = DATAFLOW_PACKAGE;
        String className = codeGenerator.getDefaultClassName(dataFlowConfig);
        DataFlowInstanceCodeGenConfig codeGenConfig =
                new DataFlowInstanceCodeGenConfig(
                        packageName,
                        className,
                        dataFlowConfig.isIncrementalMode(),
                        dataFlowConfig.isEventsEnabled(),
                        true);
        JavaFileObject sourceFile =
                processingEnv.getFiler().createSourceFile(packageName + "." + className);
        try (PrintWriter fileWriter = new PrintWriter(sourceFile.openWriter())) {
            IndentPrintWriter out = new IndentPrintWriter(fileWriter);
            out.print(codeGenerator.generateSource(dataFlowConfig, rawYamlConfig, codeGenConfig));
        }

        for (DataFlowConfig childDataFlowConfig : dataFlowConfig.getChildDataFlows().values()) {
            writeDataFlowInstanceClass(childDataFlowConfig, rawYamlConfig, registry);
        }
    }
}
