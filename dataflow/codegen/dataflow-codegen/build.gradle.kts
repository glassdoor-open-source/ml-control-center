

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(libs.googleAutoService)
    api(projects.dataflowCore)
    api(projects.dataflowCodegenUtil)
    api(libs.googleAutoService)
    testImplementation(libs.junit)
    testImplementation(libs.compileTesting)
}

group = "dataflow"
description = "dataflow-codegen"
