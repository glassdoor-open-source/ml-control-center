# DataFlow Language Guide

Pipelines are defined using a YAML configuration. A pipeline consists of a directed acyclic graph (DAG) 
of components.  Components are configured by specifying property values that are used to initialize 
the component and input values that are used to compute the output value. Connections between 
components are formed by using the output of one component as an input to another.



### Value References
The output value of a component can be referenced using $(identifier) where identifier is the id of the component. 
If the output value is a map then a value from the map can be referenced using $(identifier.key).
Values provided as input parameters to the pipeline can also be referenced using the $(identifier) syntax.

## Pipeline

| Name       | Type            | Description                                                                           |
|------------|-----------------|---------------------------------------------------------------------------------------|
| id         | string          | A unique identifier for the data flow pipeline                                        |
| components | List<Component> | A list of components that are connected to form the data flow graph                   |
| output     | ValueRef        | A reference to the component output value that is used as the output for the pipeline |

### Component

| Name    | Type                | Description                                                                                                                                                                                                                  |
|---------|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id      | string              | A unique identifier for the component                                                                                                                                                                                        |
| extends | string              | The identifier of a component that this component extends. A copy of the configuration of the extended component will be used as a base and then any properties set on this component will be applied on that.               |
| input   | Map<string, object> | A map of name to value that is used as the input to the component. The values are passed as the parameters to the component's getValue method. The values can be constants or references to other component's output values  |
| timeout | string              | The maximum time to wait for an asynchronous component to complete execution before timing out. This is a string in java Duration format see: Duration.parse                                                                 |
| sink    | boolean             | Usually components that are not used either directly or indirectly to get the pipeline output value are pruned. This flag can be used to ensure that the component is executed even if there are no references to the output |

any other properties set on a component will be passed to the component's constructor.

## Component Library
DataFlow provides a library of reusable components. Some of the most commonly used components are 
described below:

### ConstValueProvider
A component that provides a given constant value.
### RESTServiceValueProvider
A component that outputs a value from a REST service call.
### SQLQueryValueProvider
A component that outputs result records from a SQL query
### ExpressionValueProvider
A component that outputs the result of an expression. @(identifier) is used instead of $(identifier) in expression strings

## Custom Components
The DataFlow language can be extended by implementing new components.
Custom components are defined by implementing a class annotated with the @DataFlowComponent annotation.
- The class must define a single constructor annotated with @DataFlowConfigurable with parameters annotated with @DataFlowConfigProperty. The properties from the configuration will be passed as constructor parameters.
- The class must define a single method named getValue annotated with @OutputValue. The return value of the method will be used as the output of the component.
- The getValue method parameters should be annotated with @InputValue. Values from other components will be passed as the method parameters as specified in the configuration

The following is an example of a custom component:

    @DataFlowComponent  
    public class BinnedValueProvider {
      private List<Double> bins;

      @DataFlowConfigurable
      public BinnedValueProvider(@DataFlowConfigProperty List<Double> bins) {
        this.bins = bins;
      }

      @OutputValue
      public int getValue(@InputValue double value) {
        for (int idx = 0; idx < bins.size(); idx++) {
            if (bins.get(idx) > value) {
                return idx;
            }
        }
        return bins.size();
      }
    }

In order to be used the component must be registered with the DataFlow engine. 
This can be done using the registerComponent method for example:

    engine.registerComponent(BinnedValueProviderMetadata.INSTANCE, 
       new BinnedValueProviderBuilder(engine));


The metadata and builder classes are dynamically generated using an annotation processor.

## Asynchronous Execution
A component can be made asynchronous in one of the following ways:
- The getValue method returns a CompletableFuture. In this case the provider will be responsible for asynchronously completing the future.
- executeAsync = true is set in the @DataFlowComponent annotation. In this case the getValue method will be executed asynchronously in a separate thread.

## Dynamic Inputs
It is possible for a component to have inputs that are defined dynamically as part of the configuration. In this case the getValue method should accept a single Map<String, Object> parameter that is annotated with @InputValues. The map is a map of the input name to its value

## Placeholder Strings
If a component reference is used in a string then it will be replaced with the output value from the component before the string is passed to the component.

## Dynamic Code Generation
For advanced use cases it is possible to create components whose functionality is implemented using dynamically generated code. See ExpressionValueProviderCodeGenerator for an example of how to do this 
