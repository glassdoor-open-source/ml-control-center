

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowComponentCodegen)
    annotationProcessor(projects.dataflowCodegen)
    
    api(projects.dataflowCore)
    api(projects.dataflowAvro)
    api(projects.dataflowAws)
    api(projects.dataflowData)
    api(projects.dataflowElasticsearch)
    api(projects.dataflowPostgres)
    api(projects.dataflowPythonJep)
    api(projects.dataflowRest)
    api(projects.dataflowSql)
    api(projects.dataflowStream)
    api(projects.dataflowComponentCore)
    implementation(projects.dataflowComponentCodegen)
    implementation(projects.dataflowCodegen)
}

configurations.all {
    exclude(group = "commons-logging", module = "commons-logging")
}

group = "dataflow"
description = "dataflow-docgen"
