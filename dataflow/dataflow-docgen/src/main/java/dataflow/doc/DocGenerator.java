/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DocGenerator.java
 */
package dataflow.doc;

import dataflow.core.config.DataflowDiscoveryConfig;
import dataflow.core.doc.ReferenceDocGenerator;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowDiscoveryService;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.core.parser.DataFlowDiscoveryConfigParser;
import java.io.IOException;
import java.util.Collections;

public class DocGenerator {

    public static void main(String[] args) throws IOException {
        DataFlowEnvironment env =
                new DataFlowEnvironment("docGen", new SimpleDependencyInjector(), 0L);
        DataflowDiscoveryConfig discoveryConfig =
                new DataFlowDiscoveryConfigParser()
                        .parse(ClassLoader.getSystemResourceAsStream("dataflow.yaml"));
        DataFlowDiscoveryService discoveryService = new DataFlowDiscoveryService();
        discoveryService.register(discoveryConfig, env);
        String basePath = args != null && args.length > 0 ? args[0] : "dataflow_ref_doc/";
        ReferenceDocGenerator.generate(basePath, Collections.emptyList(), env, true);
    }
}
