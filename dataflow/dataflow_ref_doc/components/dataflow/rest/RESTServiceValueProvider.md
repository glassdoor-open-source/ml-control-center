# RESTServiceValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| serviceId | String | true |
| path | String | true |
| method | org.springframework.http.HttpMethod | false |
| contentType | String | false |
| authNames | String | false |
| accepts | String | false |
| returnType | String | false |
| cacheId | String | false |
| retry | dataflow.retry.Retry | false |
| metricNamePrefix | String | false |
| metricTags | Map\<String, String> | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| headers | Map\<String, Object> | false |
| body | Object | false |
| queryParams | Map\<String, Object> | false |
| formParams | Map\<String, Object> | false |

## Output
Object
