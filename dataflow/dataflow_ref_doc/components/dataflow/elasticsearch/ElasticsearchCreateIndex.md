# ElasticsearchCreateIndex

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| clusterId | String | true |
| type | String | true |
| name | String | true |
| version | String | false |
| schema | DataSource | true |
| schemaVersion | int | true |
| numPriorSchemaVersionsToKeep | Integer | false |
| numActiveBackupsForCurrentVersion | Integer | false |
| minIndexAgeForCleanupHours | Integer | false |
| disableReplicas | Boolean | false |
| transactionLogAsync | Boolean | false |
| cleanupFailedIndexes | Boolean | false |
| metricNamePrefix | String | false |
| metricTags | Map\<String, String> | false |

## Inputs
None

## Output
dataflow.elasticsearch.IndexMetadata
