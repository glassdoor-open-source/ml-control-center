# ElasticsearchSetActiveIndex

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| clusterId | String | true |  |
| type | String | true |  |
| name | String | true |  |
| version | String | true |  |
| switchDurationMinutes | Integer | false |  |
| replicaCount | Integer | false |  |
| forceMergeSegments | Boolean | false |  |
| transactionLogAsync | Boolean | false |  |
| waitForHealthyCluster | Boolean | false | Wait for the cluster to be healthy before switching |
| maxNumSegments | Integer | false |  |
| metricNamePrefix | String | false |  |
| metricTags | Map\<String, String> | false |  |

## Inputs
None

## Output
dataflow.elasticsearch.IndexMetadata
