# ElasticsearchDeleteDocs

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| clusterId | String | true |
| type | String | true |
| name | String | true |
| version | String | false |
| waitForIndexingToFinish | Boolean | false |
| timeoutMillis | Integer | false |
| retryCount | Integer | false |
| retryIntervalMillis | Integer | false |
| metricNamePrefix | String | false |
| metricTags | Map\<String, String> | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| docIds | Set\<String> | true |

## Output
Set\<String>
