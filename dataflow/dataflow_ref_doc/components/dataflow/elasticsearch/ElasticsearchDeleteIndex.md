# ElasticsearchDeleteIndex

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| clusterId | String | true |
| type | String | true |
| name | String | true |
| version | String | true |
| metricNamePrefix | String | false |
| metricTags | Map\<String, String> | false |

## Inputs
None

## Output
dataflow.elasticsearch.IndexMetadata
