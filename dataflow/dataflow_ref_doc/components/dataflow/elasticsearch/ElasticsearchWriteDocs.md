# ElasticsearchWriteDocs

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| clusterId | String | true |
| type | String | true |
| name | String | true |
| version | String | false |
| waitForIndexingToFinish | Boolean | false |
| timeoutMillis | Integer | false |
| retryCount | Integer | false |
| retryIntervalMillis | Integer | false |
| metricNamePrefix | String | false |
| metricTags | Map\<String, String> | false |

## Inputs
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| documents | Object | true | The documents to write to the index. The documents must be json serializable. Can be a map of doc id string to document or a list of documents. If a list is provided then the documents will be added with auto generated ids. |

## Output
| Type | Description |
| :---- | ----------- |
| Map\<String, Object> | Returns a map of document id string to document |
