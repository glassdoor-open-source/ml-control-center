# DataRecordsSplitList
Splits a list contained in DataRecords so that a new record is created for each item in the list. Columns from the original record and data from the list items can be copied to columns in the new record

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| listColumn | String | false | The column that contains the list |
| listPath | String | false | The path of the list within the column. If the column itself is a list then this does not need to be set |
| listPaths | List\<DataRecordPath> | false | The paths of the lists if multiple lists should be processed |
| includeColumns | Set\<String> | false | The names of columns in the original record to copy to the new records |
| excludeColumns | Set\<String> | false | The names of columns in the original record to exclude. All other columns will be copied to the new records |
| columnNameItemPathMap | Map\<String, String> | false | A map of column names to paths in the list items. The values at the given paths will be copied to the new records in columns with the specified names. If this is not not set and the list items are maps (or json objects) then the map values will be copied to columns using the map keys as the column names |
| keyColumns | List\<String> | false | The key column(s) of the newly created records |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| records | DataRecords | true |

## Output
DataRecords
