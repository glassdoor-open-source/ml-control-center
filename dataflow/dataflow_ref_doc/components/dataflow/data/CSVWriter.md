# CSVWriter

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| dataSource | DataSource | true |
| columnNames | List\<String> | false |
| delimiter | String | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| records | DataRecords | true |

## Output
DataRecords
