# TransformDataRecords

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| transform | String | true |
| recordTransform | String | true |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| records | DataRecords | true |

## Output
DataRecords
