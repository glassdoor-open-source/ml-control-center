# LeftJoinDataRecords

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| joinColumn | String | false | The column to use for the join. This column will be used for both the left and right records |
| leftJoinColumn | String | false | The column in the left records to use for the join |
| rightJoinColumn | String | false | The column in the right records to use for the join |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| left | DataRecords | true |
| right | DataRecords | true |

## Output
DataRecords
