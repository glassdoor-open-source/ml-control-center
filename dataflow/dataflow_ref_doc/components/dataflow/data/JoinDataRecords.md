# JoinDataRecords

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| dataRecordsList | List\<DataRecords> | true |

## Output
DataRecords
