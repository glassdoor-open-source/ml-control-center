# CSVRecordStreamValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| source | DataSource | true |
| schema | String | true |
| recordValidators | List\<RecordValidator> | false |
| delimiter | String | false |
| withFirstRecordAsHeader | Boolean | false |
| failOnInvalidRecord | Boolean | false |
| skipInvalidRecords | Boolean | false |
| dedupeStrings | Boolean | false |

## Inputs
None

## Output
stream.Stream\<Map\<String, Object>>
