# PostgreSQLWriterComponent
Writes the value to a database.

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| clusterId | String | false | A string that identifies the cluster to useA cluster with the given id should be registered with the cluster manager |
| tableName | String | true | Name of destination table to be created |
| keyColumnNames | List\<String> | true | Key columns in destination table |
| columns | List\<Map\<String, String>> | true | List of column definitions. A column definition is a map with key 'name' and 'type' |
| createSQL | String | false | Optional SQL statement to create table |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| dataRecords | Object | true |

## Output
DataRecords
