# SQLQueryValueProvider
Provides a value from a SQL query

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| baseQuery | String | true | The query to use to get values. Input values will be used to replace named parameters.<br>Example:<br>select age from products where id = :product_id<br><br>In this example the input named product_id will be used to replace :product_id<br> |
| countBaseQuery | String | false | The base query to get the estimated number of result records |
| whereClause | String | false | The where clause used to filter the resultsInput values will be used to replace named parameters.<br><br>Example: age >= :min_age |
| keyColumns | List\<String> | false | The names of the columns that form the key of the table |
| recordsKeyColumns | List\<String> | false | The names of the columns that form the key of output records. Will default to keyColumns if not specified |
| orderByClause | String | false | Order by clause |
| lastUpdateTimeColumn | String | false | The name of the column that holds the last updated time |
| decorators | List\<SQLQueryDecorator> | false | Decorators that modify the query |
| outputMode | SQLDatabaseOutputMode | false | SINGLE_ROW (default)<br>The provider will output a single map<string, object> instead of a list. If the query returns multiple rows only the first row is used.<br><br>MULTI_ROW<br>The provider will output a list rows where each row is a map<string, object><br><br>STREAM<br>The provider will output a stream of rows where each row is a map<string, object><br><br>SINGLE_COLUMN<br>The provider will output a single value. If the query returns multiple rows only the first row is used. Only one column can be specified in the columns list.<br> |
| clusterId | String | false | A string that identifies the cluster to useA cluster with the given id should be registered with the cluster manager |
| fromKey | Object | false | The starting key |
| fetchSize | Integer | false | The number of rows returned in a batch |
| maxRowCount | Integer | false | The maximum number of rows to return |
| batchSize | Integer | false |  |
| dataSource | javax.sql.DataSource | false |  |
| iterationMode | SQLDatabaseIterationMode | false |  |
| maxKeyQuery | String | false | A query to get the maximum key if using the KEY_BASED iteration mode |
| retry | dataflow.retry.Retry | false |  |
| metricNamePrefix | String | false |  |
| metricTags | Map\<String, String> | false |  |

## Inputs
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| paramValues | Map\<String, Object> | false | The param values. The name of each param should match a placeholder in the baseQuery or whereClause. |

## Output
Object
