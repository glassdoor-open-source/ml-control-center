# SQLQuery
Provides results from a SQL query

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| baseQuery | String | true | The query to use to get values. Input values will be used to replace named parameters.<br>Example:<br>select age from products where id = :product_id<br><br>In this example the input named product_id will be used to replace :product_id<br> |
| countBaseQuery | String | false | The base query to get the estimated number of result records |
| whereClause | String | false | The where clause used to filter the resultsInput values will be used to replace named parameters.<br><br>Example: age >= :min_age |
| keyColumns | List\<String> | false | The names of the columns that form the key of the table |
| recordsKeyColumns | List\<String> | false | The names of the columns that form the key of output records. Will default to keyColumns if not specified |
| orderByClause | String | false | Order by clause |
| lastUpdateTimeColumn | String | false | The name of the column that holds the last updated time |
| decorators | List\<SQLQueryDecorator> | false | Decorators that modify the query |
| clusterId | String | false | A string that identifies the cluster to useA cluster with the given id should be registered with the cluster manager |
| fromKey | Object | false | The starting key |
| fetchSize | Integer | false | The number of rows returned in a batch |
| maxRowCount | Integer | false | The maximum number of rows to return |
| dataSource | javax.sql.DataSource | false |  |
| iterationMode | SQLDatabaseIterationMode | false |  |
| retry | dataflow.retry.Retry | false |  |
| metricNamePrefix | String | false |  |
| metricTags | Map\<String, String> | false |  |

## Inputs
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| paramValues | Map\<String, Object> | false | The param values. The name of each param should match a placeholder in the baseQuery or whereClause. |

## Output
DataRecords
