# TestRetryValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| failureCount | int | true |
| retry | dataflow.retry.Retry | false |

## Inputs
None

## Output
String
