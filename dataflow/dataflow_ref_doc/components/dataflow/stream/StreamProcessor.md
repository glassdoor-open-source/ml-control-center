# StreamProcessor

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| batchSize | Integer | false |  |
| parallelism | Integer | false |  |
| prefetchCount | Integer | false |  |
| prefetchAllItems | Boolean | false |  |
| dryRun | Boolean | false |  |
| dryRunRecordCount | Integer | false |  |
| properties | Map\<String, Object> | false |  |
| dryRunProperties | Map\<String, Object> | false |  |
| timeoutSeconds | Long | false |  |
| retry | dataflow.retry.Retry | false |  |
| metricNamePrefix | String | false |  |
| itemValueName | String | false | The name of the item value in the mapping DataFlow |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| stream | stream.Stream\<?> | true |
| pipeline | DataFlowInstance | true |
| beforeStart | DataFlowInstance | false |
| afterComplete | DataFlowInstance | false |

## Output
Object
