# DataFlowValueProvider
Provides the output value of the dataflow with the given id and parameters.

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| dataFlowId | String | true |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| values | Map\<String, Object> | false |

## Output
Object
