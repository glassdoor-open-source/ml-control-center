# DataFlowInstanceValueProvider
Provides a DataFlowInstance for a given DataFlow id.

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| dataFlowId | String | true |
| values | Map\<String, Object> | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| values | Map\<String, Object> | false |

## Output
DataFlowInstance
