# MappedListValueProvider

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| itemValueName | String | false | The name of the item value in the mapping DataFlow |
| parallel | Boolean | false |  |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| list | List\<?> | true |
| mappingDataFlow | DataFlowInstance | true |

## Output
List\<?>
