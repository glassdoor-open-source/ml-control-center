# ListItemValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| index | int | true |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| list | List\<Object> | true |

## Output
Object
