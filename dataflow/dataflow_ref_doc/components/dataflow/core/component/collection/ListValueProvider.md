# ListValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| itemKeys | List\<String> | true |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Map\<String, Object> | false |

## Output
List\<Object>
