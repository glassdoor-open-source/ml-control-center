# CollectionSizeValueProvider

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| collection | Collection | true |

## Output
int
