# MapValueProvider

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Map\<String, Object> | false |

## Output
Map\<String, Object>
