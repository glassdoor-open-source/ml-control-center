# ListForEach

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| itemValueName | String | false | The name of the item value in the DataFlow |
| parallel | Boolean | false |  |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| list | List\<?> | true |
| doFlow | DataFlowInstance | true |

## Output
Void
