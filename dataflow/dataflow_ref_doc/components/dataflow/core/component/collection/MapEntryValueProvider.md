# MapEntryValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| key | String | true |
| failIfMissing | Boolean | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| map | Map | true |

## Output
Object
