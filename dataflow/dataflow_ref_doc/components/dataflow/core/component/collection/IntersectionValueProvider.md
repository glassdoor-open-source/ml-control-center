# IntersectionValueProvider

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| collection1 | Collection | true |
| collection2 | Collection | true |

## Output
Collection
