# BinaryFileWriter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| out | OutputStream | true |
| data | byte[] | true |

## Output
Void
