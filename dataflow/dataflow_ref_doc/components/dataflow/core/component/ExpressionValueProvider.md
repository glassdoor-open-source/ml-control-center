# ExpressionValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| expression | String | true |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| inputValues | Map\<String, Object> | false |

## Output
Object
