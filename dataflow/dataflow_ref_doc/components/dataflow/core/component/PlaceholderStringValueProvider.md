# PlaceholderStringValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| placeholderString | String | true |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| replacementValues | Map\<String, Object> | false |

## Output
String
