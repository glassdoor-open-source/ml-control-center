# Logger

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| name | String | false |
| level | String | false |
| enabled | Boolean | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Map\<String, Object> | false |

## Output
Void
