# ConstValueProvider
A ValueProvider that provides a given constant value.

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| value | Object | true | The value that will be provided |

## Inputs
None

## Output
| Type | Description |
| :---- | ----------- |
| Object | The specified constant value |
