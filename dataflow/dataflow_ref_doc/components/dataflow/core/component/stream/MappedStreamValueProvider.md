# MappedStreamValueProvider

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| itemValueName | String | false | The name of the item value in the mapping DataFlow |
| parallel | Boolean | false |  |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| stream | stream.Stream\<?> | true |
| mappingDataFlow | DataFlowInstance | true |

## Output
stream.Stream\<?>
