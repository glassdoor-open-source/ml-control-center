# FilteredStreamValueProvider

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| itemValueName | String | false |
| parallel | Boolean | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| stream | stream.Stream\<?> | true |
| filterDataFlow | DataFlowInstance | true |

## Output
stream.Stream\<?>
