# FallbackValueProvider

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | Object | true |
| fallbackValues | List | true |

## Output
Object
