# String_to_DoubleValue_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
dataflow.core.type.DoubleValue
