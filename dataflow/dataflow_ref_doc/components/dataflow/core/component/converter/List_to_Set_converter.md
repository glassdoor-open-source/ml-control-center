# List_to_Set_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| list | List | true |

## Output
Set
