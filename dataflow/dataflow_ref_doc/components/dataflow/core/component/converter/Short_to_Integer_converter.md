# Short_to_Integer_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Short | true |

## Output
Integer
