# Double_to_double_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | Double | true |

## Output
double
