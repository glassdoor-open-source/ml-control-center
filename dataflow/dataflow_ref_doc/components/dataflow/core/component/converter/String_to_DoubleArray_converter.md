# String_to_DoubleArray_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
double[]
