# String_to_LongValue_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
dataflow.core.type.LongValue
