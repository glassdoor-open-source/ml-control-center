# Long_to_long_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Long | true |

## Output
long
