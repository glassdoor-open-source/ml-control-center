# String_to_Integer_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
Integer
