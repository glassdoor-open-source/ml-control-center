# IntegerValue_to_Integer_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | dataflow.core.type.IntegerValue | true |

## Output
Integer
