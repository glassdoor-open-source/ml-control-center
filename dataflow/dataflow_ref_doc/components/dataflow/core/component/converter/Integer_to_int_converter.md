# Integer_to_int_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Integer | true |

## Output
int
