# IntegerValue_to_int_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | dataflow.core.type.IntegerValue | true |

## Output
int
