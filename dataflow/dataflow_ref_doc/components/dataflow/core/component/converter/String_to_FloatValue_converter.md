# String_to_FloatValue_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
dataflow.core.type.FloatValue
