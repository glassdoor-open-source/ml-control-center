# String_to_Enum_converter

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| toType | dataflow.core.type.ValueType | true |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
Enum
