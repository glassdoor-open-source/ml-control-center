# String_to_List_converter

## Properties
| Name | Type | Required |
| :---- | ---- | -------- |
| toValueType | dataflow.core.type.ValueType | false |
| delimiter | String | false |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
List
