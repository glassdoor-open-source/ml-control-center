# Long_to_LongValue_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Long | true |

## Output
dataflow.core.type.LongValue
