# Integer_to_IntegerValue_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Integer | true |

## Output
dataflow.core.type.IntegerValue
