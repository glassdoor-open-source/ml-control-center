# String_to_Date_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
Date
