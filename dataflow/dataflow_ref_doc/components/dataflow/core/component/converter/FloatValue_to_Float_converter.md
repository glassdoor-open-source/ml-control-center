# FloatValue_to_Float_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | dataflow.core.type.FloatValue | true |

## Output
Float
