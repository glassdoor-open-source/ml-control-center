# Double_to_DoubleValue_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | Double | true |

## Output
dataflow.core.type.DoubleValue
