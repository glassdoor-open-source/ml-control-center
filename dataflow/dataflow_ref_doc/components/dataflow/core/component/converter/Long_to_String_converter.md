# Long_to_String_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| input | Long | true |

## Output
String
