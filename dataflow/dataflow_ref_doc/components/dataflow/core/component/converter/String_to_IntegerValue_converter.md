# String_to_IntegerValue_converter

## Properties
None

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| value | String | true |

## Output
dataflow.core.type.IntegerValue
