# PythonFunctionValueProvider
Provides the value returned by calling a python function

## Properties
| Name | Type | Required | Description |
| :---- | ---- | -------- | ----------- |
| imports | List\<String> | false | Imports |
| init | String | false | Initialization code |
| functionExpr | String | true | An expression that evaluates to a function to call |
| threadCount | Integer | false | The maximum number of interpreter threads |

## Inputs
| Name | Type | Required |
| :---- | ---- | -------- |
| inputs | Map\<String, Object> | false |

## Output
Object
