/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSourceFeatureDatabaseUTest.java
 */
package mlcc.feature.core.dataflow;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.ImmutableSet;
import dataflow.avro.AvroDataRecordSerializationScheme;
import dataflow.core.datasource.LocalFileDataSource;
import dataflow.data.DataRecord;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.FileSystemUtils;
import reactor.core.publisher.Flux;

public class DataSourceFeatureDatabaseUTest {

    private static final String TEST_SCHEMA_1 =
            "{\"type\":\"record\",\"namespace\":\"test\",\"name\":\"User\",\"fields\":[{\"name\":\"Name\",\"type\":\"string\"},{\"name\":\"Age\",\"type\":\"int\"}]}";
    private DataSourceFeatureDatabase featureDB;
    private File tempDir;

    @Before
    public void setUp() throws IOException {
        tempDir = Files.createTempDirectory(getClass().getSimpleName()).toFile();
        featureDB =
                new DataSourceFeatureDatabase(
                        "test",
                        "test",
                        new LocalFileDataSource(tempDir.getAbsolutePath()),
                        new AvroDataRecordSerializationScheme(TEST_SCHEMA_1),
                        2);
    }

    @After
    public void cleanUp() {
        assertTrue(FileSystemUtils.deleteRecursively(tempDir));
    }

    @Test
    public void testWriteAndRead() throws Exception {
        FeatureMetadata featureMetadata = new FeatureMetadata();
        featureMetadata.setFeatureId("test");
        featureMetadata.setVariant("default");
        featureMetadata.setVersion(1);
        featureDB.createFeatureVersion(featureMetadata);
        try (FeatureData featureData = featureDB.getFeatureData(featureMetadata)) {
            DataRecord r1 = new DataRecord();
            r1.put("Name", "Jack");
            r1.put("Age", 30);
            featureData.write(r1);
            DataRecord r2 = new DataRecord();
            r2.put("Name", "Jill");
            r2.put("Age", 40);
            featureData.write(r2);
            DataRecord r3 = new DataRecord();
            r3.put("Name", "James");
            r3.put("Age", 72);
            featureData.write(r3);
            DataRecord r4 = new DataRecord();
            r4.put("Name", "Jenny");
            r4.put("Age", 26);
            featureData.write(r4);
        }

        try (FeatureData featureData = featureDB.getFeatureData(featureMetadata)) {
            List<DataRecord> allRecords = Flux.from(featureData.readAll()).collectList().block();
            assertEquals(4, allRecords.size());
            assertEquals(
                    ImmutableSet.of("Jack", "Jill", "James", "Jenny"),
                    allRecords.stream().map(r -> r.get("Name")).collect(Collectors.toSet()));
        }
    }
}
