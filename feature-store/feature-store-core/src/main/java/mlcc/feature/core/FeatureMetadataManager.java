/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureMetadataManager.java
 */
package mlcc.feature.core;

import java.io.IOException;
import java.util.List;
import mlcc.feature.api.FeatureMetadata;

public interface FeatureMetadataManager {

    List<FeatureMetadata> getFeatureMetadata() throws IOException;

    List<FeatureMetadata> getFeatureMetadata(String featureId) throws IOException;

    FeatureMetadata getActiveFeatureActiveVersionMetadata(String featureId, String variant)
            throws IOException;

    void setActiveFeatureMetadata(FeatureMetadata metadata) throws IOException;

    void addFeatureMetadata(FeatureMetadata metadata) throws IOException;

    void updateFeatureMetadata(FeatureMetadata metadata) throws IOException;

    void deleteFeatureMetadata(FeatureMetadata metadata) throws IOException;
}
