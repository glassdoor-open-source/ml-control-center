/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSourceFeatureData.java
 */
package mlcc.feature.core.dataflow;

import static dataflow.core.datasource.DataSourceUtil.getChildWithPath;

import dataflow.core.datasource.DataSource;
import dataflow.data.DataRecord;
import dataflow.data.DataRecordReader;
import dataflow.data.DataRecordSerializationScheme;
import dataflow.data.DataRecordWriter;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.DataFilter;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureDataKey;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.core.dataflow.DataSourceFeatureDatabase.NameProvider;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

public class DataSourceFeatureData implements FeatureData {

    private final DataSource root;
    private final FeatureMetadata metadata;
    private final DataRecordSerializationScheme serializationScheme;
    private final int recordsPerFile;
    private final NameProvider nameProvider;

    private DataRecordWriter recordWriter;
    private int currentRecordCount = 0;

    public DataSourceFeatureData(
            FeatureMetadata metadata,
            DataSource root,
            DataRecordSerializationScheme serializationScheme,
            int recordsPerFile,
            NameProvider nameProvider) {
        this.root = root;
        this.metadata = metadata;
        this.serializationScheme = serializationScheme;
        this.recordsPerFile = recordsPerFile;
        this.nameProvider = nameProvider;
    }

    @Override
    public String getFeatureId() {
        return metadata.getFeatureId();
    }

    @Override
    public String getVariant() {
        return metadata.getVariant();
    }

    @Override
    public long getVersion() {
        return metadata.getVersion();
    }

    @Override
    public FeatureMetadata getMetadata() {
        return metadata;
    }

    @Override
    public DataRecord read(FeatureDataKey key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DataRecords read(List<FeatureDataKey> keys) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DataRecords read(List<DataFilter> filters, List<String> columns) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Stream<DataRecords> readStream(
            List<DataFilter> filters, List<String> columns, int batchSize) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CompletableFuture<DataRecords> readAsync(
            List<DataFilter> filters, List<String> columns) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void write(DataRecord record) throws IOException {
        prepareFileForWrite(metadata);
        recordWriter.write(record);
        currentRecordCount++;
    }

    @Override
    public void write(DataRecords records) throws IOException, InvalidDataException {
        prepareFileForWrite(metadata);
        for (DataRecord record : records) {
            write(record);
        }
    }

    @Override
    public void write(Publisher<DataRecord> records) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void write(Publisher<DataRecord> publisher, int batchSize) {
        Flux.from(publisher)
                .buffer(batchSize)
                .subscribe(
                        recordList -> {
                            DataRecords records = new DataRecords();
                            records.setKeyColumnNames(metadata.getKeyColumnNames());
                            records.setColumnNames(metadata.getColumnNames());
                            records.addAll(recordList);
                            try {
                                write(records);
                            } catch (IOException | InvalidDataException e) {
                                throw new RuntimeException(e);
                            }
                        });
    }

    @Override
    public void delete(List<FeatureDataKey> keys) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Publisher<DataRecord> readAll() {
        return Flux.create(
                dataRecordFluxSink -> {
                    try {
                        String path = nameProvider.getFeatureDataPath(metadata);
                        DataSource featureRoot = getChildWithPath(root, path).orElse(null);
                        if (featureRoot != null) {
                            for (DataSource child : featureRoot.getChildren()) {
                                DataRecordReader reader =
                                        serializationScheme.createReader(
                                                new BufferedInputStream(child.getInputStream()));
                                while (reader.hasNext()) {
                                    dataRecordFluxSink.next(reader.next());
                                }
                            }
                        }
                    } catch (IOException e) {
                        dataRecordFluxSink.error(e);
                    }
                    dataRecordFluxSink.complete();
                });
    }

    private void prepareFileForWrite(FeatureMetadata metadata) throws IOException {
        if (currentRecordCount >= recordsPerFile) {
            try {
                recordWriter.close();
            } catch (Exception e) {
                throw new IOException("Failed to close record writer", e);
            }
            recordWriter = null;
            currentRecordCount = 0;
        }
        if (recordWriter == null) {
            String path = nameProvider.getFeatureDataPath(metadata, UUID.randomUUID().toString());
            DataSource child = getChildWithPath(root, path, true).orElse(null);
            if (child == null) {
                throw new IOException("Failed to open data source with path: " + path);
            }
            recordWriter = serializationScheme.createWriter(child.getOutputStream());
        }
    }

    @Override
    public void close() throws Exception {
        if (recordWriter != null) {
            recordWriter.close();
            recordWriter = null;
        }
    }
}
