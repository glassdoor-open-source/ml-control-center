/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureValueProvider.java
 */
package mlcc.feature.core.dataflow;

import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValues;
import dataflow.core.component.annotation.OutputValue;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.exception.DataFlowConfigurationException;
import dataflow.core.exception.DataFlowExecutionException;
import dataflow.data.DataRecords;
import dataflow.data.query.DataFilter;
import dataflow.data.query.DataQueryValueProvider;
import dataflow.data.query.KeyRangeDataFilter;
import dataflow.data.query.LastUpdateTimeFilter;
import dataflow.data.query.MaxRowCountDataFilter;
import dataflow.data.query.MultiKeyDataFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;
import mlcc.feature.api.FeatureDBManager;
import mlcc.feature.api.FeatureData;

@DataFlowComponent
public class FeatureValueProvider implements DataQueryValueProvider {

    private final FeatureDBManager featureDBManager;
    private final FeatureData featureData;
    private final List<String> columns;
    private List<DataFilter> filters;
    private List<String> keyColumns;

    @DataFlowConfigurable
    public FeatureValueProvider(
            @DataFlowConfigProperty String featureId,
            @DataFlowConfigProperty(required = false) String variant,
            @DataFlowConfigProperty(required = false) Integer version,
            @DataFlowConfigProperty(required = false) String dbType,
            @DataFlowConfigProperty(required = false) String clusterId,
            @DataFlowConfigProperty(required = false) List<String> keyColumns,
            @DataFlowConfigProperty(required = false) List<String> columns) {
        this.featureDBManager =
                DataFlowExecutionContext.getCurrentExecutionContext()
                        .getDependencyInjector()
                        .getInstance(FeatureDBManager.class);
        try {
            featureData =
                    featureDBManager.getFeatureData(featureId, variant, version, dbType, clusterId);
        } catch (IOException e) {
            throw new DataFlowConfigurationException("Unable to get feature data", e);
        }
        this.keyColumns = keyColumns;
        this.columns = columns;
    }

    @OutputValue
    public CompletableFuture<DataRecords> getValue(@InputValues Map<String, Object> inputs)
            throws DataFlowExecutionException {
        List<DataFilter> inputFilters = new ArrayList<>();
        if (inputs.containsKey("keys")) {
            inputFilters.add(new MultiKeyDataFilter((List<?>) inputs.get("keys")));
        } else if (inputs.containsKey("key")) {
            List<Object> keys = new ArrayList<>();
            keys.add(inputs.get("key"));
            inputFilters.add(new MultiKeyDataFilter(keys));
        }

        try {
            List<DataFilter> allFilters = new ArrayList<>(inputFilters);
            allFilters.addAll(filters);
            return featureData.readAsync(allFilters, columns);
        } catch (IOException e) {
            throw new DataFlowExecutionException(
                    "Unable to read data from feature " + featureData.getFeatureId(), e);
        }
    }

    @Override
    public boolean isFilterSupported(DataFilter filter) {
        return filter.getClass() == MultiKeyDataFilter.class
                || filter.getClass() == KeyRangeDataFilter.class
                || filter.getClass() == LastUpdateTimeFilter.class
                || filter.getClass() == MaxRowCountDataFilter.class;
    }

    @Override
    public DataRecords query(Map<String, Object> inputValues, List<DataFilter> filters)
            throws IOException {
        return featureData.read(filters, columns);
    }

    @Override
    public Stream<DataRecords> queryForStream(
            Map<String, Object> map, List<DataFilter> list, int batchSize) throws IOException {
        return featureData.readStream(filters, columns, batchSize);
    }

    @Override
    public Optional<Long> getEstimatedResultCount(
            Map<String, Object> inputValues, List<DataFilter> filters) {
        // Not supported yet.
        return Optional.empty();
    }

    @Override
    public List<String> getKeyColumns() {
        return keyColumns;
    }

    @Override
    public void setOrderByKeyIfNotSpecified(boolean b) {
        // Feature data is returned in key order by default.
    }

    @Override
    public void setRequireOrderBy(boolean b) {
        // Feature data is returned in key order by default.
    }
}
