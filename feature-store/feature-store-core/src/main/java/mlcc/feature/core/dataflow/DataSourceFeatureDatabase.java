/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataSourceFeatureDatabase.java
 */
package mlcc.feature.core.dataflow;

import static dataflow.core.datasource.DataSourceUtil.getChildWithPath;

import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.datasource.DataSource;
import dataflow.data.DataRecordSerializationScheme;
import java.io.IOException;
import mlcc.feature.api.FeatureData;
import mlcc.feature.api.FeatureMetadata;
import mlcc.feature.core.FeatureDatabase;

/**
 * A {@link FeatureDatabase} implementation that reads and writes data to a {@link DataSource}. Not
 * that this implementation only supports writes and reading all data. Because the data is not
 * indexed it is not possible to efficiently fetch a record with a given key.
 */
public class DataSourceFeatureDatabase implements FeatureDatabase {

    public interface NameProvider {

        String getFeatureDataPath(String featureId);

        String getFeatureDataPath(FeatureMetadata metadata);

        String getFeatureDataPath(FeatureMetadata metadata, String identifier);
    }

    public static class DefaultNameProvider implements NameProvider {

        @Override
        public String getFeatureDataPath(String featureId) {
            return featureId + DataSource.PATH_SEPARATOR;
        }

        @Override
        public String getFeatureDataPath(FeatureMetadata metadata) {
            return getFeatureDataPath(metadata, "");
        }

        @Override
        public String getFeatureDataPath(FeatureMetadata metadata, String identifier) {
            return getFeatureDataPath(metadata.getFeatureId())
                    + metadata.getVariant()
                    + DataSource.PATH_SEPARATOR
                    + metadata.getVersion()
                    + DataSource.PATH_SEPARATOR
                    + identifier;
        }
    }

    private final String typeId;
    private final String clusterId;
    private final DataSource root;
    private final DataRecordSerializationScheme serializationScheme;
    private final NameProvider nameProvider;
    private final int recordsPerFile;

    @DataFlowConfigurable
    public DataSourceFeatureDatabase(
            @DataFlowConfigProperty String typeId,
            @DataFlowConfigProperty String clusterId,
            @DataFlowConfigProperty DataSource root,
            @DataFlowConfigProperty DataRecordSerializationScheme serializationScheme,
            @DataFlowConfigProperty int recordsPerFile,
            @DataFlowConfigProperty(required = false) NameProvider nameProvider) {
        this.typeId = typeId;
        this.clusterId = clusterId;
        this.root = root;
        this.serializationScheme = serializationScheme;
        this.recordsPerFile = recordsPerFile;
        this.nameProvider = nameProvider != null ? nameProvider : new DefaultNameProvider();
    }

    public DataSourceFeatureDatabase(
            String typeId,
            String clusterId,
            DataSource root,
            DataRecordSerializationScheme serializationScheme,
            int recordsPerFile) {
        this(
                typeId,
                clusterId,
                root,
                serializationScheme,
                recordsPerFile,
                new DefaultNameProvider());
    }

    @Override
    public String getTypeId() {
        return typeId;
    }

    @Override
    public String getClusterId() {
        return clusterId;
    }

    @Override
    public void deleteFeature(String featureId) throws IOException {
        DataSource child =
                getChildWithPath(root, nameProvider.getFeatureDataPath(featureId)).orElse(null);
        if (child != null) {
            child.delete();
        }
    }

    @Override
    public void createFeatureVersion(FeatureMetadata featureMetadata) {}

    @Override
    public void setFeatureActiveVersion(FeatureMetadata metadata) {}

    @Override
    public void deleteFeatureVersion(FeatureMetadata metadata) throws IOException {
        // Get the parent of the first file
        DataSource child =
                getChildWithPath(root, nameProvider.getFeatureDataPath(metadata)).orElse(null);
        if (child != null) {
            child = child.getParent();
        }

        if (child != null) {
            child.delete();
        }
    }

    @Override
    public FeatureData getFeatureData(FeatureMetadata metadata) {
        return new DataSourceFeatureData(
                metadata, root, serializationScheme, recordsPerFile, nameProvider);
    }
}
