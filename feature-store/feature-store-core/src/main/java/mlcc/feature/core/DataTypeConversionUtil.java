/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  DataTypeConversionUtil.java
 */
package mlcc.feature.core;

import static dataflow.core.type.TypeUtil.convert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.data.InvalidDataException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import mlcc.feature.api.FeatureColumnType;

public final class DataTypeConversionUtil {

    private static final DataFlowEnvironment DEFAULT_ENVIRONMENT =
            new DataFlowEnvironment("", new SimpleDependencyInjector(), null);

    private static final Map<String, Boolean> STRING_BOOLEAN_MAP;

    static {
        STRING_BOOLEAN_MAP = new HashMap<>();
        STRING_BOOLEAN_MAP.put("1", true);
        STRING_BOOLEAN_MAP.put("0", false);
    }

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static Object convertTypeIfNeeded(Object obj, FeatureColumnType toType)
            throws InvalidDataException {
        if (obj == null) {
            return null;
        }
        switch (toType) {
            case BOOLEAN:
                return convertToBoolean(obj);
            case INTEGER:
                return convertToInteger(obj);
            case LONG:
                return convertToLong(obj);
            case FLOAT:
                return convertToFloat(obj);
            case DOUBLE:
                return convertToDouble(obj);
            case STRING:
                return convertTo(obj, String.class);
            case DATE:
            case DATETIME:
                return convertTo(obj, Date.class);
            case JSON:
                return convertToJson(obj);
            case OBJECT:
                return obj;
        }
        throw conversionException(obj, toType.name());
    }

    private static Boolean convertToBoolean(Object obj) throws InvalidDataException {
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (str.equals("1")) {
                return true;
            }
            return Boolean.parseBoolean(str);
        }
        if (obj instanceof Number) {
            Number number = (Number) obj;
            return number.intValue() == 1;
        }
        throw conversionException(obj, "boolean");
    }

    private static Integer convertToInteger(Object obj) throws InvalidDataException {
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        if (obj instanceof Short) {
            return ((Short) obj).intValue();
        }
        if (obj instanceof String) {
            return Integer.valueOf((String) obj);
        }
        throw conversionException(obj, "integer");
    }

    private static Long convertToLong(Object obj) throws InvalidDataException {
        if (obj instanceof Long) {
            return (Long) obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((Integer) obj);
        }
        if (obj instanceof String) {
            return Long.valueOf((String) obj);
        }
        throw conversionException(obj, "long");
    }

    private static Float convertToFloat(Object obj) throws InvalidDataException {
        if (obj instanceof Float) {
            return (Float) obj;
        }
        if (obj instanceof String) {
            return Float.valueOf((String) obj);
        }
        throw conversionException(obj, "float");
    }

    private static Double convertToDouble(Object obj) throws InvalidDataException {
        if (obj instanceof Double) {
            return (Double) obj;
        }
        if (obj instanceof Float) {
            return Double.valueOf((Float) obj);
        }
        if (obj instanceof Integer) {
            return Double.valueOf((Integer) obj);
        }
        if (obj instanceof Long) {
            return Double.valueOf((Long) obj);
        }
        if (obj instanceof String) {
            return Double.valueOf((String) obj);
        }
        throw conversionException(obj, "double");
    }

    private static String convertToJson(Object obj) throws InvalidDataException {
        if (obj instanceof String) {
            return (String) obj;
        }
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new InvalidDataException(conversionException(obj, "json").getMessage(), e);
        }
    }

    private static <T> T convertTo(Object obj, Class<T> type) throws InvalidDataException {
        if (type.isAssignableFrom(obj.getClass())) {
            return (T) obj;
        }
        return convert(obj, type, getEnvironment().getRegistry().getTypeRegistry());
    }

    private static DataFlowEnvironment getEnvironment() {
        DataFlowExecutionContext context = DataFlowExecutionContext.getCurrentExecutionContext();
        DataFlowEnvironment env = DEFAULT_ENVIRONMENT;
        if (context != null) {
            env = context.getEnvironment();
        }
        return env != null ? env : DEFAULT_ENVIRONMENT;
    }

    private static InvalidDataException conversionException(Object obj, String targetType) {
        return new InvalidDataException(
                String.format(
                        "Unable to convert from %s type to %s",
                        obj.getClass().getSimpleName(), targetType));
    }
}
