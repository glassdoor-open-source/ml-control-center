

plugins {
    id("java-library")
}

dependencies {
    api(projects.featureStoreApi)
    api(projects.dataflowCore)
    api(projects.dataflowComponentCore)
    api(libs.slf4jApi)
    api(libs.springJdbc)
    api(libs.reactiveStreams)
    api(libs.reactorCore)
    api(libs.jacksonAnnotations)
    api(libs.jacksonDatabind)
    api(projects.dataflowAvro)
    testImplementation(libs.guava)
    testImplementation(libs.mockitoCore)
    testImplementation(libs.h2)
}

description = "feature-store-core"
