

plugins {
    id("java-library")
}

dependencies {
    api(projects.dataflowCore)
    api(projects.dataflowData)
    api(libs.jacksonAnnotations)
    api(libs.reactiveStreams)
}

description = "feature-store-api"
