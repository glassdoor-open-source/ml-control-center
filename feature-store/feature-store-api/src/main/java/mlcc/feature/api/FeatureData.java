/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureData.java
 */
package mlcc.feature.api;

import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.data.InvalidDataException;
import dataflow.data.query.DataFilter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;
import org.reactivestreams.Publisher;

public interface FeatureData extends AutoCloseable {

    String getFeatureId();

    String getVariant();

    long getVersion();

    FeatureMetadata getMetadata();

    DataRecord read(FeatureDataKey key) throws IOException;

    DataRecords read(List<FeatureDataKey> keys) throws IOException;

    DataRecords read(List<DataFilter> filters, List<String> columns) throws IOException;

    Stream<DataRecords> readStream(List<DataFilter> filters, List<String> columns, int batchSize)
            throws IOException;

    CompletableFuture<DataRecords> readAsync(List<DataFilter> filters, List<String> columns)
            throws IOException;

    void write(DataRecord record) throws IOException, InvalidDataException;

    void write(DataRecords records) throws IOException, InvalidDataException;

    void write(Publisher<DataRecord> records) throws IOException;

    void write(Publisher<DataRecord> publisher, int batchSize) throws IOException;

    void delete(List<FeatureDataKey> keys) throws IOException;

    Publisher<DataRecord> readAll() throws IOException;
}
