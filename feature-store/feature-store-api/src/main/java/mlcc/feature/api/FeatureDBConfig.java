/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  FeatureDBConfig.java
 */
package mlcc.feature.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeatureDBConfig {

    private String typeId;
    private String clusterId;
    private int numOldVersionsToKeep;
    private int minAgeForOldVersionCleanupHours;
    private boolean autoCreateColumns;
    private Integer defaultFetchSize;
    private Map<String, String> extraConfig = new HashMap<>();

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }

    public int getNumOldVersionsToKeep() {
        return numOldVersionsToKeep;
    }

    public int getMinAgeForOldVersionCleanupHours() {
        return minAgeForOldVersionCleanupHours;
    }

    public Map<String, String> getExtraConfig() {
        return extraConfig;
    }

    public boolean isAutoCreateColumns() {
        return autoCreateColumns;
    }

    public void setNumOldVersionsToKeep(int numOldVersionsToKeep) {
        this.numOldVersionsToKeep = numOldVersionsToKeep;
    }

    public void setMinAgeForOldVersionCleanupHours(int minAgeForOldVersionCleanupHours) {
        this.minAgeForOldVersionCleanupHours = minAgeForOldVersionCleanupHours;
    }

    public void setAutoCreateColumns(boolean autoCreateColumns) {
        this.autoCreateColumns = autoCreateColumns;
    }

    public void setExtraConfig(Map<String, String> extraConfig) {
        this.extraConfig = extraConfig;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getClusterId() {
        return clusterId;
    }

    public Integer getDefaultFetchSize() {
        return defaultFetchSize;
    }

    public void setDefaultFetchSize(Integer defaultFetchSize) {
        this.defaultFetchSize = defaultFetchSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FeatureDBConfig that = (FeatureDBConfig) o;
        return getNumOldVersionsToKeep() == that.getNumOldVersionsToKeep()
                && getMinAgeForOldVersionCleanupHours() == that.getMinAgeForOldVersionCleanupHours()
                && isAutoCreateColumns() == that.isAutoCreateColumns()
                && Objects.equals(getTypeId(), that.getTypeId())
                && Objects.equals(getClusterId(), that.getClusterId())
                && Objects.equals(getDefaultFetchSize(), that.getDefaultFetchSize())
                && Objects.equals(getExtraConfig(), that.getExtraConfig());
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                getTypeId(),
                getClusterId(),
                getNumOldVersionsToKeep(),
                getMinAgeForOldVersionCleanupHours(),
                isAutoCreateColumns(),
                getDefaultFetchSize(),
                getExtraConfig());
    }

    @Override
    public String toString() {
        return "FeatureDBConfig{"
                + "typeId='"
                + typeId
                + '\''
                + ", clusterId='"
                + clusterId
                + '\''
                + ", numOldVersionsToKeep="
                + numOldVersionsToKeep
                + ", minAgeForOldVersionCleanupHours="
                + minAgeForOldVersionCleanupHours
                + ", autoCreateColumns="
                + autoCreateColumns
                + ", defaultFetchSize="
                + defaultFetchSize
                + ", extraConfig="
                + extraConfig
                + '}';
    }
}
