

plugins {
    id("java-library")
}

dependencies {
    annotationProcessor(projects.dataflowCodegen)
    annotationProcessor(projects.dataflowComponentCodegen)

    api(projects.featureStoreApi)
    api(projects.featureStoreCore)
    api(projects.dataflowCore)
    api(projects.dataflowRest)
    api(libs.slf4jApi)
    api(libs.micrometerCore)
    api(libs.springContext)
    api(libs.jacksonAnnotations)
    api(libs.jacksonDatabind)
    testImplementation(libs.guava)
    testImplementation(libs.mockitoCore)
    implementation(projects.dataflowComponentCodegen)
}

description = "mlflow"
