/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MLFlowRestValueProvider.java
 */
package dataflow.mlflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dataflow.core.component.annotation.DataFlowComponent;
import dataflow.core.component.annotation.DataFlowConfigProperty;
import dataflow.core.component.annotation.DataFlowConfigurable;
import dataflow.core.component.annotation.InputValue;
import dataflow.core.component.annotation.OutputValue;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.rest.RESTServiceValueProvider;
import dataflow.retry.Retry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

@DataFlowComponent
public class MLFlowRestValueProvider {

    private final Logger logger = LoggerFactory.getLogger(MLFlowRestValueProvider.class);

    @JsonIgnoreProperties(ignoreUnknown = true)
    static final class MLFlowDataRecords {

        private MLFlowDataRecordsSplit dataframe_split = new MLFlowDataRecordsSplit();

        public MLFlowDataRecordsSplit getDataframe_split() {
            return dataframe_split;
        }

        public void setDataframe_split(MLFlowDataRecordsSplit dataframe_split) {
            this.dataframe_split = dataframe_split;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            MLFlowDataRecords that = (MLFlowDataRecords) o;
            return Objects.equals(dataframe_split, that.dataframe_split);
        }

        @Override
        public int hashCode() {
            return Objects.hash(dataframe_split);
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static final class MLFlowDataRecordsSplit {

        private List<String> columns = new ArrayList<>();
        private List<List<Object>> data = new ArrayList<>();

        public List<String> getColumns() {
            return columns;
        }

        public void setColumns(List<String> columns) {
            this.columns = columns;
        }

        public List<List<Object>> getData() {
            return data;
        }

        public void setData(List<List<Object>> data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            MLFlowDataRecordsSplit that = (MLFlowDataRecordsSplit) o;
            return Objects.equals(columns, that.columns) && Objects.equals(data, that.data);
        }

        @Override
        public int hashCode() {
            return Objects.hash(columns, data);
        }
    }

    private RESTServiceValueProvider restValueProvider;

    @DataFlowConfigurable
    public MLFlowRestValueProvider(
            @DataFlowConfigProperty String serviceId,
            @DataFlowConfigProperty String path,
            @DataFlowConfigProperty String returnType,
            @DataFlowConfigProperty(required = false) String cacheId,
            @DataFlowConfigProperty(required = false) Retry retry,
            @DataFlowConfigProperty(required = false) String metricNamePrefix,
            @DataFlowConfigProperty(required = false) Map<String, String> metricTags) {
        if (DataRecords.class.getName().equals(returnType)) {
            returnType = MLFlowDataRecords.class.getName();
        }
        this.restValueProvider =
                new RESTServiceValueProvider(
                        serviceId,
                        path,
                        HttpMethod.POST,
                        "application/json",
                        null,
                        null,
                        returnType,
                        cacheId,
                        retry,
                        metricNamePrefix,
                        metricTags);
    }

    @OutputValue
    public Object getValue(@InputValue DataRecords records) throws Exception {
        try {
            MLFlowDataRecords mlflowRecords = convertToMLFlowDataRecords(records);
            Object retVal =
                    restValueProvider.getValue(
                            Collections.emptyMap(),
                            mlflowRecords,
                            Collections.emptyMap(),
                            Collections.emptyMap());
            if (retVal instanceof MLFlowDataRecords) {
                retVal = convertToDataRecords((MLFlowDataRecords) retVal);
            }
            return retVal;
        } catch (Throwable ex) {
            logger.warn("Error calling mlFlow service: " + records, ex);
            throw ex;
        }
    }

    private static MLFlowDataRecords convertToMLFlowDataRecords(DataRecords records) {
        MLFlowDataRecords mlflowData = new MLFlowDataRecords();
        MLFlowDataRecordsSplit mlflowDataSplit = mlflowData.getDataframe_split();
        mlflowDataSplit.setColumns(records.getColumnNames());

        List<List<Object>> data = new ArrayList<>();
        for (DataRecord record : records) {
            List<Object> colValues = new ArrayList<>();
            for (String colName : records.getColumnNames()) {
                colValues.add(record.get(colName));
            }
            data.add(colValues);
        }
        mlflowDataSplit.setData(data);
        return mlflowData;
    }

    private static DataRecords convertToDataRecords(MLFlowDataRecords mlFlowDataRecords) {
        MLFlowDataRecordsSplit mlFlowDataRecordsSplit = mlFlowDataRecords.getDataframe_split();
        DataRecords resultRecords = new DataRecords();
        resultRecords.setColumnNames(mlFlowDataRecordsSplit.getColumns());
        for (List<Object> row : mlFlowDataRecordsSplit.getData()) {
            DataRecord record = new DataRecord();
            for (int idx = 0; idx < row.size(); idx++) {
                if (mlFlowDataRecordsSplit.getColumns().size() > idx) {
                    String colName = mlFlowDataRecordsSplit.getColumns().get(idx);
                    record.put(colName, row.get(idx));
                }
            }
            resultRecords.add(record);
        }
        return resultRecords;
    }
}
