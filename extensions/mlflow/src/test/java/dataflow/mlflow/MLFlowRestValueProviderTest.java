/*
 * Copyright 2021-2024 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  MLFlowRestValueProviderTest.java
 */
package dataflow.mlflow;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import dataflow.core.engine.DataFlowExecutionContext;
import dataflow.core.engine.DependencyInjector;
import dataflow.core.engine.SimpleDependencyInjector;
import dataflow.core.environment.DataFlowEnvironment;
import dataflow.data.DataRecord;
import dataflow.data.DataRecords;
import dataflow.mlflow.MLFlowRestValueProvider.MLFlowDataRecords;
import dataflow.rest.RESTServiceManager;
import dataflow.retry.Retry;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class MLFlowRestValueProviderTest {

    private MLFlowRestValueProvider valueProvider;

    private static final Retry retry = new Retry(1, true, 1, false, null, null);

    @Mock private RestTemplate restTemplate;
    @Captor private ArgumentCaptor<RequestEntity<String>> requestEntityCaptor;

    private final ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() {
        mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        RESTServiceManager restServiceManager = new RESTServiceManager();
        restServiceManager.register("mlflow", "http://localhost:8080/", restTemplate);
        DependencyInjector dependencyInjector = new SimpleDependencyInjector(restServiceManager);

        DataFlowEnvironment env = new DataFlowEnvironment("test", dependencyInjector, null);
        DataFlowExecutionContext.createExecutionContext(env);

        valueProvider =
                new MLFlowRestValueProvider(
                        "mlflow", "/", ArrayList.class.getName(), null, retry, null, null);
    }

    @Test
    public void getValue() throws Exception {
        DataRecords requestRecords = new DataRecords();
        requestRecords.setColumnNames(ImmutableList.of("Name", "Age"));
        requestRecords.setKeyColumnNames(ImmutableList.of("Name"));
        DataRecord r1 = new DataRecord();
        r1.put("Name", "abc");
        r1.put("Age", 3);
        requestRecords.add(r1);
        DataRecord r2 = new DataRecord();
        r2.put("Name", "efg");
        r2.put("Age", 6);
        requestRecords.add(r2);

        String responseBody = "ok";
        when(restTemplate.exchange(requestEntityCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(responseBody));
        Object result = valueProvider.getValue(requestRecords);
        assertEquals(responseBody, result);
        assertEquals(
                "{\"dataframe_split\":{\"columns\":[\"Name\",\"Age\"],\"data\":[[\"abc\",3],[\"efg\",6]]}}",
                mapper.writeValueAsString(requestEntityCaptor.getValue().getBody()));
    }

    @Test
    public void getValue_returnsRecords() throws Exception {
        valueProvider =
                new MLFlowRestValueProvider(
                        "mlflow", "/", DataRecords.class.getName(), null, retry, null, null);

        MLFlowRestValueProvider.MLFlowDataRecords response = new MLFlowDataRecords();
        response.getDataframe_split().setColumns(ImmutableList.of("Name", "Age"));
        response.getDataframe_split().getData().add(ImmutableList.of("abc", 3));
        response.getDataframe_split().getData().add(ImmutableList.of("efg", 6));

        when(restTemplate.exchange(requestEntityCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(response));
        Object result = valueProvider.getValue(new DataRecords());
        assertEquals(
                "{\"dataframe_split\":{\"columns\":[],\"data\":[]}}",
                mapper.writeValueAsString(requestEntityCaptor.getValue().getBody()));
        DataRecords responseRecords = (DataRecords) result;
        assertEquals(
                "{\"columnNames\":[\"Name\",\"Age\"],\"records\":[{\"Age\":3,\"Name\":\"abc\"},{\"Age\":6,\"Name\":\"efg\"}]}",
                mapper.writeValueAsString(responseRecords));
    }
}
