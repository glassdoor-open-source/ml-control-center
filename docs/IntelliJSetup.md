# IntelliJ Run config for dataflows

For projects that rely on dataflow components, an error appears where changes to the dataflow yamls do not actually affect the run behavior. This error may be caused by erroneous cacheing of the auto-generated dataflow code.  To avoid this error, you can modify your IntelliJ run config to have a `Before Launch` task that builds without cache.


## Set up config

1. Have an existing run or debug config. For example, this can be a Spring Boot application.

    <img src="images/intellij/exampleBaseConfig.png" alt="Regular Spring or Gradle IntelliJ Run Configuration" width="800"/>


2. Click the `+` simbol in the top left to add a new `Gradle` configuration.

    <img src="images/intellij/addNewConfiguration.png" width="800"/>

3. Fill the config fields

    - Name the config `no-cache build`
    - Enter `run` command `clean build --no-build-cache -x test`

    <img src="images/intellij/noCacheBuild.png" width="800"/>

4. Go back to your existing run or debug config. Select `Modify Options`, then `Add before launch task`.

    <img src="images/intellij/beforeLaunchTask.png" width="800"/>

5. A new field `Before Launch` should appear in your config. 

    - Remove any existing tasks there,
    - Select `Add task`
    - Select `Run Another Configuration`
    - Select your `no-cache build` task

    <img src="images/intellij/addNewTask.png" width="800"/>

6. Your config should now have a `Before launch` task defined.

    <img src="images/intellij/configComplete.png" width="800"/>


## Verifying config

Here are two options to verify the config has now fixed the cacheing issue.

1. Check the auto-generated code is updated in `./build/generated/sources/annotationProcessor/java/main/dataflow/core/environment/{datatflow}.java`

1. Add this `!ExpressionValueProvider` and verify you can see the print expression appearing when running the dataflow.

```yaml
    - !ExpressionValueProvider
      id: debug
      sink: true
      expression: |
        System.out.println("Debug print expression!")
```
